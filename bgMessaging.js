import { Platform } from "react-native";
import firebase from 'react-native-firebase';
import type { RemoteMessage } from 'react-native-firebase';

export default async (message: RemoteMessage) => {
  const channel = new firebase.notifications.Android.Channel('testAppChnl', 'testAppChnl Channel', firebase.notifications.Android.Importance.Max)
  .setDescription('My apps test channel');
  
  console.log('Msg:----> ', message);
  firebase.notifications().android.createChannel(channel);
  const notification = new firebase.notifications.Notification()
    .setNotificationId(new Date().getTime().toString())
    .setTitle(message.data.title)
    .setBody(message.data.message)
    .setData(message.data.data)
  {
    Platform.OS == 'android' &&
      notification
        .android.setChannelId('testAppChnl')

  }
  firebase.notifications().displayNotification(notification).catch(err => console.error('err of noti', err));
 

  return Promise.resolve();
}