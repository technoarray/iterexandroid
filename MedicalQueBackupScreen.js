import React, { Component } from 'react';
import {AsyncStorage,StyleSheet,Text,View,Image,TouchableOpacity,Platform,Alert,ScrollView,TextInput,Button,StatusBar,PushNotificationIOS,AppState,} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions } from 'react-navigation'
import Spacer from '../common/Spacer'
import Header from '../common/signupHeader'
import CommonButton from '../common/CommonButton'
import Swiper from 'react-native-swiper';
import Animbutton from './animAnsbutton'
import AnimSinglebutton from './animSingleAnsOptions';
import RNPickerSelect from 'react-native-picker-select';
import { jsonData } from '../data/Question';
import { pickerData } from '../data/pickerData';
import LinearGradient from 'react-native-linear-gradient';
import Spinner from 'react-native-loading-spinner-overlay';
import Picker from 'react-native-q-picker';
import Orientation from 'react-native-orientation'
import * as commonFunctions from '../../utils/CommonFunctions'

var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;

/* Images */
//var thankyou_icon = require('../../themes/Images/thank-you.png');
var logo = require('../../themes/Images/logo.png');
var back_icon = require( '../../themes/Images/left-whitearrow.png')
var cigrate_icon = require('../../themes/Images/cigrate.png');


let arrnew = []
let ans_track = []
let profile_data = []
let qus_result =[]
let final_profile_data = []
var unknown_arr=[]
let options_arr=[]
let final_options_arr=[]
let python_result =[]
let python_array =[]
let vitals=[]
var current_ans=''

const MyStatusBar = ({backgroundColor, ...props}) => (
  <View style={[styles.statusBar, { backgroundColor }]}>
    <StatusBar translucent backgroundColor={backgroundColor} {...props} />
  </View>
);

var cigrate_data= [];

//cigrate_data
for( var j = 1; j <=60; j++){
  var numbers = {
      name: j+' ',
      id: j
  };
  cigrate_data.push(numbers);
}
let _that
export default class MedicalQusScreen extends Component {
  constructor (props) {
      super(props)
      this.qno = 0


      const jdata = jsonData.medicalqus.qus1
      arrnew = Object.keys(jdata).map( function(k) { return jdata[k] });
      this.state = {
        question : arrnew[this.qno].question,
        options : arrnew[this.qno].options,
        suboption : arrnew[this.qno].suboption,
        type : arrnew[this.qno].type,
        countCheck : [],
        qus_no : 0,
        uid : 0,
        gender :'',
        birthdate :'',
        height :'',
        weight:'',
        screenName:'',
        cigrates:'',
        cigrate_data:cigrate_data,
        python_result:[],
        python_array:[],
        isVisible: false,
        Modal_Visibility: false,
        status:false,
        scrollPosition:'',
    },
    _that = this;
  }

  componentDidMount() {
    Orientation.lockToPortrait();
    AsyncStorage.getItem('screenName').then((screenName) => {
      var screenName = JSON.parse(screenName);
      _that.setState({screenName:screenName})
    //  console.log("screenName "+screenName)
    })

    AsyncStorage.getItem('loggedIn').then((value) => {
        var loggedIn = JSON.parse(value);
        if (loggedIn) {
            AsyncStorage.getItem('UserData').then((UserData) => {
                const data = JSON.parse(UserData)
                //console.log(val)
                _that.setState({
                  uid:data.id,
                  gender : JSON.parse(data.gender),
                  birthdate : JSON.parse(data.birth_date),
                  height : JSON.parse(data.height),
                  weight: JSON.parse(data.weight),
                });

            })

        }
    })

  }



  componentDidUpdate(prevProps, prevState) {
    //console.log(prevState)
    if (prevState.question !== this.state.question) {
      setTimeout(() => {
        _that.scroll_to_top();
      }, 400)
    }
    //_that.scroll_to_top();
  }


  scroll_to_top=()=>{
    _that.scroll.scrollTo({x: 0, y: 0, animated: false})
  }

  scroll_to_end=()=>{
    _that.scroll.scrollToEnd({animated: true})
  }

  updateAlert = () => {
        this.setState({Modal_Visibility: !this.state.Modal_Visibility});
  }

  validationAndApiParameter(apiname) {
      //console.log(final_profile_data)
      //console.log('uid='+_that.state.uid)
      if(apiname == 'pyhton_profile'){
        var data = {
            X_test: final_profile_data
        };
        python_array.push({
            name: 'profile',
            value: data,
        });
          _that.setState({ python_array: python_array })
        console.log(data);
          _that.setState({isVisible: true});
          this.postToApiCalling('POST_1', apiname, Constant.URL_profile,data);
      }
      else if(apiname == 'web_profile'){
        options_arr.sort(function(a,b) {
            return a.order - b.order;
        });
        var mail='';
      if(this.state.screenName!="thank"){
        vitals=[];
        mail='yes';
        //AsyncStorage.setItem('vitals', JSON.stringify(vitals));
      }
      else{
        mail='no';
        vitals=[]
      }
        var json_options_arr = JSON.stringify(options_arr);
        var json_python_array = JSON.stringify(this.state.python_array);
        var json_python_result = JSON.stringify(this.state.python_result);
        var json_qus_result=JSON.stringify(qus_result)
        var json_vitals=JSON.stringify(vitals)


        var data = {
            uid: this.state.uid,
            score_result : json_python_result,
            score_array : json_python_array,
            variable : json_options_arr,
            qus_result : json_qus_result,
            vitals : json_vitals,
            vitals_data: json_vitals,
            mail:mail
        };
          _that.setState({isVisible: true});
          this.postToApiCalling('POST', apiname, Constant.URL_saveVariable,data);
      }
    }

  postToApiCalling(method, apiKey, apiUrl, data) {
    //console.log(apiUrl)
      new Promise(function(resolve, reject) {
          if (method == 'POST') {
              resolve(WebServices.callWebService(apiUrl, data));
          } else {
              resolve(WebServices.callWebService_POST(apiUrl, data));
          }
      }).then((jsonRes) => {
         console.log(jsonRes)
            _that.setState({ isVisible: false })
          if(apiKey=="pyhton_profile"){
            _that.apiSuccessfullResponse(apiKey, jsonRes)
          }
          else{
            if ((!jsonRes) || (jsonRes.code == 0)) {
              setTimeout(() => {
                    Alert.alert(jsonRes.message);
                }, 200);
            } else {
                if (jsonRes.code == 1) {
                    _that.apiSuccessfullResponse(apiKey, jsonRes)
                }
            }
          }
      }).catch((error) => {
          console.log("ERROR" + error);
          _that.setState({ isVisible: false })
          setTimeout(() => {
              Alert.alert("Server Error");
          }, 200);
      })
  }

  apiSuccessfullResponse(apiKey, jsonRes) {
    if(apiKey=="pyhton_profile"){
      python_result.push({
          name: 'profile',
          value: jsonRes,
      });
      _that.setState({ python_result: python_result })
      _that.validationAndApiParameter('web_profile');
    }
    if(apiKey=="web_profile"){
      arrnew = []
      ans_track = []
      profile_data = []
      qus_result =[]
      final_profile_data = []
      unknown_arr=[]
      options_arr=[]
      final_options_arr=[]
      python_result =[]
      python_array =[]
      vitals=[]
      current_ans=''
      //  AsyncStorage.setItem(login_push, 'login_push');
      _that.props.navigation.navigate('ThankYouScreen', {message: 'Thank you for setting up your health profile'});
    }
  }

  prev= () =>{
    //console.log(this.qno)
    if(this.qno > 0){

      if(this.qno==6){
        this.qno=this.qno-3
      }
      else{
        this.qno--
      }
      //console.log(this.qno)

      this.setState({ qus_no: this.qno })
      this.setState({ countCheck: [],question: arrnew[this.qno].question, options: arrnew[this.qno].options, type: arrnew[this.qno].type,suboption : arrnew[this.qno].suboption})
    }
  }
  next= () =>{

    // variables
    current_ans=0
    final_thank=0;
    const { countCheck} = this.state

//console.log(this.state.qus_no)
    /*if(this.qno==3 && this.state.countCheck.indexOf('')>= 0){
        Alert.alert("","Please select which of the following statements apply to you")
    }
    else*/

    if(this.qno!=3 && this.state.countCheck == 0){
      Alert.alert("Invalid Value","Missing or blank value is not allowed")
    }
    else if(this.qno < arrnew.length-1){

      if(this.qno == 0 || this.qno == 1 || this.qno == 2 || this.qno == 3){
        if(this.qno == 3){type="risk_factor"}else{type="condition"}
        qus_result.push({
            type :type,
            options: this.qno,
            value: ans_track.toString(),
        });
      }

      if(countCheck.indexOf('asthma')!=-1){
        console.log(arrnew.length);
        console.log(this.qno);
        this.qno=7
        console.log(this.qno);
      }
      else if(countCheck.indexOf('copd')!=-1){
        console.log(this.qno);
        this.qno++
        console.log(this.qno);
      }
      else{
        console.log(this.qno);
        this.qno++
        console.log(this.qno);
      }

    //  console.log(qus_result)

      // answers array
      //console.log(this.qno)
      {/*
        if(this.qno == 0){
          console.log(countCheck)
          // asthma
          //console.log(countCheck.indexOf('asthma'));
          if(countCheck.indexOf('asthma')!=-1){
            profile_data[17]=1
            options_arr.push({var_name: 'asthma',value: 1,mean:0.0770410118819471, std:0.26665651008395197,order:17,});
          }
          else {
            profile_data[17]=0
            options_arr.push({var_name: 'asthma',value: 0,mean:0.0770410118819471, std:0.26665651008395197,order:17,});
          }

          // coronary_artery_disease
          if(countCheck.indexOf('coronary_artery_disease')!=-1){
            profile_data[14]=1
          options_arr.push({var_name: 'coronary_artery_disease',  value: 1,mean:0.07819087773093139, std:0.268471719871196,order:14,});
          }
          else {
            profile_data[14]=0
            options_arr.push({var_name: 'coronary_artery_disease',  value: 0,mean:0.07819087773093139, std:0.268471719871196,order:14,});
          }

          // congestive_heart_failure
          if(countCheck.indexOf('congestive_heart_failure')!=-1){
            profile_data[15]=1
            options_arr.push({var_name: 'congestive_heart_failure',value: 1,mean:0.04599463395937141, std:0.20947345322573657,order:15 });
          }
          else {
            profile_data[15]=0
            options_arr.push({var_name: 'congestive_heart_failure',value: 0,mean:0.04599463395937141, std:0.20947345322573657,order:15 });
          }

          // high_blood_pressure
          if(countCheck.indexOf('high_blood_pressure')!=-1){
            profile_data[12]=1
            options_arr.push({var_name: 'high_blood_pressure',value: 1,mean : 0.023763894212341895, std:0.1523127425536239,order:12,});
          }
          else {
            profile_data[12]=0
            options_arr.push({var_name: 'high_blood_pressure',value: 0,mean : 0.023763894212341895, std:0.1523127425536239,order:12,});
          }

          // anemia
          if(countCheck.indexOf('anemia')!=-1){
            profile_data[18]=1
            options_arr.push({var_name: 'anemia',value: 1,mean:0.02414718282866999, std:0.15350601418220972,order:18,});
          }
          else {
            profile_data[18]=0
            options_arr.push({var_name: 'anemia',value: 0,mean:0.02414718282866999, std:0.15350601418220972,order:18,});
          }

          // chronic_kidney_disease
          if(countCheck.indexOf('chronic_kidney_disease')!=-1){
            profile_data[16]=1
            options_arr.push({var_name: 'chronic_kidney_disease',value: 1,mean:0.08087389804522806, std:0.2726413590418705,order:16,});
          }
          else {
            profile_data[16]=0
            options_arr.push({var_name: 'chronic_kidney_disease',value: 0,mean:0.08087389804522806, std:0.2726413590418705,order:16,});
          }

          // diabetes
          if(countCheck.indexOf('diabetes')!=-1){
            profile_data[13]=1
            options_arr.push({var_name: 'diabetes',  value: 1,mean:0.08394020697585282, std:0.277298122295669,order:13,});
          }
          else {
            profile_data[13]=0
            options_arr.push({var_name: 'diabetes',  value: 0,mean:0.08394020697585282, std:0.277298122295669,order:13,});
          }

          // acid_reflux
          if(countCheck.indexOf('acid_reflux')!=-1){
            profile_data[19]=1
            options_arr.push({var_name: 'acid_reflux',value: 1,mean:0.026446914526638558, std:0.1604601983005731,order:19,});
          }
          else {
            profile_data[19]=0
            options_arr.push({var_name: 'acid_reflux',value: 0,mean:0.026446914526638558, std:0.1604601983005731,order:19,});
          }

          // pulmonary_hypertension
          if(countCheck.indexOf('pulmonary_hypertension')!=-1){
            profile_data[7]=1
            options_arr.push({var_name: 'pulmonary_hypertension',value: 1,mean:0.06285933307780758, std:0.24270978003125634,order:7});
          }
          else {
            profile_data[7]=0
            options_arr.push({var_name: 'pulmonary_hypertension',value: 0,mean:0.06285933307780758, std:0.24270978003125634,order:7});
          }

      }
      else if(this.qno == 1){

        if(countCheck.indexOf('gold_nan')!=-1){
          profile_data[30]=1
          options_arr.push({var_name: 'gold_nan',value: 1,mean:0.354925258719816, std:0.4784906680829708,order:30,});
        }
        else {
          profile_data[30]=0
          options_arr.push({var_name: 'gold_nan',value: 0,mean:0.354925258719816, std:0.4784906680829708,order:30,});
        }

        if(countCheck.indexOf('gold_1')!=-1){
          profile_data[31]=1
          options_arr.push({var_name: 'gold_1',value: 1,mean:0.10923725565350709, std:0.31193665643972884,order:31,});
        }
        else {
          profile_data[31]=0
          options_arr.push({var_name: 'gold_1',value: 0,mean:0.10923725565350709, std:0.31193665643972884,order:31,});
        }

        if(countCheck.indexOf('gold_2')!=-1){
          profile_data[32]=1
          options_arr.push({var_name: 'gold_2',value: 1,mean:0.17133001149865848, std:0.3767970788879978,order:32,});
        }
        else {
          profile_data[32]=0
            options_arr.push({var_name: 'gold_2',value: 0,mean:0.17133001149865848, std:0.3767970788879978,order:32,});
        }

        if(countCheck.indexOf('gold_3')!=-1){
          profile_data[33]=1
          options_arr.push({var_name: 'gold_3',value: 1,mean:0.1851284016864699, std:0.3884016948669032,order:33,});
        }
        else {
          profile_data[33]=0
          options_arr.push({var_name: 'gold_3',value: 0,mean:0.1851284016864699, std:0.3884016948669032,order:33,});
        }

        if(countCheck.indexOf('gold_4')!=-1){
          profile_data[34]=1
          options_arr.push({var_name: 'gold_4',value: 1,mean:0.1793790724415485, std:0.3836694160492313,order:34,});
        }
        else {
          profile_data[34]=0
          options_arr.push({var_name: 'gold_4',value: 0,mean:0.1793790724415485, std:0.3836694160492313,order:34,});
        }

    }
      else if(this.qno == 2){
        //console.log(countCheck)
        if(countCheck.indexOf('base_dyspnea_1')!=-1){
          profile_data[25]=1
          options_arr.push({var_name: 'base_dyspnea_1',value: 1,mean:0.11881947106170947, std:0.3235759638142566,order:25,});
        }
        else {
          profile_data[25]=0
          options_arr.push({var_name: 'base_dyspnea_1',value: 0,mean:0.11881947106170947, std:0.3235759638142566,order:25,});
        }

        if(countCheck.indexOf('base_dyspnea_2')!=-1){
          profile_data[26]=1
          options_arr.push({var_name: 'base_dyspnea_2',value: 1,mean:0.18321195860482944, std:0.3868401954670839,order:26,});
        }
        else {
          profile_data[26]=0
          options_arr.push({var_name: 'base_dyspnea_2',value: 0,mean:0.18321195860482944, std:0.3868401954670839,order:26,});
        }

        if(countCheck.indexOf('base_dyspnea_3')!=-1){
          profile_data[27]=1
          options_arr.push({var_name: 'base_dyspnea_3',value: 1,mean:0.29091605979302415, std:0.4541848808002373,order:27,});
        }
        else {
          profile_data[27]=0
          options_arr.push({var_name: 'base_dyspnea_3',value: 0,mean:0.29091605979302415, std:0.4541848808002373,order:27,});
        }

        if(countCheck.indexOf('base_dyspnea_4')!=-1){
          profile_data[28]=1
          options_arr.push({var_name: 'base_dyspnea_4',  value: 1,mean:0.22844001533154465, std:0.41982755355844414,order:28,});
        }
        else {
          profile_data[28]=0
          options_arr.push({var_name: 'base_dyspnea_4',  value: 0,mean:0.22844001533154465, std:0.41982755355844414,order:28,});
        }

        if(countCheck.indexOf('base_dyspnea_5')!=-1){
          profile_data[29]=1
          options_arr.push({var_name: 'base_dyspnea_5',value: 1,mean:0.1786124952088923, std:0.38302750784264267,order:29,});
        }
        else {
          profile_data[29]=0
          options_arr.push({var_name: 'base_dyspnea_5',value: 0,mean:0.1786124952088923, std:0.38302750784264267,order:29,});
        }

      }
      else if(this.qno == 3){
        //console.log(countCheck)
        if(countCheck.indexOf('oxygen_therapy')!=-1){
          profile_data[9]=1
          options_arr.push({var_name: 'oxygen_therapy',value: 1,mean:0.022997316979685704, std:0.14989476438962626,order:9,});
        }
        else {
          profile_data[9]=0
          options_arr.push({var_name: 'oxygen_therapy',value: 0,mean:0.022997316979685704, std:0.14989476438962626,order:9,});
        }

        if(countCheck.indexOf('daily_activities')!=-1){
          profile_data[8]=1
          options_arr.push({var_name: 'daily_activities',  value: 1,mean:0.017247987734764277, std:0.13019406535578223,order:8,});
        }
        else {
          profile_data[8]=0
          options_arr.push({var_name: 'daily_activities',  value: 0,mean:0.017247987734764277, std:0.13019406535578223,order:8,});
        }

        if(countCheck.indexOf('live_alone')!=-1){
          profile_data[10]=1
          options_arr.push({var_name: 'live_alone',value: 1,mean:0.02108087389804523, std:0.14365399630271322,order:10,});
        }
        else {
          profile_data[10]=0
          options_arr.push({var_name: 'live_alone',value: 0,mean:0.02108087389804523, std:0.14365399630271322,order:10,});
        }

        if(countCheck.indexOf('smoke')!=-1){
          profile_data[6]=1
          options_arr.push({var_name: 'smoke',value: 1,mean:0.027213491759294748, std:0.16270500184555342,order:6,});
        }
        else {
          profile_data[6]=0
          options_arr.push({var_name: 'smoke',value: 0,mean:0.027213491759294748, std:0.16270500184555342,order:6,});
        }

        if(countCheck.indexOf('hospitalized_due_to_COPD')!=-1){
          profile_data[11]=1
          options_arr.push({var_name: 'hospitalized_due_to_COPD',  value: 1,mean:0.0770410118819471, std:0.26665651008395197,order:11,});
        }
        else {
          profile_data[11]=0
          options_arr.push({var_name: 'hospitalized_due_to_COPD',  value: 0,mean:0.0770410118819471, std:0.26665651008395197,order:11,});
        }

      }*/}

      this.setState({ qus_no: this.qno })
        ans_track =[];
          // if no selection in qustion 4
          if(this.qno==4 && (countCheck.length == 0)){
            final_thank=1;
          }
          // if Both not selected
          else if(profile_data[6]==0 && profile_data[11]==0){
            final_thank=1;
          }
          // if Both selected in qustion 4 i.e. smoke and hospitalized
          else if(profile_data[6]==1 && profile_data[11]==1){
            final_thank=0;
          }
          // if hospitalized selected in qustion 4
          else if(profile_data[6]==0 && profile_data[11]==1){
            this.qno=this.qno+2
            this.setState({ qus_no: this.qno })
          }
            // if smoke selected in qustion 4
          else if(this.qno==6 && profile_data[6]==1 && profile_data[11]==0){
            final_thank=1;
          }


      if(final_thank != 1){
        this.setState({ countCheck: [], question: arrnew[this.qno].question, options: arrnew[this.qno].options, type: arrnew[this.qno].type, suboption : arrnew[this.qno].suboption});
      }
   }
   else{
     final_thank=1;
   }

    if(final_thank == 1) {
      profile_data[20]= 0    //(2 or More Exacerbations In Last Year )
      options_arr.push({var_name: 'exacerbations_last_year',value: 0,mean:0.019164430816404752, std:0.1371027184554996,order:20,});

      profile_data[21]= 1    //(cur_med_nan)
      profile_data[22]= 0    //(cur_med_1.0)
      profile_data[23]= 0    //(cur_med_2.0)
      profile_data[24]= 0    //(cur_med_3.0)

      options_arr.push({var_name: 'cur_med_nan',value: 1,mean:0.34610962054426986, std:0.4757286528168882,order:21});
      options_arr.push({var_name: 'cur_med_1',value: 0,mean:0.07512456880030663, std:0.26359223805505855,order:22});
      options_arr.push({var_name: 'cur_med_2',value: 0,mean:0.33882713683403604, std:0.4733110057656439,order:23});
      options_arr.push({var_name: 'cur_med_3',value: 0,mean:0.2399386738213875, std:0.4270457898472028,order:24});

      profile_data[35]= 1   //(basefev_nan)
      profile_data[36]= 0   //(basefev_(70, 100])
      profile_data[37]= 0   //(basefev_(50, 70])
      profile_data[38]= 0   //(basefev_(30, 50])
      profile_data[39]= 0   //(basefev_(0, 30])

      options_arr.push({var_name: 'basefev_nan',value: 1,order:35});
      options_arr.push({var_name: 'basefev_70_100',value: 0,order:36});
      options_arr.push({var_name: 'basefev_50_70',value: 0,order:37});
      options_arr.push({var_name: 'basefev_30_50',value: 0,order:38});
      options_arr.push({var_name: 'basefev_0_30',value: 0,order:39});

      profile_data[40]= 1  // (basehr_nan)
      profile_data[41]= 0   //(basehr_(100, 250])
      profile_data[42]= 0   //(basehr_(80, 100])
      profile_data[43]= 0   //(basehr_(60, 80])
      profile_data[44]= 0   //(basehr_(0, 60])

      options_arr.push({var_name: 'basehr_nan',value: 1,order:40});
      options_arr.push({var_name: 'basehr_100_250',value: 0,order:41});
      options_arr.push({var_name: 'basehr_80_100',value: 0,order:42});
      options_arr.push({var_name: 'basehr_60_80',value: 0,order:43});
      options_arr.push({var_name: 'basehr_0_60',value: 0,order:44});

      profile_data[45]= 1   //(baseox_nan)
      profile_data[46]= 0   //(baseox_(93, 100])
      profile_data[47]= 0   //(baseox_(91, 93])
      profile_data[48]= 0   //(baseox_(89, 91])
      profile_data[49]= 0   //(baseox_(85, 89])
      profile_data[50]= 0   //(baseox_(0, 85])

      options_arr.push({var_name: 'baseox_nan',value: 1,order:45});
      options_arr.push({var_name: 'baseox_93_100',value: 0,order:46});
      options_arr.push({var_name: 'baseox_91_93',value: 0,order:47});
      options_arr.push({var_name: 'baseox_89_91',value: 0,order:48});
      options_arr.push({var_name: 'baseox_85_89',value: 0,order:49});
      options_arr.push({var_name: 'baseox_0_85',value: 0,order:50});

      profile_data[4]=0    //(Visited ICU for COPD in Last Year)
      options_arr.push({var_name: 'visited_icu_copd_last_year',value: 0,mean : 0.07627443464929091, std: 0.26543670670843916,order:4});

     if(unknown_arr.length>0){
        profile_data[5]=0   // unkonwn
        options_arr.push({var_name: 'unkonwn',value: 0,mean : 0.1399003449597547, std: 0.34688360935607254,order:5});
      }
      else{
        profile_data[5]=1   // unkonwn
        options_arr.push({var_name: 'unkonwn',value: 1,mean : 0.1399003449597547, std: 0.34688360935607254,order:5});

      }

      profile_data[0]= this.state.gender   //(gender)
      // if a patient ever enters an age less than 40, I want you to list their age as normal in the profile, but for the purpose of the algorithm, the value that should be passed to the array for age should be '41'.
      if(this.state.birthdate < 40){
        profile_data[1]= 41   //(age)
      }
      else{
        profile_data[1]= this.state.birthdate   //(age)
      }
      //console.log('Birthday'+profile_data[1]);
      profile_data[2]= this.state.height   //(height)
      bmi = 703 * this.state.weight/ (this.state.height * this.state.height) // 703 x Weight(lbs) / [Height(in)]^2
      profile_data[3]= bmi   //(weight)

      final_profile_data.push(profile_data)

      options_arr.push({var_name: 'gender',value: this.state.gender, mean : 0.5001916443081641, std: 0.49999996327245777, order:0});
      options_arr.push({var_name: 'birthdate',value: profile_data[1], mean : 65.0176312763511, std: 12.7350147767877, order:1});
      options_arr.push({var_name: 'height',value: this.state.height, mean : 66.49367573783059, std: 4.141524043941354, order:2});
      options_arr.push({var_name: 'bmi',value: bmi, mean : 26.49137600613262, std: 5.995080900369012, order:3});

      _that.validationAndApiParameter('pyhton_profile');
      //console.log(options_arr)


      }
  }


  _answer(status,ans){
    //console.log(this.ans)
    if(this.state.type=="multiple"){
      if(status == false){
        ans_track.push(ans);
      }
      else{
        var index = ans_track.indexOf(ans);
        if (index !== -1) ans_track.splice(index, 1);
        //ans_track.splice(-1,1);
      }
    }
    else if(this.state.type=="single"){
      ans_track.splice(-1,1);
      ans_track.push(ans);
    }

    if(this.qno ==0 || this.qno ==3){
      if(ans !=='copd'){
        unknown_arr.push(ans);
      }
    }
  //  console.log(unknown_arr)
    current_ans=ans;
    this.setState({ countCheck: ans_track })
  }



back_btn(){
  _that.props.navigation.navigate("MedicalFormSelectedScreen");
}


  render() {
    //console.log(this.state.python_result);
    const headerProp = {
      title: 'Medical Profile setup!',
      screens: 'MedicalQusScreen',
    };
    let _this = this
      const currentOptions = this.state.options
      const options = Object.keys(currentOptions).map( function(k) {
        if(k==current_ans){status=true}else{status=false}
        return (  <View key={k} style={{margin:5, }}>
          { _this.state.type=='multiple' ?
            <Animbutton onColor={"#2c808f"} effect={"pulse"}  _onPress={(status) => _this._answer(status,k)} text={currentOptions[k]} />
          :
            <AnimSinglebutton onColor={"#2c808f"} effect={"pulse"} status={status} _onPress={(status) => _this._answer(status,k)} text={currentOptions[k]} />
          }

        </View>)
      });

      var DropDown=<View style={{margin:AppSizes.ResponsiveSize.Padding(5) }}>
       {/* <Text style={styles.dropdowntext}>Oxygen Saturation </Text> */}
       <View style={{ paddingTop: AppSizes.ResponsiveSize.Padding(5) }} />

        <Picker PickerData={this.state.cigrate_data}
         labelIcon={cigrate_icon}
         styleImage={styles.imageContainer}
         placeholder={"Select total cigarettes"}
         getTxt={(val,label)=>this.setState({cigrates:val,countCheck:val})}/>

         <View style={{ paddingBottom: AppSizes.ResponsiveSize.Padding(5) }} />

      </View>;


      if(this.state.qus_no!=0){
        headerBack=<TouchableOpacity style={styles.menuWrapper} onPress={this.prev}>
                  <Image style={styles.image} source={back_icon}/>
                  </TouchableOpacity>;
        }
        else{
          headerBack=<TouchableOpacity style={styles.menuWrapper} onPress={this.back_btn}>
                    <Image style={styles.image} source={back_icon}/>
                    </TouchableOpacity>;
      }

    return (
      <View style={styles.container}>
      <LinearGradient colors={['#48c3d5','#3fafc0','#369aaa','#287b88']}>

        <MyStatusBar barStyle="light-content"  backgroundColor="#4eb4c4"/>
          <View style={styles.appBar} >
              <View style={{ flex:1,flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', }}>
                <View style={styles.imageContainer}>
                {headerBack}
                </View>

                <View style={{ marginTop:0,width:'60%',justifyContent: 'center',alignItems:'center' }}>
                    <Text style={styles.headertitle}>Medical Profile setup!</Text>
                </View>

                <View style={{width:'15%',flexDirection: 'row',justifyContent: 'flex-end'}}>
                  {/* <TouchableOpacity style={styles.qusWrapper} onPress={() => { this.Show_Custom_Alert(true) }}>
                            <Image style={styles.image} source={qus_icon}/>
                            </TouchableOpacity> */}
                </View>

              </View>
          </View>


      </LinearGradient>

        <ScrollView style={{backgroundColor: '#F5FCFF',paddingTop: 10}}
        ref={(c) => {this.scroll = c}} >
		<View style={styles.container1}>
          <Text style={styles.heading}>  {this.state.question} </Text>
          <View style={styles.hr}></View>
      </View>
      <View style={styles.container2}>
            { this.state.qus_no == 5 ? DropDown : this.state.qus_no == 11 ? DropDown:options }
      </View>
      <View style={styles.container3}>
        <TouchableOpacity style={styles.btn} activeOpacity={.6} onPress={this.next}>
          <CommonButton label='Next'/>
          </TouchableOpacity>
      </View>

      </ScrollView>

      <Spinner visible={this.state.isVisible}  />

      </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
  },
  statusBar: {
    height: AppSizes.statusBarHeight,
  },
  appBar: {
      height: AppSizes.navbarHeight,
      justifyContent:'center',
      ...Platform.select({
      android: {
      paddingTop:AppSizes.ResponsiveSize.Padding(4),
      },
    }),
    },
  headertitle:{
    color:'#ffffff',
    fontSize:AppSizes.ResponsiveSize.Sizes(16),
    fontWeight:'bold',
    letterSpacing:1,
  },
  imageContainer:{
    width:'20%'
  },
  menuWrapper: {
    width:'60%',
    height:'60%',
    marginLeft:'30%',
  },
  image: {
    flex: 1,
    width: undefined,
    height: undefined,
    resizeMode:'contain'
  },
  container1:{
    flex:2,
    flexDirection:'column',
    alignItems:'center',
    justifyContent:'center'
  },
  heading:{
    fontSize:AppSizes.ResponsiveSize.Sizes(18),
    color:'#000',
    justifyContent:'center',
    alignItems:'center',
    fontWeight:'600',
    width:'89%',

  },
  hr:{
    width:50,
    height:3,
    backgroundColor:'#000',
    marginTop:10,

  },
  container2:{
    flex:6,
    flexDirection:'column',
    width:AppSizes.screen.width,
   paddingLeft:AppSizes.ResponsiveSize.Padding(5),
   paddingRight:AppSizes.ResponsiveSize.Padding(5),
   paddingTop:AppSizes.ResponsiveSize.Padding(2),
  },
  container3:{
    flex:2,
    justifyContent:'flex-start',
    alignItems:'center',

  },
  ans:{
    borderBottomWidth:1,
    borderColor:'#000',
    paddingTop:5,
    paddingBottom:10,
    width:'90%',
    marginBottom:25,

  },
  ansfield:{
    fontSize:AppSizes.ResponsiveSize.Sizes(13),
    color:'#5a5a5a',
  },
  btn:{
    width:'100%',
    marginBottom:20
  }
});
