Gender : category		| 1 for Male, 0 for Female
Age : float64
Height(in) : float64		| Height in inches (should be converted from feet + inches
BMI : float64			| Computed using BMI formula 703 x Weight(lbs) / [Height(in)]^2
Visited ICU for COPD in Last Year : | 1 for selected, 0 for no seletion COPD 1st qus always 1)
Unknown : category					| When no Risk Factor or any condition (not COPD) selected (1 for copd option  1st qus selection)
Smoker : category					| Risk Factor = Medical Profile item (1 for if i smoke option   selected 4th qus)
Pulmonary Hypertension : category			| Condition
Needs Help Performing Daily Activities : category	| Risk Factor = Medical Profile item qus(I need help with daily activities selected 1 or 0)
Long Term Oxygen User : category			| Risk Factor = Medical Profile item (I am on Oxygen therapy)
Lives Alone : category					| Risk Factor = Medical Profile item (I live alone)
Hospitalized for COPD in Last Year : category		| - (I have been hospitalized due to COPD in the  last 12 months)
High Blood Pressure : category				|  (High Blood Pressure)
Diabetes : category					|
Coronary Artery Disease : category			|
Congestive Heart Failure : category			| Conditions
Chronic Kidney Disease : category			|
Asthma : category					|
Anemia : category					|
Acid Reflux : category					|
2 or More Exacerbations In Last Year : category		| -  (optional but default 1)
cur_med_nan : category		|
cur_med_1.0 : category		| Current Medication: Default to curr_med_nan. (curr_med_nan means 1 or other for 0)
cur_med_2.0 : category		|
cur_med_3.0 : category		|
base_dyspnea_1 : category		| "Breathlessness only with demanding exercise" (selected value 1 or other 0- single choice qus)
base_dyspnea_2 : category		| .
base_dyspnea_3 : category		| .  "Breathlessness during activity" question.
base_dyspnea_4 : category		| .
base_dyspnea_5 : category		| "Too breathless to leave the house"
gold_nan : category		|				I don't know
gold_1.0 : category		|				Stage 1
gold_2.0 : category		| COPD GOLD STAGE question	. (selected value 1 or other 0- single choice qus)
gold_3.0 : category		|				.
gold_4.0 : category		|				Stage 4
basefev_nan : category			|
basefev_(70, 100] : category		|
basefev_(50, 70] : category		| Will default to basefev_nan since we do not collect it for now.
basefev_(30, 50] : category		|
basefev_(0, 30] : category		|
basehr_nan : category		|
basehr_(100, 250] : category	|
basehr_(80, 100] : category	| Baseline heart rate avg calculated using the formula provided and binned.
basehr_(60, 80] : category	|
basehr_(0, 60] : category	|
baseox_nan : category			|
baseox_(93, 100] : category		|
baseox_(91, 93] : category		|
baseox_(89, 91] : category		| Baseline Oxygen saturation avg (using the formula) and binned.
baseox_(85, 89] : category		|
baseox_(0, 85] : category		|
