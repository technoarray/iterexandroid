import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { StackNavigator } from 'react-navigation';
import { Platform, StyleSheet, TouchableOpacity, Image,Text, View,AppState ,AsyncStorage,Alert} from 'react-native';
import SplashScreen from 'react-native-smart-splash-screen'

import { AppStyles, AppSizes, AppColors } from './themes/'

import MainScreen from './components/drawer/MainScreen';
import invitationScreen from './components/drawer/invitationScreen';
import SignupScreen from './components/drawer/SignupScreen';
import ForgetPasswordScreen from './components/drawer/ForgetPasswordScreen';
import LoginScreen from './components/drawer/LoginScreen';
import StartScreen from './components/drawer/StartScreen';
import HomeScreen from './components/drawer/HomeScreen';
import signupPolicies from './components/drawer/signupPolicies';
import SignupGroupCode from './components/drawer/SignupGroupCode';
import MedicalProfileScreen from './components/drawer/MedicalProfileScreen';
import MedicalQusScreen from './components/drawer/MedicalQusScreen';
import MedicalAsthmaQusScreen from './components/drawer/MedicalAsthmaQusScreen';
import MedicalFormScreen from './components/drawer/MedicalFormScreen';
import MedicalFormSelectedScreen from './components/drawer/MedicalFormSelectedScreen';
import ThankYouScreen from './components/drawer/ThankYouScreen';
import AmIOkScreen from './components/drawer/AmIOkScreen';
import AmIOkQusScreen from './components/drawer/AmIOkQusScreen';
import HADSScreen from './components/drawer/HADSScreen';
import HADSQusScreen from './components/drawer/HADSQusScreen';
import CCQScreen from './components/drawer/CCQScreen';
import CCQQusScreen from './components/drawer/CCQQusScreen';
import MedicalAttentionScreen from './components/drawer/MedicalAttentionScreen';
import MedicalHelpScreen from './components/drawer/MedicalHelpScreen';
import TrackingScreen from './components/drawer/TrackingScreen';
import ChartScreen from './components/drawer/ChartScreen';
import HealthProfileScreen from './components/drawer/HealthProfileScreen';
import AsthmaHealthProfile from './components/drawer/AsthmaHealthProfile';
import ShareScreen from './components/drawer/ShareScreen';
import MedicalInAttentionScreen from './components/drawer/MedicalInAttentionScreen';
import infoQusScreen from './components/drawer/infoQusScreen';
import VitalScreen from './components/drawer/VitalScreen';
import MedicalHealthQueScreen from './components/drawer/MedicalHealthQueScreen';
import ACTQusScreen from './components/drawer/ACTQusScreen';
import AmIOkAsthmaQusScreen from './components/drawer/AmIOkAsthmaQusScreen';
import MedicalQusSelectionScreen from './components/drawer/MedicalQusSelectionScreen';
import AsthmaToDo from './components/drawer/AsthmaToDo';
import MedicalAsthmaAttentionScreen from './components/drawer/MedicalAsthmaAttentionScreen';
import AsthmaVitals from './components/drawer/AsthmaVitals';
import Thanku from './components/drawer/Thanku';
import AsthmaCharts from './components/drawer/AsthmaCharts'
import Medication from './components/drawer/Medication'
import ConditionQus from './components/drawer/ConditionQus'
import ActSetup from './components/drawer/ActSetup'
import UniqueCode from './components/drawer/UniqueCode'

const createStackNavigator = currentUser => StackNavigator({

  MainScreen: {
    screen: MainScreen,
    navigationOptions: {
        header: null
    }
  },
    invitationScreen: {
      screen: invitationScreen,
      navigationOptions: {
          header: null
      }
    },
    SignupScreen: {
        screen: SignupScreen,
        navigationOptions: {
            header: null
        }
    },
	SignupGroupCode: {
        screen: SignupGroupCode,
        navigationOptions: {
            header: null
        }
    },
	MedicalProfileScreen: {
        screen: MedicalProfileScreen,
        navigationOptions: {
            header: null
        }
    },
	MedicalQusScreen: {
        screen: MedicalQusScreen,
        navigationOptions: {
            header: null
        }
    },
	MedicalFormScreen: {
        screen: MedicalFormScreen,
        navigationOptions: {
            header: null
        }
    },
  MedicalFormSelectedScreen:{
      screen: MedicalFormSelectedScreen,
      navigationOptions: {
          header: null
      }
    },
	ThankYouScreen: {
        screen: ThankYouScreen,
        navigationOptions: {
            header: null
        }
    },
  AmIOkScreen: {
      screen: AmIOkScreen,
        navigationOptions: {
        header: null
      }
  },
  AmIOkQusScreen: {
      screen: AmIOkQusScreen,
        navigationOptions: {
        header: null
      }
  },
  HADSScreen: {
      screen: HADSScreen,
        navigationOptions: {
        header: null
      }
  },
  HADSQusScreen: {
      screen: HADSQusScreen,
        navigationOptions: {
        header: null
      }
  },
  CCQScreen: {
      screen: CCQScreen,
        navigationOptions: {
        header: null
      }
  },
  CCQQusScreen: {
      screen: CCQQusScreen,
        navigationOptions: {
        header: null
      }
  },
  MedicalAttentionScreen:{
    screen: MedicalAttentionScreen,
      navigationOptions: {
      header: null
    }
  },
  MedicalHelpScreen:{
    screen: MedicalHelpScreen,
      navigationOptions: {
      header: null
    }
  },
    ForgetScreen: {
        screen: ForgetPasswordScreen,
        navigationOptions: {
            header: null
        }
    },
    LoginScreen: {
        screen: LoginScreen,
        navigationOptions: {
            header: null
        }
    },
    StartScreen: {
        screen: StartScreen,
        navigationOptions: {
            header: null
        }
    },
    signupPolicies: {
        screen: signupPolicies,
        navigationOptions: {
            header: null
        }
    },
    HomeScreen: {
        screen: HomeScreen,
    },
    TrackingScreen:{
        screen: TrackingScreen,
    },
    ChartScreen:{
        screen: ChartScreen,
    },
    MedicalInAttentionScreen:{
        screen: MedicalInAttentionScreen,
    },
    HealthProfileScreen:{
        screen: HealthProfileScreen,
    },
    AsthmaHealthProfile:{
        screen: AsthmaHealthProfile,
    },
    ShareScreen:{
        screen: ShareScreen,
    },
    infoQusScreen:{
        screen: infoQusScreen,
    },
    VitalScreen:{
        screen: VitalScreen,
    },
    MedicalHealthQueScreen:{
        screen: MedicalHealthQueScreen,
    },
    ACTQusScreen:{
        screen: ACTQusScreen,
    },
    AmIOkAsthmaQusScreen:{
      screen:AmIOkAsthmaQusScreen
    },
    MedicalAsthmaQusScreen:{
      screen:MedicalAsthmaQusScreen
    },
    MedicalQusSelectionScreen:{
      screen:MedicalQusSelectionScreen
    },
    AsthmaToDo:{
      screen:AsthmaToDo
    },
    MedicalAsthmaAttentionScreen:{
      screen:MedicalAsthmaAttentionScreen
    },
    AsthmaVitals:{
      screen:AsthmaVitals
    },
    Thanku:{
      screen:Thanku
    },
    AsthmaCharts:{
      screen:AsthmaCharts
    },
    Medication:{
      screen:Medication
    },
    ConditionQus:{
      screen:ConditionQus
    },
    ActSetup:{
      screen:ActSetup
    },
    UniqueCode:{
      screen:UniqueCode
    }
}, {
    //initialRouteName: isEmpty(currentUser) ? 'StartScreen' : 'LoginScreen',
    initialRouteName: 'MainScreen',
    //initialRouteName: 'MedicalAsthmaQusScreen',
    headerMode: 'none',
});

class App extends Component {

  constructor(props) {
    super(props);

    //this.handleAppStateChange = this.handleAppStateChange.bind(this);
  }
  // componentDidMount() {
  //    AppState.addEventListener('change', this.handleAppStateChange);
  //
  // }

  // componentWillUnmount() {
  //   AppState.removeEventListener('change', this.handleAppStateChange);
  // }

  // handleAppStateChange(appState) {
  //   console.log(appState);
  //   if (appState === 'background') {
  //
  //     PushNotification.localNotificationSchedule({
  //         //... You can use all the options from localNotifications
  //         message: "Hey! We noticed you haven’t checked in with us since you enrolled. Tap here to check your symptoms now.", // (required)
  //         date: new Date(Date.now() + (1 * 1000)) // in 60 secs
  //       });
  //   }
  // }
  componentWillMount() {
      if (Platform.OS === 'android') {
          SplashScreen.close({
              animationType: SplashScreen.animationType.scale,
              duration: 850,
              delay: 2000
          })
      }
  }
  render() {
      const { currentUser } = this.props;
      const Navigator = createStackNavigator(currentUser);
      return ( < Navigator / > );
  }
}

App.propTypes = {
    currentUser: PropTypes.object.isRequired,
};

const mapStateToProps = store => ({
    currentUser: store.currentUser,
});

export default connect(mapStateToProps)(App);

const styles = StyleSheet.create({
    text: {
        color: '#fff',
        fontSize: 20,
        fontWeight: 'bold'
    },
    imageSizes:{
        height: AppSizes.screen.height/7, width:AppSizes.screen.width/5, resizeMode:'cover'
    },
    imageSizes_header:{
        height: 40, width:40, resizeMode:'cover'
    },
    imageSizes_header_logo:{
        height: 50, width:50, resizeMode:'cover'
    }
});
