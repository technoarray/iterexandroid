class Constant {
    static SITE_URL = "www.iterextherapeutics.com";
    static BASE_URL = "http://3.17.56.219/slim_api/public/";
    static Chart_URL = "http://3.17.56.219/chart/";
    static AsthmaCharts = "http://3.17.56.219/asthmacharts/"

    static SITE_URL = Constant.SITE_URL;
    static PYTHON_URL = "http://3.17.56.219:8080/";
    static URL_register = Constant.BASE_URL + 'users/signup';
    static URL_login = Constant.BASE_URL + 'users/login';
    static URL_socialLogin = Constant.BASE_URL + 'users/social_login';
    static URL_forgotPassword = Constant.BASE_URL + 'users/forgetPassword';
    static URL_invitationCode = Constant.BASE_URL + 'users/invitationCode';
    static URL_Homedata = Constant.BASE_URL + 'questions/showdata';
    static URL_Chartdata = Constant.BASE_URL + 'questions/chartdata';
    static URL_Userdata = Constant.BASE_URL + 'users/show';
    static URL_UserRiskFactorShow = Constant.BASE_URL + 'questions/RiskFactorShow';
    static URL_UserGroupCode = Constant.BASE_URL + 'users/group_code';
    static URL_UserPersonalInfo = Constant.BASE_URL + 'users/edit_info';
    static URL_AccountEdit = Constant.BASE_URL + 'users/account_edit';
    static URL_saveVariable = Constant.BASE_URL + 'questions/saveCopd';
    static URL_showVarData = Constant.BASE_URL + 'questions/showCopdProfile';
    static URL_saveVitalData = Constant.BASE_URL + 'questions/copdVital';
    static URL_scoreResult = Constant.BASE_URL + 'questions/scoreResult';
    static URL_showHealthCondition = Constant.BASE_URL + 'questions/showHealthCondition';
    static URL_todo = Constant.BASE_URL + 'questions/todo';
    static URL_baselineData = Constant.BASE_URL + 'questions/baselineData';
    static URL_saveUserData= Constant.BASE_URL + 'users/change_status';
    static URL_UpdateToken = Constant.BASE_URL + 'questions/updatedevicetoken';
    static URL_GetNotificationStatus = Constant.BASE_URL + 'users/check_status';
    static URL_SetNotificationStatus = Constant.BASE_URL + 'users/save_status';

    // asthma ApiRules
    static URL_asthmaVariables = Constant.BASE_URL + 'questions/saveAsthma'
    static URL_asthmaProfileVariable= Constant.BASE_URL + 'questions/showVarProfile'
    static URL_saveVital =Constant.BASE_URL +'questions/asthmaVital'
    static URL_Temp = Constant.BASE_URL + 'questions/asthmaVitaltmp'
    static URL_comborboties = Constant.BASE_URL + 'questions/saveMedical'
    static URL_GetComborboties= Constant.BASE_URL +'questions/showMedical'
    static URL_saveRandom = Constant.BASE_URL + 'questions/saveRandom'
    static URL_showRandom = Constant.BASE_URL + 'questions/showRandom'
    static URL_saveAsthma= Constant.BASE_URL + 'questions/saveAsthmanew'
    static URL_Unique= Constant.BASE_URL + 'questions/savecode'

    // python ApiRules
    static URL_profile =  Constant.PYTHON_URL + 'profile';
    static URL_symptom =  Constant.PYTHON_URL + 'symptom';
    static URL_vital =  Constant.PYTHON_URL + 'vital';
    static URL_triage =  Constant.PYTHON_URL + 'triage';
    static URL_triageprob =  Constant.PYTHON_URL + 'triageprob';
    static URL_med =  Constant.PYTHON_URL + 'med';
    static URL_medprob =  Constant.PYTHON_URL + 'med_proba';
    static URL_exacerb =  Constant.PYTHON_URL + 'exacerb';
    }

var WebServices = {
    callWebService: function(url, body) {
      var formBody = [];
      for (var property in body) {
        var encodedKey = encodeURIComponent(property);
        var encodedValue = encodeURIComponent(body[property]);
        formBody.push(encodedKey + "=" + encodedValue);
      }
      formBody = formBody.join("&");
      //console.log(formBody);
        return fetch(url, {
                method: 'POST',
                headers: {
                    //'Accept': 'application/json',
                    'Accept':'application/json; charset=utf-8',
                    'Content-Type':'application/x-www-form-urlencoded;charset=UTF-8'
                    //'CallToken': body.CallToken ? body.CallToken : '',
                },
                body: formBody
            })
            .then(response => response.text()) // Convert to text instead of res.json()
            .then((text) => {
                return text;
            })
            .then(response => JSON.parse(response)) // Parse the text.
            .then((jsonRes) => {
              //console.log(jsonRes)
                return jsonRes; //main output
            });
    },

    callWebService_GET: function(url, body) {
        return fetch(url, { // Use your url here
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    //'CallToken': body.CallToken ? body.CallToken : '',
                },
            })
            .then(response => response.text()) // Convert to text instead of res.json()
            .then((text) => {
              return text;
            })
            .then(response => JSON.parse(response)) // Parse the text.
            .then((jsonRes) => {

                // if ((!jsonRes) || (jsonRes.ResponseCode != '200')) {
                //     onError(jsonRes);
                // } else {
                //     onRequestComplete(jsonRes);
                // }

                return jsonRes;
            });
    },

    // json post Data
    callWebService_POST: function(url, body) {
        return fetch(url, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    //'CallToken': body.CallToken ? body.CallToken : '',
                },
                body: JSON.stringify(body)
            })
            .then(response => response.text()) // Convert to text instead of res.json()
            .then((text) => {
                return text;
            })
            .then(response => JSON.parse(response)) // Parse the text.
            .then((jsonRes) => {
                return jsonRes; //main output
            });
    }

}
module.exports = {
    Constant,
    WebServices
}
