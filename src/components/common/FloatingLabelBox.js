import React, { Component } from 'react';
import {StyleSheet,Text,View,Image,TouchableOpacity,Platform,ScrollView,Alert} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'

/* Images */
var pencil_icon = require('../../themes/Images/edit-pin.png');

export default class FloatingLabelBox extends Component{

  constructor(props) {
    super(props)

    this.state = {
      expanded: this.props.expanded,
    }
  }



  render() {
    return (
      <View style={styles.box}>
        <View style={styles.titleContainer}>
          <View style={styles.imageContainer}>
          <Image
              source={this.props.labelIcon}
              style={styles.textImage}
          />
          </View>
          <View style={[styles.label]}>
            {this.props.children}
          </View>
          {/*<View style={styles.imageContainer}>
            <Image
                source={pencil_icon}
                style={styles.buttonImage}
            />
          </View> */}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  box: {
    flex: 1,
    overflow: 'hidden',
    borderBottomWidth:1,
    borderBottomColor:'#000'
  },
  titleContainer: {
    width:'92%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',

  },
label:{
 flex:5,
},
  textImage: {
  //  flex: 1,
    height:'40%',
    width:'40%',
    resizeMode:'contain',
  },

  imageContainer:{
    flex:1,
    alignItems: 'flex-end',
    paddingTop: (Platform.OS === 'ios') ? AppSizes.ResponsiveSize.Padding(3) : AppSizes.ResponsiveSize.Padding(6),
    //backgroundColor:'red'
  },
  buttonImage: {
  //  flex: 1,
    height:'30%',
    width:'30%',
    resizeMode:'contain',
  },

});
