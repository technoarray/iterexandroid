import React, { Component } from 'react';
import { StyleSheet,Text, View, Image, TouchableWithoutFeedback, Animated} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'

/* Images */
var pencil_icon = require('../../themes/Images/edit-pin.png');

export default class panel extends Component{

  constructor(props) {
    super(props)

    this.icons = {
      'up': this.props.arrowUpType,
      'down': this.props.arrowDownType,
    }

    this.state = {
      expanded: this.props.expanded,
    }
  }

  toggle = () => {
    console.log(this.state.animation)
    let initialValue = this.state.expanded ? this.state.maxHeight + this.state.minHeight : this.state.minHeight
    let finalValue = this.state.expanded ? this.state.minHeight : this.state.minHeight + this.state.maxHeight

    this.setState({
      expanded: !this.state.expanded
    })

    this.state.animation.setValue(initialValue)
    Animated.spring(
      this.state.animation,
      {
        toValue: finalValue,
        bounciness: 0,
      }
    ).start()
  }

  setMaxHeight = (event) => {
    if (!this.state.maxHeight) {
      this.setState({
        maxHeight: event.nativeEvent.layout.height
      })
    }
  }

  setMinHeight = (event) => {
    if (!this.state.animation) {
      this.setState({animation:
        this.state.expanded ?
          new Animated.Value() :
          new Animated.Value(parseInt(event.nativeEvent.layout.height))
      })
    }
    this.setState({
      minHeight: event.nativeEvent.layout.height
    })
  }

  render() {
    return (
      <Animated.View style={[styles.box, this.props.style, {height: this.state.animation}]}>
        <TouchableWithoutFeedback
          onPress={this.toggle}
          onLayout={this.setMinHeight}
        >
        <View style={styles.titleContainer}>
        <Image
            source={this.props.textImg}
            style={styles.textImage}
        />
          <Text style={styles.label}>{this.props.label}</Text>
          {this.props.value ? <Text style={styles.value}>{this.props.value}</Text> : null}
          <Image
              source={pencil_icon}
              style={styles.buttonImage}
          />
          </View>
        </TouchableWithoutFeedback>
        <View style={styles.body} onLayout={this.setMaxHeight}>
          {this.props.children}
        </View>
      </Animated.View>
    );
  }
}

const styles = StyleSheet.create({
  touchableContainer: {
    flex: 1,
    flexDirection: 'row',
  },
  box: {
    flex: 1,
    overflow: 'hidden',
  //  borderBottomWidth:1,
  //  borderBottomColor:'#7c7c7c',
    borderWidth:1,
    borderColor:'#000'
  },
  titleContainer: {
    flexDirection: 'row',
    padding: AppSizes.ResponsiveSize.Padding(3),
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  label: {
    flex: 5,
    fontSize:AppSizes.ResponsiveSize.Sizes(14),
  },
  value: {
    flex: 5,
    fontWeight: 'bold',
    textAlign: 'right',
  },
  textImage: {
    flex: 1,
    height:'50%',
    width:'50%',
    resizeMode:'contain',
  },
  buttonImage: {
    flex: 1,
    height:'50%',
    width:'50%',
    resizeMode:'contain',
  },
  body: {
    padding: 0,
  }
});
