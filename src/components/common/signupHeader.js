import React, { Component } from 'react';
import {AsyncStorage,StyleSheet,Text,View,Image,TouchableOpacity,WebView,Platform, ActivityIndicator,StatusBar,Modal,ScrollView} from 'react-native';
import { DrawerActions,NavigationActions, StackActions  } from 'react-navigation';
import LinearGradient from 'react-native-linear-gradient';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import * as commonFunctions from '../../utils/CommonFunctions'
import Sound from 'react-native-sound'

/* Images */
var logo = require( '../../themes/Images/logo_white.png')
var menu_icon = require( '../../themes/Images/menu_icon_196.png')
var back_icon = require( '../../themes/Images/left-whitearrow.png')
var qus_icon = require('../../themes/Images/que_icon_196.png');
var mice=require('../../themes/Images/mice.png')
var mice_off=require('../../themes/Images/mice-off.png')

const MyStatusBar = ({backgroundColor, ...props}) => (
  <View style={[styles.statusBar, { backgroundColor }]}>
    <StatusBar translucent backgroundColor={backgroundColor} {...props} />
  </View>
);
export default class signupHeader extends React.Component {
  constructor (props) {
    super(props);
    Sound.setCategory('Playback', true);
    this.state = {
    routename: '',
    Modal_Visibility: false,
    audioStatus: false,
    audioImg: mice,
  },
  _that = this;
  }



  back_click() {
    console.log(_that.props.info.screens);
    if(_that.props.info.screens=="ThankYouScreen" || _that.props.info.screens=="TrackingScreen"|| _that.props.info.screens=="AsthmaToDo" ||_that.props.info.screens=="ShareScreen"|| _that.props.info.screens=="VitalScreen" || _that.props.info.screens=="ChartScreen" || _that.props.info.screens=="HealthProfileScreen" || _that.props.info.screens=="AsthmaHealthProfile"){
      const resetAction = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: 'StartScreen' })],
        });
    _that.props.navigation.dispatch(resetAction);
    }
    else if(_that.props.info.screens=="AsthmaVital"){
      _that.props.navigation.navigate("AsthmaToDo");
    }
    else if(_that.props.info.screens=="MedicalProfileScreen"){
      _that.props.navigation.navigate("SignupGroupCode");
    }
    else if(_that.props.info.screens=="MedicalFormScreen"){
      _that.props.navigation.navigate("MedicalProfileScreen");
    }
    else if(_that.props.info.screens=="MedicalQusScreen"){
      _that.props.navigation.navigate("MedicalFormScreen");
    }
    else{
  	 //console.log(_that.props.info.screens);
      _that.props.navigation.navigate(_that.props.info.screens);
    }
  }


  playTrack = () => {
    this.setState({audioStatus: !this.state.audioStatus})
    if(this.state.audioStatus==true){
      this.track = new Sound(this.props.info.qus_audio, (e) => {
        if (e) {
          console.log('error loading track:', e)
        } else {

            this.setState({audioImg: mice_off});
            this.track.play((success) => {
                 if (success) {
                     this.stop();
                 }
             });
          }
        })
      }
        else {
          this.stop();
        }
      }

  stop() {
     if (!this.track) return;
     this.track.stop();
     this.track.release();
     this.track = null;
     this.setState({audioStatus: false});
     this.setState({audioImg: mice});
 }

  Show_Custom_Alert(visible) {
    this.stop();
    this.setState({Modal_Visibility: visible});
    this.props.updateAlert();
  }

  ok_Button=()=>{

    Alert.alert("OK Button Clicked.");

  }
    render() {
      const routename=this.props.info.screens;
      const qus_content=this.props.info.qus_content;
      if(routename=='HomeScreen'){
      /*  headerTitle=<View style={styles.logoimageContainer}>
        <View style={styles.logoWrapper}>
            <Image style={styles.image} source={logo}/>
        </View>
        </View>;*/
        headerTitle=<Text allowFontScaling={false} style={styles.headertitle}>{this.props.info.title}</Text>;
      }
      else {
        headerTitle=<Text allowFontScaling={false} style={styles.headertitle}>{this.props.info.title}</Text>;
      }

		if(routename!='signupPolicies'){
        headerBack=<TouchableOpacity style={styles.menuWrapper} onPress={this.back_click}>
                <Image style={styles.image} source={back_icon}/>
                </TouchableOpacity>;
    }
      else{
		  headerBack=<Text></Text>;
	  }

    if(qus_content!=''){
      qusView=<TouchableOpacity style={styles.qusWrapper} onPress={() => { this.Show_Custom_Alert(true) }}>
         <Image style={styles.image} source={qus_icon}/>
     </TouchableOpacity>;
   }
   else{
     qusView=<Text></Text>;
   }
      return (
      <LinearGradient colors={['#48c3d5','#3fafc0','#369aaa','#287b88']}>

        <MyStatusBar barStyle="light-content"  backgroundColor="#4eb4c4"/>
          <View style={styles.appBar} >
              <View style={{ flex:1,flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
                <View style={styles.imageContainer}>
				            {headerBack}
                </View>

                <View style={{ marginTop:0,width:'60%',justifyContent: 'center',alignItems:'center' }}>
                    {headerTitle}
                </View>

                <View style={{width:'15%',flexDirection: 'row',justifyContent: 'flex-end'}}>
                  {qusView}
                </View>

              </View>
          </View>

        <Modal visible={this.state.Modal_Visibility}
          transparent={true}
          animationType={"fade"}
          onRequestClose={ () => { this.Show_Custom_Alert(!this.state.Modal_Visibility)} } >
            <View style={{ flex:1, alignItems: 'center', justifyContent: 'center',zIndex:20,backgroundColor:'#000000a3', }}>
              <View style={styles.Alert_Main_View}>
                  <View style={{ flex:1,flexDirection:'column',alignItems: 'center', justifyContent: 'center',}}>
                      <View style={{height:'15%',width:AppSizes.screen.width/10,paddingTop:'6%',marginBottom:8}}>
                        <TouchableOpacity style={{width:'100%',height:'100%'}} onPress={this.playTrack}>
                          <Image style={styles.image} source={this.state.audioImg}/>
                        </TouchableOpacity>
                      </View>
                      <View style={{height:'70%',flexDirection:'row',alignItems: 'center', justifyContent: 'center',paddingBottom:'2%',}}>
                        <ScrollView contentContainerStyle={styles.modal}>
                          <Text allowFontScaling={false} style={styles.Alert_Message}>
                            {this.props.info.qus_content}
                          </Text>
                        </ScrollView>
                      </View>
                  </View>
                  <View style={{width:'100%',flex:0.2,  backgroundColor: '#ebebeb',borderBottomLeftRadius:20,borderBottomRightRadius:20,}}>
                      <TouchableOpacity style={styles.buttonStyle} activeOpacity={0.7}
                        onPress={() => { this.Show_Custom_Alert(!this.state.Modal_Visibility)} }  >
                            <Text allowFontScaling={false} style={styles.TextStyle}> CANCEL </Text>
                      </TouchableOpacity>
                  </View>
                </View>
              </View>
          </Modal>

      </LinearGradient>
    )
    }
}

const styles = StyleSheet.create({
  statusBar: {
    height: AppSizes.statusBarHeight,
  },
  appBar: {
      height: AppSizes.navbarHeight,
      justifyContent:'center',
      ...Platform.select({
      android: {
      paddingTop:AppSizes.ResponsiveSize.Padding(4),
      },
    }),
  },
  headertitle:{
    color:'#ffffff',
    fontSize:(Platform.OS === 'ios') ? AppSizes.ResponsiveSize.Sizes(14) :AppSizes.ResponsiveSize.Sizes(16),
    fontWeight:'bold',
    letterSpacing:1,
  },
  imageContainer:{
    width:'20%'
  },
  logoimageContainer:{
    width:'50%'
  },
  logoWrapper:{
    width:'100%',
    height:'100%',
  },
  menuWrapper: {
    width:'60%',
    height:'60%',
    marginLeft:'30%',
  },
  qusWrapper: {
    width:'40%',
    height:'40%',
    marginRight:'30%',
    marginTop:'15%'
  },
  image: {
    flex: 1,
    width: undefined,
    height: undefined,
    resizeMode:'contain'
  },
  Alert_Main_View:{
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#ffffff',
    height:'40%',
    width: '80%',
    borderRadius:20,
    flexDirection:'column',
    justifyContent: 'center',
    alignItems: 'center',
  },

  Alert_Title:{
    fontSize: AppSizes.ResponsiveSize.Sizes(25),
    color: "#000",
    textAlign: 'center',
    padding: 10,
    height: '28%'
  },
    Alert_Message:{
      fontSize: AppSizes.ResponsiveSize.Sizes(18),
      color: "#000",
      textAlign: 'center',
      padding: 10,
    },
    buttonStyle: {
      width: '100%',
      height: '100%',
      justifyContent: 'center',
      alignItems: 'center'
    },
    TextStyle:{
      color:'#000',
      textAlign:'center',
      fontSize: AppSizes.ResponsiveSize.Sizes(18),
    },

});
