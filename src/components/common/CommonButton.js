import React, { Component } from 'react';
import {StyleSheet,Text,View,Image,TouchableOpacity,Platform,Alert,Button} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { AppStyles, AppSizes, AppColors } from '../../themes/'


export default class CommonButton extends Component {
  render() {
    return (
      <View style={{ alignItems:'center', justifyContent: 'center', marginBottom:10,marginTop:20}}>
        <View style={{ width: '90%', justifyContent:'space-around'}}>
              <LinearGradient colors={['#36c4d8', '#2aa5b4', '#198391', '#15767d']} style={{width: '100%', height:AppSizes.screen.width/9, borderRadius: 25, alignItems:'center', justifyContent: 'center'}}>
                  <Text allowFontScaling={false} style={styles.textbtn}>{this.props.label}</Text>
              </LinearGradient>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
textbtn:{
  color:'#ffffff',
  fontSize:AppSizes.ResponsiveSize.Sizes(16),
}
});
