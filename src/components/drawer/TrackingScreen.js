import React, { Component } from 'react';
import {AsyncStorage,StyleSheet,Text,View,Image,TouchableOpacity,Platform,ScrollView,Alert} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions } from 'react-navigation'
import LinearGradient from 'react-native-linear-gradient';
import Header from '../common/signupHeader'
import * as commonFunctions from '../../utils/CommonFunctions'
import Spinner from 'react-native-loading-spinner-overlay';
import Orientation from 'react-native-orientation'

var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;

/* Images */
var arrow_icon=require('../../themes/Images/arrow_icon_196.png')

var help_audio=require('../../themes/sound/ToDoHelp.mp3')

let _that
export default class TrackingScreen extends Component {
    constructor (props) {
      super(props)
      this.state = {
      isVisible: false,
      Modal_Visibility: false,
      tab_tos:true,
      tab_privacy:false,
      uid:'',
      amiok:1,
      amiok_date:'',
      amiok_status:'',
      ccq:1,
      ccq_date:'',
      ccq_status:'',
      hads:1,
      hads_date:'',
      hads_status:'',
      vital:1,
      vital_date:'',
      vital_status:'',
    },
    _that = this;
  }

  componentDidMount() {
    Orientation.lockToPortrait();
    AsyncStorage.getItem('loggedIn').then((value) => {
        var loggedIn = JSON.parse(value);
        if (loggedIn) {
            AsyncStorage.getItem('UserData').then((UserData) => {
                const data = JSON.parse(UserData)
                console.log('Data',data)
                var uid=data.id;
                _that.setState({uid:  uid});
                _that.validationAndApiParameter('todo');
            })
       }
    })
  }

  validationAndApiParameter(apiname) {
    if(apiname == 'todo'){
      var data = {
          uid: this.state.uid,

      };
      console.log(data);
      _that.setState({isVisible: true});
      this.postToApiCalling('POST', apiname, Constant.URL_showVarData,data);
    }
  }

  postToApiCalling(method, apiKey, apiUrl, data) {
      //console.log(apiUrl)
        new Promise(function(resolve, reject) {
            if (method == 'POST') {
                resolve(WebServices.callWebService(apiUrl, data));
            } else {
              //console.log('callWebService_POST')
                resolve(WebServices.callWebService_POST(apiUrl, data));
            }
        }).then((jsonRes) => {
          //  console.log(jsonRes)
              _that.setState({ isVisible: false })
              if ((!jsonRes) || (jsonRes.code == 0)) {
                setTimeout(() => {
                      Alert.alert(jsonRes.message);
                  }, 200);
              } else {
                  if (jsonRes.code == 1) {
                      _that.apiSuccessfullResponse(apiKey, jsonRes)
                  }
              }

        }).catch((error) => {
            console.log("ERROR" + error);
            _that.setState({ isVisible: false })
            setTimeout(() => {
                Alert.alert("Server Error");
            }, 200);
        })
    }

  apiSuccessfullResponse(apiKey, jsonRes) {
      if(apiKey=="todo"){
        jdata=jsonRes.profile;
        console.log(jdata)
        this.setState({
          amiok:jdata.amiok_todo,
          amiok_date:jdata.amiok_date,
          ccq:jdata.ccq_todo,
          ccq_date:jdata.ccq_date,
          ccq_status:jdata.ccq_status,
          hads:jdata.hads_todo,
          hads_date:jdata.hads_date,
          vital:jdata.vital_todo,
          vital_date:jdata.vital_date,
        })
      }
    }

  updateAlert = (visible) => {
        this.setState({Modal_Visibility: !this.state.Modal_Visibility});
  }


  tabtosChange=()=>{
    this.setState({tab_tos: true});
    this.setState({tab_privacy: false});
  }

  tabPrivacyChange=()=>{
    this.setState({tab_privacy: true});
    this.setState({tab_tos: false});
  }

  vitalsupdate=()=>{
  _that.props.navigation.navigate('VitalScreen', {qus_no: 0,  name: 'VitalScreen' });
  }

  amiok=()=>{
    _that.props.navigation.navigate('AmIOkQusScreen', {qus_no: 0,  name: 'AmIOkScreen' });
  }

  hospitalCCQ=()=>{
    _that.props.navigation.navigate('CCQQusScreen');
  }

  hads=()=>{
   _that.props.navigation.navigate('HADSQusScreen', {qus_no: 0,  name: 'HADSScreen' });
  }

  activeTab(title){
    return (
      <LinearGradient colors={['#36c4d8', '#2aa5b4', '#198391', '#15767d']} style={styles.tab_linear}>
      <Text allowFontScaling={false} style={[styles.textbtn,styles.tab_activeText]}>{title}</Text>
      </LinearGradient>
    );
  }

  inactiveTab(title){
    return (
      <LinearGradient colors={['#ebebeb', '#ebebeb', '#ebebeb', '#ebebeb']} style={styles.tab_linear}>
      <Text allowFontScaling={false} style={styles.textbtn}>{title}</Text>
      </LinearGradient>
    );
  }

  render() {
    const headerProp = {
      title: 'To Do',
      screens: 'TrackingScreen',
      qus_content:'These items are important for tracking your condition and for a more accurate “Am I OK” assessment.\n\nThe default “Pending” section lists tasks that show up in regular, periodic intervals and should be completed as soon as possible by you.\nThe “Done” section lists all the items that you have already completed for the active period. Use this section if you want to update items that have not already shown up in your “Pending” section.',
      qus_audio: help_audio
    };


    return (

      <View style={styles.container}>
        <Header info={headerProp} navigation={_that.props.navigation} updateAlert={this.updateAlert}/>
        <View style={[styles.content,this.state.Modal_Visibility ? {backgroundColor: 'rgba(0,0,0,0.4)',zIndex:5} : '#ffffff']}>
          <View style={styles.tabContainer}>
            <View style={{ width: '50%'}}>
              <TouchableOpacity activeOpacity={.6} onPress={this.tabtosChange}>
                {this.state.tab_tos ?
                  this.activeTab('Pending')
                :
                  this.inactiveTab('Pending')
                }
              </TouchableOpacity>
            </View>

            <View style={{ width: '50%',}} >
              <TouchableOpacity activeOpacity={.6} onPress={this.tabPrivacyChange}>
                {this.state.tab_privacy ?
                  this.activeTab('Done')
                :
                  this.inactiveTab('Done')
                }
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.tabContent}>
            {this.state.tab_tos ?
              <View style={{flex:1,flexDirection:'column'}}>
                {this.state.ccq==0 ?
                  <TouchableOpacity activeOpacity={.6} onPress={this.hospitalCCQ}  style={[styles.boxDesign12,styles.shadow]}>
                    <View style={styles.boxtitlebox}>
                      <Text allowFontScaling={false} style={styles.boxtitle1}>COPD Health Assessment</Text>
                    </View>
                    <View style={styles.border}/>
                    <Text allowFontScaling={false} style={styles.boxtitle2}>Update Due On : {this.state.ccq_date}</Text>
                  </TouchableOpacity>
                :
                  null
                }
                {this.state.amiok==0 ?
                <TouchableOpacity activeOpacity={.6} onPress={this.amiok} style={[styles.boxDesign12,styles.shadow]}>
                  <View style={styles.boxtitlebox}>
                    <Text allowFontScaling={false} style={styles.boxtitle1}>Am I OK?  </Text>
                  </View>
                  <View style={styles.border}/>
                  <Text allowFontScaling={false} style={styles.boxtitle2}>Update Due On : {this.state.amiok_date}</Text>
                  {/* <Text style={styles.boxtitle2}>Last Value : {this.state.amiok_status}</Text> */ }
                </TouchableOpacity>
                : null
              }
                {this.state.hads==0 ?
                  <TouchableOpacity activeOpacity={.6} onPress={this.hads} style={[styles.boxDesign12,styles.shadow]}>
                    <View style={styles.boxtitlebox}>
                        <Text allowFontScaling={false} style={styles.boxtitle1}>Anxiety Assessment</Text>
                    </View>
                    <View style={styles.border}/>
                    <Text allowFontScaling={false} style={styles.boxtitle2}>Update Due On : {this.state.hads_date}</Text>
                  </TouchableOpacity>
                  : null
                }
                {this.state.vital==0 ?
                <TouchableOpacity activeOpacity={.6} onPress={this.vitalsupdate} style={[styles.boxDesign12,styles.shadow]}>
                  <View style={styles.boxtitlebox}>
                      <Text allowFontScaling={false} style={styles.boxtitle1}>Vitals update?  </Text>
                  </View>
                  <View style={styles.border}/>
                  <Text allowFontScaling={false} style={styles.boxtitle2}>Update Due On : {this.state.vital_date}</Text>
                  <Text allowFontScaling={false} style={styles.boxtitle2}>Last Value : {this.state.vital_status}</Text>
                </TouchableOpacity>
                : null
              }
              </View>
            :
              null
            }
            {this.state.tab_privacy ?
              <View style={{flex:1,flexDirection:'column',}}>
                {this.state.ccq==1 ?
                  <TouchableOpacity activeOpacity={.6} onPress={this.hospitalCCQ}  style={[styles.boxDesign12,styles.shadow]}>
                    <View style={styles.boxtitlebox}>
                      <Text allowFontScaling={false} style={styles.boxtitle1}>COPD Health Assessment</Text>
                    </View>
                    <View style={styles.border}/>
                    <Text allowFontScaling={false} style={styles.boxtitle2}>Last updated : {this.state.ccq_date}</Text>
                    {/*<Text style={styles.boxtitle2}>Last Value : {this.state.ccq_status}</Text>*/}
                  </TouchableOpacity>
                :
                  null
                }

                {this.state.amiok==1 ?
                  <TouchableOpacity activeOpacity={.6} onPress={this.amiok} style={[styles.boxDesign12,styles.shadow]}>
                    <View style={styles.boxtitlebox}>
                      <Text allowFontScaling={false} style={styles.boxtitle1}>Am I OK?  </Text>
                    </View>
                    <View style={styles.border}/>
                    <Text allowFontScaling={false} style={styles.boxtitle2}>Last updated : {this.state.amiok_date}</Text>
                    {/*<Text style={styles.boxtitle2}>Last Value : {this.state.amiok_status}</Text>*/}
                  </TouchableOpacity>
                :
                  null
                }

                {this.state.hads==1 ?
                  <TouchableOpacity activeOpacity={.6} onPress={this.hads} style={[styles.boxDesign12,styles.shadow]}>
                    <View style={styles.boxtitlebox}>
                      <Text allowFontScaling={false} style={styles.boxtitle1}>Anxiety Assessment</Text>
                    </View>
                    <View style={styles.border}/>
                      <Text allowFontScaling={false} style={styles.boxtitle2}>Last updated : {this.state.hads_date}</Text>
                  </TouchableOpacity>
                :
                  null
                }

                {this.state.vital==1 ?
                  <TouchableOpacity activeOpacity={.6} onPress={this.vitalsupdate} style={[styles.boxDesign12,styles.shadow]}>
                    <View style={styles.boxtitlebox}>
                        <Text allowFontScaling={false} style={styles.boxtitle1}>Vitals update?  </Text>
                    </View>
                    <View style={styles.border}/>
                    <Text allowFontScaling={false} style={styles.boxtitle2}>Last updated : {this.state.vital_date}</Text>
                    <Text allowFontScaling={false} style={styles.boxtitle2}>Last Value : {this.state.vital_status} severity</Text>
                  </TouchableOpacity>
                :
                  null
                }
              </View>
            :
              null
            }
          </View>
        </View>
        <Spinner visible={this.state.isVisible}  />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,

  },

  content: {
      flex: 1,
      backgroundColor: '#ffffff',
  },
tabContainer:{
  flexDirection:'row',
  alignItems:'center',
  paddingBottom:AppSizes.ResponsiveSize.Padding(1),
  height:'11%',
},
tabContent:{
  flex:1,
  height:500,
  paddingLeft:AppSizes.ResponsiveSize.Padding(5),
  paddingRight:AppSizes.ResponsiveSize.Padding(5),

},
tab_title:{
  textAlign:'center',
  fontSize:AppSizes.ResponsiveSize.Sizes(20),
  color:AppColors.primary,
  paddingBottom:AppSizes.ResponsiveSize.Padding(5),
},

textbtn:{
  fontWeight:'bold',
  textAlign:'center',
  textTransform: 'uppercase',
  fontSize:AppSizes.ResponsiveSize.Sizes(14),
  color:'#3e3e3e'
},
tab_activeText:{
  color:'#ffffff'
},
tab_linear:{
  width: '100%',
  height: '100%',
  alignItems:'center',
  justifyContent: 'center'
},

shadow:{
 borderWidth:1,
 borderRadius: 2,
 borderColor: '#fff',
 justifyContent:'center',
 backgroundColor:'#fff',
 borderColor: '#ddd',
 borderBottomWidth: 1,
 shadowColor: '#999',
 shadowOffset: { width: 0, height: 2 },
 shadowOpacity: 0.8,
 shadowRadius: 2,
 elevation: 0,
 //backgroundColor:'red',

},
boxcontainer1:{
  width:'30%',
  height:'100%',
  justifyContent:'center',
  alignItems:'center',
},

boxicon:{
  flexGrow:1,
  height:null,
  width:null,
  alignItems: 'center',
  justifyContent:'center',
  resizeMode:'contain',
},
boxtitle1:{
  fontSize: AppSizes.ResponsiveSize.Sizes(14),
  fontWeight:'bold',
  color:AppColors.primary,
  shadowOpacity: 0,
  //lineHeight:25,
  //lineHeight: AppSizes.ResponsiveSize.Sizes(13 * 1.70),
  marginTop:AppSizes.ResponsiveSize.Padding(2),



},
boxtitlebox:{
  height:AppSizes.screen.height/15,
  justifyContent:'center',
  //backgroundColor:'red',
},
boxtitle2:{
  fontSize: AppSizes.ResponsiveSize.Sizes(11),
  fontWeight:'600',
  color:AppColors.smalltext,
  shadowOpacity: 0,
},

border:{
  borderBottomWidth:1,
  borderBottomColor:'#a9a9a9',
  marginTop:AppSizes.ResponsiveSize.Padding(0.5),
  marginBottom:AppSizes.ResponsiveSize.Padding(1.5),
},
overdue:{
  color:'red'
},
overbox:{
  backgroundColor:'red',
  width:'20%',
  //position:'absolute',
  alignItems:'center',
  marginTop:AppSizes.ResponsiveSize.Padding(2)
},
overtext:{
  fontSize: AppSizes.ResponsiveSize.Sizes(10),
  fontWeight:'400',
  color:'#fff',
  lineHeight: AppSizes.ResponsiveSize.Sizes(10 * 1.70),
},
sideicon:{
  position: 'absolute',
  bottom: '-16%',
  right:'-6%',
  width:AppSizes.screen.width/12,
  height:AppSizes.screen.width/12,
  //width:AppSizes.ResponsiveSize.width/2,
  //height:AppSizes.ResponsiveSize.height/2,

},
boxicon:{
  flexGrow:1,
  height:null,
  width:null,
  alignItems: 'center',
  justifyContent:'center',
  resizeMode:'contain',
},
/*maintwoflex:{
  flex:1.7,

},
mainoneflex:{
  flex:1.2,

},*/
titlemain:{
  //height:AppSizes.screen.height/,
  //backgroundColor:'red',
  paddingBottom:AppSizes.ResponsiveSize.Padding(2)

},
boxDesign12:{
  padding:AppSizes.ResponsiveSize.Padding(4),
  borderWidth:1,
  borderColor:'#000',
  justifyContent:'center',
  marginBottom:AppSizes.ResponsiveSize.Padding(3),
  height:AppSizes.screen.height/6,

}
});
