import React, { Component } from 'react';
import {StyleSheet,Text,View,Image,TouchableOpacity,Platform,ScrollView,Alert,AsyncStorage,Keyboard} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions } from 'react-navigation'
import Spacer from '../common/Spacer'
import Header from '../common/HeaderBeforeLogin'
import ToggleBox from '../common/ToggleBox'
import CommonButton from '../common/CommonButton'
import LinearGradient from 'react-native-linear-gradient';
import FloatingLabelBox from '../common/FloatingLabelBox'
import FloatingLabel from 'react-native-floating-labels';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Spinner from 'react-native-loading-spinner-overlay';
import Orientation from 'react-native-orientation';
import * as commonFunctions from '../../utils/CommonFunctions';

var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;

/* Images */
var email_icon = require('../../themes/Images/email.png');
var phone_icon = require('../../themes/Images/phone.png');
var lock_icon = require('../../themes/Images/lock.png');
var navigation_icon = require('../../themes/Images/navigation.png');
var group_code_icon = require('../../themes/Images/group-code.png');
var calender_icon = require('../../themes/Images/calender.png');
var gender_icon = require('../../themes/Images/gender.png');


let _that

export default class SignupScreen extends Component {
  constructor (props) {
    super(props)
    this.state = {
    devicetoken: '',
    devicetype: '',
    user_name : '',
    email : '',
    phone : '',
    password : '',
    address_full_name : '',
    street_address : '',
    city :'',
    state:'',
    isVisible: false,
    Modal_Visibility: false,
    passwordErrorMessage:'',
    passwordErrorArray:[],
    emailErrorMessage:'',
    nameErrorMessage:''
  },
  _that = this;
}

componentDidMount() {
  var type=Platform.OS=='ios'?'1':'2'
  this.setState({devicetype:type})
  Orientation.lockToPortrait();
  AsyncStorage.setItem('screenName', JSON.stringify('SignupScreen'));
  AsyncStorage.getItem('DeviceToken').then((value) =>{
    var token=value
    this.setState({devicetoken:token})
  })
}

updateAlert = () => {
      this.setState({Modal_Visibility: !this.state.Modal_Visibility});
  }

accountSave = () => {
    //_that.props.navigation.navigate('signupPolicies');
  this.validationAndApiParameter()

}
passwordValidation(){
  var arr=[];
  const {password } = this.state

  if ((password.indexOf(' ') >= 0 || password.length <= 0)) {
   this.setState({passwordErrorMessage:'Please enter password!'})
  }else if (!commonFunctions.validatePassword(password)){
   this.setState({passwordErrorMessage:'Password should contain:\n'})

   if(!commonFunctions.validatePasswordupper(password)){
     arr.push('* must contain atleast 1 upper case character\n');
   }
   else{
     arr=commonFunctions.validateRemovearr(arr,'* must contain atleast 1 upper case character\n');
   }

   if(!commonFunctions.validatePasswordlower(password)){
     arr.push('* must contain atleast 1 lower case character\n');
   }
   else{
     arr=commonFunctions.validateRemovearr(arr,'* must contain atleast 1 lower case character\n');
   }

   if(!commonFunctions.validatePasswordspecial(password)){
     arr.push('* must contain atleast 1 special character\n');
   }
   else{
     arr=commonFunctions.validateRemovearr(arr,'* must contain atleast 1 special character\n');
   }

   if(!commonFunctions.validatePassworddigit(password)){
     arr.push('* must contain atleast 1 numeric value\n');
   }
   else{
     arr=commonFunctions.validateRemovearr(arr,'* must contain atleast 1 numeric value\n');
   }

   if(!commonFunctions.validatePasswordLength(password)){
     arr.push('* length cannot be less than 10 characters\n');
   }
   else{
     arr=commonFunctions.validateRemovearr(arr,'* length cannot be less than 8 characters\n');
   }

   //console.log(password)
  // console.log(arr)
  this.setState({passwordErrorMessage:''})
   this.setState({passwordErrorArray:arr})
 }
 else{
   this.setState({passwordErrorMessage:''})
   this.setState({passwordErrorArray:[]})

 }
}
    validationAndApiParameter() {
      var arr=[];
      const { method, devicetoken, devicetype, user_name,email, password, mobile_number,mailing_full_name,mailing_street_address,mailing_city,mailing_state,isVisible } = this.state
      var error=0;

      if ((user_name.length <= 0)) {
          this.setState({nameErrorMessage:'Please enter User name!'})
          error=1;
      }
       if ((email.indexOf(' ') >= 0 || email.length <= 0)) {
        this.setState({emailErrorMessage:'Please enter Email address!'})
        error=1;
      }else if (!commonFunctions.validateEmail(email)) {
        this.setState({emailErrorMessage:'Please enter valid Email address!'})
        error=1;
      }

       if ((password.indexOf(' ') >= 0 || password.length <= 0)) {
            this.setState({passwordErrorMessage:'Please enter password!'})
            error=1;
       }
      else if (!commonFunctions.validatePassword(password)){
        error=1;
        this.setState({passwordErrorMessage:'Password should contain:\n'})
        if(!commonFunctions.validatePasswordupper(password)){
          console.log('1');
          arr.push('* must contain atleast 1 upper case character\n');
        }
        else{
          arr=commonFunctions.validateRemovearr(arr,'* must contain atleast 1 upper case character\n');
        }

        if(!commonFunctions.validatePasswordlower(password)){
          console.log('2');
          arr.push('* must contain atleast 1 lower case character\n');
        }
        else{
          arr=commonFunctions.validateRemovearr(arr,'* must contain atleast 1 lower case character\n');
        }

        if(!commonFunctions.validatePasswordspecial(password)){
          console.log('3');
          arr.push('* must contain atleast 1 special character\n');
        }
        else{
          arr=commonFunctions.validateRemovearr(arr,'* must contain atleast 1 special character\n');
        }

        if(!commonFunctions.validatePassworddigit(password)){
          console.log('4');
          arr.push('* must contain atleast 1 numeric value\n');
        }
        else{
          arr=commonFunctions.validateRemovearr(arr,'* must contain atleast 1 numeric value\n');
        }

        if(!commonFunctions.validatePasswordLength(password)){
          console.log('5');
          arr.push('* length cannot be less than 10 characters\n');
        }
        else{
          arr=commonFunctions.validateRemovearr(arr,'* length cannot be less than 10 characters\n');
        }
        this.setState({passwordErrorArray:arr})
      }

      if(error==0) {
        Keyboard.dismiss()
        var data = {
            deviceToken: devicetoken,
            deviceType: devicetype,
            username: user_name,
            email: email,
            password: password,
        };
        console.log(data);
        _that.setState({
            isVisible: true
        });
        this.postToApiCalling('POST', 'register', Constant.URL_register, data);
      }
    }

postToApiCalling(method, apiKey, apiUrl, data) {

   new Promise(function(resolve, reject) {
        if (method == 'POST') {
            resolve(WebServices.callWebService(apiUrl, data));
        } else {
            resolve(WebServices.callWebService_GET(apiUrl, data));
        }
    }).then((jsonRes) => {
      _that.setState({ isVisible: false })
        if ((!jsonRes) || (jsonRes.code == 0)) {
//console.log(jsonRes)
        //_that.setState({ isVisible: false })
        setTimeout(()=>{
            Alert.alert(jsonRes.message);
        },200);

        } else {
            if (jsonRes.code == 1) {
              AsyncStorage.setItem("loggedIn", JSON.stringify(true)).done();
              AsyncStorage.setItem('screenName', JSON.stringify('signupPolicies'));
              AsyncStorage.setItem('UserData', JSON.stringify(jsonRes.data));
              _that.props.navigation.navigate('signupPolicies');
            }
        }
    }).catch((error) => {
        console.log("ERROR" + error);
        _that.setState({ isVisible: false })

        setTimeout(()=>{
            Alert.alert("Server issue");
        },200);
    });
}


  render() {
    const headerProp = {
      title: 'Signup',
      screens: 'SignupScreen',
    };
    return (

      <View style={styles.container}>
        <Header info={headerProp} navigation={_that.props.navigation} />
        <View style={[styles.content,this.state.Modal_Visibility ? {backgroundColor: 'rgba(0,0,0,0.4)',zIndex:5} : '#ffffff']}>
        <KeyboardAwareScrollView
            innerRef={() => {return [this.refs.user_name,this.refs.email,this.refs.password]}} >

            <View style={{padding:AppSizes.ResponsiveSize.Padding(3)}} />
            <FloatingLabelBox style={styles.inputbox} labelIcon={group_code_icon}>
              <FloatingLabel
                  labelStyle={styles.labelInput}
                  inputStyle={styles.input}
                  style={[styles.formInput]}
                  value={this.state.user_name}
                  ref="user_name"
                  placeholder='john smith'
                  returnKeyType={ "next"}
                  keyboardType={ 'default'}
                  onChangeText={(user_name)=>{this.setState({user_name});
                this.setState({nameErrorMessage:''})} }
                >User Name</FloatingLabel>
            </FloatingLabelBox>
              <View style={styles.passwordErrorConatainer}>
            <Text allowFontScaling={false} style={styles.errorText}>{this.state.nameErrorMessage}</Text>
            </View>
        <FloatingLabelBox style={styles.inputbox} labelIcon={email_icon}>
          <FloatingLabel
              labelStyle={styles.labelInput}
              inputStyle={styles.input}
              style={styles.formInput}
              value={this.state.email}
              ref="email"
              autoCapitalize = 'none'
              placeholder='john@email.com'
              returnKeyType={ "next"}
              keyboardType={ 'default'}
              onChangeText={(email)=> {this.setState({email});
            this.setState({emailErrorMessage:''})}}
              keyboardType={ 'email-address'}
            >Email Address</FloatingLabel>
        </FloatingLabelBox>
          <View style={styles.passwordErrorConatainer}>
        <Text allowFontScaling={false} style={styles.errorText}>{this.state.emailErrorMessage}</Text>
        </View>

        <FloatingLabelBox style={styles.inputbox} labelIcon={lock_icon}>
          <FloatingLabel
              labelStyle={styles.labelInput}
              inputStyle={styles.input}
              style={styles.formInput}
              value={this.state.password}
              placeholder='Enter Password'
              ref="password"
              autoCapitalize = 'none'
              secureTextEntry={true}
              returnKeyType = { "done" }
              onChangeText={(password)=> {this.setState({password});
              this.passwordValidation()
             }}
            >Password</FloatingLabel>
        </FloatingLabelBox>
        <View style={styles.passwordErrorConatainer}>
          <Text allowFontScaling={false} style={styles.errorText}>{this.state.passwordErrorMessage}</Text>
          {this.state.passwordErrorArray.map(text=>
            <Text allowFontScaling={false} style={styles.errorText}>{text}</Text>)
          }
        </View>

        </KeyboardAwareScrollView>

        <TouchableOpacity activeOpacity={.6} onPress={this.accountSave}>
          <CommonButton label='Save'/>
          </TouchableOpacity>
      </View>
        <Spinner visible={this.state.isVisible}  />

    </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
  },

  content: {
      flex: 1,
      backgroundColor: '#ffffff',
      height:'70%',
  },

  title:{
    textAlign:'center',
    fontSize:AppSizes.ResponsiveSize.Sizes(14),
    paddingTop:'3%'
  },

  ToggalContainer: {
    backgroundColor: '#fff',
    borderBottomWidth: 2,
    borderBottomColor:'#000'
  },

  ToggleInner: {
  height: 100,
  alignItems: 'center',
  justifyContent: 'center',
  backgroundColor: '#ebebeb'
},

labelInput: {
  color: AppColors.contentColor,
  //fontWeight:'300',
  fontSize:AppSizes.ResponsiveSize.Sizes(14),
},
input: {
  borderWidth: 0,
  color: AppColors.contentColor,
  fontWeight:'300',
  fontSize:AppSizes.ResponsiveSize.Sizes(15),
},
inputbox:{
  marginBottom:AppSizes.ResponsiveSize.Padding(3),
  backgroundColor:'red'
},
formInput: {
  borderWidth: 0,
  borderColor: '#333',

},
titleContainer1: {
  width:'92%',
  flex:1,
  flexDirection: 'column',
  justifyContent: 'center',
  alignItems: 'center',
  borderBottomWidth:1,
  borderBottomColor:'#000'
},
passwordErrorConatainer:{
  paddingLeft:'17%',
  paddingTop:'3%'
},
errorText:{
  color:'red'
}
});
