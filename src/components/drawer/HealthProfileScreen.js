import React, { Component } from 'react';
import {AsyncStorage,StyleSheet,Text,View,Image,TouchableOpacity,Platform,ScrollView,TextInput,Alert,Keyboard} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions } from 'react-navigation'
import LinearGradient from 'react-native-linear-gradient';
import Header from '../common/signupHeader'
import * as commonFunctions from '../../utils/CommonFunctions'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button'
import CommonButton from '../common/CommonButton'
import FlipToggle from 'react-native-flip-toggle-button';
import { jsonData } from '../data/Question';
import FloatingLabelBox from '../common/FloatingLabelBox'
import FloatingLabel from 'react-native-floating-labels';
import { pickerData } from '../data/pickerData';
import Picker from 'react-native-picker';
import Orientation from 'react-native-orientation'
import Spinner from 'react-native-loading-spinner-overlay';

var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;

let _that
let options_arr=[]
let python_result=[]

/* Images */
var arrow_icon=require('../../themes/Images/arrow_icon_196.png');
var age_icon = require('../../themes/Images/calender.png');
var weight_icon = require('../../themes/Images/weight.png');
var height_icon = require('../../themes/Images/height.png');
var edit_icon = require('../../themes/Images/edit-pin.png');
var edit_icon1 = require('../../themes/Images/edit.png');

var help_audio=require('../../themes/sound/HealthProfileHelp.mp3')

var age_data = [];

//heart_data
for( var j = 18; j <=100; j++){
  age_data.push(j);
}

export default class HealthProfileScreen extends Component {
  constructor (props) {
    super(props)
    this.state = {
      oxygen_therapy:false,
      daily_activities:false,
      smoker:false,
      live_alone:false,
      isVisible: false,
      conditions :[],
      risk_factor :[],
      Modal_Visibility: false,
      tab_conditions:true,
      tab_vba:false,
      height:'',
      heightlabel:'',
      height_data:pickerData.height_data,
      age_data: age_data,
      birth_date:'',
      uid: '',
      weight:'',
    },
    _that = this;
  }

  componentDidMount() {
    Orientation.lockToPortrait();
    AsyncStorage.getItem('loggedIn').then((value) => {
        var loggedIn = JSON.parse(value);
        if (loggedIn) {
            AsyncStorage.getItem('UserData').then((UserData) => {
                const data = JSON.parse(UserData)
                var height=parseInt(data.height);
                var birth_date=parseInt(data.birth_date);
                var height_feet=commonFunctions.toFeet(height)

                var uid=data.id;
                _that.setState({
                  uid:  uid,
                });
                _that.validationAndApiParameter('healthQusShow');
            })
        }
    })
  }

  vbaUpdate=()=>{
    _that.validationAndApiParameter('healthQusEdit');
  }

  validationAndApiParameter(apiname) {

      if(apiname == 'healthQusShow'){
        var data = {
            uid: this.state.uid
        };
        console.log(data);
          _that.setState({isVisible: true});
          this.postToApiCalling('POST', apiname, Constant.URL_showVarData,data);
      }
      else if(apiname == 'healthQusEdit'){
        var error=0;
        const { oxygen_therapy, daily_activities, smoker, live_alone,height,weight,birth_date,isVisible } = this.state
        if(height.length <= 0){
          Alert.alert("Error","Height is required");
          error=1;
        }
        if(weight.length <= 0){
          Alert.alert("Error","Weight is required");
          error=1;
        }
        if(birth_date.length <= 0){
          Alert.alert("Error","Age is required");
          error=1;
        }

        if(error==0){
          if(oxygen_therapy==false)
          {
            options_arr.push({var_name: 'ltou',value: 0});
          } else {
            //option1=1
            options_arr.push({var_name: 'ltou',value: 1});
          }
          if(daily_activities==false){
            options_arr.push({var_name: 'nhp',  value: 0});
          } else {
            options_arr.push({var_name: 'nhp',  value: 1});
          }
          if(smoker==false){
            options_arr.push({var_name: 'smoker',value: 0});
          } else {
            options_arr.push({var_name: 'smoker',value: 1});
          }

          if(live_alone==false){
            options_arr.push({var_name: 'la',value: 0});
          } else {
            options_arr.push({var_name: 'la',value: 1});
          }

          if(birth_date < 40){
            birth_date_var= 41
            options_arr.push({var_name: 'bl_age',value: birth_date_var});
          }
          else{
            birth_date_var= birth_date
            options_arr.push({var_name: 'bl_age',value: birth_date_var});
          }
          options_arr.push({var_name: 'ac_age',value: birth_date.toString()});
          options_arr.push({var_name: 'outp_height',value: height.toString()});
          options_arr.push({var_name: 'weight',value: weight.toString()});
          var vari=JSON.stringify(options_arr)
          var data = {
            uid: this.state.uid,
            variable : vari,
          };
          console.log(data)
          _that.setState({isVisible: true});
          this.postToApiCalling('POST', apiname, Constant.URL_saveVariable,data);
        }
      }
    }

  postToApiCalling(method, apiKey, apiUrl, data) {
    //console.log(apiUrl)
      new Promise(function(resolve, reject) {
          if (method == 'POST') {
              resolve(WebServices.callWebService(apiUrl, data));
          } else {
              resolve(WebServices.callWebService_GET(apiUrl, data));
          }
      }).then((jsonRes) => {
          console.log(jsonRes)
            _that.setState({ isVisible: false })
          if ((!jsonRes) || (jsonRes.code == 0)) {
            setTimeout(() => {
                  Alert.alert(jsonRes.message);
              }, 200);
          } else {
              if (jsonRes.code == 1) {
                  _that.apiSuccessfullResponse(apiKey, jsonRes)
              }
          }
      }).catch((error) => {
          console.log("ERROR" + error);
          _that.setState({ isVisible: false })
          setTimeout(() => {
              Alert.alert("Server Error");
          }, 200);
      })
  }

  apiSuccessfullResponse(apiKey, jsonRes) {
    if(apiKey == 'healthQusShow'){
      console.log(jsonRes);
        const jdata = jsonRes.profile;
        console.log(jdata);
        if(jdata.ltou==0){ option1=false } else { option1=true }
        if(jdata.nhp==0){ option2=false } else { option2=true }
        if(jdata.smokers==0){ option3=false } else { option3=true }
        if(jdata.la==0){ option4=false } else { option4=true }

        _that.setState({
          ltou : option1,
          nhp : option2,
          smoker : option3,
          la : option4,
          conditions:jsonRes.condition,
          weight:jdata.weight,
          birth_date:jdata.ac_age,
          height:jdata.outp_height
        });
    }
    else if(apiKey == 'healthQusEdit'){
      console.log('hi');
    }
  }

  updateAlert = (visible) => {
        this.setState({Modal_Visibility: !this.state.Modal_Visibility});
  }

  onSelect(index, value){
    this.setState({
      text: `Selected index: ${index} , value: ${value}`
    })
  }

  chartbtn=()=>{
    _that.props.navigation.navigate('ChartScreen', {oxy_sat: this.state.oxygen_sat,heart_rate: this.state.heart_rate,temperature: this.state.temperature });
  }

  conditionSave=()=>{
   _that.props.navigation.navigate('MedicalQusScreen');
  }

  conditionFunc(qus,arr,qus_arr){
    if(qus=='ast'||qus=='cad'||qus=='chf'||qus=='hbp'||qus=='ane'||qus=='ckd'||qus=='dbt'||qus=='arf'||qus=='pul'){
       _that.props.navigation.navigate('MedicalHealthQueScreen', {ques: 0,ques_title:arr,qus_label:qus_arr})
    }
    else if(qus=='gold_1'||qus=='gold_2'||qus=='gold_3'||qus=='gold_4'||qus=='gold_nan'){
       _that.props.navigation.navigate('MedicalHealthQueScreen', {ques: 1,ques_name:qus,qus_label:''})
    }
    else if(qus=='base_dyspnea_1'||qus=='base_dyspnea_2'||qus=='base_dyspnea_3'||qus=='base_dyspnea_4'||qus=='base_dyspnea_5'){
       _that.props.navigation.navigate('MedicalHealthQueScreen', {ques: 2,ques_name:qus,qus_label:''})
    }
  }
  activeTab(title){
    return (
      <LinearGradient colors={['#36c4d8', '#2aa5b4', '#198391', '#15767d']} style={styles.tab_linear}>
      <Text allowFontScaling={false} style={[styles.textbtn,styles.tab_activeText]}>{title}</Text>
      </LinearGradient>
    );
  }

  inactiveTab(title){
    return (
      <LinearGradient colors={['#ebebeb', '#ebebeb', '#ebebeb', '#ebebeb']} style={styles.tab_linear}>
      <Text allowFontScaling={false} style={styles.textbtn}>{title}</Text>
      </LinearGradient>
    );
  }

  age() {
    Picker.init({
        pickerData: this.state.age_data,
        onPickerConfirm: pickedValue => {
            this.setState({birth_date:pickedValue})
        },
        onPickerCancel: pickedValue => {
            console.log('area', pickedValue);
        },
        onPickerSelect: pickedValue => {
            console.log('area', pickedValue);
        }
    });
    Picker.show();
  }

  height() {
    Picker.init({
        pickerData: this.state.height_data,
        onPickerConfirm: pickedValue => {
            this.setState({height:pickedValue})
        },
        onPickerCancel: pickedValue => {
            console.log('area', pickedValue);
        },
        onPickerSelect: pickedValue => {
            console.log('area', pickedValue);
        }
    });
    Picker.show();
  }


  render() {
    const headerProp = {
      title: 'Health Profile',
      screens: 'HealthProfileScreen',
      qus_content:'Maintain the medical history related to your condition. \n\n Click “Update” to change your profile conditions and flip the switches to update the risk factors.\n\n Access your charts through “Access charts” link or by clicking on the icons on the measurements.',
      qus_audio:help_audio
    };

    let _this = this
    const conditions = this.state.conditions
    var answer='';
    var arr=[];
    var qus_arr=[];

    const conditions_option = Object.keys(conditions).map( function(k) {
      if(conditions[k]== 'ast'){answer='Asthma'; arr.push('Asthma');qus_arr.push('ast')}
      else if(conditions[k]== 'cad'){answer='Coronary Artery Disease'; arr.push('Coronary Artery Disease');qus_arr.push('cad')}
      else if(conditions[k]== 'chf'){answer='Congestive Heart Failure'; arr.push('Congestive Heart Failure');qus_arr.push('chf')}
      else if(conditions[k]== 'hbp'){answer='High Blood Pressure'; arr.push('High Blood Pressure');qus_arr.push('hbp')}
      else if(conditions[k]== 'anm'){answer='Anemia'; arr.push('Anemia');qus_arr.push('anm')}
      else if(conditions[k]== 'ckd'){answer='Chronic Kidney disease'; arr.push('Chronic Kidney disease');qus_arr.push('ckd')}
      else if(conditions[k]== 'dbt'){answer='Diabetes'; arr.push('Diabetes');qus_arr.push('dbt')}
      else if(conditions[k]== 'arf'){answer='Acid Reflux'; arr.push('Acid Reflux');qus_arr.push('arf')}
      else if(conditions[k]== 'pul'){answer='Pulmonary Hypertension'; arr.push('Pulmonary Hypertension');qus_arr.push('pul')}

      else if(conditions[k]== 'gold_1'){answer='Stage 1- Mild(FEV1>=80%normal)'; }
      else if(conditions[k]== 'gold_2'){answer='Stage 2-Moderate(FEV1=50-79% normal)'; }
      else if(conditions[k]== 'gold_3'){answer='Stage 3-Severe(FEV1=30-49% normal)'; }
      else if(conditions[k]== 'gold_4'){answer='Stage 4-Very Severe(FEV1<30% normal)'; }
      else if(conditions[k]== 'gold_nan'){answer='I don’t know'; }

      else if(conditions[k]== 'base_dyspnea_1'){answer='Breathless only with demanding exercise';  }
      else if(conditions[k]== 'base_dyspnea_2'){answer='Breathless when hurrying on flat ground'; }
      else if(conditions[k]== 'base_dyspnea_3'){answer='Walk slower than people of my same age'; }
      else if(conditions[k]== 'base_dyspnea_4'){answer='Need to stop to breathe after walking about 100 yards'; }
      else if(conditions[k]== 'base_dyspnea_5'){answer='Too breathless to leave the house and dress myself'; }

      return (
        <TouchableOpacity style={styles.lines} onPress={_this.conditionFunc.bind(_this,conditions[k],arr,qus_arr)}>
          {/*<Image source={bullet} style={styles.bullet} />*/}
            <Text allowFontScaling={false} style={styles.conditiontext}> • {answer}</Text>
          </TouchableOpacity>
      )
    });

    return (
      <View style={styles.container}>
        <Header info={headerProp} navigation={_that.props.navigation} updateAlert={this.updateAlert}/>
        <View style={[styles.content,this.state.Modal_Visibility ? {backgroundColor: 'rgba(0,0,0,0.4)',zIndex:5} : '#ffffff']}>
          <View style={{flex:1,paddingLeft:AppSizes.ResponsiveSize.Padding(2),paddingRight:AppSizes.ResponsiveSize.Padding(2)}}>
            <View style={{flex:(Platform.OS === 'ios') ? 1.2 :1.2, marginTop:(Platform.OS === 'android') ? AppSizes.ResponsiveSize.Padding(2) : AppSizes.ResponsiveSize.Padding(0),}}>
             <View style={styles.boxDesignthree}>
              <TouchableOpacity activeOpacity={.6} onPress={this.chartbtn} style={[styles.shadow,styles.boxcontainer1]}>
                <Text style={styles.boxnumber} allowFontScaling={false}>{this.props.navigation.state.params.oxy_sat}</Text>
                <Text style={styles.boxtitlesmall} allowFontScaling={false}>O<Text allowFontScaling={false} style={styles.thintext}>2</Text> Saturation</Text>
              </TouchableOpacity>
              <TouchableOpacity activeOpacity={.6} onPress={this.chartbtn} style={[styles.shadow,styles.boxcontainer1]}>
                <Text style={styles.boxnumber} allowFontScaling={false}>{this.props.navigation.state.params.heart_rate}</Text>
                <Text style={styles.boxtitlesmall} allowFontScaling={false}>Heart Rate</Text>
              </TouchableOpacity>
                 <TouchableOpacity activeOpacity={.6} onPress={this.chartbtn} style={[styles.shadow,styles.boxcontainer1]}>
                     <Text style={styles.boxnumber} allowFontScaling={false}>{this.props.navigation.state.params.temperature}</Text>
                      <Text style={styles.boxtitlesmall} allowFontScaling={false}>Temperature</Text>
                 </TouchableOpacity>
             </View>
            </View>
            <View style={{flex:3}}>
            <ScrollView>

            <View styles={{alignItems:'center',width:'100%'}}>
             <Text style={styles.boxtitle22} allowFontScaling={false}>Profile</Text>

             <View style={styles.border} />
             <View style={styles.formContainer}>
             { this.state.birth_date != "" ?
               <View style={{width:'100%'}}>
                  <View style={{marginTop: 10,flexDirection:'row', marginLeft: 10,paddingTop:20,paddingBottom:10}}>
                    <View style={styles.icon}>
                      <Image style={styles.cicon} source={age_icon}/>
                    </View>
                    <View style={styles.texttbox}>
                      <Text style={{color:'#000',fontSize:AppSizes.ResponsiveSize.Sizes(15)}}>Age</Text>
                      {this.state.birth_date.length>0?<Text style={{color:'#000',fontSize:AppSizes.ResponsiveSize.Sizes(15)}}>{this.state.birth_date}</Text>:<Text style={{color:'#bdc3c7',fontSize:AppSizes.ResponsiveSize.Sizes(15)}}>Select your age</Text>}
                    </View>
                    <TouchableOpacity onPress={()=>this.age()} style={styles.icon1}>
                        <Image style={styles.cicon} source={edit_icon}/>
                    </TouchableOpacity>
                  </View>
                  <View style={{width:'100%',height:1,backgroundColor:'#000'}}/>
                </View>
              :
                null
              }
             </View>

             <View style={{padding:AppSizes.ResponsiveSize.Padding(2)}} />
             <View style={styles.formContainer}>
             { this.state.height != "" ?
               <View style={{width:'100%'}}>
                  <View style={{flexDirection:'row', marginLeft: 10,paddingTop:20,paddingBottom:10}}>
                    <View style={styles.icon}>
                        <Image style={styles.cicon} source={height_icon}/>
                    </View>
                    <View style={styles.texttbox}>
                      <Text style={{color:'#000',fontSize:AppSizes.ResponsiveSize.Sizes(15)}}>Height</Text>
                      {this.state.height.length>0?<Text style={{color:'#000',fontSize:AppSizes.ResponsiveSize.Sizes(15)}}>{this.state.height}</Text>:<Text style={{color:'#bdc3c7',fontSize:AppSizes.ResponsiveSize.Sizes(15)}}>Enter height</Text>}
                    </View>
                    <TouchableOpacity onPress={()=>this.height()}  style={styles.icon1}>
                        <Image style={styles.cicon} source={edit_icon}/>
                    </TouchableOpacity>
                  </View>
                  <View style={{width:'100%',height:1,backgroundColor:'#000'}}/>
                </View>
              :
                null
              }
             </View>

             <View style={{padding:AppSizes.ResponsiveSize.Padding(2)}} />
             <FloatingLabelBox labelIcon={weight_icon}>
               <FloatingLabel
                   labelStyle={styles.labelInput}
                   inputStyle={styles.input}
                   style={styles.formInput}
                   value={this.state.weight}
                   placeholder='Enter weight'
                   ref="weight"
                   autoCapitalize = 'none'
                   keyboardType={ 'numeric'}
                   returnKeyType = { "next" }
                   onChangeText={weight=> this.setState({weight})}
                 >Weight</FloatingLabel>
             </FloatingLabelBox>

             <View style={{padding:AppSizes.ResponsiveSize.Padding(2)}} />

            </View>

            <View styles={{alignItems:'center',width:'100%'}}>
             <Text style={styles.boxtitle22} allowFontScaling={false}>Conditions</Text>

            <View style={styles.border} />
              {conditions_option}
            </View>

            <View style={{flex:3,marginTop:AppSizes.ResponsiveSize.Padding(4),}}>
            <Text style={styles.boxtitle22} allowFontScaling={false}>Risk Factor</Text>
            <View style={styles.border} />
             <View style={[styles.boxDesignvba2,styles.shadow]}>
               <View style={{width:'75%'}}>
                 <Text style={styles.boxtitle12} allowFontScaling={false}>Are you on oxygen therapy?</Text>
               </View>
                 <View style={{width:'25%'}}>
                 <FlipToggle
                   value={this.state.ltou}
                   buttonOnColor={"#4eb4c4"}
                   buttonOffColor={"#9e9e9e"}
                   sliderOnColor={"#ffffff"}
                   sliderOffColor={"#ffffff"}
                   buttonWidth={AppSizes.screen.width/5}
                   buttonHeight={AppSizes.screen.width/15}
                   buttonRadius={50}
                   onLabel={'Yes'}
                   offLabel={'No'}
                   onToggle={(value) => {
                     this.setState({ ltou: value });
                   }}
                   changeToggleStateOnLongPress={false}
                   onToggleLongPress={() => {
                     console.log('Long Press');
                   }}
                 />
               </View>
             </View>


             <View style={[styles.boxDesignvba2,styles.shadow]}>
               <View style={{width:'75%'}}>
                 <Text style={styles.boxtitle12} allowFontScaling={false}>Do you need help with routine daily activities?</Text>
               </View>
                 <View style={{width:'25%'}}>
                 <FlipToggle
                   value={this.state.nhp}
                   buttonOnColor={"#4eb4c4"}
                   buttonOffColor={"#9e9e9e"}
                   sliderOnColor={"#ffffff"}
                   sliderOffColor={"#ffffff"}
                   buttonWidth={AppSizes.screen.width/5}
                   buttonHeight={AppSizes.screen.width/15}
                   buttonRadius={50}
                   onLabel={'Yes'}
                   offLabel={'No'}
                   onToggle={(value) => {
                     this.setState({ nhp: value });
                   }}
                   changeToggleStateOnLongPress={false}
                   onToggleLongPress={() => {
                     console.log('Long Press');
                   }}
                 />
               </View>
             </View>

             <View style={[styles.boxDesignvba2,styles.shadow]}>
               <View style={{width:'75%'}}>
                 <Text style={styles.boxtitle12} allowFontScaling={false}>Are you a smoker?</Text>
               </View>
                 <View style={{width:'25%'}}>
                 <FlipToggle
                   value={this.state.smoker}
                   buttonOnColor={"#4eb4c4"}
                   buttonOffColor={"#9e9e9e"}
                   sliderOnColor={"#ffffff"}
                   sliderOffColor={"#ffffff"}
                   buttonWidth={AppSizes.screen.width/5}
                   buttonHeight={AppSizes.screen.width/15}
                   buttonRadius={50}
                   onLabel={'Yes'}
                   offLabel={'No'}
                   onToggle={(value) => {
                     this.setState({ smoker: value });
                   }}
                   changeToggleStateOnLongPress={false}
                   onToggleLongPress={() => {
                     console.log('Long Press');
                   }}
                 />
               </View>
             </View>

             <View style={[styles.boxDesignvba2,styles.shadow]}>
               <View style={{width:'75%'}}>
                 <Text style={styles.boxtitle12} allowFontScaling={false}>Do you live alone?</Text>
               </View>
                 <View style={{width:'25%'}}>
                 <FlipToggle
                   value={this.state.la}
                   buttonOnColor={"#4eb4c4"}
                   buttonOffColor={"#9e9e9e"}
                   sliderOnColor={"#ffffff"}
                   sliderOffColor={"#ffffff"}
                   buttonWidth={AppSizes.screen.width/5}
                   buttonHeight={AppSizes.screen.width/15}
                   buttonRadius={50}
                   onLabel={'Yes'}
                   offLabel={'No'}
                   onToggle={(value) => {
                     this.setState({ la: value });
                   }}
                   changeToggleStateOnLongPress={false}
                   onToggleLongPress={() => {
                     console.log('Long Press');
                   }}
                 />
               </View>
            </View>
            </View>
            </ScrollView>
            </View>
            <View style={{flex:1,justifyContent:'center'}}>
            <TouchableOpacity activeOpacity={.6} onPress={this.chartbtn}>
              <Text style={styles.smalltext} allowFontScaling={false}>Access Measurement History</Text>
            </TouchableOpacity>
            <TouchableOpacity activeOpacity={.6} onPress={this.vbaUpdate} >
              <CommonButton label='Update'/>
              </TouchableOpacity>
            </View>
          </View>
          <Spinner visible={this.state.isVisible}  />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
  },

  content: {
      flex: 1,
      backgroundColor: '#ffffff',
  },
tabContainer:{
  flexDirection:'row',
  alignItems:'center',
  paddingBottom:AppSizes.ResponsiveSize.Padding(1),
  height:'11%',
},

boxDesign1:{
  position: 'relative',
  alignItems:'center',
  marginBottom:AppSizes.ResponsiveSize.Padding(4),
  //backgroundColor:'red'
},
heading:{
  backgroundColor:'#8e8e8e',
  paddingLeft:AppSizes.ResponsiveSize.Padding(2),
  paddingRight:AppSizes.ResponsiveSize.Padding(2),
  width:'100%',
  paddingTop:AppSizes.ResponsiveSize.Padding(.50),
  paddingBottom:AppSizes.ResponsiveSize.Padding(.50),
  textAlign:'center',
  alignItems:'center'
},
boxDesign2:{
  padding:AppSizes.ResponsiveSize.Padding(2),
  paddingTop:AppSizes.ResponsiveSize.Padding(5),
  paddingBottom:AppSizes.ResponsiveSize.Padding(0),
  marginBottom:AppSizes.ResponsiveSize.Padding(4),
  position: 'relative',
  flex:1
},
boxDesignvba2:{
  /*paddingTop:AppSizes.ResponsiveSize.Padding(2),
  paddingBottom:AppSizes.ResponsiveSize.Padding(2),*/
  paddingLeft:AppSizes.ResponsiveSize.Padding(4),
  paddingRight:AppSizes.ResponsiveSize.Padding(4),
  marginBottom:AppSizes.ResponsiveSize.Padding(4),
  flexDirection:'row',
  justifyContent:'center',
  alignItems:'center',
  height:AppSizes.screen.height/10
},
shadow:{
 borderWidth:1,
 borderRadius: 2,
 borderColor: '#fff',
 backgroundColor:'#fff',
 borderColor: '#ddd',
 borderBottomWidth: 1,
 shadowColor: '#999',
 shadowOffset: { width: 0, height: 2 },
 shadowOpacity: 0.8,
 shadowRadius: 2,
 elevation: 0,
 //backgroundColor:'red',


},
boxcontainer100:{
  width:'31%',
  height:'90%',
  alignItems:'center',
  padding:AppSizes.ResponsiveSize.Padding(0),
  justifyContent:'center',

},
boxcontainer1:{
  width:'32%',
  height:'100%',
  justifyContent:'center',
  alignItems:'center',

},
boxcontainer22:{
  width:'100%',
  justifyContent:'center',


},
border:{
  width:30,
  backgroundColor:'#999',
  borderBottomWidth:2,
  alignItems:'center'
},
lines:{
color:AppColors.smalltext,
  borderBottomWidth: 1,
  paddingLeft:AppSizes.ResponsiveSize.Padding(3),
  paddingTop:AppSizes.ResponsiveSize.Padding(1),
  paddingBottom:AppSizes.ResponsiveSize.Padding(1),

},
boxicon:{
  flexGrow:1,
  height:null,
  width:null,
  alignItems: 'center',
  justifyContent:'center',
  resizeMode:'contain',
},
boxtitle1:{
  fontSize: AppSizes.ResponsiveSize.Sizes(16),
  marginBottom:AppSizes.ResponsiveSize.Sizes(4),
  fontWeight:'bold',
  color:'#fff',
  shadowOpacity: 0,
  //backgroundColor:'red'
},
boxtitlesmall:{
  fontSize: AppSizes.ResponsiveSize.Sizes(11),
  fontWeight:'bold',
  color:AppColors.primary,
  textAlign:'center',
  shadowOpacity: 0,

},
boxtitle12:{
  fontSize: AppSizes.ResponsiveSize.Sizes(13),
  fontWeight:'600',
  color:AppColors.smalltext,
  shadowOpacity: 0,

},
boxtitle22:{
  fontSize: AppSizes.ResponsiveSize.Sizes(18),
  fontWeight:'600',
  color:AppColors.smalltext,
  shadowOpacity: 0,
  paddingLeft:AppSizes.ResponsiveSize.Padding(3),
  paddingTop:AppSizes.ResponsiveSize.Padding(1),
  paddingBottom:AppSizes.ResponsiveSize.Padding(1),
  textAlign:'center'
},
boxtitle2:{
  fontSize: AppSizes.ResponsiveSize.Sizes(12),
  fontWeight:'400',
  color:AppColors.smalltext,
  shadowOpacity: 0,
  paddingTop:AppSizes.ResponsiveSize.Padding(0),
  paddingBottom:AppSizes.ResponsiveSize.Padding(0),
  backgroundColor:'#fff',
  //borderBottomWidth:2,
  //borderBottomColor:'#000',
},
bordercon:{
  borderBottomWidth:2,
  borderBottomColor:'#000',
  width:'100%',
},
border:{
  borderBottomWidth:2,
  borderBottomColor:'#a9a9a9',
  width:20,
  marginLeft:'auto',
  marginRight:'auto',
  marginBottom:AppSizes.ResponsiveSize.Padding(2)
},

sideicon:{
  position: 'absolute',
  bottom: '-16%',
  right:'-6%',
  width:AppSizes.screen.width/12,
  height:AppSizes.screen.width/12,
  //width:AppSizes.ResponsiveSize.width/2,
  //height:AppSizes.ResponsiveSize.height/2,

},
boxicon:{
  flexGrow:1,
  height:null,
  width:null,
  alignItems: 'center',
  justifyContent:'center',
  resizeMode:'contain',
},
maintwoflex:{
  flex:1,
  paddingTop:AppSizes.ResponsiveSize.Padding(15),
  paddingBottom:AppSizes.ResponsiveSize.Padding(5)
},
mainoneflex:{
  flex:.4,
  paddingTop:AppSizes.ResponsiveSize.Padding(2.5)
},
mainfourflex:{
  flex:1.7,
  paddingTop:AppSizes.ResponsiveSize.Padding(14),
  paddingBottom:AppSizes.ResponsiveSize.Padding(1)
},
inputfield:{
  width:AppSizes.ResponsiveSize.width,
  borderBottomWidth:0.60,
  borderBottomColor:'#999',
  paddingBottom:AppSizes.ResponsiveSize.Padding(.20)
},
inputtext:{
  marginTop:AppSizes.ResponsiveSize.Padding(3),
  fontSize:AppSizes.ResponsiveSize.Sizes(12),
  fontWeight:'600',
  marginBottom:AppSizes.ResponsiveSize.Padding(0)
},
que:{
  backgroundColor:'red',
  flex:1
},
boxnumber:{
  fontSize:AppSizes.ResponsiveSize.Sizes(22),
  color:AppColors.contentColor,
  fontWeight:'bold',
  textAlign:'center',
  //backgroundColor:'red'
  //marginTop:AppSizes.ResponsiveSize.Padding(-1)
},
spaceBox:{
  marginBottom:'25%'
},
boxDesignthree:{
  //height:AppSizes.screen.height/3,
  flexDirection: 'row',
  //flex:1.5,
  height:'100%',
  flexWrap:'wrap',
  alignItems:'center',
  justifyContent: 'space-around',
  paddingBottom:AppSizes.ResponsiveSize.Padding(1),

  //paddingTop:AppSizes.ResponsiveSize.Padding(1),
  //marginBottom:AppSizes.ResponsiveSize.Padding(8),
  justifyContent:'space-between',
  //backgroundColor:'green'
},
/*formbox:{
    height:AppSizes.screen.height/4
},*/
textbox:{
    height:AppSizes.screen.height/3.5
},
mainbtn:{
  justifyContent:'flex-end',

},
smalltext:{
  textAlign:'center',
  fontSize: AppSizes.ResponsiveSize.Sizes(12),
  fontWeight:'bold',
  color:AppColors.primary,
},
/*extrapad:{
  paddingTop:AppSizes.ResponsiveSize.Padding(14)
}*/
bullet:{
  fontSize:10,
  marginRight:AppSizes.ResponsiveSize.Padding(1)
},
bullettext:{
  paddingLeft:AppSizes.ResponsiveSize.Padding(2)
},
thintext:{
  fontSize:AppSizes.ResponsiveSize.Sizes(6)
},
labelInput: {
  color: AppColors.contentColor,
  //fontWeight:'300',
  //fontSize:AppSizes.ResponsiveSize.Sizes(13),
},
input: {
  borderWidth: 0,
  color: AppColors.contentColor,
  fontWeight:'300',
  fontSize:AppSizes.ResponsiveSize.Sizes(15),
},
formInput: {
  borderWidth: 0,
  borderColor: '#333',
},
formContainer:{
  flexDirection: 'row',
//  paddingLeft:AppSizes.ResponsiveSize.Padding(5),
//  paddingRight:AppSizes.ResponsiveSize.Padding(5),
},
imageContainer:{
  flex:1.3,
  alignItems: 'flex-end',
  //backgroundColor:'red',
},
imageContainer1:{
  flex:.1,
  alignItems: 'flex-end',
},
editButton:{
  height:20,
  width:20
},
conditiontext:{
  flex: 1,
  paddingTop:(Platform.OS === 'ios') ? 0 :AppSizes.ResponsiveSize.Padding(2),
  fontSize:Platform.OS === 'ios' ? AppSizes.ResponsiveSize.Sizes(14) : AppSizes.ResponsiveSize.Sizes(16),
  color:AppColors.smalltext,
},
icon:{
  width:'10%',
  marginRight:10,
  alignItems:'center',
  justifyContent:'center',
},
icon1:{
  width:'10%',
  alignItems:'center',
  justifyContent:'center',
},
texttbox:{
  width:'75%'
},

cicon:{
  width:20,
  height:20
}
});
