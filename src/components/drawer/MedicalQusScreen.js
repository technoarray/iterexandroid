import React, { Component } from 'react';
import {AsyncStorage,StyleSheet,Text,View,Image,TouchableOpacity,Platform,Alert,ScrollView,TextInput,Button,StatusBar} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions } from 'react-navigation'
import Spacer from '../common/Spacer'
import Header from '../common/signupHeader'
import CommonButton from '../common/CommonButton'
import Swiper from 'react-native-swiper';
import Animbutton from './animAnsbutton'
import AnimSinglebutton from './animSingleAnsOptions';
import RNPickerSelect from 'react-native-picker-select';
import { jsonData } from '../data/Question';
import { pickerData } from '../data/pickerData';
import LinearGradient from 'react-native-linear-gradient';
import Spinner from 'react-native-loading-spinner-overlay';
import Picker from 'react-native-q-picker';
import Orientation from 'react-native-orientation'
import * as commonFunctions from '../../utils/CommonFunctions'

var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;

/* Images */
//var thankyou_icon = require('../../themes/Images/thank-you.png');
var logo = require('../../themes/Images/logo.png');
var back_icon = require( '../../themes/Images/left-whitearrow.png')
var cigrate_icon = require('../../themes/Images/cigrate.png');


let arrnew = []
let ans_track = []
let qus_result =[]
var unknown_arr=[]
let options_arr=[]
let final_options_arr=[]
let python_result =[]
let python_array =[]
let vitals=[]
var current_ans=''
var smoker=0
var hosp=0

const MyStatusBar = ({backgroundColor, ...props}) => (
  <View style={[styles.statusBar, { backgroundColor }]}>
    <StatusBar translucent backgroundColor={backgroundColor} {...props} />
  </View>
);

var cigrate_data= [];

//cigrate_data
for( var j = 1; j <=60; j++){
  var numbers = {
      name: j+' ',
      id: j
  };
  cigrate_data.push(numbers);
}
let _that
export default class MedicalQusScreen extends Component {
  constructor (props) {
      super(props)
      this.qno = 0


      const jdata = jsonData.medicalqus.qus1
      arrnew = Object.keys(jdata).map( function(k) { return jdata[k] });
      this.state = {
        question : arrnew[this.qno].question,
        options : arrnew[this.qno].options,
        suboption : arrnew[this.qno].suboption,
        type : arrnew[this.qno].type,
        countCheck : [],
        qus_no : 0,
        uid : 0,
        gender :'',
        birthdate :'',
        height :'',
        weight:'',
        screenName:'',
        cigrates:'',
        cigrate_data:cigrate_data,
        python_result:[],
        python_array:[],
        isVisible: false,
        Modal_Visibility: false,
        status:false,
        scrollPosition:'',
        primary:'copd'
    },
    _that = this;
  }

  componentDidMount() {
    Orientation.lockToPortrait();
    AsyncStorage.getItem('screenName').then((screenName) => {
      var screenName = JSON.parse(screenName);
      _that.setState({screenName:screenName})
    //  console.log("screenName "+screenName)
    })

    AsyncStorage.getItem('loggedIn').then((value) => {
        var loggedIn = JSON.parse(value);
        if (loggedIn) {
            AsyncStorage.getItem('UserData').then((UserData) => {
                const data = JSON.parse(UserData)
                console.log(data);
                _that.setState({
                  uid:data.id,
                  gender : data.gender,
                  birthdate : data.birth_date,
                  height : data.height,
                  weight: data.weight,
                });

            })

        }
    })

  }

  componentDidUpdate(prevProps, prevState) {
    //console.log(prevState)
    if (prevState.question !== this.state.question) {
      setTimeout(() => {
        _that.scroll_to_top();
      }, 400)
    }
    //_that.scroll_to_top();
  }


  scroll_to_top=()=>{
    _that.scroll.scrollTo({x: 0, y: 0, animated: false})
  }

  scroll_to_end=()=>{
    _that.scroll.scrollToEnd({animated: true})
  }

  updateAlert = () => {
        this.setState({Modal_Visibility: !this.state.Modal_Visibility});
  }

  validationAndApiParameter(apiname) {
      if(apiname == 'web_profile'){
        options_arr.sort(function(a,b) {
            return a.order - b.order;
        });
        var json_options_arr = JSON.stringify(options_arr);


        var data = {
            uid: this.state.uid,
            type:this.state.primary,
            variable : json_options_arr,
        };
        console.log(data);
          _that.setState({isVisible: true});
          this.postToApiCalling('POST', apiname, Constant.URL_saveVariable,data);
      }
    }

  postToApiCalling(method, apiKey, apiUrl, data) {
    //console.log(apiUrl)
      new Promise(function(resolve, reject) {
          if (method == 'POST') {
              resolve(WebServices.callWebService(apiUrl, data));
          } else {
              resolve(WebServices.callWebService_POST(apiUrl, data));
          }
      }).then((jsonRes) => {
         console.log(jsonRes)
            _that.setState({ isVisible: false })
          if(apiKey=="pyhton_profile"){
            _that.apiSuccessfullResponse(apiKey, jsonRes)
          }
          else{
            if ((!jsonRes) || (jsonRes.code == 0)) {
              setTimeout(() => {
                    Alert.alert(jsonRes.message);
                }, 200);
            } else {
                if (jsonRes.code == 1) {
                    _that.apiSuccessfullResponse(apiKey, jsonRes)
                }
            }
          }
      }).catch((error) => {
          console.log("ERROR" + error);
          _that.setState({ isVisible: false })
          setTimeout(() => {
              Alert.alert("Server Error");
          }, 200);
      })
  }

  apiSuccessfullResponse(apiKey, jsonRes) {
    if(apiKey=="web_profile"){
      arrnew = []
      ans_track = []
      profile_data = []
      qus_result =[]
      unknown_arr=[]
      options_arr=[]
      final_options_arr=[]
      python_result =[]
      python_array =[]
      vitals=[]
      current_ans=''
      _that.props.navigation.navigate('ThankYouScreen', {message: 'Thank you for setting up your health profile'});
    }
  }

  prev= () =>{
    console.log(options_arr)
    if(this.qno > 0){

      if(this.qno==6){
        this.qno=this.qno-3
      }
      else{
        this.qno--
      }
      if(this.qno==0){
        options_arr.splice(0,9)
      }
      else if(this.qno==1){
        options_arr.splice(9,5)
      }
      else if (this.qno==2) {
        options_arr.splice(14,5)
      }
      else if (this.qno==3) {
        options_arr.splice(19,5)
      }
      console.log(options_arr)

      this.setState({ qus_no: this.qno })
      this.setState({ countCheck: [],question: arrnew[this.qno].question, options: arrnew[this.qno].options, type: arrnew[this.qno].type,suboption : arrnew[this.qno].suboption})
    }
  }
  next= () =>{

    // variables
    current_ans=0
    final_thank=0;
    const { countCheck} = this.state

//console.log(this.state.qus_no)
    /*if(this.qno==3 && this.state.countCheck.indexOf('')>= 0){
        Alert.alert("","Please select which of the following statements apply to you")
    }
    else*/

    if((this.qno==1 || this.qno==2 || this.qno==4 || this.qno==5 || this.qno==6) && this.state.countCheck == 0){
      Alert.alert("Invalid Value","Missing or blank value is not allowed")
    }
    else if(this.qno < arrnew.length-1){

      if(this.qno == 0 || this.qno == 1 || this.qno == 2 || this.qno == 3){
        if(this.qno == 3){type="risk_factor"}else{type="condition"}
        qus_result.push({
            type :type,
            options: this.qno,
            value: ans_track.toString(),
        });
      }

     console.log(options_arr)

      // answers array
      //console.log(this.qno)
      if(this.qno == 0){
          console.log('countCheck',countCheck)
          //asthma
          if(countCheck.indexOf('ast')!=-1){
            options_arr.push({var_name: 'ast',  value: 1})
          }
          else {
            options_arr.push({var_name: 'ast',  value: 0})
          }
          //coronary_artery_disease
          if(countCheck.indexOf('cad')!=-1){
            options_arr.push({var_name: 'cad',  value: 1})
          }
          else {
            options_arr.push({var_name: 'cad',  value: 0})
          }

          //congestive_heart_failure
          if(countCheck.indexOf('chf')!=-1){
            options_arr.push({var_name: 'chf',  value: 1})
          }
          else {
            options_arr.push({var_name: 'chf',  value: 0})
          }

          //high_blood_pressure
          if(countCheck.indexOf('hbp')!=-1){
            options_arr.push({var_name: 'hbp',value: 1});
          }
          else {
            options_arr.push({var_name: 'hbp',value: 0});
          }
          //anemia
          if(countCheck.indexOf('anm')!=-1){
            options_arr.push({var_name: 'anm',value: 1});
          }
          else {
            options_arr.push({var_name: 'anm',value: 0});
          }

          //chronic_kidney_disease
          if(countCheck.indexOf('ckd')!=-1){
            options_arr.push({var_name: 'ckd',value: 1});
          }
          else {
            options_arr.push({var_name: 'ckd',value: 0});
          }

          //diabetes
          if(countCheck.indexOf('dbt')!=-1){
            options_arr.push({var_name: 'dbt',value: 1});
          }
          else {
            options_arr.push({var_name: 'dbt',value: 0});
          }

          //acid_reflux
          if(countCheck.indexOf('arf')!=-1){
            options_arr.push({var_name: 'arf',value: 1});
          }
          else {
            options_arr.push({var_name: 'arf',value: 0});
          }

          //pulmonary_hypertension
          if(countCheck.indexOf('pul')!=-1){
            options_arr.push({var_name: 'pul',value: 1});
          }
          else {
            options_arr.push({var_name: 'pul',value: 0});
          }
      }
      else if(this.qno == 1){
        if(countCheck.indexOf('gold_nan')!=-1){
          options_arr.push({var_name: 'gold_nan',value: 1});
        }
        else {
          options_arr.push({var_name: 'gold_nan',value: 0});
        }

        if(countCheck.indexOf('gold_1')!=-1){
          options_arr.push({var_name: 'gold_1',value: 1});
        }
        else {
          options_arr.push({var_name: 'gold_1',value: 0});
        }

        if(countCheck.indexOf('gold_2')!=-1){
          options_arr.push({var_name: 'gold_2',value: 1});
        }
        else {
            options_arr.push({var_name: 'gold_2',value: 0});
        }

        if(countCheck.indexOf('gold_3')!=-1){
          options_arr.push({var_name: 'gold_3',value: 1});
        }
        else {
          options_arr.push({var_name: 'gold_3',value: 0});
        }

        if(countCheck.indexOf('gold_4')!=-1){
          options_arr.push({var_name: 'gold_4',value: 1});
        }
        else {
          options_arr.push({var_name: 'gold_4',value: 0});
        }

    }
      else if(this.qno == 2){
        //console.log(countCheck)
        if(countCheck.indexOf('base_dyspnea_1')!=-1){
          options_arr.push({var_name: 'base_dyspnea_1',value: 1});
        }
        else {
          options_arr.push({var_name: 'base_dyspnea_1',value: 0});
        }

        if(countCheck.indexOf('base_dyspnea_2')!=-1){
          options_arr.push({var_name: 'base_dyspnea_2',value: 1});
        }
        else {
          options_arr.push({var_name: 'base_dyspnea_2',value: 0});
        }

        if(countCheck.indexOf('base_dyspnea_3')!=-1){
          options_arr.push({var_name: 'base_dyspnea_3',value: 1});
        }
        else {
          options_arr.push({var_name: 'base_dyspnea_3',value: 0});
        }

        if(countCheck.indexOf('base_dyspnea_4')!=-1){
          options_arr.push({var_name: 'base_dyspnea_4',  value: 1});
        }
        else {
          options_arr.push({var_name: 'base_dyspnea_4',  value: 0});
        }

        if(countCheck.indexOf('base_dyspnea_5')!=-1){
          options_arr.push({var_name: 'base_dyspnea_5',value: 1});
        }
        else {
          options_arr.push({var_name: 'base_dyspnea_5',value: 0});
        }

      }
      else if(this.qno == 3){
        //console.log(countCheck)
        //oxygen_therapy
        if(countCheck.indexOf('ltou')!=-1){
          options_arr.push({var_name: 'ltou',value: 1});
        }
        else {
          options_arr.push({var_name: 'ltou',value: 0});
        }

        //daily_activities
        if(countCheck.indexOf('nhp')!=-1){
          options_arr.push({var_name: 'nhp',  value: 1});
        }
        else {
          options_arr.push({var_name: 'nhp',  value: 0});
        }

        //lives_alone
        if(countCheck.indexOf('la')!=-1){
          options_arr.push({var_name: 'la',value: 1});
        }
        else {
          options_arr.push({var_name: 'la',value: 0});
        }

        //smoker
        if(countCheck.indexOf('smoker')!=-1){
          smoker=1
          options_arr.push({var_name: 'smoker',value: 1});
        }
        else {
          smoker=0
          options_arr.push({var_name: 'smoker',value: 0});
        }

        //hospitalized_due_to_COPD
        if(countCheck.indexOf('hcopd')!=-1){
          hosp=1
          options_arr.push({var_name: 'hcopd',  value: 1});
        }
        else {
          hosp=0
          options_arr.push({var_name: 'hcopd',  value: 0});
        }
      }
      // if no selection in qustion 4
      if(this.qno==4 && (countCheck.length == 0)){
        final_thank=1;
      }
      // if Both not selected
      else if(this.qno==3){
        if(smoker==0 && hosp==0){
          final_thank=1;
        }
        // if Both selected in qustion 4 i.e. smoke and hospitalized
        else if(smoker==1 && hosp==1){
          final_thank=0;
        }
        // if hospitalized selected in qustion 4
        else if(smoker==0 && hosp==1){
          this.qno=this.qno+2
          this.setState({ qus_no: this.qno })
        }
          // if smoke selected in qustion 4
        else if(this.qno==6 && smoker==1 && hosp==0){
          final_thank=1;
        }
      }

      this.qno++
      this.setState({ qus_no: this.qno })
      ans_track =[];


      if(final_thank != 1){
        this.setState({ countCheck: [], question: arrnew[this.qno].question, options: arrnew[this.qno].options, type: arrnew[this.qno].type, suboption : arrnew[this.qno].suboption});
      }
   }
   else{
     final_thank=1;
   }

    if(final_thank == 1) {
      //(2 or More Exacerbations In Last Year )
      options_arr.push({var_name: 'me2',value: 0});

      //(Visited ICU for COPD in Last Year)
      options_arr.push({var_name: 'vicu',value: 0});

      options_arr.push({var_name: 'unknown',value: 0});


      // if a patient ever enters an age less than 40, I want you to list their age as normal in the profile, but for the purpose of the algorithm, the value that should be passed to the array for age should be '41'.
      var age
      if(this.state.birthdate < 40){
        age= 41
      }
      else{
        age= this.state.birthdate   //(age)
      }
      var h=this.state.height.split(" ")
      console.log('height',this.state.height);
      var f=parseInt(h[0].charAt(0))
      var i
      if(h[1].length>0){
        i=parseInt(h[1].charAt(0))
      }
      else{
        i=0
      }
      var th=f*12+i
      bmi = 703 * this.state.weight/ (th * th) // 703 x Weight(lbs) / [Height(in)]^2
      console.log(bmi);

      options_arr.push({var_name: 'bl_age',value: age});
      options_arr.push({var_name: 'bmi',value: bmi});
      var date = new Date().getDate();
      var month = new Date().getMonth() + 1;
      var year = new Date().getFullYear();
      var current_date=year + '-' + month + '-' + date
      options_arr.push({var_name:'hads_date',value:current_date});
      options_arr.push({var_name:'ccq_date',value:current_date});
      options_arr.push({var_name:'vital_date',value:current_date});
      options_arr.push({var_name:'oxygen_date',value:current_date});
      options_arr.push({var_name:'temp_date',value:current_date});
      options_arr.push({var_name:'heart_date',value:current_date});

      //_that.validationAndApiParameter('pyhton_profile');
      _that.validationAndApiParameter('web_profile');
      }
  }


  _answer(status,ans){
    //console.log(this.ans)
    if(this.state.type=="multiple"){
      if(status == false){
        ans_track.push(ans);
      }
      else{
        var index = ans_track.indexOf(ans);
        if (index !== -1) ans_track.splice(index, 1);
        //ans_track.splice(-1,1);
      }
    }
    else if(this.state.type=="single"){
      ans_track.splice(-1,1);
      ans_track.push(ans);
    }
    current_ans=ans;
    this.setState({ countCheck: ans_track })
  }



back_btn(){
  _that.props.navigation.navigate("MedicalQusSelectionScreen");
}


  render() {
    //console.log(this.state.python_result);
    const headerProp = {
      title: 'Medical Profile setup!',
      screens: 'MedicalQusScreen',
    };
    let _this = this
      const currentOptions = this.state.options
      const options = Object.keys(currentOptions).map( function(k) {
        if(k==current_ans){status=true}else{status=false}
        return (  <View key={k} style={{margin:5, }}>
          { _this.state.type=='multiple' ?
            <Animbutton onColor={"#2c808f"} effect={"pulse"}  _onPress={(status) => _this._answer(status,k)} text={currentOptions[k]} />
          :
            <AnimSinglebutton onColor={"#2c808f"} effect={"pulse"} status={status} _onPress={(status) => _this._answer(status,k)} text={currentOptions[k]} />
          }

        </View>)
      });

      var DropDown=<View style={{margin:AppSizes.ResponsiveSize.Padding(5) }}>
       {/* <Text style={styles.dropdowntext}>Oxygen Saturation </Text> */}
       <View style={{ paddingTop: AppSizes.ResponsiveSize.Padding(5) }} />

        <Picker PickerData={this.state.cigrate_data}
         labelIcon={cigrate_icon}
         styleImage={styles.imageContainer}
         placeholder={"Select total cigarettes"}
         getTxt={(val,label)=>this.setState({cigrates:val,countCheck:val})}/>

         <View style={{ paddingBottom: AppSizes.ResponsiveSize.Padding(5) }} />

      </View>;


      if(this.state.qus_no!=0){
        headerBack=<TouchableOpacity style={styles.menuWrapper} onPress={this.prev}>
                  <Image style={styles.image} source={back_icon}/>
                  </TouchableOpacity>;
        }
        else{
          headerBack=<TouchableOpacity style={styles.menuWrapper} onPress={this.back_btn}>
                    <Image style={styles.image} source={back_icon}/>
                    </TouchableOpacity>;
      }

    return (
      <View style={styles.container}>
      <LinearGradient colors={['#48c3d5','#3fafc0','#369aaa','#287b88']}>

        <MyStatusBar barStyle="light-content"  backgroundColor="#4eb4c4"/>
          <View style={styles.appBar} >
              <View style={{ flex:1,flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', }}>
                <View style={styles.imageContainer}>
                {headerBack}
                </View>

                <View style={{ marginTop:0,width:'60%',justifyContent: 'center',alignItems:'center' }}>
                    <Text allowFontScaling={false} style={styles.headertitle}>Medical Profile setup!</Text>
                </View>

                <View style={{width:'15%',flexDirection: 'row',justifyContent: 'flex-end'}}>
                  {/* <TouchableOpacity style={styles.qusWrapper} onPress={() => { this.Show_Custom_Alert(true) }}>
                            <Image style={styles.image} source={qus_icon}/>
                            </TouchableOpacity> */}
                </View>

              </View>
          </View>


      </LinearGradient>

        <ScrollView style={{backgroundColor: '#F5FCFF',paddingTop: 10}}
        ref={(c) => {this.scroll = c}} >
		<View style={styles.container1}>
          <Text allowFontScaling={false} style={styles.heading}>  {this.state.question} </Text>
          <View style={styles.hr}></View>
      </View>
      <View style={styles.container2}>
            { this.state.qus_no != 5 ? options : DropDown }
      </View>
      <View style={styles.container3}>
        <TouchableOpacity style={styles.btn} activeOpacity={.6} onPress={this.next}>
          <CommonButton label='Next'/>
          </TouchableOpacity>
      </View>

      </ScrollView>

      <Spinner visible={this.state.isVisible}  />

      </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
  },
  statusBar: {
    height: AppSizes.statusBarHeight,
  },
  appBar: {
      height: AppSizes.navbarHeight,
      justifyContent:'center',
      ...Platform.select({
      android: {
      paddingTop:AppSizes.ResponsiveSize.Padding(4),
      },
    }),
    },
  headertitle:{
    color:'#ffffff',
    fontSize:AppSizes.ResponsiveSize.Sizes(16),
    fontWeight:'bold',
    letterSpacing:1,
  },
  imageContainer:{
    width:'20%'
  },
  menuWrapper: {
    width:'60%',
    height:'60%',
    marginLeft:'30%',
  },
  image: {
    flex: 1,
    width: undefined,
    height: undefined,
    resizeMode:'contain'
  },
  container1:{
    flex:2,
    flexDirection:'column',
    alignItems:'center',
    justifyContent:'center'
  },
  heading:{
    fontSize:AppSizes.ResponsiveSize.Sizes(18),
    color:'#000',
    justifyContent:'center',
    alignItems:'center',
    fontWeight:'600',
    width:'89%',

  },
  hr:{
    width:50,
    height:3,
    backgroundColor:'#000',
    marginTop:10,

  },
  container2:{
    flex:6,
    flexDirection:'column',
    width:AppSizes.screen.width,
   paddingLeft:AppSizes.ResponsiveSize.Padding(5),
   paddingRight:AppSizes.ResponsiveSize.Padding(5),
   paddingTop:AppSizes.ResponsiveSize.Padding(2),
  },
  container3:{
    flex:2,
    justifyContent:'flex-start',
    alignItems:'center',

  },
  ans:{
    borderBottomWidth:1,
    borderColor:'#000',
    paddingTop:5,
    paddingBottom:10,
    width:'90%',
    marginBottom:25,

  },
  ansfield:{
    fontSize:AppSizes.ResponsiveSize.Sizes(13),
    color:'#5a5a5a',
  },
  btn:{
    width:'100%',
    marginBottom:20
  }
});
