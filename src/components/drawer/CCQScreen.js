import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    TouchableHighlight,
    ScrollView,
    Dimensions,
    TextInput,
    TouchableOpacity,
    Alert,
    Keyboard,
    NetInfo,
    Platform,
    AsyncStorage,
    StatusBar
} from 'react-native';
import * as stateActions from '../../stateActions';
import { connect } from 'react-redux';
import { NavigationActions } from 'react-navigation'
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Spinner from 'react-native-loading-spinner-overlay';
import LinearGradient from 'react-native-linear-gradient';
import Header from '../common/signupHeader'
import Spacer from '../common/Spacer'
import * as commonFunctions from '../../utils/CommonFunctions'
import CommonButton from '../common/CommonButton'
import FloatingLabel from 'react-native-floating-labels';

var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;

const logo = require('../../themes/Images/logo.png')

let _that
export default class CCQScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
          isVisible: false,
        },
        _that = this;
    }




  start(){
	 _that.props.navigation.navigate('CCQQusScreen');
  }

remindlater(){
  _that.props.navigation.navigate('HADSQusScreen', {qus_no: 3,  name: 'HADSScreen' });
}

    static navigationOptions = {
        header: null,
    }


    render() {
      const headerProp = {
        title: 'COPD Clinical Questionnaire',
        screens: 'TrackingScreen',
      };
        return (
          <View style={styles.wrapper}>
          <Header info={headerProp} navigation={_that.props.navigation} updateAlert={this.updateAlert}/>
          <View style={styles.titleContainer}>
            <Text allowFontScaling={false} style={styles.headertitle}>COPD Questionnaire</Text>
            <Text allowFontScaling={false} style={styles.headersubtitle}>
            This questionnaire will help you and your healthcare professional measure the impact COPD (Chronic Obstructive Pulmonary Disease) is having on your wellbeing and daily life. Your answers and test score can be used by you and your healthcare professional to help improve the management of your COPD and get the greatest benefit from treatment.

            </Text>
            {/*<TouchableOpacity activeOpacity={.6} onPress={this.remindlater} >
                <CommonButton label='Remind Me Later'/>
            </TouchableOpacity> */}
            <TouchableOpacity activeOpacity={.6} onPress={this.start} >
                <CommonButton label='Start'/>
            </TouchableOpacity>
          </View>

          </View>
        );
    }
}

const styles = {
  wrapper: {
    flex: 1,
    flexDirection:'column',
    backgroundColor:'#ffffff'
  },
titleContainer:{
  flex:1,
  justifyContent:'flex-start',
  paddingTop:AppSizes.ResponsiveSize.Padding(2),
},

headertitle:{
  textAlign:'center',
  color:AppColors.primary,
  fontSize:AppSizes.ResponsiveSize.Sizes(18),
  fontWeight:'600',
  letterSpacing:1,
  padding:AppSizes.ResponsiveSize.Padding(2),
},
headersubtitle:{
  textAlign:'center',
  color:AppColors.primary,
  fontSize:AppSizes.ResponsiveSize.Sizes(15),
  fontWeight:'500',
  letterSpacing:1,
  padding:AppSizes.ResponsiveSize.Padding(5),
},
}
