import React, { Component } from 'react';
import {AsyncStorage,StyleSheet,Text,View,Image,TouchableOpacity,Platform,Alert,ScrollView,TextInput,Button,StatusBar,Modal} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions } from 'react-navigation'
import Spacer from '../common/Spacer'
import Spinner from 'react-native-loading-spinner-overlay';
import Header from '../common/signupHeader'
import CommonButton from '../common/CommonButton'
import Swiper from 'react-native-swiper';
import Animbutton from './animAnsbutton'
import AnimSinglebutton from './animSingleAnsOptions';
import { jsonData } from '../data/Question';
import { pickerData } from '../data/pickerData';
import Picker from 'react-native-picker';
import Sound from 'react-native-sound'
import Orientation from 'react-native-orientation'
import LinearGradient from 'react-native-linear-gradient';

import * as commonFunctions from '../../utils/CommonFunctions'

var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;

/* Images */
//var thankyou_icon = require('../../themes/Images/thank-you.png');
var logo = require('../../themes/Images/logo.png');
var back_icon = require( '../../themes/Images/left-whitearrow.png')
var oxy_icon = require('../../themes/Images/oxy.png');
var temp_icon = require('../../themes/Images/temp.png');
var heartRate_icon = require('../../themes/Images/heart_rate.png');
var qus_icon = require('../../themes/Images/que_icon_196.png');
var mice=require('../../themes/Images/mice.png')
var mice_off=require('../../themes/Images/mice-off.png')

var help_audio1=require('../../themes/sound/amiokQus1Help.mp3')
var help_audio2=require('../../themes/sound/amiokQus2Help.mp3')
var help_audio3=require('../../themes/sound/amiokQus3Help.mp3')
var help_audio4=require('../../themes/sound/amiokQus4Help.mp3')

let _that
let arrnew = []
let ans_track = []
let options_arr=[]
let vitals_data=[]
var current_ans=''
var move_last=0;
var next_move_last='';
const MyStatusBar = ({backgroundColor, ...props}) => (
  <View style={[styles.statusBar, { backgroundColor }]}>
    <StatusBar translucent backgroundColor={backgroundColor} {...props} />
  </View>
);

var oxygen_data = [];
var heart_data = [];
var fev_data= [];
var temperature_data= [];

//oxygen_data
for( var i = 73; i <=100; i++){
  oxygen_data.push(i);
}
//console.log(oxygen_data);

//heart_data
for( var j = 30; j <151; j++){
  heart_data.push(j);
}

export default class AmIOkQusScreen extends Component {
  constructor (props) {
    super(props)
    this.qno = this.props.navigation.state.params.qus_no
    this.score = 0
    this.type ='qus'
    this.inputRefs = {};

    const jdata = jsonData.amiokqus.qus1
    arrnew = Object.keys(jdata).map( function(k) { return jdata[k] });

    this.state = {
      question : arrnew[this.qno].question,
      title : arrnew[this.qno].title,
      options : arrnew[this.qno].options,
      suboption : arrnew[this.qno].suboption,
      type : arrnew[this.qno].type,
      modal_title:arrnew[this.qno].modal_title,
      countCheck : [],
      dataStorage:[],
      qus_no : 0,
      uid:'',
      isVisible: false,
      Modal_Visibility: false,
      status:'ans',
      oxygen_sat: 0,
      items1: oxygen_data,
      heart_rate: 0,
      items2: heart_data,
      temperature: 0,
      items3: pickerData.temprature,
      temperature_val:'',
      screentype:'',
      fev: 0,
      items4: fev_data,
      baseline_heart_rate:0,
      baseline_oxygen_sat:0,
      baseline_heart_rate_count:0,
      baseline_oxygen_sat_count:0,
      api:0,
      Modal_Visibility: false,
      audioStatus: false,
      audioImg: mice,
    },
    _that = this;
  }

  playTrack = () => {
    this.setState({audioStatus: !this.state.audioStatus})
    if(this.state.audioStatus==true){
      if(this.qno==0){
        help_audio=help_audio1;
      }
      else if(this.qno==1){
        help_audio=help_audio2;
      }
      else if(this.qno==2){
        help_audio=help_audio3;
      }
      else{
        help_audio=help_audio4;
      }

      this.track = new Sound(help_audio,  (e) => {
        if (e) {
          console.log('error loading track:', e)
        } else {

            this.setState({audioImg: mice_off});
            this.track.play((success) => {
                 if (success) {
                     this.stop();
                 }
             });
          }
        })
      }
        else {
          this.stop();
        }
      }

  stop() {
     if (!this.track) return;
     this.track.stop();
     this.track.release();
     this.track = null;
     this.setState({audioStatus: false});
     this.setState({audioImg: mice});
 }

  Show_Custom_Alert(visible) {
    this.stop();
    this.setState({Modal_Visibility: visible});
  }

componentDidMount() {
  Orientation.lockToPortrait();
  AsyncStorage.getItem('loggedIn').then((value) => {
      var loggedIn = JSON.parse(value);
      console.log(loggedIn);
      if (loggedIn) {
          AsyncStorage.getItem('UserData').then((UserData) => {
              const data = JSON.parse(UserData)
              console.log(data)
              var uid=data.id;
              _that.setState({uid:  uid});
              _that.validationAndApiParameter('profile_result',uid);
          })
     }
  })
}

validationAndApiParameter(apiname,uid) {
  if(apiname == 'hit_python'){
    var json_options_arr = JSON.stringify(options_arr);
    var data = {
        uid: this.state.uid,
        phython:'phython',
        variable : json_options_arr,
    };
    console.log(data);
    _that.setState({isVisible: true});
    this.postToApiCalling('POST', apiname, Constant.URL_saveVariable,data);
  }
  else if(apiname == 'profile_result'){
    var data = {
        uid:uid,
    };
    console.log(data);
    _that.setState({isVisible: true});
    this.postToApiCalling('POST', apiname, Constant.URL_showVarData,data);
  }
  else if (apiname=='save_vitals') {
    var error=0
    console.log('vitals_data',vitals_data.length);
    if(vitals_data.length==0){
      error=1
    }
    if(error==0){
      var data = {
        uid:this.state.uid,
        variable : JSON.stringify(vitals_data)
      };
      console.log(data);
      _that.setState({isVisible: true});
      this.postToApiCalling('POST', apiname, Constant.URL_saveVitalData,data);
    }
  }
  else if (apiname=='save') {
    var final=[{var_name:'amiok_inactive',value:0},{var_name:'inactive_sent',value:0}]
    var json_python_result = JSON.stringify(final);
    var data = {
        uid: this.state.uid,
        variable : json_python_result,
    };
    console.log(data);
    _that.setState({isVisible: true});
    this.postToApiCalling('POST', apiname, Constant.URL_saveUserData,data);
  }
  else if (apiname=='saveV') {
    var final=[]
    if(this.state.oxygen_sat!=0){
      final.push({var_name:'oxygen_inactive',value:0})
    }
    if(this.state.heart_rate!=0){
      final.push({var_name:'heart_inactive',value:0})
    }
    if(this.state.temperature!=0){
      final.push({var_name:'temperature_inactive',value:0})
    }
    var json_python_result = JSON.stringify(final);
    var data = {
        uid: this.state.uid,
        variable : json_python_result,
    };
    console.log(data);
    _that.setState({isVisible: true});
    this.postToApiCalling('POST', apiname, Constant.URL_saveUserData,data);
  }
}

postToApiCalling(method, apiKey, apiUrl, data) {
  //console.log(apiUrl +" "+ apiKey)
    new Promise(function(resolve, reject) {
        if (method == 'POST') {
            resolve(WebServices.callWebService(apiUrl, data));
        } else {
          //console.log('callWebService_POST')
            resolve(WebServices.callWebService_POST(apiUrl, data));
        }
    }).then((jsonRes) => {
      if ((!jsonRes) || (jsonRes.code == 0)) {
        setTimeout(() => {
              Alert.alert(jsonRes.message);
          }, 200);
      } else {
          if (jsonRes.code == 1) {
              _that.apiSuccessfullResponse(apiKey, jsonRes)
          }
      }
    }).catch((error) => {
        console.log("ERROR" + error);
        _that.setState({ isVisible: false })
        setTimeout(() => {
            Alert.alert("Server Error");
        }, 200);
    })
}

apiSuccessfullResponse(apiKey, jsonRes) {

  if(apiKey=="hit_python"){
    console.log(jsonRes);
    _that.setState({ isVisible: false })
    arrnew = []
    ans_track = []
    options_arr=[]
    vitals_data=[]
    current_ans=''
    move_last=0;
    next_move_last='';
    if(this.state.screentype=="MedicalInAttentionScreen"){
      _that.props.navigation.navigate('MedicalInAttentionScreen',{exacerb:jsonRes.exacerb,profile:jsonRes.profile,symptom:jsonRes.symptom,triage:jsonRes.triage,triageprob:jsonRes.triageprob,vital:jsonRes.vital,med:jsonRes.med,med_proba:jsonRes.med_proba});
    }
    else if(this.state.screentype=="MedicalAttentionScreen"){
      _that.props.navigation.navigate('MedicalAttentionScreen');
    }
  }
  if(apiKey=='save_vitals'){
    _that.setState({ isVisible: false })
    _that.validationAndApiParameter('saveV')
  }
  if(apiKey=='saveV'){
    _that.setState({ isVisible: false })
  }
  if(apiKey=="profile_result"){
    console.log(jsonRes);
    _that.setState({ isVisible: false })
    var data=jsonRes.profile

    var baseline_oxygen_sat=parseInt(data.oxygen_sat_base);
    var baseline_heart_rate=parseInt(data.heart_rate_base);
    var baseline_heart_rate_count=parseInt(data.heart_count);
    var baseline_oxygen_sat_count=parseInt(data.oxygen_count);
    if(data.oxygen_sat_base!=''){
      this.setState({baseline_oxygen_sat:baseline_oxygen_sat})
    }
    if(data.heart_rate_base!=''){
      this.setState({baseline_heart_rate:baseline_heart_rate})
    }

    this.setState({
      baseline_heart_rate_count:baseline_heart_rate_count,
      baseline_oxygen_sat_count:baseline_oxygen_sat_count
    })
  }
  if(apiKey=='save'){
    _that.setState({ isVisible: false })
    _that.validationAndApiParameter('hit_python')
  }
}

  updateAlert = () => {
        this.setState({Modal_Visibility: !this.state.Modal_Visibility});
  }

  prev= () =>{
    if(this.qno > 0){
      if(this.qno==2){
        _that.setState({
          oxygen_sat : '',
          heart_rate : '',
          temperature : '',
        });
      }
      this.qno--
      this.setState({ qus_no: this.qno })
      this.setState({ question: arrnew[this.qno].question,qus_id: arrnew[this.qno].qus_id, options: arrnew[this.qno].options, type: arrnew[this.qno].type,suboption : arrnew[this.qno].suboption,modal_title : arrnew[this.qno].modal_title})
    }
  }
  next= () =>{

    current_ans=0

    const { countCheck} = this.state
    ans_track =[];
    //console.log('count'+countCheck)
    if(this.state.qus_no==0){
      if(countCheck.indexOf('rws_1') != -1){
        move_last=1
      }
      else{
        move_last=0
      }
    }
    //console.log("move_last "+move_last)
    if(this.state.countCheck.length <= 0 && this.state.qus_no!=1){
      Alert.alert("Invalid Value","Missing or blank value is not allowed")
    }
    else if(this.qno == 1){
      const { oxygen_sat, heart_rate, temperature, fev, baseline_oxygen_sat,baseline_heart_rate_count,baseline_oxygen_sat_count,temperature_val, baseline_heart_rate } = this.state
      var status=0;
      console.log('temperature',this.state.temperature);
      console.log('oxy_sat',this.state.oxygen_sat);
      console.log('heart_rate',this.state.heart_rate);

      if(this.state.oxygen_sat!=0){
        options_arr.push({var_name:'show_oxygen',value:1})
        vitals_data.push({var_name:'oxygen_sat',value: this.state.oxygen_sat});
      }
      if(this.state.heart_rate!=0){
        options_arr.push({var_name:'show_heart',value:1})
        vitals_data.push({var_name: 'heart_rate',value: this.state.heart_rate});
      }
      if(this.state.temperature!=0){
        options_arr.push({var_name:'show_temp',value:1})
        vitals_data.push({var_name: 'temperature',value: this.state.temperature});
      }

      if (oxygen_sat <= 86 && oxygen_sat != 0) {
        status=1;
      }
      if( heart_rate > 120){
        status=1;
      }

      if(temperature > 104){
        status=1;
      }

      // (heart rate difference)
      diff_hr=heart_rate-baseline_heart_rate
      if(heart_rate==0){
        options_arr.push({var_name: 'hrdf20inf',value: 0});
        options_arr.push({var_name: 'hrdf1020',value: 0});
        options_arr.push({var_name: 'hrdf010',value: 0});
        options_arr.push({var_name: 'hrdf100',value: 0});
        options_arr.push({var_name: 'hrdfinf10',value: 0});
        options_arr.push({var_name: 'hrdiff_nan',value: 1});
      }
      else if(diff_hr>20){
        options_arr.push({var_name: 'hrdf20inf',value: 1});
        options_arr.push({var_name: 'hrdf1020',value: 0});
        options_arr.push({var_name: 'hrdf010',value: 0});
        options_arr.push({var_name: 'hrdf100',value: 0});
        options_arr.push({var_name: 'hrdfinf10',value: 0});
        options_arr.push({var_name: 'hrdiff_nan',value: 0});
      }
      else if (diff_hr>10 && diff_hr<=20) {
        options_arr.push({var_name: 'hrdf20inf',value: 0});
        options_arr.push({var_name: 'hrdf1020',value: 1});
        options_arr.push({var_name: 'hrdf010',value: 0});
        options_arr.push({var_name: 'hrdf100',value: 0});
        options_arr.push({var_name: 'hrdfinf10',value: 0});
        options_arr.push({var_name: 'hrdiff_nan',value: 0});
      }
      else if (diff_hr>0 && diff_hr<=10) {
        options_arr.push({var_name: 'hrdf20inf',value: 0});
        options_arr.push({var_name: 'hrdf1020',value: 0});
        options_arr.push({var_name: 'hrdf010',value: 1});
        options_arr.push({var_name: 'hrdf100',value: 0});
        options_arr.push({var_name: 'hrdfinf10',value: 0});
        options_arr.push({var_name: 'hrdiff_nan',value: 0});
      }
      else if (diff_hr>-10 && diff_hr<=0) {
        options_arr.push({var_name: 'hrdf20inf',value: 0});
        options_arr.push({var_name: 'hrdf1020',value: 0});
        options_arr.push({var_name: 'hrdf010',value: 0});
        options_arr.push({var_name: 'hrdf100',value: 1});
        options_arr.push({var_name: 'hrdfinf10',value: 0});
        options_arr.push({var_name: 'hrdiff_nan',value: 0});
      }
      else if (diff_hr<-10) {
        options_arr.push({var_name: 'hrdf20inf',value: 0});
        options_arr.push({var_name: 'hrdf1020',value: 0});
        options_arr.push({var_name: 'hrdf010',value: 0});
        options_arr.push({var_name: 'hrdf100',value: 0});
        options_arr.push({var_name: 'hrdfinf10',value: 1});
        options_arr.push({var_name: 'hrdiff_nan',value: 0});
      }

      //(oxygen difference)
      diff_oxygen_sat=oxygen_sat-baseline_oxygen_sat
      if(oxygen_sat==0){
        options_arr.push({var_name: 'ox_diff493',value: 0});
        options_arr.push({var_name: 'ox_diff4293',value: 0});
        options_arr.push({var_name: 'ox_diff293',value: 0});
        options_arr.push({var_name: 'ox_diff290',value: 0});
        options_arr.push({var_name: 'ox_diff2900',value: 0});
        options_arr.push({var_name: 'ox_diff19294',value: 0});
        options_arr.push({var_name: 'ox_diff19394',value: 0});
        options_arr.push({var_name: 'ox_diff1',value: 0});
        options_arr.push({var_name: 'ox_diff_nan',value: 1});
      }
      else if(diff_oxygen_sat<-4 && baseline_oxygen_sat<=93){
        options_arr.push({var_name: 'ox_diff493',value: 1});
        options_arr.push({var_name: 'ox_diff4293',value: 0});
        options_arr.push({var_name: 'ox_diff293',value: 0});
        options_arr.push({var_name: 'ox_diff290',value: 0});
        options_arr.push({var_name: 'ox_diff2900',value: 0});
        options_arr.push({var_name: 'ox_diff19294',value: 0});
        options_arr.push({var_name: 'ox_diff19394',value: 0});
        options_arr.push({var_name: 'ox_diff1',value: 0});
        options_arr.push({var_name: 'ox_diff_nan',value: 0});
      }
      else if ((diff_oxygen_sat<-2 || diff_oxygen_sat>-4) && baseline_oxygen_sat<=93) {
        options_arr.push({var_name: 'ox_diff493',value: 0});
        options_arr.push({var_name: 'ox_diff4293',value: 1});
        options_arr.push({var_name: 'ox_diff293',value: 0});
        options_arr.push({var_name: 'ox_diff290',value: 0});
        options_arr.push({var_name: 'ox_diff2900',value: 0});
        options_arr.push({var_name: 'ox_diff19294',value: 0});
        options_arr.push({var_name: 'ox_diff19394',value: 0});
        options_arr.push({var_name: 'ox_diff1',value: 0});
        options_arr.push({var_name: 'ox_diff_nan',value: 0});
      }
      else if (diff_oxygen_sat<-2 && baseline_oxygen_sat>93) {
        options_arr.push({var_name: 'ox_diff493',value: 0});
        options_arr.push({var_name: 'ox_diff4293',value: 0});
        options_arr.push({var_name: 'ox_diff293',value: 1});
        options_arr.push({var_name: 'ox_diff290',value: 0});
        options_arr.push({var_name: 'ox_diff2900',value: 0});
        options_arr.push({var_name: 'ox_diff19294',value: 0});
        options_arr.push({var_name: 'ox_diff19394',value: 0});
        options_arr.push({var_name: 'ox_diff1',value: 0});
        options_arr.push({var_name: 'ox_diff_nan',value: 0});
      }
      else if (diff_oxygen_sat==-2 && baseline_oxygen_sat<=90) {
        options_arr.push({var_name: 'ox_diff493',value: 0});
        options_arr.push({var_name: 'ox_diff4293',value: 0});
        options_arr.push({var_name: 'ox_diff293',value: 0});
        options_arr.push({var_name: 'ox_diff290',value: 1});
        options_arr.push({var_name: 'ox_diff2900',value: 0});
        options_arr.push({var_name: 'ox_diff19294',value: 0});
        options_arr.push({var_name: 'ox_diff19394',value: 0});
        options_arr.push({var_name: 'ox_diff1',value: 0});
        options_arr.push({var_name: 'ox_diff_nan',value: 0});
      }
      else if (diff_oxygen_sat==-2 && baseline_oxygen_sat>90) {
        options_arr.push({var_name: 'ox_diff493',value: 0});
        options_arr.push({var_name: 'ox_diff4293',value: 0});
        options_arr.push({var_name: 'ox_diff293',value: 0});
        options_arr.push({var_name: 'ox_diff290',value: 0});
        options_arr.push({var_name: 'ox_diff2900',value: 1});
        options_arr.push({var_name: 'ox_diff19294',value: 0});
        options_arr.push({var_name: 'ox_diff19394',value: 0});
        options_arr.push({var_name: 'ox_diff1',value: 0});
        options_arr.push({var_name: 'ox_diff_nan',value: 0});
      }
      else if (diff_oxygen_sat==-1 && (baseline_oxygen_sat<=92 || baseline_oxygen_sat==94)) {
        options_arr.push({var_name: 'ox_diff493',value: 0});
        options_arr.push({var_name: 'ox_diff4293',value: 0});
        options_arr.push({var_name: 'ox_diff293',value: 0});
        options_arr.push({var_name: 'ox_diff290',value: 0});
        options_arr.push({var_name: 'ox_diff2900',value: 0});
        options_arr.push({var_name: 'ox_diff19294',value: 1});
        options_arr.push({var_name: 'ox_diff19394',value: 0});
        options_arr.push({var_name: 'ox_diff1',value: 0});
        options_arr.push({var_name: 'ox_diff_nan',value: 0});
      }
      else if (diff_oxygen_sat==-1 && (baseline_oxygen_sat==93 || baseline_oxygen_sat>94)) {
        options_arr.push({var_name: 'ox_diff493',value: 0});
        options_arr.push({var_name: 'ox_diff4293',value: 0});
        options_arr.push({var_name: 'ox_diff293',value: 0});
        options_arr.push({var_name: 'ox_diff290',value: 0});
        options_arr.push({var_name: 'ox_diff2900',value: 0});
        options_arr.push({var_name: 'ox_diff19294',value: 0});
        options_arr.push({var_name: 'ox_diff19394',value: 1});
        options_arr.push({var_name: 'ox_diff1',value: 0});
        options_arr.push({var_name: 'ox_diff_nan',value: 0});
      }
      else if (diff_oxygen_sat>-1) {
        options_arr.push({var_name: 'ox_diff493',value: 0});
        options_arr.push({var_name: 'ox_diff4293',value: 0});
        options_arr.push({var_name: 'ox_diff293',value: 0});
        options_arr.push({var_name: 'ox_diff290',value: 0});
        options_arr.push({var_name: 'ox_diff2900',value: 0});
        options_arr.push({var_name: 'ox_diff19294',value: 0});
        options_arr.push({var_name: 'ox_diff19394',value: 0});
        options_arr.push({var_name: 'ox_diff1',value: 1});
        options_arr.push({var_name: 'ox_diff_nan',value: 0});
      }


      baseline_profile_oxygen_sat=((baseline_oxygen_sat*baseline_oxygen_sat_count)+oxygen_sat)/(baseline_oxygen_sat_count+1);
      baseline_profile_heart_rate=((baseline_heart_rate*baseline_heart_rate_count)+heart_rate)/(baseline_heart_rate_count+1);;
      //algo variables for profile
      if(heart_rate==0){
        options_arr.push({var_name: 'basehr_nan',value: 1});
        options_arr.push({var_name: 'basehr100250',value: 0});
        options_arr.push({var_name: 'basehr80100',value: 0});
        options_arr.push({var_name: 'basehr6080',value: 0});
        options_arr.push({var_name: 'basehr060',value: 0});
      }
      else if(baseline_heart_rate  >= 100 && baseline_heart_rate  < 250){
        options_arr.push({var_name: 'basehr_nan',value: 0});
        options_arr.push({var_name: 'basehr100250',value: 1});
        options_arr.push({var_name: 'basehr80100',value: 0});
        options_arr.push({var_name: 'basehr6080',value: 0});
        options_arr.push({var_name: 'basehr060',value: 0});
      }
      else if(baseline_heart_rate  >= 80 && baseline_heart_rate  < 100){
        options_arr.push({var_name: 'basehr_nan',value: 0});
        options_arr.push({var_name: 'basehr100250',value: 0});
        options_arr.push({var_name: 'basehr80100',value: 1});
        options_arr.push({var_name: 'basehr6080',value: 0});
        options_arr.push({var_name: 'basehr060',value: 0});
      }
      else if(baseline_heart_rate  >= 60 && baseline_heart_rate  < 80){
        options_arr.push({var_name: 'basehr_nan',value: 0});
        options_arr.push({var_name: 'basehr100250',value: 0});
        options_arr.push({var_name: 'basehr80100',value: 0});
        options_arr.push({var_name: 'basehr6080',value: 1});
        options_arr.push({var_name: 'basehr060',value: 0});
      }
      else if(baseline_heart_rate  >= 0 && baseline_heart_rate  < 60){
        options_arr.push({var_name: 'basehr_nan',value: 0});
        options_arr.push({var_name: 'basehr100250',value: 0});
        options_arr.push({var_name: 'basehr80100',value: 0});
        options_arr.push({var_name: 'basehr6080',value: 0});
        options_arr.push({var_name: 'basehr060',value: 1});
      }

      if(heart_rate==0){
        options_arr.push({var_name: 'curhr_nan',value: 1});
        options_arr.push({var_name: 'curhr120inf',value: 0});
        options_arr.push({var_name: 'curhr110120',value: 0});
        options_arr.push({var_name: 'curhr100110',value: 0});
        options_arr.push({var_name: 'curhr90100',value: 0});
        options_arr.push({var_name: 'curhr8090',value: 0});
        options_arr.push({var_name: 'curhr6080',value: 0});
        options_arr.push({var_name: 'curhr060',value: 0});
      }
      else if(heart_rate  >120 ){
        options_arr.push({var_name: 'curhr_nan',value: 0});
        options_arr.push({var_name: 'curhr120inf',value: 1});
        options_arr.push({var_name: 'curhr110120',value: 0});
        options_arr.push({var_name: 'curhr100110',value: 0});
        options_arr.push({var_name: 'curhr90100',value: 0});
        options_arr.push({var_name: 'curhr8090',value: 0});
        options_arr.push({var_name: 'curhr6080',value: 0});
        options_arr.push({var_name: 'curhr060',value: 0});
      }
      else if(heart_rate  >= 110 && heart_rate  < 120){
        options_arr.push({var_name: 'curhr_nan',value: 0});
        options_arr.push({var_name: 'curhr120inf',value: 0});
        options_arr.push({var_name: 'curhr110120',value: 1});
        options_arr.push({var_name: 'curhr100110',value: 0});
        options_arr.push({var_name: 'curhr90100',value: 0});
        options_arr.push({var_name: 'curhr8090',value: 0});
        options_arr.push({var_name: 'curhr6080',value: 0});
        options_arr.push({var_name: 'curhr060',value: 0});
      }
      else if(heart_rate  >= 100 && heart_rate  < 110){
        options_arr.push({var_name: 'curhr_nan',value: 0});
        options_arr.push({var_name: 'curhr120inf',value: 0});
        options_arr.push({var_name: 'curhr110120',value: 0});
        options_arr.push({var_name: 'curhr100110',value: 1});
        options_arr.push({var_name: 'curhr90100',value: 0});
        options_arr.push({var_name: 'curhr8090',value: 0});
        options_arr.push({var_name: 'curhr6080',value: 0});
        options_arr.push({var_name: 'curhr060',value: 0});
      }
      else if(heart_rate  >= 90 && heart_rate  < 100){
        options_arr.push({var_name: 'curhr_nan',value: 0});
        options_arr.push({var_name: 'curhr120inf',value: 0});
        options_arr.push({var_name: 'curhr110120',value: 0});
        options_arr.push({var_name: 'curhr100110',value: 0});
        options_arr.push({var_name: 'curhr90100',value: 1});
        options_arr.push({var_name: 'curhr8090',value: 0});
        options_arr.push({var_name: 'curhr6080',value: 0});
        options_arr.push({var_name: 'curhr060',value: 0});
      }
      else if(heart_rate  >= 80 && heart_rate  < 90){
        options_arr.push({var_name: 'curhr_nan',value: 0});
        options_arr.push({var_name: 'curhr120inf',value: 0});
        options_arr.push({var_name: 'curhr110120',value: 0});
        options_arr.push({var_name: 'curhr100110',value: 0});
        options_arr.push({var_name: 'curhr90100',value: 0});
        options_arr.push({var_name: 'curhr8090',value: 1});
        options_arr.push({var_name: 'curhr6080',value: 0});
        options_arr.push({var_name: 'curhr060',value: 0});
      }
      else if(heart_rate  >= 60 && heart_rate  < 80){
        options_arr.push({var_name: 'curhr_nan',value: 0});
        options_arr.push({var_name: 'curhr120inf',value: 0});
        options_arr.push({var_name: 'curhr110120',value: 0});
        options_arr.push({var_name: 'curhr100110',value: 0});
        options_arr.push({var_name: 'curhr90100',value: 0});
        options_arr.push({var_name: 'curhr8090',value: 0});
        options_arr.push({var_name: 'curhr6080',value: 1});
        options_arr.push({var_name: 'curhr060',value: 0});
      }
      else if(heart_rate  >= 0 && heart_rate  < 60){
        options_arr.push({var_name: 'curhr_nan',value: 0});
        options_arr.push({var_name: 'curhr120inf',value: 0});
        options_arr.push({var_name: 'curhr110120',value: 0});
        options_arr.push({var_name: 'curhr100110',value: 0});
        options_arr.push({var_name: 'curhr90100',value: 0});
        options_arr.push({var_name: 'curhr8090',value: 0});
        options_arr.push({var_name: 'curhr6080',value: 0});
        options_arr.push({var_name: 'curhr060',value: 1});
      }

      if(oxygen_sat==0){
        options_arr.push({var_name: 'baseox_nan',value: 1});
        options_arr.push({var_name: 'baseox93100',value: 0});
        options_arr.push({var_name: 'baseox9193',value: 0});
        options_arr.push({var_name: 'baseox8991',value: 0});
        options_arr.push({var_name: 'baseox8589',value: 0});
        options_arr.push({var_name: 'baseox085',value: 0});
      }
      else if(baseline_oxygen_sat  >= 93 && baseline_oxygen_sat  < 100){
        options_arr.push({var_name: 'baseox_nan',value: 0});
        options_arr.push({var_name: 'baseox93100',value: 1});
        options_arr.push({var_name: 'baseox9193',value: 0});
        options_arr.push({var_name: 'baseox8991',value: 0});
        options_arr.push({var_name: 'baseox8589',value: 0});
        options_arr.push({var_name: 'baseox085',value: 0});
      }
      else if(baseline_oxygen_sat  >= 91 && baseline_oxygen_sat  < 93){
        options_arr.push({var_name: 'baseox_nan',value: 0});
        options_arr.push({var_name: 'baseox93100',value: 0});
        options_arr.push({var_name: 'baseox9193',value: 1});
        options_arr.push({var_name: 'baseox8991',value: 0});
        options_arr.push({var_name: 'baseox8589',value: 0});
        options_arr.push({var_name: 'baseox085',value: 0});
      }
      else if(baseline_oxygen_sat  >= 89 && baseline_oxygen_sat  < 91){
        options_arr.push({var_name: 'baseox_nan',value: 0});
        options_arr.push({var_name: 'baseox93100',value: 0});
        options_arr.push({var_name: 'baseox9193',value: 0});
        options_arr.push({var_name: 'baseox8991',value: 1});
        options_arr.push({var_name: 'baseox8589',value: 0});
        options_arr.push({var_name: 'baseox085',value: 0});
      }
      else if(baseline_oxygen_sat  >= 85 && baseline_oxygen_sat  < 89){
        options_arr.push({var_name: 'baseox_nan',value: 0});
        options_arr.push({var_name: 'baseox93100',value: 0});
        options_arr.push({var_name: 'baseox9193',value: 0});
        options_arr.push({var_name: 'baseox8991',value: 0});
        options_arr.push({var_name: 'baseox8589',value: 1});
        options_arr.push({var_name: 'baseox085',value: 0});
      }
      else if(baseline_oxygen_sat  >= 0 && baseline_oxygen_sat  < 85){
        options_arr.push({var_name: 'baseox_nan',value: 0});
        options_arr.push({var_name: 'baseox93100',value: 0});
        options_arr.push({var_name: 'baseox9193',value: 0});
        options_arr.push({var_name: 'baseox8991',value: 0});
        options_arr.push({var_name: 'baseox8589',value: 0});
        options_arr.push({var_name: 'baseox085',value: 1});
      }

      if(oxygen_sat==0){
        options_arr.push({var_name: 'curox_nan',value: 1});
        options_arr.push({var_name: 'curox_nan93100',value: 0});
        options_arr.push({var_name: 'curox_nan9193',value: 0});
        options_arr.push({var_name: 'curox_nan8991',value: 0});
        options_arr.push({var_name: 'curox_nan8789',value: 0});
        options_arr.push({var_name: 'curox_nan8587',value: 0});
        options_arr.push({var_name: 'curox_nan085',value: 0});
      }
      else if(oxygen_sat  >= 93 && oxygen_sat  < 100){
        options_arr.push({var_name: 'curox_nan',value: 0});
        options_arr.push({var_name: 'curox_nan93100',value: 1});
        options_arr.push({var_name: 'curox_nan9193',value: 0});
        options_arr.push({var_name: 'curox_nan8991',value: 0});
        options_arr.push({var_name: 'curox_nan8789',value: 0});
        options_arr.push({var_name: 'curox_nan8587',value: 0});
        options_arr.push({var_name: 'curox_nan085',value: 0});
      }
      else if(oxygen_sat  >= 91 && oxygen_sat  < 93){
        options_arr.push({var_name: 'curox_nan',value: 0});
        options_arr.push({var_name: 'curox_nan93100',value: 0});
        options_arr.push({var_name: 'curox_nan9193',value: 1});
        options_arr.push({var_name: 'curox_nan8991',value: 0});
        options_arr.push({var_name: 'curox_nan8789',value: 0});
        options_arr.push({var_name: 'curox_nan8587',value: 0});
        options_arr.push({var_name: 'curox_nan085',value: 0});
      }
      else if(oxygen_sat  >= 89 && oxygen_sat  < 91){
        options_arr.push({var_name: 'curox_nan',value: 0});
        options_arr.push({var_name: 'curox_nan93100',value: 0});
        options_arr.push({var_name: 'curox_nan9193',value: 0});
        options_arr.push({var_name: 'curox_nan8991',value: 1});
        options_arr.push({var_name: 'curox_nan8789',value: 0});
        options_arr.push({var_name: 'curox_nan8587',value: 0});
        options_arr.push({var_name: 'curox_nan085',value: 0});
      }
      else if(oxygen_sat  >= 87 && oxygen_sat  < 89){
        options_arr.push({var_name: 'curox_nan',value: 0});
        options_arr.push({var_name: 'curox_nan93100',value: 0});
        options_arr.push({var_name: 'curox_nan9193',value: 0});
        options_arr.push({var_name: 'curox_nan8991',value: 0});
        options_arr.push({var_name: 'curox_nan8789',value: 1});
        options_arr.push({var_name: 'curox_nan8587',value: 0});
        options_arr.push({var_name: 'curox_nan085',value: 0});
      }
      else if(oxygen_sat  >= 85 && oxygen_sat  < 87){
        options_arr.push({var_name: 'curox_nan',value: 0});
        options_arr.push({var_name: 'curox_nan93100',value: 0});
        options_arr.push({var_name: 'curox_nan9193',value: 0});
        options_arr.push({var_name: 'curox_nan8991',value: 0});
        options_arr.push({var_name: 'curox_nan8789',value: 0});
        options_arr.push({var_name: 'curox_nan8587',value: 1});
        options_arr.push({var_name: 'curox_nan085',value: 0});
      }
      else if(oxygen_sat  >= 0 && oxygen_sat  < 85){
        options_arr.push({var_name: 'curox_nan',value: 0});
        options_arr.push({var_name: 'curox_nan93100',value: 0});
        options_arr.push({var_name: 'curox_nan9193',value: 0});
        options_arr.push({var_name: 'curox_nan8991',value: 0});
        options_arr.push({var_name: 'curox_nan8789',value: 0});
        options_arr.push({var_name: 'curox_nan8587',value: 0});
        options_arr.push({var_name: 'curox_nan085',value: 1});
      }

      if(temperature==0){
        options_arr.push({var_name: 'temp_nan',value: 1});
        options_arr.push({var_name: 'temp_nan101120',value: 0});
        options_arr.push({var_name: 'temp_nan100101',value: 0});
        options_arr.push({var_name: 'temp_nan97100',value: 0});
      }
      else if (temperature<=120 && temperature>101.6) {
        options_arr.push({var_name: 'temp_nan',value: 0});
        options_arr.push({var_name: 'temp_nan101120',value: 1});
        options_arr.push({var_name: 'temp_nan100101',value: 0});
        options_arr.push({var_name: 'temp_nan97100',value: 0});
      }
      else if (temperature<=101.6 && temperature>100.4) {
        options_arr.push({var_name: 'temp_nan',value: 0});
        options_arr.push({var_name: 'temp_nan101120',value: 0});
        options_arr.push({var_name: 'temp_nan100101',value: 1});
        options_arr.push({var_name: 'temp_nan97100',value: 0});
      }
      else if (temperature<=100.4 && temperature>97) {
        options_arr.push({var_name: 'temp_nan',value: 0});
        options_arr.push({var_name: 'temp_nan101120',value: 0});
        options_arr.push({var_name: 'temp_nan100101',value: 0});
        options_arr.push({var_name: 'temp_nan97100',value: 1});
      }

      if(status==0){
          if(move_last==1 && this.qno==1){
          this.setState({ screentype: 'MedicalInAttentionScreen' })
          _that.validationAndApiParameter('save');
        }
        else{
          _that.validationAndApiParameter('save_vitals');
          this.qno++
          this.setState({ qus_no: this.qno })
          this.setState({ countCheck: [], question: arrnew[this.qno].question,qus_id: arrnew[this.qno].qus_id, options: arrnew[this.qno].options,type: arrnew[this.qno].type, suboption : arrnew[this.qno].suboption,modal_title : arrnew[this.qno].modal_title})
        }
      }
      else if(status==1){
        this.setState({ screentype: 'MedicalAttentionScreen' })
        _that.validationAndApiParameter('save');
      }
  }
  else if(this.qno < arrnew.length-1){

    if(this.qno == 0){
      if(countCheck.indexOf('rws_2')!=-1){
        options_arr.push({var_name: 'rws_2',value: 1,});
      }
      else {
        options_arr.push({var_name: 'rws_2',value: 0,});
      }

      if(countCheck.indexOf('rws_3')!=-1){
        options_arr.push({var_name: 'rws_3',value: 1,});
      }
      else {
        options_arr.push({var_name: 'rws_3',value: 0,});
      }

      if(countCheck.indexOf('rws_4')!=-1){
        options_arr.push({var_name: 'rws_4',value: 1,});
      }
      else {
        options_arr.push({var_name: 'rws_4',value: 0,});
      }

      // if rws_1 (No selected)
      if(countCheck.indexOf('rws_1')!=-1){
        options_arr.push({var_name: 'rws_1',value: 1,});
      }
      else{
        options_arr.push({var_name: 'rws_1',value: 0,});
      }
    }
    else if(this.qno == 2){
        next_move_last=1;
        if(countCheck.indexOf('shofb')!=-1){
          next_move_last=0
          options_arr.push({var_name: 'shofb_1',value: 1});
          options_arr.push({var_name: 'shofb_0',value: 0});
        }
        else {
          options_arr.push({var_name: 'shofb_1',value: 0});
          options_arr.push({var_name: 'shofb_0',value: 1});
        }

      if(countCheck.indexOf('cough')!=-1){
        options_arr.push({var_name: 'cough_1',value: 1});
        options_arr.push({var_name: 'cough_0',value: 0});
      }
      else {
        options_arr.push({var_name: 'cough_1',value: 0});
        options_arr.push({var_name: 'cough_0',value: 1});
      }

      // Wheezing
      if(countCheck.indexOf('wheez')!=-1){
        options_arr.push({var_name: 'wheez_1',value: 1});
        options_arr.push({var_name: 'wheez_0',value: 0});
      }
      else {
        options_arr.push({var_name: 'wheez_1',value: 0});
        options_arr.push({var_name: 'wheez_0',value: 1});
        }

      // mucus/phlegm discoloration  mucus/phlegm volume
      if(countCheck.indexOf('mucus_discoloration')!=-1 && countCheck.indexOf('mucus_volume')!=-1){
        options_arr.push({var_name: 'sputum_1',value: 0});
        options_arr.push({var_name: 'sputum_2',value: 0});
        options_arr.push({var_name: 'sputum_3',value: 0});
        options_arr.push({var_name: 'sputum_4',value: 1});
      }else if(countCheck.indexOf('mucus_discoloration')!=-1){
        options_arr.push({var_name: 'sputum_1',value: 0});
        options_arr.push({var_name: 'sputum_2',value: 0});
        options_arr.push({var_name: 'sputum_3',value: 1});
        options_arr.push({var_name: 'sputum_4',value: 0});
      }else if(countCheck.indexOf('mucus_volume')!=-1){
        options_arr.push({var_name: 'sputum_1',value: 0});
        options_arr.push({var_name: 'sputum_2',value: 1});
        options_arr.push({var_name: 'sputum_3',value: 0});
        options_arr.push({var_name: 'sputum_4',value: 0});
      }else{
        options_arr.push({var_name: 'sputum_1',value: 1});
        options_arr.push({var_name: 'sputum_2',value: 0});
        options_arr.push({var_name: 'sputum_3',value: 0});
        options_arr.push({var_name: 'sputum_4',value: 0});
      }

      //Runny nose, sore throat or cold symptoms
      if(countCheck.indexOf('runny_nose')!=-1){
        options_arr.push({var_name: 'inf',value: 1});
      }
      else {
        options_arr.push({var_name: 'inf',value: 0});
      }

      //Waking up at night from breathing  symptoms
      if(countCheck.indexOf('respiratory_symptoms')!=-1){
        options_arr.push({var_name: 'rswy',value: 1});
      }
      else {
        options_arr.push({var_name: 'rswy',value: 0});
      }
    }
    if(next_move_last==1){
      this.setState({ screentype: 'MedicalInAttentionScreen' })
      _that.validationAndApiParameter('save');
    }
    else{
      console.log("next qus");
      this.setState({ countCheck: [] })
      this.qno++
      this.setState({ qus_no: this.qno })
      console.log(this.qno)
      this.setState({ countCheck: [], question: arrnew[this.qno].question, qus_id: arrnew[this.qno].qus_id,options: arrnew[this.qno].options, type: arrnew[this.qno].type, suboption : arrnew[this.qno].suboption,modal_title : arrnew[this.qno].modal_title})
    }
  }
  else {
    if(this.qno == 4){
        if(countCheck.indexOf('cur_dyspnea_1')!=-1){
          options_arr.push({var_name: 'cur_dyspnea_1',value: 1});
          options_arr.push({var_name: 'cur_dyspnea_2',value: 0});
          options_arr.push({var_name: 'cur_dyspnea_3',value: 0});
          options_arr.push({var_name: 'cur_dyspnea_4',value: 0});
          options_arr.push({var_name: 'cur_dyspnea_5',value: 0});
        }
        else if(countCheck.indexOf('cur_dyspnea_2')!=-1){
          options_arr.push({var_name: 'cur_dyspnea_1',value: 0});
          options_arr.push({var_name: 'cur_dyspnea_2',value: 1});
          options_arr.push({var_name: 'cur_dyspnea_3',value: 0});
          options_arr.push({var_name: 'cur_dyspnea_4',value: 0});
          options_arr.push({var_name: 'cur_dyspnea_5',value: 0});
        }
        else if(countCheck.indexOf('cur_dyspnea_3')!=-1){
          options_arr.push({var_name: 'cur_dyspnea_1',value: 0});
          options_arr.push({var_name: 'cur_dyspnea_2',value: 0});
          options_arr.push({var_name: 'cur_dyspnea_3',value: 1});
          options_arr.push({var_name: 'cur_dyspnea_4',value: 0});
          options_arr.push({var_name: 'cur_dyspnea_5',value: 0});
        }
        else if(countCheck.indexOf('cur_dyspnea_4')!=-1){
          options_arr.push({var_name: 'cur_dyspnea_1',value: 0});
          options_arr.push({var_name: 'cur_dyspnea_2',value: 0});
          options_arr.push({var_name: 'cur_dyspnea_3',value: 0});
          options_arr.push({var_name: 'cur_dyspnea_4',value: 1});
          options_arr.push({var_name: 'cur_dyspnea_5',value: 0});
        }
        else if(countCheck.indexOf('cur_dyspnea_5')!=-1){
          options_arr.push({var_name: 'cur_dyspnea_1',value: 0});
          options_arr.push({var_name: 'cur_dyspnea_2',value: 0});
          options_arr.push({var_name: 'cur_dyspnea_3',value: 0});
          options_arr.push({var_name: 'cur_dyspnea_4',value: 0});
          options_arr.push({var_name: 'cur_dyspnea_5',value: 1});
        }
      }
      options_arr.push({var_name:'amiok_todo',value:1});
      var date = new Date().getDate();
      var month = new Date().getMonth() + 1;
      var year = new Date().getFullYear();
      var current_date=year + '-' + month + '-' + date
      options_arr.push({var_name:'amiok_date',value:current_date});

      this.setState({ screentype: 'MedicalInAttentionScreen'})
      _that.validationAndApiParameter('save');
    }
  }

  _answer(status,ans){
    if(this.state.type=="multiple"){
      if(status == false){
        ans_track.push(ans);
      }
      else{
        var index = ans_track.indexOf(ans);
        if (index !== -1) ans_track.splice(index, 1);
        //ans_track.splice(-1,1);
      }
    }
    else if(this.state.type=="single"){
      ans_track.splice(-1,1);
      ans_track.push(ans);
    }

    current_ans=ans;
    this.setState({ countCheck: ans_track })
    //console.log(this.state.countCheck)
  }

  back_click() {
    const resetAction = NavigationActions.navigate({ routeName: 'StartScreen'})
    _that.props.navigation.dispatch(resetAction);
  }

  oxygen() {
        Picker.init({
            pickerData: this.state.items1,
            onPickerConfirm: pickedValue => {
                console.log('area', pickedValue);
                var data= pickedValue.toString()
                console.log('area', data);
                this.setState({oxygen_sat:Number(data)})
            },
            onPickerCancel: pickedValue => {
                console.log('area', pickedValue);
            },
            onPickerSelect: pickedValue => {
                //Picker.select(['山东', '青岛', '黄岛区'])
                console.log('area', pickedValue);
            }
        });
        Picker.show();
    }
    heart() {
        Picker.init({
            pickerData: this.state.items2,
            onPickerConfirm: pickedValue => {
                this.setState({heart_rate:Number(pickedValue)})
            },
            onPickerCancel: pickedValue => {
                console.log('area', pickedValue);
            },
            onPickerSelect: pickedValue => {
                //Picker.select(['山东', '青岛', '黄岛区'])
                console.log('area', pickedValue);
            }
        });
        Picker.show();
    }
    temperature() {
        Picker.init({
            pickerData: this.state.items3,
            onPickerConfirm: pickedValue => {
                this.setState({temperature:Number(pickedValue)})
            },
            onPickerCancel: pickedValue => {
                console.log('area', pickedValue);
            },
            onPickerSelect: pickedValue => {
                //Picker.select(['山东', '青岛', '黄岛区'])
                console.log('area', pickedValue);
            }
        });
        Picker.show();
    }

  render() {
    const headerProp = {
      title: 'Assessing your health!',
      screens: 'AmIOkQusScreen',
    };

    let _this = this
      const currentOptions = this.state.options
      const options = Object.keys(currentOptions).map( function(k) {
        if(k==current_ans){status=true}else{status=false}
        return (  <View key={k} style={{margin:5, }}>
          { _this.state.type=='multiple' ?
            <Animbutton onColor={"#2c808f"} effect={"pulse"}  _onPress={(status) => _this._answer(status,k)} text={currentOptions[k]} />
          :
            <AnimSinglebutton onColor={"#2c808f"} effect={"pulse"} status={status} _onPress={(status) => _this._answer(status,k)} text={currentOptions[k]} />
          }

        </View>)
      });
      var DropDown = <View style={{margin:5}}>
          <TouchableOpacity onPress={()=>this.oxygen()} style={{marginTop: 10,flexDirection:'row', marginLeft: 10,paddingTop:20,paddingBottom:10}}>
            <View style={styles.icon}>
                <Image style={styles.cicon} source={oxy_icon}/>
            </View>
            <View style={styles.texttbox}>
            <Text style={{color:'#000',fontSize:AppSizes.ResponsiveSize.Sizes(18)}}>Oxygen Saturation</Text>
            {this.state.oxygen_sat>0?<Text style={{color:'#000',fontSize:AppSizes.ResponsiveSize.Sizes(18)}}>{this.state.oxygen_sat}</Text>:<Text style={{color:'#bdc3c7',fontSize:AppSizes.ResponsiveSize.Sizes(18)}}>Select Your Oxygen Saturation</Text>}
            </View>
          </TouchableOpacity>
          <View style={{width:'100%',height:2,backgroundColor:'#000'}}/>
          <TouchableOpacity onPress={()=>this.heart()} style={{marginTop: 10,flexDirection:'row', marginLeft: 10,paddingTop:20,paddingBottom:10}}>
            <View style={styles.icon}>
                <Image style={styles.cicon} source={heartRate_icon}/>
            </View>
            <View style={styles.texttbox}>
              <Text style={{color:'#000',fontSize:AppSizes.ResponsiveSize.Sizes(18)}}>Heart Rate</Text>
              {this.state.heart_rate>0?<Text style={{color:'#000',fontSize:AppSizes.ResponsiveSize.Sizes(18)}}>{this.state.heart_rate}</Text>:<Text style={{color:'#bdc3c7',fontSize:AppSizes.ResponsiveSize.Sizes(18)}}>Select Your Heart Rate</Text>}
            </View>
          </TouchableOpacity>
          <View style={{width:'100%',height:2,backgroundColor:'#000'}}/>
          <TouchableOpacity onPress={()=>this.temperature()} style={{marginTop: 10, marginLeft: 10,flexDirection:'row',paddingTop:20,paddingBottom:10}}>
            <View style={styles.icon}>
                <Image style={styles.cicon} source={temp_icon}/>
            </View>
            <View style={styles.texttbox}>
              <Text style={{color:'#000',fontSize:AppSizes.ResponsiveSize.Sizes(18)}}>Temperature</Text>
              {this.state.temperature>0?<Text style={{color:'#000',fontSize:AppSizes.ResponsiveSize.Sizes(18)}}>{this.state.temperature}</Text>:<Text style={{color:'#bdc3c7',fontSize:AppSizes.ResponsiveSize.Sizes(18)}}>Select Your Temperature</Text>}
            </View>
          </TouchableOpacity>
          <View style={{width:'100%',height:2,backgroundColor:'#000'}}/>
      </View>

      {/*var DropDown=<View style={{margin:5, }}>
        <Text style={styles.dropdowntext}>Oxygen Saturation </Text>
        <Picker PickerData={this.state.items1}
         selectedValue={0}
         labelIcon={oxy_icon}
         styleImage={styles.imageContainer}
         placeholder={"Select Your Oxygen Saturation"}
         title={"Oxygen Saturation"}
         isshow={this.state.isshow1}
         getTxt={(val,label)=> this.check_data(val,label,'oxy_sat')}
         />

         <View style={{ paddingVertical: AppSizes.ResponsiveSize.Padding(2) }} />
           <Text style={styles.dropdowntext}>Heart Rate </Text>
           <Picker PickerData={this.state.items2}
           selectedValue={0}
           labelIcon={heartRate_icon}
           styleImage={styles.imageContainer}
           placeholder={"Select Your Heart Rate"}
           title={"Heart Rate"}
           isshow={this.state.isshow2}
           //getTxt={(val,label)=>this.setState({heart_rate: val})}
            getTxt={(val,label)=> this.check_data(val,label,'heart_rate')}
           />

         <View style={{ paddingVertical: AppSizes.ResponsiveSize.Padding(2) }} />
         <Text style={styles.dropdowntext}>Temperature </Text>
           <Picker PickerData={this.state.items3}
            selectedValue={0}
            labelIcon={temp_icon}
            styleImage={styles.imageContainer}
            placeholder={"Select Your Temperature"}
            title={"Temperature"}
            isshow={this.state.isshow3}
            getTxt={(val,label)=> this.check_data(val,label,'temperature')}/>

         <View style={{ paddingVertical: AppSizes.ResponsiveSize.Padding(2) }} />
          <Text style={styles.dropdowntext}>FEV1 </Text>
         <Picker PickerData={this.state.items4}
          selectedValue={0}
          label={'Select Your FEV1'}
          getTxt={(val,label)=>this.setState({fev: val})}/>
      </View>;*/}

      if(this.state.qus_no!=0){
          headerBack=<TouchableOpacity style={styles.menuWrapper} onPress={this.prev}>
                  <Image style={styles.image} source={back_icon}/>
                  </TouchableOpacity>;
        }
        else{
          headerBack=<TouchableOpacity style={styles.menuWrapper} onPress={this.back_click}>
                  <Image style={styles.image} source={back_icon}/>
                  </TouchableOpacity>;
  	  }

    return (
      <View style={styles.container}>
      <LinearGradient colors={['#48c3d5','#3fafc0','#369aaa','#287b88']}>

        <MyStatusBar barStyle="light-content"  backgroundColor="#4eb4c4"/>
          <View style={styles.appBar} >
              <View style={{ flex:1,flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', }}>
                <View style={styles.imageContainer}>
                {headerBack}
                </View>

                <View style={{ marginTop:0,width:'60%',justifyContent: 'center',alignItems:'center' }}>
                    <Text allowFontScaling={false} style={styles.headertitle}>Assessing your health!</Text>
                </View>

                <View style={{width:'15%',flexDirection: 'row',justifyContent: 'flex-end'}}>
        					<TouchableOpacity style={styles.qusWrapper} onPress={() => { this.Show_Custom_Alert(true) }}>
                      <Image style={styles.image} source={qus_icon}/>
                    </TouchableOpacity>
                </View>

              </View>
          </View>


      </LinearGradient>
      <View style={[styles.content,this.state.Modal_Visibility ? {backgroundColor: 'rgba(0,0,0,0.4)',zIndex:5} : '#ffffff']}>

      <ScrollView style={{paddingTop: 10}}>

      <View style={styles.container1}>
            <Text allowFontScaling={false} style={styles.heading}>{this.state.question}</Text>
            <View style={styles.hr}></View>
      </View>

      <View style={styles.container2}>
          { this.state.qus_no != 1 ? options : DropDown }
      </View>

      <View style={styles.container3}>
        <TouchableOpacity style={styles.btn} activeOpacity={.6} onPress={this.next}>
          <CommonButton label='Next'/>
          </TouchableOpacity>
      </View>
    </ScrollView>
    </View>
        <Spinner visible={this.state.isVisible}  />

        <Modal visible={this.state.Modal_Visibility}
          transparent={true}
          animationType={"fade"}
          onRequestClose={ () => { this.Show_Custom_Alert(!this.state.Modal_Visibility)} } >
              <View style={{ flex:1, alignItems: 'center', justifyContent: 'center',zIndex:20,backgroundColor:'#000000a3', }}>
              <View style={styles.Alert_Main_View}>
                  <View style={{ flex:1,flexDirection:'column',alignItems: 'center', justifyContent: 'center',}}>
                  <View style={{height:'15%',width:AppSizes.screen.width/10,paddingTop:'6%',marginBottom:8}}>
                        <TouchableOpacity style={{width:'100%',height:'100%'}} onPress={this.playTrack}>
                          <Image style={styles.image} source={this.state.audioImg}/>
                        </TouchableOpacity>
                      </View>
                      <View style={{height:'70%',flexDirection:'row',alignItems: 'center', justifyContent: 'center',paddingBottom:'2%',}}>
                        <ScrollView contentContainerStyle={styles.modal}>
                          <Text allowFontScaling={false} style={styles.Alert_Message}>
                            {this.state.modal_title}
                          </Text>
                        </ScrollView>
                      </View>
                  </View>
                  <View style={{width:'100%',flex:0.2,  backgroundColor: '#ebebeb',borderBottomLeftRadius:20,borderBottomRightRadius:20,}}>
                      <TouchableOpacity style={styles.buttonStyle} activeOpacity={0.7}
                        onPress={() => { this.Show_Custom_Alert(!this.state.Modal_Visibility)} }  >
                            <Text allowFontScaling={false} style={styles.TextStyle}> CANCEL </Text>
                      </TouchableOpacity>
                  </View>
                </View>
              </View>
          </Modal>

      </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
    backgroundColor: '#F5FCFF',
  },
  content: {
      flex: 1,
  },
  statusBar: {
    height: AppSizes.statusBarHeight,
  },
  appBar: {
      height: AppSizes.navbarHeight,
      justifyContent:'center',
      ...Platform.select({
      android: {
      paddingTop:AppSizes.ResponsiveSize.Padding(4),
      },
    }),
    },
  headertitle:{
    color:'#ffffff',
    fontSize:(Platform.OS === 'ios') ? AppSizes.ResponsiveSize.Sizes(14) :AppSizes.ResponsiveSize.Sizes(16),
    fontWeight:'bold',
    letterSpacing:1,
    textAlign:'center'
  },
  imageContainer:{
    width:'20%'
  },
  menuWrapper: {
    width:'60%',
    height:'60%',
    marginLeft:'30%',
  },
  image: {
    flex: 1,
    width: undefined,
    height: undefined,
    resizeMode:'contain'
  },
  container1:{
    flex:2,
    flexDirection:'column',
    alignItems:'center',
    justifyContent:'center'
  },
  heading:{
    fontSize:AppSizes.ResponsiveSize.Sizes(18),
    color:'#000',
    justifyContent:'center',
    alignItems:'center',
    fontWeight:'600',
    width:'89%',

  },
  hr:{
    width:50,
    height:3,
    backgroundColor:'#000',
    marginTop:10,
},
  container2:{
    flex:6,
    flexDirection:'column',
    padding: AppSizes.ResponsiveSize.Padding(5),
    width:AppSizes.screen.width,
  },
  container3:{
    flex:2,
    justifyContent:'flex-start',
    alignItems:'center',

  },

  dropdowntext:{
    fontSize:AppSizes.ResponsiveSize.Sizes(14),
    fontWeight:'500'
  },
  ansfield:{
    fontSize:AppSizes.ResponsiveSize.Sizes(13),
    color:'#5a5a5a',
  },
  btn:{
    width:'100%',
    marginBottom:20
  },
  imageContainer:{
    flex:1,
    alignItems: 'flex-start',
    //backgroundColor:'red',
  },
  qusWrapper: {
    width:'40%',
    height:'40%',
    marginRight:'30%',
    marginTop:'15%'
  },
  image: {
    flex: 1,
    width: undefined,
    height: undefined,
    resizeMode:'contain'
  },
  Alert_Main_View:{
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#ffffff',
    height:'40%',
    width: '80%',
    borderRadius:20,
    flexDirection:'column',
    justifyContent: 'center',
    alignItems: 'center',
  },

  Alert_Title:{
    fontSize: AppSizes.ResponsiveSize.Sizes(25),
    color: "#000",
    textAlign: 'center',
    padding: 10,
    height: '28%'
  },
    Alert_Message:{
      fontSize: AppSizes.ResponsiveSize.Sizes(18),
      color: "#000",
      textAlign: 'center',
      padding: 10,
    },
    buttonStyle: {
      width: '100%',
      height: '100%',
      justifyContent: 'center',
      alignItems: 'center'
    },
    TextStyle:{
      color:'#000',
      textAlign:'center',
      fontSize: AppSizes.ResponsiveSize.Sizes(18),
    },
    icon:{
      width:'10%',
      marginRight:10,
      alignItems:'center',
      justifyContent:'center',
    },
    texttbox:{
      width:'90%'
    },

    cicon:{
      width:30,
      height:30
    }
});
