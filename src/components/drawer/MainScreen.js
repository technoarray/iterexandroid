import React, { Component } from 'react';
import { AppRegistry, StyleSheet, Text, View, Image, TouchableHighlight, ScrollView, Dimensions, TouchableOpacity, Alert, NetInfo, Platform, AsyncStorage, StatusBar, ImageBackground, } from 'react-native';
import * as stateActions from '../../stateActions';
import { connect } from 'react-redux';
import firebase from 'react-native-firebase';
import { NavigationActions, StackActions } from 'react-navigation';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Spinner from 'react-native-loading-spinner-overlay';
import LinearGradient from 'react-native-linear-gradient';
import { LoginManager, LoginButton, AccessToken, GraphRequest, GraphRequestManager } from 'react-native-fbsdk';
import { GoogleSignin, GoogleSigninButton, statusCodes } from 'react-native-google-signin';
import Orientation from 'react-native-orientation'
import Spacer from '../common/Spacer'
import * as commonFunctions from '../../utils/CommonFunctions'
import CommonButton from '../common/CommonButton'

var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;

const logo = require('../../themes/Images/logo_white.png')
const location = require('../../themes/Images/map_410.png')
const fb_icon = require('../../themes/Images/facebook-logo.png')
const google_icon = require('../../themes/Images/google-plus-logo.png')
const email_icon = require('../../themes/Images/email.png')

var screenN = ''
var page_name = ''
var entry = ''
let _that

const MyStatusBar = ({ backgroundColor, ...props }) => (
    <View style={[styles.statusBar, { backgroundColor }]}>
        <StatusBar translucent backgroundColor={backgroundColor} {...props} />
    </View>
);
export default class MainScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            devicetoken: '',
            devicetype: '',
            method: '',
            username: '',
            email: '',
            password: '',
            isVisible: false,
            loggedInn: false,
            userProfileData: {},
        },
            _that = this;
    }

    componentWillMount() {
        Orientation.lockToPortrait();

        //#region pushnotification
        let channel = new firebase.notifications.Android.Channel('wfth425', 'wfth425 Channel', firebase.notifications.Android.Importance.High).setDescription('My wfth425 channel').setBypassDnd(true).enableVibration(true).setVibrationPattern([100, 200, 300, 400, 500, 400, 300, 200, 400])
        .enableLights(true).setLightColor('#ff0000');
        firebase.notifications().android.createChannel(channel);
        firebase.messaging().hasPermission().then(enabled => {
            if (!enabled) {
                firebase.messaging().requestPermission()
                    .then(() => { }).catch(error => { });
            }
        });
        firebase.messaging().getToken().then(fcmToken => {
            console.log('fcmToken', fcmToken);
            this.setState({ devicetoken: fcmToken })
            AsyncStorage.setItem('DeviceToken', fcmToken)
        })
        firebase.messaging().onTokenRefresh(fcmToken => { });
        //App Closed
        firebase.notifications().getInitialNotification().then((notificationOpen) => {
            console.log(notificationOpen);
            if (notificationOpen) {
                const data = notificationOpen.notification._data;
                console.log('---------**------', data);
                page_name = data.page_name
                this.entry('Firebase')
            }
            else {
              console.log('hi')
              this.entry('DidMount')
            }
        });

        // App in Foreground and background
        this.notificationListener = firebase.notifications().onNotificationOpened((notificationOpen) => {
            console.log(notificationOpen);
            if (notificationOpen) {
                const data = notificationOpen.notification._data;
                console.log('+++++++++/////++++++++', data);
                page_name = data.page_name
                this.entry('Firebase')
            }
            else {
                console.log('hi')
                this.entry('DidMount')
            }
        });
        firebase.notifications().onNotification(async (notification: Notification) => {
          console.log('============notification============');
          console.log(notification);
          console.log('====================================');
          const newNotification = new firebase.notifications.Notification()
            .setNotificationId(new Date().getTime().toString())
            .setTitle(notification.title)
            .setSubtitle(notification.subtitle || "")
            .setBody(notification.body)
            .setData(notification.data)
          {
            Platform.OS == 'android' &&
              newNotification
                .android.setChannelId('wfth425')
          }
          await firebase.notifications().displayNotification(newNotification).catch((err) => {
            console.log('err---****', err);
          });
        });

        //#endregion

    }

    entry(name) {
        console.log('name', name);
        if (name == 'Firebase') {
            AsyncStorage.getItem('loggedIn').then((value) => {
                var loggedIn = JSON.parse(value);
                if (loggedIn) {
                    AsyncStorage.getItem('UserData').then((UserData) => {
                        const udata = JSON.parse(UserData)
                        var uid = udata.id;
                        _that.setState({
                            uid: uid,
                        });
                        var data = {
                            uid: uid,
                            deviceid: _that.state.devicetoken,
                        };
                        console.log(data);
                        _that.postToApiCalling('POST', 'saveToken', Constant.URL_UpdateToken, data);
                    })
                }
            })
        }
        else if (name == 'DidMount') {
            AsyncStorage.getItem('screenName').then((screenName) => {
                console.log(screenName);
                screenN = JSON.parse(screenName);
                if (screenN != "thank") {
                    AsyncStorage.getItem('UserData').then((UserData) => {
                        const udata = JSON.parse(UserData)
                        var uid = udata.id;
                        _that.setState({
                            uid: uid,
                        });
                        var data = {
                            uid: uid,
                            deviceid: _that.state.devicetoken,
                        };
                        console.log(data);
                        _that.postToApiCalling('POST', 'screenName', Constant.URL_UpdateToken, data);
                    })
                }
                else {
                    AsyncStorage.getItem('loggedIn').then((value) => {
                        var loggedIn = JSON.parse(value);
                        if (loggedIn) {
                            AsyncStorage.getItem('UserData').then((UserData) => {
                                const udata = JSON.parse(UserData)
                                var uid = udata.id;
                                _that.setState({
                                    uid: uid,
                                });
                                var data = {
                                    uid: uid,
                                    deviceid: _that.state.devicetoken,
                                };
                                console.log(data);
                                _that.postToApiCalling('POST', 'logged', Constant.URL_UpdateToken, data);
                            })
                        }
                    })
                }
            })
        }
    }

    async componentDidMount() {
        this._configureGoogleSignIn();
    }

    _configureGoogleSignIn() {
        GoogleSignin.configure({
            webClientId: '840704295874-53uo22kq20438ovghqu6f199kmgore91.apps.googleusercontent.com',
            offlineAccess: false,
        });
    }

    _fbAuth() {
        LoginManager.logInWithReadPermissions(['public_profile', 'email']).then(function (result) {
            if (result.isCancelled) {
                alert('Error', 'Something went wrong, Please try again!');
            } else {
                AccessToken.getCurrentAccessToken().then((data) => {
                    let accessToken = data.accessToken
                    // console.log(accessToken.toString()) // access token

                    const responseInfoCallback = (error, result) => {
                        if (error) {
                            console.log(error)
                            alert('Error fetching data: ' + error.toString());
                        } else {
                            console.log(result);
                            var type = Platform.OS == 'ios' ? '1' : '2'
                            var data = {
                                deviceToken: _that.state.devicetoken,
                                deviceType: type,
                                username: result.first_name + " " + result.last_name,
                                email: result.email,
                                facebook_id: result.id,
                            };

                            console.log(data);

                            _that.setState({
                                isVisible: true
                            });
                            _that.postToApiCalling('POST', 'login', Constant.URL_socialLogin, data);
                        }
                    }

                    const infoRequest = new GraphRequest('/me', {
                        accessToken: accessToken,
                        parameters: {
                            fields: {
                                string: 'email,name,first_name,middle_name,last_name'
                            }
                        }
                    }, responseInfoCallback);

                    // Start the graph request.
                    new GraphRequestManager()
                        .addRequest(infoRequest)
                        .start()

                })
            }
        }, function (error) {
            alert('Login fail with error: ' + error);
        });
    }

    _googleAuth = async () => {
        console.log('hi');
        try {
            console.log('hello');
            await GoogleSignin.hasPlayServices();
            const result = await GoogleSignin.signIn();
            console.log('result', result);
            var type = Platform.OS == 'ios' ? '1' : '2'
            var data = {
                deviceToken: _that.state.devicetoken,
                deviceType: type,
                username: result.user.givenName + " " + result.user.familyName,
                email: result.user.email,
                google_id: result.user.id,
            };

            console.log(data);

            _that.setState({
                isVisible: true
            });
            _that.postToApiCalling('POST', 'login', Constant.URL_socialLogin, data);

        } catch (error) {
            if (error.code === statusCodes.SIGN_IN_CANCELLED) {
                // sign in was cancelled
                //Alert.alert('Info','You cancelled the Sign in with Google+');
            } else if (error.code === statusCodes.IN_PROGRESS) {
                // operation in progress already
                //Alert.alert('Info','Your Sign in with Google+ is in Progress');
            } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
                //Alert.alert('play services not available or outdated');
            } else {
                //Alert.alert('Something went wrong', error.toString());
                //this.setState({
                //  error,
                //});
                alert('Error', 'Something went wrong, Please try again!');
            }
        }
    };

    postToApiCalling(method, apiKey, apiUrl, data) {

        new Promise(function (resolve, reject) {
            if (method == 'POST') {
                resolve(WebServices.callWebService(apiUrl, data));
            } else {
                resolve(WebServices.callWebService_GET(apiUrl, data));
            }
        }).then((jsonRes) => {
            console.log(jsonRes);
            if ((!jsonRes) || (jsonRes.code == 0)) {
                setTimeout(() => {                    Alert.alert('Login Error', jsonRes.message);
                }, 200);

            } else {
                if (jsonRes.code == 1) {
                    _that.apiSuccessfullResponse(apiKey, jsonRes)
                }
            }
        }).catch((error) => {
            console.log("ERROR" + error);
            _that.setState({ isVisible: false })

            setTimeout(() => {
                Alert.alert("Server issue");
            }, 200);
        });
    }

    apiSuccessfullResponse(apiKey, jsonRes) {
        if (apiKey == 'login' || apiKey == 'register') {
            _that.setState({
                isVisible: false,
                userProfileData: jsonRes.data
            });
            _that.setState({ isVisible: false })

            AsyncStorage.setItem("loggedIn", JSON.stringify(true)).done();
            AsyncStorage.setItem('UserData', JSON.stringify(jsonRes.data));
            var verify_invitation = jsonRes.data.verify_invitation;
            //console.log(verify_invitation)
            if (verify_invitation == 0) {
                AsyncStorage.setItem('screenName', JSON.stringify('invitationScreen'));
                _that.props.navigation.navigate('invitationScreen');
            }
            else {
                var vitals = []
                vitals.push({ name: 'oxygen_sat', value: jsonRes.vitals.oxygen_sat });
                vitals.push({ name: 'heart_rate', value: jsonRes.vitals.heart_rate });
                vitals.push({ name: 'temperature', value: jsonRes.vitals.temperature });
                //console.log(vitals)
                AsyncStorage.setItem('vitals', JSON.stringify(vitals));

                AsyncStorage.setItem('screenName', JSON.stringify('thank'));
                const resetAction = StackActions.reset({
                    index: 0,
                    actions: [NavigationActions.navigate({ routeName: 'StartScreen' })],
                });
                _that.props.navigation.dispatch(resetAction);
            }
        }
        if (apiKey == 'saveToken') {
            console.log('hi');
            if (page_name == 'AmIOkQusScreen') {
                this.props.navigation.navigate(page_name, { qus_no: 0, name: 'HomeScreen' });
            }
            else if (page_name == 'HADSQusScreen') {
                this.props.navigation.navigate(page_name, { qus_no: 0 });
            }
            else {
                this.props.navigation.navigate(page_name);
            }

        }
        if (apiKey == 'logged') {
            console.log('hi');
            _that.props.navigation.navigate('StartScreen');
        }
        if (apiKey == 'screenName') {
            console.log(screenN);
            _that.props.navigation.navigate(screenN);
        }
    }

    userLogin() {
        _that.props.navigation.navigate('LoginScreen');
    }

    userSignup() {
        _that.props.navigation.navigate('invitationScreen', { socail: 'no' });
    }



    static navigationOptions = {
        header: null,
    }


    render() {
        return (
            <View style={styles.wrapper}>
                <MyStatusBar barStyle="light-content" backgroundColor="#4eb4c4" />
                <LinearGradient colors={['#48c3d5', '#3fafc0', '#369aaa', '#287b88']} style={{ flex: 1 }}>

                    <View style={styles.container1} />

                    <View style={styles.container2}>
                        <View style={{ width: AppSizes.screen.width - 180, height: AppSizes.screen.width / 4, justifyContent: 'center', marginLeft: AppSizes.ResponsiveSize.Padding(5) }}>
                            { /* <Image source={logo} style={{flexGrow:1,height:null,width:null,alignItems: 'center',justifyContent:'center',resizeMode:'contain',}} /> */}
                        </View>
                    </View>

                    <View style={styles.container4}>
                        <View style={{ width: AppSizes.screen.width, height: AppSizes.screen.width / 4, justifyContent: 'center', }}>
                            <Image source={logo} style={{ flexGrow: 1, height: null, width: null, alignItems: 'center', justifyContent: 'center', resizeMode: 'contain' }} />
                        </View>
                    </View>
                    <View style={styles.container5}>

                        <TouchableOpacity activeOpacity={.6} onPress={this._fbAuth} style={[styles.buttonSocailContainer, { backgroundColor: '#4867aa' }]}>
                            <View style={{ width: '100%', height: '100%', justifyContent: 'center' }}>
                                <View style={styles.buttonimgContainer}>
                                    <Image source={fb_icon} style={styles.btnimg} />
                                </View>
                                <View style={styles.buttontextContainer}>
                                    <Text allowFontScaling={false} style={styles.socaillogin}>Login with Facebook</Text>
                                </View>
                            </View>
                        </TouchableOpacity>


                        <TouchableOpacity activeOpacity={.6} onPress={this._googleAuth} style={[styles.buttonSocailContainer, { backgroundColor: '#d34338' }]}>
                            <View style={{ width: '100%', height: '100%', justifyContent: 'center' }}>
                                <View style={styles.buttonimgContainer}>
                                    <Image source={google_icon} style={styles.btnimg} />
                                </View>
                                <View style={styles.buttontextContainer}>
                                    <Text allowFontScaling={false} style={styles.socaillogin}>Sign in with Google+</Text>
                                </View>
                            </View>
                        </TouchableOpacity>

                        <TouchableOpacity activeOpacity={.6} onPress={this.userLogin} style={[styles.buttonContainer, { backgroundColor: '#ffffff' }]}>
                            <View>
                                <View style={styles.buttonimgContainer}>
                                    {/* <Image source={email_icon} style={styles.btnimg} /> */}
                                </View>
                                <View style={styles.buttontextContainer}>
                                    <Text allowFontScaling={false} style={styles.textlogin}>Sign in via email</Text>
                                </View>
                            </View>
                        </TouchableOpacity>

                        <TouchableOpacity activeOpacity={.6} onPress={this.userSignup} style={styles.buttonContainer}>
                            <View>
                                <View style={styles.buttonimgContainer}>

                                </View>
                                <View style={styles.buttontextContainer}>
                                    <Text allowFontScaling={false} style={styles.textcreateAccount}>Create account</Text>
                                </View>
                            </View>
                        </TouchableOpacity>


                    </View>


                </LinearGradient>
            </View>
        );
    }
}

const styles = {
    statusBar: {
        height: AppSizes.statusBarHeight,
    },
    appBar: {
        height: AppSizes.navbarHeight,
        justifyContent: 'center',
    },
    wrapper: {
        flex: 1,
        backgroundColor: '#386b98'
    },
    container1: {
        flex: 2,
        alignItems: 'center',
        //backgroundColor:'#36c4d8'

    },
    container2: {
        flex: 2,
        flexDirection: 'column',
        justifyContent: 'center',
        //backgroundColor:'red'
    },
    container3: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',//replace with flex-end or center
        justifyContent: 'center',
        //backgroundColor:'#36c4d8'
    },

    container4: {
        flex: 3,
        flexDirection: 'column',
        alignItems: 'center',//replace with flex-end or center
        justifyContent: 'center',
        marginBottom: AppSizes.ResponsiveSize.Padding(5),
        //backgroundColor:'blue'
    },
    container5: {
        flex: 10,
        flexDirection: 'column',
        alignItems: 'center',//replace with flex-end or center
        justifyContent: 'flex-start',
        //backgroundColor:'yellow'
    },
    buttonContainer: {
        justifyContent: 'center',
        marginBottom: AppSizes.ResponsiveSize.Padding(3),
        width: '90%',
        height: AppSizes.screen.width / 8,
        borderWidth: 1,
        borderColor: '#ffffff',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        position: 'relative'
    },
    buttonimgContainer: {
        width: '15%',
        height: AppSizes.screen.width / 15,
        position: 'absolute',
        left: '5%',
        top: '20%'
    },
    buttontextContainer: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    buttonSocailContainer: {
        justifyContent: 'center',
        marginBottom: AppSizes.ResponsiveSize.Padding(3),
        width: '90%',
        height: AppSizes.screen.width / 8,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        position: 'relative'
    },

    textcreateAccount: {
        color: '#ffffff',
        fontSize: AppSizes.ResponsiveSize.Sizes(14),
        fontWeight: '500'
    },
    textlogin: {
        color: AppColors.primary,
        fontSize: AppSizes.ResponsiveSize.Sizes(14),
        fontWeight: '500',
    },
    socaillogin: {
        color: '#fff',
        fontSize: AppSizes.ResponsiveSize.Sizes(14),
        fontWeight: '500',
    },
    btnimg: {
        flex: 1,
        width: undefined,
        height: undefined,
        resizeMode: 'contain',
    },
    imageWrapper: {
        width: '100%',
        height: AppSizes.screen.width / 8,
    },
    image: {
        flex: 1,
        width: undefined,
        height: undefined,
        resizeMode: 'contain'
    },
}
