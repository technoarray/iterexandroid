import React, { Component } from 'react';
import {StyleSheet,Text,View,Image,TouchableOpacity,Platform,Alert,ScrollView,TextInput,Button} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions } from 'react-navigation'
import Spacer from '../common/Spacer'
import Header from '../common/signupHeader'
import CommonButton from '../common/CommonButton'
import Orientation from 'react-native-orientation'
import * as commonFunctions from '../../utils/CommonFunctions'

var Constant = require('../../api/ApiRules').Constant;

/* Images */
var warning = require('../../themes/Images/warning.png');
var logo = require('../../themes/Images/logo.png');



export default class MedicalAttentionScreen extends Component {
  constructor (props) {
      super(props)
      this.state = {
      isVisible: false,
      Modal_Visibility: false
    },
    _that = this;
  }

  componentDidMount(){
    Orientation.lockToPortrait();
  }

	closebtn(){
    _that.props.navigation.navigate('infoQusScreen', {qus_no: 0,  name: 'infoQusScreen' });
	}
  render() {
    const headerProp = {
      title: 'Am I OK?',
      screens: 'MedicalAttentionScreen',
      qus_content:'',
    };

    return (
      <View style={styles.container}>
        <Header info={headerProp} navigation={_that.props.navigation} updateAlert={this.updateAlert}/>
        <View style={styles.content}>
        <View style={styles.container1}>

            <View style={styles.boxiconcontainer}>
              <Image style={styles.boxicon} source={warning}/>
          </View>
          <View style={styles.thankcon}>
            <Text allowFontScaling={false} style={styles.head}>You need immediate medical attention. Please go to the emergency room or call 911.</Text>
          </View>


        </View>
        <View style={{borderBottomWidth:2,borderBottomColor:'#f5f5f5'}}/>

        <View style={styles.container3}>
            <Text allowFontScaling={false} style={styles.subhead}>This application is not a substitute for professional medical consultation. If you feel that you are experiencing a life-threating emergency. Please call 911.If you are experiencing other symtoms not listed here, but do not feel that you are experiencing an emergency. Please call your doctor.</Text>
        </View>
        <View style={styles.container2}>
          <TouchableOpacity activeOpacity={.6} onPress={this.closebtn}>
            <CommonButton label='Close'/>
            </TouchableOpacity>
        </View>
       </View>
      </View>


    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
	backgroundColor:'#ffffff'
  },
  container1:{
    flex:6,
    flexDirection:'column',
    alignItems:'center',
  },
  container2:{
     justifyContent: 'flex-end',
     flex:1,
     marginBottom:20,
  },

  container3:{
     flex:2,

    alignItems:'center',
    justifyContent:'center'
  },
  content: {
    flex: 1,
    padding:  AppSizes.ResponsiveSize.Padding(5),
  },

  boxiconcontainer:{
    width:AppSizes.screen.width/3,
    height:AppSizes.screen.width/3,
    justifyContent:'center',
  },

  boxicon:{
    width:null,
    height:null,
    flexGrow:1
  },
  thank:{
    width:'20%',
    flexDirection:'column',
  },
  thankcon:{
    justifyContent:'center',
    textAlign:'center',
    width:'90%'
  },
 head:{
   fontSize:Platform.OS === 'ios' ? AppSizes.ResponsiveSize.Sizes(25) : AppSizes.ResponsiveSize.Sizes(32),
   color:'#000',
    justifyContent:'center',
    textAlign:'center',
    fontWeight:'800',
    marginBottom:10
  },
  subhead:{
     fontSize:AppSizes.ResponsiveSize.Sizes(12),
     color:'#000',
     justifyContent:'center',
     textAlign:'center',
     marginBottom:10
   },
  thankcontent:{
      textAlign:'center',
  },

  logoblk:{
    width:'80%',
    height:'50%',

  }

});
