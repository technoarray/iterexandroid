import React, { Component } from 'react';
import {
    View, AsyncStorage,Text, Alert
} from 'react-native';
import * as stateActions from '../../stateActions';
import { connect } from 'react-redux';
import {NavigationActions} from 'react-navigation';
import Orientation from 'react-native-orientation'

let _that
class LogoutScreen extends Component {

    constructor(props) {
        super(props)
        _that = this
    }

    componentWillMount() {
      Orientation.lockToPortrait();
      AsyncStorage.removeItem('loggedIn');
      AsyncStorage.removeItem('UserData');
      AsyncStorage.removeItem('vitals');
      AsyncStorage.removeItem('screenName');
      AsyncStorage.removeItem('group')

      AsyncStorage.removeItem('heart_rate_vitaldate');
      AsyncStorage.removeItem('oxy_sat_vitaldate');
      AsyncStorage.removeItem('temp_vitaldate');
      AsyncStorage.removeItem('user_email', function () {
            Alert.alert("You are logout successfully.")
              _that.props.navigation.navigate('MainScreen');
            // const resetAction = NavigationActions.navigate({ routeName: 'MainScreen'})
            //  _that.props.navigation.dispatch(resetAction);
        });
    }

    static navigationOptions = {
        header: null,
    }
    render() {
        return (
        	<View><Text allowFontScaling={false}>Logout</Text></View>
        );
    }
}

const mapStateToProps = store => ({
    currentUser: store.currentUser,
});

const mapDispatchToProps = {
    updateCurrentUser: stateActions.updateCurrentUser,
};

export default connect(mapStateToProps, mapDispatchToProps)(LogoutScreen);
