import {
    AppRegistry,
    StyleSheet,
    Dimensions,
    Button,
    Image,
    View,
    Text,
    ScrollView,
    TouchableOpacity,
    TouchableHighlight
} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import React from 'react';

import { createDrawerNavigator } from 'react-navigation';

import HomeScreen from './HomeScreen';
import HomeScreen2 from './HomeScreen2';
import AccountScreen from './AccountScreen';
import AboutScreen from './AboutScreen';
import SettingScreen from './SettingScreen';
import PoliciesScreen from './PoliciesScreen';
import LogoutScreen from './LogoutScreen';
import SideMenu from './SideMenu';
import Header from '../common/Header';
import signupHeader from '../common/signupHeader';


export default createDrawerNavigator({
    HomeScreen: { screen: HomeScreen},
    HomeScreen2: { screen: HomeScreen2},
    AccountScreen: { screen: AccountScreen},
    AboutScreen: { screen: AboutScreen},
    SettingScreen: { screen: SettingScreen},
    PoliciesScreen: { screen: PoliciesScreen},
    LogoutScreen: { screen: LogoutScreen},
    signupHeader: { screen: signupHeader},
    Header: { screen: Header},
}, {
  contentComponent: SideMenu,
  drawerWidth: AppSizes.screen.width
});
