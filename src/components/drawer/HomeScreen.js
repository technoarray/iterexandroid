import React, { Component } from 'react';
import {AsyncStorage,StyleSheet,Text,View,Image,TouchableOpacity,Platform,Alert,Modal,ScrollView} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions } from 'react-navigation'
import Spacer from '../common/Spacer'
import Spinner from 'react-native-loading-spinner-overlay';
import Sound from 'react-native-sound'
import Orientation from 'react-native-orientation'
import Header from '../common/Header'
import * as commonFunctions from '../../utils/CommonFunctions'

var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;

let _that
/* Images */
var heart_icon=require('../../themes/Images/heart_icon_196.png')
var share_icon=require('../../themes/Images/share.png')
var medical_icon=require('../../themes/Images/medical_icon_196.png')
var lungs_icon=require('../../themes/Images/lungs_icon_196.png')
var arrow_icon=require('../../themes/Images/arrow_icon_196.png')
var mice=require('../../themes/Images/mice.png')
var mice_off=require('../../themes/Images/mice-off.png')

var help_audio=require('../../themes/sound/HomeHelp.mp3')

export default class HomeScreen extends Component {
  constructor (props) {
      super(props)
      this.state = {
      uid:'',
      oxygen_sat: '-',
      heart_rate: '-',
      temperature:'-',
      isVisible: false,
      Modal_Visibility: false,
      audioStatus: false,
      audioImg: mice,
      modal_title:'',
      account_type:'',
      oxygen_date:'',
      heart_date:'',
      temperature_date:''
    },
    _that = this;
  }


  componentWillMount() {
    Orientation.unlockAllOrientations();
    Orientation.lockToPortrait();
    AsyncStorage.setItem('screenName', JSON.stringify('HomeScreen'));

    AsyncStorage.getItem('loggedIn').then((value) => {
        var loggedIn = JSON.parse(value);
        console.log(loggedIn);
        if (loggedIn) {
          AsyncStorage.getItem('UserData').then((UserData) => {
              const data = JSON.parse(UserData)
              console.log(data)
              var uid=data.id;
              var type=data.account_type
              console.log(type);
              _that.setState({
                uid:  uid,
              });
              if(type=='asthma'){
                _that.props.navigation.navigate('HomeScreen2');
              }
            //console.log('uid='+_that.state.uid)
            _that.validationAndApiParameter('showdata');
          })
        }
    })
  }

  validationAndApiParameter(apiname) {
      //console.log(apiname)
      //console.log('uid='+_that.state.uid)
      if(apiname == 'showdata'){
        var data = {
          uid: this.state.uid
        };
        console.log(data);
        _that.setState({isVisible: true});
        this.postToApiCalling('POST', 'showdata', Constant.URL_showVarData,data);
      }
    }

  postToApiCalling(method, apiKey, apiUrl, data) {
    //console.log(apiUrl)
      new Promise(function(resolve, reject) {
          if (method == 'POST') {
              resolve(WebServices.callWebService(apiUrl, data));
          } else {
              resolve(WebServices.callWebService_GET(apiUrl, data));
          }
      }).then((jsonRes) => {
          console.log(jsonRes)
          _that.setState({ isVisible: false })
          if ((!jsonRes) || (jsonRes.code == 0)) {
            setTimeout(() => {
                  Alert.alert(jsonRes.message);
              }, 200);
          } else {
              if (jsonRes.code == 1) {
                  _that.apiSuccessfullResponse(apiKey, jsonRes)
              }
          }
      }).catch((error) => {
          console.log("ERROR" + error);
          _that.setState({ isVisible: false })
          setTimeout(() => {
              Alert.alert("Server Error");
          }, 200);
      })
  }

  apiSuccessfullResponse(apiKey, jsonRes) {
    if (apiKey == 'showdata') {
      const jdata = jsonRes.profile
      console.log(jdata)
      if(jdata.show_oxygen==1){
        this.setState({oxygen_sat : jdata.oxygen_sat})
      }
      if(jdata.show_heart==1){
        this.setState({heart_rate : jdata.heart_rate})
      }
      if(jdata.show_temp==1){
        this.setState({temperature : jdata.temperature})
      }
      const date_date=jsonRes.vitals
      if(date_date.oxygen_date!=null){
        this.setState({oxygen_date:date_date.oxygen_date})
      }
      if(date_date.heart_date!=null){
        this.setState({heart_date:date_date.heart_date})
      }
      if(date_date.temperature_date!=null){
        this.setState({temperature_date:date_date.temperature_date})
      }
    }
  }

  updateAlert = (visible) => {
        this.setState({Modal_Visibility: !this.state.Modal_Visibility});
  }

 userAmIOk=()=>{
   _that.props.navigation.navigate('AmIOkQusScreen', {qus_no: 0,  name: 'HomeScreen' });
 }

 tracking=()=>{
   _that.props.navigation.navigate('TrackingScreen');
 }

healthProfile=()=>{
  _that.props.navigation.navigate('HealthProfileScreen', {oxy_sat: _that.state.oxygen_sat,  heart_rate: _that.state.heart_rate,temperature: _that.state.temperature});
}

skipBTN=(valid_date,type,current_date)=>{
  console.log(valid_date+" "+type+" "+current_date)
  AsyncStorage.setItem(valid_date, JSON.stringify(current_date));
  _that.props.navigation.navigate('ChartScreen', {vital_type:type })
}

chartbtn1(vital_type,type){
  var vital_date=this.state.oxygen_date;
  var today = new Date();

  var date = new Date().getDate();
  var month = new Date().getMonth() + 1;
  var year = new Date().getFullYear();
  var current_date=year + '-' + month + '-' + date
  if(current_date !== vital_date){
    Alert.alert(
    vital_type,
    'You haven’t entered your '+vital_type+' today. If you wish to, please measure and enter it below',
    [
      {text: 'SKIP', onPress: this.skipBTN.bind(this,"oxy_sat_vitaldate",type,current_date)},
      {text: 'OK', onPress: () => _that.props.navigation.navigate('VitalScreen', {qus_no: 0,  name: 'VitalScreen' }) },
    ],
    { cancelable: false }
    )
  }
  else{
    _that.props.navigation.navigate('ChartScreen', {vital_type:type });
  }
}

  chartbtn2(vital_type,type){
    var vital_date=this.state.heart_date;
    var today = new Date();

    var date = new Date().getDate();
    var month = new Date().getMonth() + 1;
    var year = new Date().getFullYear();
    var current_date=year + '-' + month + '-' + date
    console.log('current_date',current_date);
    console.log('vital_date',vital_date);
    if(current_date !== vital_date){
        Alert.alert(
        vital_type,
        'You haven’t entered your '+vital_type+' today. If you wish to, please measure and enter it below',
        [
          {text: 'SKIP', onPress: this.skipBTN.bind(this,"heart_rate_vitaldate",type,current_date)},
          {text: 'OK', onPress: () => _that.props.navigation.navigate('VitalScreen', {qus_no: 0,  name: 'VitalScreen' }) },
        ],
        { cancelable: false }
      )
    }
    else{
        _that.props.navigation.navigate('ChartScreen', {vital_type:type });
        console.log(type);
    }
  }

chartbtn3(vital_type,type){
  var vital_date=this.state.temperature_date;
  var today = new Date();
  var date = new Date().getDate();
  var month = new Date().getMonth() + 1;
  var year = new Date().getFullYear();
  var current_date=year + '-' + month + '-' + date
  if(current_date !== vital_date){
      Alert.alert(
      vital_type,
      'You haven’t entered your '+vital_type+' today. If you wish to, please measure and enter it below',
      [
        {text: 'SKIP', onPress: this.skipBTN.bind(this,"temp_vitaldate",type,current_date)},
        {text: 'OK', onPress: () => _that.props.navigation.navigate('VitalScreen', {qus_no: 0,  name: 'VitalScreen' }) },
      ],
      { cancelable: false }
    )
  }
  else{
      _that.props.navigation.navigate('ChartScreen', {vital_type:type });
  }

}


shareClick=()=>{
  _that.props.navigation.navigate('ShareScreen');
}

  render() {
  //  console.log("hello");
    var popup_data=<Text>
    <Text>The Home page enables easy access to your health tools, just tap or click on the one you want to use.{"\n\n"}</Text>
    <Text><Text style={{fontWeight:'bold'}}>•	Am I OK?</Text> Use this at least once a week or if you are not feeling well.{"\n\n"}</Text>
    <Text><Text style={{fontWeight:'bold'}}>•	To Do</Text> Check regularly for health tasks that require periodic updates or to access questionnaires.{"\n\n"}</Text>
    <Text><Text style={{fontWeight:'bold'}}>•	Health Profile</Text> Use this to check or update your baseline health profile.{"\n\n"}</Text>
    <Text>• Share icon Use this to share your data with your physician.</Text></Text>;

    const headerProp = {
      title: 'HOME',
      screens: 'HomeScreen',
      qus_content:popup_data,
      qus_audio:help_audio
    };
    return (

      <View style={styles.container}>
        <Header info={headerProp} navigation={_that.props.navigation} updateAlert={this.updateAlert}/>

       <View style={[styles.content,this.state.Modal_Visibility ? {backgroundColor: 'rgba(0,0,0,0.4)',zIndex:5} : '#ffffff']}>
          <View style={{flex:.5,alignItems:'center', justifyContent: 'center'}}>
          </View>

          <View style={{flex:3,flexDirection:'row',marginBottom:'2%'}} >
          <View style={[styles.boxDesign,styles.shadow]}>

          <TouchableOpacity activeOpacity={.6} onPress={this.userAmIOk}>
              <Text allowFontScaling={false} style={styles.boxtitle}>AM  I  OK</Text>
              </TouchableOpacity>
         </View>
          <View style={{position: 'absolute',top: '35%',right: 0,width: '25%',height: '70%',zIndex:2}}>
            <Image style={styles.boxicon} source={lungs_icon}/>
          </View>
      </View>

        <View style={{flex:3,flexDirection:'row',justifyContent: 'space-around',marginBottom:'2%'}}>
        <TouchableOpacity activeOpacity={.6} onPress={this.tracking} style={[styles.boxDesign1,styles.shadow]}>

              <View style={styles.boxiconcontainer}>
                <Image source={heart_icon} style={styles.boxicon} />
              </View>
              <View>
                 <Text style={styles.boxtitle1} allowFontScaling={false}>TO DO</Text>
              </View>
           </TouchableOpacity>
            <View style={{position: 'absolute',bottom: '15%',left:'35%',width: '20%',height: '20%'}}>
              <Image style={styles.boxicon} source={arrow_icon}/>
            </View>

            <TouchableOpacity activeOpacity={.6} onPress={this.healthProfile} style={[styles.boxDesign1,styles.shadow]}>

              <View style={styles.boxiconcontainer}>
                <Image source={medical_icon} style={styles.boxicon} />
              </View>
              <View>
                 <Text style={styles.boxtitle1} allowFontScaling={false}>HEALTH PROFILE</Text>
              </View>
           </TouchableOpacity>
            <View style={{position: 'absolute',bottom: '15%',left:'85%',width: '20%',height: '20%'}}>
              <Image style={styles.boxicon} source={arrow_icon}/>
            </View>

        </View>

        <View style={{flex:3}}>
          <View style={styles.boxDesign2}>
          <TouchableOpacity activeOpacity={.6} onPress={this.chartbtn1.bind(this,'Oxygen Saturation','oxy_sat')} style={[styles.boxcontainer1,styles.shadow]}>
            <Text style={styles.boxnumber} allowFontScaling={false}>{this.state.oxygen_sat}</Text>
            <Text allowFontScaling={false} style={styles.boxtitle1}>O<Text allowFontScaling={false} style={styles.thintext}>2</Text> Saturation</Text>
          </TouchableOpacity>

          <TouchableOpacity activeOpacity={.6} onPress={this.chartbtn2.bind(this,'Heart Rate','heart_rate')} style={[styles.boxcontainer1,styles.shadow]}>
            <Text style={styles.boxnumber} allowFontScaling={false}>{this.state.heart_rate}</Text>
            <Text style={styles.boxtitle1} allowFontScaling={false}>Heart Rate</Text>
          </TouchableOpacity>

          <TouchableOpacity activeOpacity={.6} onPress={this.chartbtn3.bind(this,'Temperature','temp')} style={[styles.boxcontainer1,styles.shadow]}>
            <Text style={styles.boxnumber} allowFontScaling={false}>{this.state.temperature}</Text>
            <Text style={styles.boxtitle1} allowFontScaling={false}>Temperature</Text>
          </TouchableOpacity>

          </View>
        </View>

        <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>

        <View style={[styles.imageContainer,{alignItems:'center',justifyContent:'center'}]}>

          <TouchableOpacity style={styles.menuWrapper} activeOpacity={.6} onPress={this.shareClick}>
            <Image style={styles.image} source={share_icon}/>
          </TouchableOpacity>
        </View>
        </View>

      </View>
      <Spinner visible={this.state.isVisible}  />

    </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
  },

  content: {
      flex: 1,
      height:'70%',
      paddingLeft:'5%',
      paddingRight:'5%',
      paddingBottom:'5%',
  },
  headTitle:{
    textAlign:'center',
    fontWeight:'500',
    fontSize: AppSizes.ResponsiveSize.Sizes(16),
    color:'#5397a0'
  },
  boxDesign:{
    flexDirection: 'row',
    width: '90%',
    alignItems:'center',
    justifyContent: 'center',
    height:'65%',
    position: 'relative',
    marginBottom:'6%',
    marginLeft:'2%',
    position:'relative'
  },
  boxDesign1:{
    width:'42%',
     height:'80%',
    justifyContent:'center',
    alignItems:'center',
    position: 'relative',
   marginRight:'5%',
  },
  boxDesign2:{
    height:'78%',
    flexDirection: 'row',
    flexWrap:'wrap',
    alignItems:'center',
    justifyContent: 'space-between',
    marginBottom:'2%',

  },
  shadow:{
    borderWidth:1,
    borderRadius: 2,
    borderColor: '#fff',
    justifyContent:'center',
    backgroundColor:'#fff',
    borderColor: '#ddd',
    borderBottomWidth: 0,
    shadowColor: '#999',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 0,
    zIndex:0,
    //backgroundColor:'red',
 },
  boxtitle:{
    textAlign:'center',
    fontWeight:'bold',
    fontSize:AppSizes.ResponsiveSize.Sizes(32),
    color:AppColors.primary,
    shadowOpacity: 0,
  },

  boxcontainer1:{
    width:'32%',
    height:'100%',
    justifyContent:'center',
    alignItems:'center',

  },
  boxiconcontainer:{
    width:AppSizes.screen.width-150,
    height:'50%',
    justifyContent: 'center',
    paddingBottom:'10%'
  },
  boxicon:{
    flexGrow:1,
    height:null,
    width:null,
    alignItems: 'center',
    justifyContent:'center',
    resizeMode:'contain',
  },
  boxtitle1:{
    fontSize: AppSizes.ResponsiveSize.Sizes(12),
    fontWeight:'bold',
    color:AppColors.primary,
    textAlign:'center',
    shadowOpacity: 0,
    marginBottom:AppSizes.ResponsiveSize.Padding(5)
  },

  imageContainer:{
    width:'20%',
  },
  menuWrapper: {
    width:90,
    height:90,
  },

  image: {
    flex: 1,
    width: 90,
    height: 90,
    resizeMode:'contain'
  },
  boxnumber:{
    fontSize:AppSizes.ResponsiveSize.Sizes(25),
    color:AppColors.contentColor,
    fontWeight:'bold',
    textAlign:'center'
  },
  spaceBox:{
    marginBottom:'10%'
  },
  extrapad:{
    paddingTop:AppSizes.ResponsiveSize.Padding(14)
  },
  thintext:{
  fontSize:AppSizes.ResponsiveSize.Sizes(6)
},
que:{
  position:'absolute',
  right:10,
  top:4
},
que2:{
  position:'absolute',
  right:-13,
  top:5,
  zIndex:1111
},
qusWrapper1:{
  backgroundColor:'#2596a6',
  height:25,
  width:25,
  padding:6,
  borderRadius:30
},
qusWrapper: {
  width:'40%',
  height:'40%',
  marginRight:'30%',
  marginTop:'15%'
},
image: {
  flex: 1,
  width: undefined,
  height: undefined,
  resizeMode:'contain'
},
Alert_Main_View:{
  alignItems: 'center',
  justifyContent: 'center',
  backgroundColor: '#ffffff',
  height:'40%',
  width: '80%',
  borderRadius:20,
  flexDirection:'column',
  justifyContent: 'center',
  alignItems: 'center',
},

Alert_Title:{
  fontSize: AppSizes.ResponsiveSize.Sizes(25),
  color: "#000",
  textAlign: 'center',
  padding: 10,
  height: '28%'
},
  Alert_Message:{
    fontSize: AppSizes.ResponsiveSize.Sizes(18),
    color: "#000",
    textAlign: 'center',
    padding: 10,
  },
  buttonStyle: {
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  TextStyle:{
    color:'#000',
    textAlign:'center',
    fontSize: AppSizes.ResponsiveSize.Sizes(18),
  },

});
