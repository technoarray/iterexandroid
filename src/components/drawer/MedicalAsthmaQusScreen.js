import React, { Component } from 'react';
import {AsyncStorage,StyleSheet,Text,View,Image,TouchableOpacity,Platform,Alert,ScrollView,Modal,TextInput,Button,StatusBar,AppState,} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions } from 'react-navigation'
import Spacer from '../common/Spacer'
import Header from '../common/signupHeader'
import CommonButton from '../common/CommonButton'
import Swiper from 'react-native-swiper';
import Animbutton from './animAnsbutton'
import AnimSinglebutton from './animSingleAnsOptions';
import { jsonData } from '../data/Question';
import { pickerData } from '../data/pickerData';
import LinearGradient from 'react-native-linear-gradient';
import Spinner from 'react-native-loading-spinner-overlay';
import FloatingLabelBox from '../common/FloatingLabelBox'
import FloatingLabel from 'react-native-floating-labels';
import Orientation from 'react-native-orientation'
import * as commonFunctions from '../../utils/CommonFunctions'

var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;

/* Images */
//var thankyou_icon = require('../../themes/Images/thank-you.png');
var logo = require('../../themes/Images/logo.png');
var back_icon = require( '../../themes/Images/left-whitearrow.png')
var cigrate_icon = require('../../themes/Images/inhaler.png');


let arrnew = []
let ans_track = []
let profile_data = []
let qus_result =[]
let final_profile_data = []
var unknown_arr=[]
let options_arr=[]
let comborbities=[]
let final_options_arr=[]
let python_result =[]
let python_array =[]
let vitals=[]
var current_ans=''
var modal=false
var current_date

const MyStatusBar = ({backgroundColor, ...props}) => (
  <View style={[styles.statusBar, { backgroundColor }]}>
    <StatusBar translucent backgroundColor={backgroundColor} {...props} />
  </View>
);

let _that
export default class MedicalQusScreen extends Component {
  constructor (props) {
      super(props)
      this.qno = 0

      const jdata = jsonData.medicalqus.qus2
      arrnew = Object.keys(jdata).map( function(k) { return jdata[k] });
      this.state = {
        question : arrnew[this.qno].question,
        options : arrnew[this.qno].options,
        suboption : arrnew[this.qno].suboption,
        type : arrnew[this.qno].type,
        countCheck : [],
        qus_no : 0,
        uid :'',
        gender :'',
        birthdate :'',
        height :'',
        weight:'',
        screenName:'',
        cigrate_data:pickerData.cigrates,
        python_result:[],
        python_array:[],
        isVisible: false,
        Modal_Visibility: false,
        status:false,
        scrollPosition:'',
        primary:'asthma'
    },
    _that = this;
  }

  componentDidMount() {
    Orientation.lockToPortrait();
    AsyncStorage.getItem('screenName').then((screenName) => {
      var screenName = JSON.parse(screenName);
      _that.setState({screenName:screenName})
      console.log("screenName "+screenName)
    })

    AsyncStorage.getItem('loggedIn').then((value) => {
        var loggedIn = JSON.parse(value);
        if (loggedIn) {
            AsyncStorage.getItem('UserData').then((UserData) => {
                const data = JSON.parse(UserData)
                _that.setState({
                  uid:data.id,
                });
            })
        }
    })
    var date = new Date().getDate();
    var month = new Date().getMonth() + 1;
    var year = new Date().getFullYear();

    current_date=year + '-' + month + '-' + date
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.question !== this.state.question) {
      setTimeout(() => {
        _that.scroll_to_top();
      }, 400)
    }
  }

  scroll_to_top=()=>{
    _that.scroll.scrollTo({x: 0, y: 0, animated: false})
  }

  scroll_to_end=()=>{
    _that.scroll.scrollToEnd({animated: true})
  }

  updateAlert = () => {
        this.setState({Modal_Visibility: !this.state.Modal_Visibility});
  }

  validationAndApiParameter(apiname) {
      if(apiname == 'web_profile'){

        var json_options_arr = JSON.stringify(options_arr);
        var data = {
            uid: this.state.uid,
            type:this.state.primary,
            variable : json_options_arr,
        };
        console.log(data);
        _that.setState({isVisible: true});
        this.postToApiCalling('POST', apiname, Constant.URL_asthmaVariables,data);
      }
      else if(apiname=='comborbities'){
        var json=JSON.stringify(comborbities)
        var data={
          uid:this.state.uid,
          variable:json
        }
        console.log(data);
        _that.setState({isVisible: true});
        this.postToApiCalling('POST', apiname, Constant.URL_comborboties,data);
      }
    }

  postToApiCalling(method, apiKey, apiUrl, data) {
    console.log(data);
      new Promise(function(resolve, reject) {
          if (method == 'POST') {
              resolve(WebServices.callWebService(apiUrl, data));
          } else {
            console.log('Hello');
              resolve(WebServices.callWebService_POST(apiUrl, data));
          }
      }).then((jsonRes) => {
         console.log(jsonRes)
            _that.setState({ isVisible: false })
            if ((!jsonRes) || (jsonRes.code == 0)) {
              setTimeout(() => {
                    Alert.alert(jsonRes.message);
                }, 200);
            } else {
                if (jsonRes.code == 1) {
                    _that.apiSuccessfullResponse(apiKey, jsonRes)
                }
            }
      }).catch((error) => {
          console.log("ERROR" + error);
          _that.setState({ isVisible: false })
          setTimeout(() => {
              Alert.alert("Server Error" + error);
          }, 200);
      })
  }

  apiSuccessfullResponse(apiKey, jsonRes) {
    console.log(jsonRes);

    if(apiKey=="web_profile"){
      arrnew = []
      ans_track = []
      profile_data = []
      qus_result =[]
      final_profile_data = []
      unknown_arr=[]
      options_arr=[]
      final_options_arr=[]
      python_result =[]
      python_array =[]
      vitals=[]
      current_ans=''
      console.log(jsonRes);

      AsyncStorage.setItem('UserData', JSON.stringify(jsonRes.data));
      _that.props.navigation.navigate('Thanku', {message: 'Thank you for setting up your health profile',screen:'medical'});
    }
    else if(apiKey=='comborbities'){
      console.log(jsonRes);
      comborbities=[]
    }
  }

  prev= () =>{
    //console.log(this.qno)
    if(this.qno > 0){
        this.qno--
      //console.log(this.qno)

      this.setState({ qus_no: this.qno })
      this.setState({ countCheck: [],question: arrnew[this.qno].question, options: arrnew[this.qno].options, type: arrnew[this.qno].type,suboption : arrnew[this.qno].suboption})
    }
  }
  next= () =>{

    // variables
    current_ans=0
    final_thank=0;
    const { countCheck} = this.state
    console.log('countCheck',countCheck);
    if(this.qno==4){
      options_arr.push({var_name: 'use_of_inhaler',  value:this.state.cigrates});
    }

    if((this.qno==3 || this.qno==4) && this.state.countCheck == 0){
      Alert.alert("Invalid Value","Missing or blank value is not allowed")
    }
    else if(this.qno < arrnew.length-1){

      if(this.qno == 0){

          // coronary_artery_disease
          if(countCheck.indexOf('CND_CD_CAD')!=-1){
            profile_data[27]=1
          options_arr.push({var_name: 'CND_CD_CAD',  value: 1});
          }
          else {
            profile_data[27]=0
            options_arr.push({var_name: 'CND_CD_CAD',  value: 0});
          }

          // congestive_heart_failure
          if(countCheck.indexOf('CND_CD_CHF')!=-1){
            profile_data[25]=1
            options_arr.push({var_name: 'CND_CD_CHF',value: 1,});
          }
          else {
            profile_data[25]=0
            options_arr.push({var_name: 'CND_CD_CHF',value: 0});
          }

          // high_blood_pressure
          if(countCheck.indexOf('CND_HYP')!=-1){
            profile_data[29]=1
            options_arr.push({var_name: 'CND_HYP',value: 1});
          }
          else {
            profile_data[29]=0
            options_arr.push({var_name: 'CND_HYP',value: 0});
          }

          // anemia
          if(countCheck.indexOf('CBY_ANE')!=-1){
            profile_data[19]=1
            options_arr.push({var_name: 'CBY_ANE',value: 1});
          }
          else {
            profile_data[19]=0
            options_arr.push({var_name: 'CBY_ANE',value: 0});
          }

          // chronic_kidney_disease
          if(countCheck.indexOf('CND_CKD')!=-1){
            profile_data[17]=1
            options_arr.push({var_name: 'CND_CKD',value: 1});
          }
          else {
            profile_data[17]=0
            options_arr.push({var_name: 'CND_CKD',value: 0});
          }

          // diabetes
          if(countCheck.indexOf('CND_DIA')!=-1){
            profile_data[30]=1
            options_arr.push({var_name: 'CND_DIA',value:1});
          }
          else {
            profile_data[30]=0
            options_arr.push({var_name: 'CND_DIA',value:0});
          }

          // acid_reflux
          if(countCheck.indexOf('CND_GERD')!=-1){
            profile_data[26]=1
            options_arr.push({var_name: 'CND_GERD',value: 1});
          }
          else {
            profile_data[26]=0
            options_arr.push({var_name: 'CND_GERD',value: 0});
          }

          // copd
          if(countCheck.indexOf('CND_PL_COPD')!=-1){
            profile_data[20]=1
            options_arr.push({var_name: 'CND_PL_COPD',value: 1});
          }
          else {
            profile_data[20]=0
            options_arr.push({var_name: 'CND_PL_COPD',value: 0});
          }

      }
      else if(this.qno == 1){

        if(countCheck.indexOf('CND_STEROIDS')!=-1){
          profile_data[21]=1
          options_arr.push({var_name: 'CND_STEROIDS',value: 1});
        }
        else {
          profile_data[21]=0
          options_arr.push({var_name: 'CND_STEROIDS',value: 0});
        }

        if(countCheck.indexOf('CBY_DOM_LA')!=-1){
          profile_data[18]=1
          options_arr.push({var_name: 'CBY_DOM_LA',value: 1});
        }
        else {
          profile_data[18]=0
          options_arr.push({var_name: 'CBY_DOM_LA',value: 0});
        }

        if(countCheck.indexOf('OUTP_HOSP')!=-1){
          profile_data[28]=1
          options_arr.push({var_name: 'OUTP_HOSP',value: 1});
        }
        else {
          profile_data[28]=0
            options_arr.push({var_name: 'OUTP_HOSP',value: 0});
        }

        if(countCheck.indexOf('OUTP_ICU')!=-1){
          profile_data[23]=1
          options_arr.push({var_name: 'OUTP_ICU',value: 1});
        }
        else {
          profile_data[23]=0
          options_arr.push({var_name: 'OUTP_ICU',value: 0});
        }

        if(countCheck.indexOf('CBY_DOM_ASS')!=-1){
          profile_data[24]=1
          options_arr.push({var_name: 'CBY_DOM_ASS',value: 1});
        }
        else {
          profile_data[24]=0
          options_arr.push({var_name: 'CBY_DOM_ASS',value: 0});
        }

        if(countCheck.indexOf('PRF_BH_SMK')!=-1){
          profile_data[22]=1
          options_arr.push({var_name: 'PRF_BH_SMK',value: 1});
        }
        else {
          profile_data[22]=0
          options_arr.push({var_name: 'PRF_BH_SMK',value: 0});
        }

        if(profile_data[23] == 1 || profile_data[28] ==1){
          options_arr.push({var_name: 'SYM_EXAC',value: 1});
        }
        else {
          options_arr.push({var_name: 'SYM_EXAC',value: 0});
        }

    }
      else if(this.qno == 2){
        if(countCheck.indexOf('inhaler')!=-1){
          comborbities.push({var_name: 'inhaler',value: 1})
        }
        else{
          comborbities.push({var_name: 'inhaler',value: 0})
        }

        if(countCheck.indexOf('low')!=-1){
          comborbities.push({var_name: 'low',value: 1})
        }
        else{
          comborbities.push({var_name: 'low',value: 0})
        }

        if(countCheck.indexOf('medium')!=-1){
          comborbities.push({var_name: 'medium',value: 1})
        }
        else{
          comborbities.push({var_name: 'medium',value: 0})
        }

        if(countCheck.indexOf('high')!=-1){
          comborbities.push({var_name: 'high',value: 1})
        }
        else{
          comborbities.push({var_name: 'high',value: 0})
        }

        if(countCheck.indexOf('longg')!=-1){
          comborbities.push({var_name: 'longg',value: 1})
        }
        else{
          comborbities.push({var_name: 'longg',value: 0})
        }

        if(countCheck.indexOf('combined')!=-1){
          comborbities.push({var_name: 'combined',value: 1})
        }
        else{
          comborbities.push({var_name: 'combined',value: 0})
        }

        if(countCheck.indexOf('injectible')!=-1){
          comborbities.push({var_name: 'injectible',value: 1})
        }
        else{
          comborbities.push({var_name: 'injectible',value: 0})
        }
        //console.log(countCheck)
        if(countCheck.indexOf('injectible') !=-1){
          options_arr.push({var_name: 'MED_RIH',value: 0});
          options_arr.push({var_name: 'MED_RST_0',  value: 0});
          options_arr.push({var_name: 'MED_RST_1',  value: 0});
          options_arr.push({var_name: 'MED_RST_2',  value: 0});
          options_arr.push({var_name: 'MED_RST_3',  value: 1});
        }
        else if (countCheck.indexOf('longg') !=-1) {
          options_arr.push({var_name: 'MED_RIH',value: 0});
          options_arr.push({var_name: 'MED_RST_0',  value: 0});
          options_arr.push({var_name: 'MED_RST_1',  value: 0});
          options_arr.push({var_name: 'MED_RST_2',  value: 1});
          options_arr.push({var_name: 'MED_RST_3',  value: 0});
        }
        else{
          if(countCheck.indexOf('inhaler')!=-1 && (countCheck.indexOf('low')==-1 && countCheck.indexOf('medium')==-1 && countCheck.indexOf('high')==-1 && countCheck.indexOf('injectible')==-1 && countCheck.indexOf('combined')==-1 )){
            profile_data[7]=1
            modal=false
            this.setState({Modal_Visibility:false})
            options_arr.push({var_name: 'MED_RIH',value: 1});
          }
          else if(countCheck.indexOf('inhaler')!=-1 && (countCheck.indexOf('low')==-1 || countCheck.indexOf('medium')==-1 || countCheck.indexOf('high')==-1 || countCheck.indexOf('injectible')==-1 || countCheck.indexOf('combined')==-1 )){
            profile_data[7]=0
            console.log('in-2');
            modal=false
            this.setState({Modal_Visibility:false})
            options_arr.push({var_name: 'MED_RIH',value: 0});
          }
          else{
            profile_data[7]=0
            this.setState({Modal_Visibility:true})
            modal=true
            console.log('in-3');
            options_arr.push({var_name: 'MED_RIH',value: 0});
          }

          if(countCheck.indexOf('low')!=-1 && countCheck.indexOf('inhaler')!=-1){
            profile_data[8]=1
            options_arr.push({var_name: 'MED_RST_0',  value: 1});
          }
          else {
            profile_data[8]=0
            options_arr.push({var_name: 'MED_RST_0',  value: 0});
          }

          if(countCheck.indexOf('medium')!=-1 && countCheck.indexOf('inhaler')!=-1){
            profile_data[9]=1
            options_arr.push({var_name: 'MED_RST_1',  value: 1});
          }
          else {
            profile_data[9]=0
            options_arr.push({var_name: 'MED_RST_1',  value: 0});
          }
        }

        _that.validationAndApiParameter('comborbities')
      }

      if(this.qno==3){
        if(countCheck.indexOf('option1')!=-1){
          options_arr.push({var_name: 'dose_of_prednisone',  value: 'Yes'});
        }
        else{
          options_arr.push({var_name: 'dose_of_prednisone',  value: 'No'});
        }
      }

      if(this.qno==2 && modal||this.state.Modal_Visibility){
        console.log(this.qno);
      }else{
        this.qno++
        this.setState({qus_no:this.qno})
        ans_track =[];

        if(final_thank != 1){
          this.setState({ countCheck: [], question: arrnew[this.qno].question, options: arrnew[this.qno].options, type: arrnew[this.qno].type, suboption : arrnew[this.qno].suboption});
        }
      }

      if(this.qno == 5){
        final_thank =1
      }
   }
    else{
     final_thank=1;
    }



    if(final_thank == 1) {
      options_arr.push({var_name: 'medical_que_date',value:current_date})
      _that.validationAndApiParameter('web_profile');
    }
  }

  skip(){
    this.setState({Modal_Visibility:false})
    modal=false
    this.qno++
    this.setState({qus_no:this.qno})
    ans_track =[];

    if(final_thank != 1){
      this.setState({ countCheck: [], question: arrnew[this.qno].question, options: arrnew[this.qno].options, type: arrnew[this.qno].type, suboption : arrnew[this.qno].suboption});
    }
    console.log('hello');
  }

  _answer(status,ans){
    //console.log(this.ans)
    if(this.state.type=="multiple"){
      if(status == false){
        ans_track.push(ans);
      }
      else{
        var index = ans_track.indexOf(ans);
        if (index !== -1) ans_track.splice(index, 1);
        //ans_track.splice(-1,1);
      }
    }
    else if(this.state.type=="single"){
      ans_track.splice(-1,1);
      ans_track.push(ans);
    }

    if(this.qno ==0 || this.qno ==2){
      if(ans !=='copd'){
        unknown_arr.push(ans);
      }
    }
  //  console.log(unknown_arr)
    current_ans=ans;
    this.setState({ countCheck: ans_track })
  }

  back_btn(){
    _that.props.navigation.navigate("MedicalQusSelectionScreen");
  }

  render() {
    //console.log(this.state.python_result);
    const headerProp = {
      title: 'Medical Profile setup!',
      screens: 'MedicalQusScreen',
    };
    let _this = this
      const currentOptions = this.state.options
      const options = Object.keys(currentOptions).map( function(k) {
        if(k==current_ans){status=true}else{status=false}
        return (  <View key={k} style={{margin:5, }}>
          { _this.state.type=='multiple' ?
            <Animbutton onColor={"#2c808f"} effect={"pulse"}  _onPress={(status) => _this._answer(status,k)} text={currentOptions[k]} />
          :
            <AnimSinglebutton onColor={"#2c808f"} effect={"pulse"} status={status} _onPress={(status) => _this._answer(status,k)} text={currentOptions[k]} />
          }

        </View>)
      });

      var DropDown=<View style={{height:60,}}>
       <FloatingLabelBox labelIcon={cigrate_icon}>
         <FloatingLabel
             labelStyle={styles.labelInput}
             inputStyle={styles.input}
             style={styles.formInput}
             value={this.state.cigrates}
             placeholder='Select'
             ref="cigrates"
             autoCapitalize = 'none'
             keyboardType={ 'numeric'}
             returnKeyType = { 'done' }
             onChangeText={cigrates=> {this.setState({cigrates}),this.setState({countCheck:parseInt(cigrates)})}}
           >How much time inhaler is used</FloatingLabel>
       </FloatingLabelBox>
        {/*<Picker PickerData={this.state.cigrate_data}
         labelIcon={cigrate_icon}
         styleImage={styles.imageContainer}
         placeholder={"How much time inhaler is used"}
         getTxt={(val,label)=>this.setState({cigrates:val,countCheck:val})}/>*/}

      </View>;


      if(this.state.qus_no!=0){
        headerBack=<TouchableOpacity style={styles.menuWrapper} onPress={this.prev}>
                  <Image style={styles.image} source={back_icon}/>
                  </TouchableOpacity>;
        }
        else{
          headerBack=<TouchableOpacity style={styles.menuWrapper} onPress={this.back_btn}>
                    <Image style={styles.image} source={back_icon}/>
                    </TouchableOpacity>;
      }

    return (
      <View style={styles.container}>
      <LinearGradient colors={['#48c3d5','#3fafc0','#369aaa','#287b88']}>

        <MyStatusBar barStyle="light-content"  backgroundColor="#4eb4c4"/>
          <View style={styles.appBar} >
              <View style={{ flex:1,flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', }}>
                <View style={styles.imageContainer}>
                {headerBack}
                </View>

                <View style={{ marginTop:0,width:'60%',justifyContent: 'center',alignItems:'center' }}>
                    <Text allowFontScaling={false} style={styles.headertitle}>Medical Profile setup!</Text>
                </View>

                <View style={{width:'15%',flexDirection: 'row',justifyContent: 'flex-end'}}>
                  {/* <TouchableOpacity style={styles.qusWrapper} onPress={() => { this.Show_Custom_Alert(true) }}>
                            <Image style={styles.image} source={qus_icon}/>
                            </TouchableOpacity> */}
                </View>

              </View>
          </View>


      </LinearGradient>

        <ScrollView style={{backgroundColor: '#F5FCFF',paddingTop: 10}}
        ref={(c) => {this.scroll = c}} >
		<View style={styles.container1}>
          <Text allowFontScaling={false} style={styles.heading}>  {this.state.question} </Text>
          <View style={styles.hr}></View>
      </View>
      <View style={styles.container2}>
            { this.qno != 4 ? options : DropDown }
      </View>
      <View style={styles.container3}>
        <TouchableOpacity style={styles.btn} activeOpacity={.6} onPress={this.next}>
          <CommonButton label='Next'/>
          </TouchableOpacity>
      </View>

      </ScrollView>

      <Modal visible={this.state.Modal_Visibility}
        transparent={true}
        animationType={"fade"}
        onRequestClose={ () => { this.Show_Custom_Alert(!this.state.Modal_Visibility)} } style={styles.mm}>
          <View style={{ flex:1, alignItems: 'center', justifyContent: 'center',zIndex:20,backgroundColor:'#000000a3', }}>
            <View style={styles.Alert_Main_View}>
                <View style={{ flex:1,flexDirection:'column',alignItems: 'center', justifyContent: 'center',}}>
                    <View style={{height:'70%',flexDirection:'row',alignItems: 'center', justifyContent: 'center',paddingBottom:'2%',}}>
                      <ScrollView contentContainerStyle={styles.modal}>
                        <Text allowFontScaling={false} style={styles.Alert_Message}>
                          You have not selected Albuterol Inhaler or Rescue Nebulizer. Please select it now or contact your physician for advice on obtaining one.
                        </Text>
                      </ScrollView>
                    </View>
                </View>
                <View style={{width:'100%',flex:0.2,flexDirection:'row', backgroundColor: '#ebebeb',borderBottomLeftRadius:20,borderBottomRightRadius:20,}}>
                    <TouchableOpacity style={[styles.buttonStyle,styles.border]} activeOpacity={0.7}
                      onPress={() => {this.setState({Modal_Visibility:false}),modal=false}}  >
                          <Text allowFontScaling={false} style={styles.TextStyle}> Ok </Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.buttonStyle} activeOpacity={0.7}
                      onPress={() => this.skip()  }>
                          <Text allowFontScaling={false} style={styles.TextStyle}> Skip </Text>
                    </TouchableOpacity>
                </View>
              </View>
            </View>
        </Modal>

      <Spinner visible={this.state.isVisible}  />

      </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
  },
  statusBar: {
    height: AppSizes.statusBarHeight,
  },
  appBar: {
      height: AppSizes.navbarHeight,
      justifyContent:'center',
      ...Platform.select({
      android: {
      paddingTop:AppSizes.ResponsiveSize.Padding(4),
      },
    }),
    },
  headertitle:{
    color:'#ffffff',
    fontSize:AppSizes.ResponsiveSize.Sizes(16),
    fontWeight:'bold',
    letterSpacing:1,
  },
  imageContainer:{
    width:'20%'
  },
  menuWrapper: {
    width:'60%',
    height:'60%',
    marginLeft:'30%',
  },
  image: {
    flex: 1,
    width: undefined,
    height: undefined,
    resizeMode:'contain'
  },
  container1:{
    flex:2,
    flexDirection:'column',
    alignItems:'center',
    justifyContent:'center'
  },
  heading:{
    fontSize:AppSizes.ResponsiveSize.Sizes(18),
    color:'#000',
    justifyContent:'center',
    alignItems:'center',
    fontWeight:'600',
    width:'89%',

  },
  hr:{
    width:50,
    height:3,
    backgroundColor:'#000',
    marginTop:10,

  },
  container2:{
    flex:6,
    flexDirection:'column',
    width:AppSizes.screen.width,
   paddingLeft:AppSizes.ResponsiveSize.Padding(5),
   paddingRight:AppSizes.ResponsiveSize.Padding(5),
   paddingTop:AppSizes.ResponsiveSize.Padding(2),
  },
  container3:{
    flex:2,
    justifyContent:'flex-start',
    alignItems:'center',

  },
  ans:{
    borderBottomWidth:1,
    borderColor:'#000',
    paddingTop:5,
    paddingBottom:10,
    width:'90%',
    marginBottom:25,

  },
  border:{
    borderRightWidth:2,
    borderColor:'blue'
  },
    Alert_Message:{
      fontSize: AppSizes.ResponsiveSize.Sizes(18),
      color: "#000",
      textAlign: 'center',
      padding: 10,
    },
    buttonStyle: {
      width: '50%',
      height: '100%',
      justifyContent: 'center',
      alignItems: 'center',
    },
    Alert_Main_View:{
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: '#ffffff',
      height:'40%',
      width: '80%',
      borderRadius:20,
      flexDirection:'column',
      justifyContent: 'center',
      alignItems: 'center',

    },
    mm:{
      backgroundColor:'red'
    },
  ansfield:{
    fontSize:AppSizes.ResponsiveSize.Sizes(13),
    color:'#5a5a5a',
  },
  btn:{
    width:'100%',
    marginBottom:20
  },
  labelInput: {
    color: AppColors.contentColor,
    //fontWeight:'300',
    fontSize:AppSizes.ResponsiveSize.Sizes(13),
  },
  input: {
    borderWidth: 0,
    color: AppColors.contentColor,
    fontWeight:'300',
    fontSize:AppSizes.ResponsiveSize.Sizes(15),
  },
  formInput: {
    borderWidth: 0,
    borderColor: '#333',
  },
});
