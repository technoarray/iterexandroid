import React, { Component } from 'react';
import {AsyncStorage,StyleSheet,Text,View,Image,TouchableOpacity,Platform,Alert,ScrollView,TextInput,Button,StatusBar,Modal} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions } from 'react-navigation'
import Spacer from '../common/Spacer'
import Spinner from 'react-native-loading-spinner-overlay';
import Header from '../common/signupHeader'
import CommonButton from '../common/CommonButton'
import Swiper from 'react-native-swiper';
import Animbutton from './animAnsbutton'
import AnimSinglebutton from './animSingleAnsOptions';
import RNPickerSelect from 'react-native-picker-select';
import { jsonData } from '../data/Question';
import { pickerData } from '../data/pickerData';
import FloatingLabelBox from '../common/FloatingLabel'
import FloatingLabel from 'react-native-floating-labels';
import Picker from 'react-native-q-picker';
import Sound from 'react-native-sound'
import Orientation from 'react-native-orientation'
import LinearGradient from 'react-native-linear-gradient';
import CountDown from 'react-native-countdown-component';

import * as commonFunctions from '../../utils/CommonFunctions'

var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;

/* Images */
//var thankyou_icon = require('../../themes/Images/thank-you.png');
var logo = require('../../themes/Images/logo.png');
var back_icon = require( '../../themes/Images/left-whitearrow.png')
var oxy_icon = require('../../themes/Images/oxy.png');
var temp_icon = require('../../themes/Images/temp.png');
var heartRate_icon = require('../../themes/Images/heart_rate.png');
var qus_icon = require('../../themes/Images/que_icon_196.png');
var mice=require('../../themes/Images/mice.png')
var mice_off=require('../../themes/Images/mice-off.png')
var weight_icon = require('../../themes/Images/weight.png');

var help_audio1=require('../../themes/sound/amiokQus1Help.mp3')
var help_audio2=require('../../themes/sound/amiokQus2Help.mp3')
var help_audio3=require('../../themes/sound/amiokQus3Help.mp3')
var help_audio4=require('../../themes/sound/amiokQus4Help.mp3')

let _that
let arrnew = []
let ans_track = []
let qus_result = new Array();
let final_arr = new Array();
var final_exacerb_data=[]
let final_triage_data = []
let final_symptom_data =[]
let final_vital_data = []
let final_med_data = []
let final_profile_data = []
let python_result=[]
let python_array=[]
let options_arr=[]
let vitals=[]
let vitals_data=[]
let vitals_api=[]
var current_ans=''
var move_last=0;
var move=0;
var next_move_last='';
var before=''
var after=''
var vital=0
var heart_base=''
var current_date

const MyStatusBar = ({backgroundColor, ...props}) => (
  <View style={[styles.statusBar, { backgroundColor }]}>
    <StatusBar translucent backgroundColor={backgroundColor} {...props} />
  </View>
);

var heart_data = [];
var fev_data= [];
var temperature_data= [];
var selected=[]
var que =[]


//heart_data
for( var j = 30; j <151; j++){
  var numbers = {
      name: j+' bpm',
      id: j
  };
  heart_data.push(numbers);
}

//fev_data
for( var k = 0; k <7; k++){
  var numbers = {
      name: k+'L',
      id: k
  };
  fev_data.push(numbers);
}

export default class AmIOkQusScreen extends Component {
  constructor (props) {
      super(props)
      this.qno = 4
      this.score = 0
      this.type ='qus'
      this.inputRefs = {};

      const jdata = jsonData.amiokasthmaqus.qus1
      arrnew = Object.keys(jdata).map( function(k) { return jdata[k] });

      this.state = {
        question : arrnew[this.qno].question,
        title : arrnew[this.qno].title,
        options : arrnew[this.qno].options,
        suboption : arrnew[this.qno].suboption,
        type : arrnew[this.qno].type,
        modal_title:arrnew[this.qno].modal_title,
        countCheck : [],
        dataStorage:[],
        qus_no : 0,
        uid:'',
        isVisible: false,
        Modal_Visibility: false,
        status:'ans',
        screentype:'',
        fev: 0,
        python_result:[],
        python_array:[],
        api:0,
        audioStatus: false,
        audioImg: mice,
  },
    _that = this;
  }

  playTrack = () => {
    this.setState({audioStatus: !this.state.audioStatus})
    if(this.state.audioStatus==true){
      if(this.qno==0){
        help_audio=help_audio1;
      }
      else if(this.qno==1){
        help_audio=help_audio2;
      }
      else if(this.qno==2){
        help_audio=help_audio3;
      }
      else{
        help_audio=help_audio4;
      }

      this.track = new Sound(help_audio,  (e) => {
        if (e) {
          console.log('error loading track:', e)
        } else {

            this.setState({audioImg: mice_off});
            this.track.play((success) => {
                 if (success) {
                     this.stop();
                 }
             });
          }
        })
      }
        else {
          this.stop();
        }
      }

  stop() {
     if (!this.track) return;
     this.track.stop();
     this.track.release();
     this.track = null;
     this.setState({audioStatus: false});
     this.setState({audioImg: mice});
 }

  Show_Custom_Alert(visible) {
    this.stop();
    this.setState({Modal_Visibility: visible});
  }

  componentDidMount() {
    Orientation.lockToPortrait();
    AsyncStorage.getItem('loggedIn').then((value) => {
        var loggedIn = JSON.parse(value);
        if (loggedIn) {
            AsyncStorage.getItem('UserData').then((UserData) => {
                const data = JSON.parse(UserData)
                console.log(data);
                var uid=data.uid;
                _that.setState({uid:  uid});
            })
       }
    })
    var date = new Date().getDate();
    var month = new Date().getMonth() + 1;
    var year = new Date().getFullYear();
    current_date=year + '-' + month + '-' + date
  }

  validationAndApiParameter(apiname,uid) {
    if(apiname == 'medication'){
      qus_result=[]
      var json_options_arr = JSON.stringify(options_arr);

      var data = {
          uid: this.state.uid,
          type:'asthma',
          variable : json_options_arr,
      };
      _that.setState({isVisible: true});
      this.postToApiCalling('POST', apiname, Constant.URL_asthmaVariables,data);
    }
  }

postToApiCalling(method, apiKey, apiUrl, data) {
  new Promise(function(resolve, reject) {
      if (method == 'POST') {
          resolve(WebServices.callWebService(apiUrl, data));
      } else {
          resolve(WebServices.callWebService_POST(apiUrl, data));
      }
  }).then((jsonRes) => {
    console.log(jsonRes)
    if ((!jsonRes) || (jsonRes.code == 0)) {
      setTimeout(() => {
            Alert.alert(jsonRes.message);
        }, 200);
    } else {
      _that.apiSuccessfullResponse(apiKey, jsonRes)
    }
  }).catch((error) => {
      console.log("ERROR" + error);
      _that.setState({ isVisible: false })
      setTimeout(() => {
          Alert.alert("Server Error");
      }, 200);
  })
}

apiSuccessfullResponse(apiKey, jsonRes) {
  if(apiKey=='medication'){
    _that.setState({ isVisible: false })
    arrnew = []
    ans_track = []
    qus_result = new Array();
    final_arr = new Array();
    symptom_data = []
    triage_data = []
    vital_data = []
    exacerb_data = []
    profile_data=[]
    final_exacerb_data=[]
    final_triage_data = []
    final_symptom_data =[]
    final_vital_data = []
    final_med_data = []
    final_profile_data=[]
    python_result=[]
    python_array=[]
    options_arr=[]
    vitals=[]
    vitals_api=[]
    vitals_data=[]
    current_ans=''
    move_last=0;
    next_move_last='';
    _that.props.navigation.navigate('StartScreen');
  }
}

  updateAlert = () => {
        this.setState({Modal_Visibility: !this.state.Modal_Visibility});
  }

  prev(){
    _that.props.navigation.navigate('AsthmaHealthProfile')
  }

  next= () =>{
    current_ans=0
    final_thank=0;

    const { countCheck} = this.state
    ans_track =[];

    que.push(this.qno)

    if(this.qno==4){
      if(countCheck.indexOf('MED_COMP_25')!=-1){
        options_arr.push({var_name: 'MED_COMP_25',value: 1});
      }
      else {
        options_arr.push({var_name: 'MED_COMP_25',value: 0});
      }

      if(countCheck.indexOf('MED_COMP_50')!=-1){
        options_arr.push({var_name: 'MED_COMP_50',value: 1});
      }
      else {
        options_arr.push({var_name: 'MED_COMP_50',value: 0});
      }
      if(countCheck.indexOf('MED_COMP_75')!=-1){
        options_arr.push({var_name: 'MED_COMP_75',value: 1});
      }
      else {
        options_arr.push({var_name: 'MED_COMP_75',value: 0});
      }
      if(countCheck.indexOf('MED_COMP_100')!=-1){
        options_arr.push({var_name: 'MED_COMP_100',value: 1});
      }
      else {
        options_arr.push({var_name: 'MED_COMP_100',value: 0});
      }
    }

    if(this.state.countCheck.length <= 0 && this.state.qus_no!=1 && this.state.qus_no!=7){

      Alert.alert("Invalid Value","Missing or blank value is not allowed")
    }
    else{
      final_thank=1
    }

    if(final_thank != 1){
      this.setState({ countCheck: [], question: arrnew[this.qno].question, options: arrnew[this.qno].options, type: arrnew[this.qno].type, suboption : arrnew[this.qno].suboption});
    }
    if(final_thank ==1) {
      options_arr.push({var_name: 'amiok_date',value:current_date})
        _that.validationAndApiParameter('medication',this.state.uid);
    }
  }

  _answer(status,ans){
    if(this.state.type=="multiple"){
      if(status == false){
        ans_track.push(ans);
      }
      else{
        var index = ans_track.indexOf(ans);
        if (index !== -1) ans_track.splice(index, 1);
        //ans_track.splice(-1,1);
      }
    }
    else if(this.state.type=="single"){
      ans_track.splice(-1,1);
      ans_track.push(ans);
    }

    current_ans=ans;
    this.setState({ countCheck: ans_track })
  }

  back_click() {
    _that.props.navigation.navigate('AsthmaHealthProfile')
  }

  render() {
    const headerProp = {
      title: 'Assessing your health!',
      screens: 'AmIOkQusScreen',
    };

    let _this = this
      const currentOptions = this.state.options
      const options = Object.keys(currentOptions).map( function(k) {
        if(k==current_ans){status=true}else{status=false}
        return (  <View key={k} style={{margin:5, }}>
          { _this.state.type=='multiple' ?
            <Animbutton onColor={"#2c808f"} effect={"pulse"}  _onPress={(status) => _this._answer(status,k)} text={currentOptions[k]} />
          :
            <AnimSinglebutton onColor={"#2c808f"} effect={"pulse"} status={status} _onPress={(status) => _this._answer(status,k)} text={currentOptions[k]} />
          }

        </View>)
      });

      if(this.state.qus_no!=0){
          headerBack=<TouchableOpacity style={styles.menuWrapper} onPress={()=>this.back_click()}>
                  <Image style={styles.image} source={back_icon}/>
                  </TouchableOpacity>;
        }
        else{
          headerBack=<TouchableOpacity style={styles.menuWrapper} onPress={this.prev}>
                  <Image style={styles.image} source={back_icon}/>
                  </TouchableOpacity>;
  	  }

    return (
      <View style={styles.container}>
      <LinearGradient colors={['#48c3d5','#3fafc0','#369aaa','#287b88']}>

        <MyStatusBar barStyle="light-content"  backgroundColor="#4eb4c4"/>
          <View style={styles.appBar} >
              <View style={{ flex:1,flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', }}>
                <View style={styles.imageContainer}>
                {headerBack}
                </View>

                <View style={{ marginTop:0,width:'60%',justifyContent: 'center',alignItems:'center' }}>
                    <Text allowFontScaling={false} style={styles.headertitle}>Assessing your health!</Text>
                </View>

                <View style={{width:'15%',flexDirection: 'row',justifyContent: 'flex-end'}}>
        					<TouchableOpacity style={styles.qusWrapper} onPress={() => { this.Show_Custom_Alert(true) }}>
                      <Image style={styles.image} source={qus_icon}/>
                    </TouchableOpacity>
                </View>

              </View>
          </View>


      </LinearGradient>
      <View style={[styles.content,this.state.Modal_Visibility ? {backgroundColor: 'rgba(0,0,0,0.4)',zIndex:5} : '#ffffff']}>

      <ScrollView style={{paddingTop: 10}}>

      <View style={styles.container1}>
            <Text allowFontScaling={false} style={styles.heading}>{this.state.question}</Text>
            <View style={styles.hr}></View>
      </View>

      <View style={styles.container2}>
          { this.qno == 4? options :null}
      </View>

      <View style={styles.container3}>
        <TouchableOpacity style={styles.btn} activeOpacity={.6} onPress={this.next}>
          <CommonButton label='Update'/>
          </TouchableOpacity>
      </View>
    </ScrollView>
    </View>
        <Spinner visible={this.state.isVisible}  />
      </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
    backgroundColor: '#F5FCFF',
  },
  content: {
      flex: 1,
  },
  statusBar: {
    height: AppSizes.statusBarHeight,
  },
  appBar: {
      height: AppSizes.navbarHeight,
      justifyContent:'center',
      ...Platform.select({
      android: {
      paddingTop:AppSizes.ResponsiveSize.Padding(4),
      },
    }),
    },
  headertitle:{
    color:'#ffffff',
    fontSize:(Platform.OS === 'ios') ? AppSizes.ResponsiveSize.Sizes(14) :AppSizes.ResponsiveSize.Sizes(16),
    fontWeight:'bold',
    letterSpacing:1,
    textAlign:'center'
  },
  imageContainer:{
    width:'20%'
  },
  menuWrapper: {
    width:'60%',
    height:'60%',
    marginLeft:'30%',
  },
  image: {
    flex: 1,
    width: undefined,
    height: undefined,
    resizeMode:'contain'
  },
  container1:{
    flex:2,
    flexDirection:'column',
    alignItems:'center',
    justifyContent:'center'
  },
  labelInput: {
    color: AppColors.contentColor,
    //fontWeight:'300',
    fontSize:AppSizes.ResponsiveSize.Sizes(13),
  },
  input: {
    borderWidth: 0,
    color: AppColors.contentColor,
    fontWeight:'300',
    fontSize:AppSizes.ResponsiveSize.Sizes(15),
  },
  formInput: {
    borderWidth: 0,
    borderColor: '#333',
  },
  heading:{
    fontSize:AppSizes.ResponsiveSize.Sizes(18),
    color:'#000',
    justifyContent:'center',
    alignItems:'center',
    fontWeight:'600',
    width:'89%',

  },
  hr:{
    width:50,
    height:3,
    backgroundColor:'#000',
    marginTop:10,
},
  container2:{
    flex:6,
    flexDirection:'column',
    padding: AppSizes.ResponsiveSize.Padding(5),
    width:AppSizes.screen.width,

  },
  container3:{
    flex:2,
    justifyContent:'flex-start',
    alignItems:'center',

  },

  dropdowntext:{
    fontSize:AppSizes.ResponsiveSize.Sizes(14),
    fontWeight:'500'
  },
  ansfield:{
    fontSize:AppSizes.ResponsiveSize.Sizes(13),
    color:'#5a5a5a',
  },
  btn:{
    width:'100%',
    marginBottom:20
  },
  imageContainer:{
    flex:1,
    alignItems: 'flex-start',
    //backgroundColor:'red',
  },
  qusWrapper: {
    width:'40%',
    height:'40%',
    marginRight:'30%',
    marginTop:'15%'
  },
  image: {
    flex: 1,
    width: undefined,
    height: undefined,
    resizeMode:'contain'
  },
  Alert_Main_View:{
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#ffffff',
    height:'40%',
    width: '80%',
    borderRadius:20,
    flexDirection:'column',
    justifyContent: 'center',
    alignItems: 'center',
  },

  Alert_Title:{
    fontSize: AppSizes.ResponsiveSize.Sizes(25),
    color: "#000",
    textAlign: 'center',
    padding: 10,
    height: '28%'
  },
    Alert_Message:{
      fontSize: AppSizes.ResponsiveSize.Sizes(18),
      color: "#000",
      textAlign: 'center',
      padding: 10,
    },
    buttonStyle: {
      width: '100%',
      height: '100%',
      justifyContent: 'center',
      alignItems: 'center'
    },
    TextStyle:{
      color:'#000',
      textAlign:'center',
      fontSize: AppSizes.ResponsiveSize.Sizes(18),
    },
});
