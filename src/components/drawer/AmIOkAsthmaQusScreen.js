import React, { Component } from 'react';
import {AsyncStorage,StyleSheet,Text,View,Image,TouchableOpacity,Platform,Alert,ScrollView,TextInput,Button,StatusBar,Modal} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions } from 'react-navigation'
import Spacer from '../common/Spacer'
import Spinner from 'react-native-loading-spinner-overlay';
import Header from '../common/signupHeader'
import CommonButton from '../common/CommonButton'
import Swiper from 'react-native-swiper';
import Animbutton from './animAnsbutton'
import AnimSinglebutton from './animSingleAnsOptions';
import { jsonData } from '../data/Question';
import { pickerData } from '../data/pickerData';
import FloatingLabelBox from '../common/FloatingLabel'
import FloatingLabel from 'react-native-floating-labels';
import Picker from 'react-native-picker';
import Sound from 'react-native-sound'
import Orientation from 'react-native-orientation'
import LinearGradient from 'react-native-linear-gradient';
import CountDown from 'react-native-countdown-component';

import * as commonFunctions from '../../utils/CommonFunctions'

var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;

/* Images */
//var thankyou_icon = require('../../themes/Images/thank-you.png');
var logo = require('../../themes/Images/logo.png');
var back_icon = require( '../../themes/Images/left-whitearrow.png')
var oxy_icon = require('../../themes/Images/oxy.png');
var temp_icon = require('../../themes/Images/temp.png');
var heartRate_icon = require('../../themes/Images/heart_rate.png');
var qus_icon = require('../../themes/Images/que_icon_196.png');
var mice=require('../../themes/Images/mice.png')
var mice_off=require('../../themes/Images/mice-off.png')
var peak_before = require('../../themes/Images/inhealer.png');
var peak_after=require('../../themes/Images/peak_flow.png');

var help_audio1=require('../../themes/sound/amiokQus1Help.mp3')
var help_audio2=require('../../themes/sound/amiokQus2Help.mp3')
var help_audio3=require('../../themes/sound/amiokQus3Help.mp3')
var help_audio4=require('../../themes/sound/amiokQus4Help.mp3')

let _that
let arrnew = []
let ans_track = []
let final_arr = new Array();
let options_arr=[]
let vitals=[]
let vitals_data=[]
let vitals_api=[]
var current_ans=''
var move_last=0;
var move=0;
var next_move_last='';
var before=''
var after=''
var vital=0
var heart_base=''
var current_date
var from
var cal
var exacerbation
var triage
var p=0
var t=0
var h=0

const MyStatusBar = ({backgroundColor, ...props}) => (
  <View style={[styles.statusBar, { backgroundColor }]}>
    <StatusBar translucent backgroundColor={backgroundColor} {...props} />
  </View>
);

var temperature_data= [];
var selected=[]
var que =[]

export default class AmIOkQusScreen extends Component {
  constructor (props) {
      super(props)
      this.qno = 0
      this.score = 0
      this.type ='qus'
      this.inputRefs = {};

      const jdata = jsonData.amiokasthmaqus.qus1
      arrnew = Object.keys(jdata).map( function(k) { return jdata[k] });

      this.state = {
        question : arrnew[this.qno].question,
        title : arrnew[this.qno].title,
        options : arrnew[this.qno].options,
        suboption : arrnew[this.qno].suboption,
        type : arrnew[this.qno].type,
        modal_title:arrnew[this.qno].modal_title,
        countCheck : [],
        dataStorage:[],
        qus_no : 0,
        uid:'',
        isVisible: false,
        Modal_Visibility: false,
        status:'ans',
        heart_rate:'',
        items2: pickerData.heart_data,
        temperature: 0,
        items3: pickerData.temprature,
        temperature_val:'',
        screentype:'',
        baseline_heart_rate:'',
        baseline_heart_rate_count:0,
        api:0,
        audioStatus: false,
        audioImg: mice,
        temp:'',
        peak:'',
        peak_puff:'',
        heart_base:'80',
        peak_base:'0.79',
        timer:false,
        peak_after_puff:false,
        heart_base_status:'0',
        peak_base_status:'0'
  },
    _that = this;
  }

  playTrack = () => {
    this.setState({audioStatus: !this.state.audioStatus})
    if(this.state.audioStatus==true){
      if(this.qno==0){
        help_audio=help_audio1;
      }
      else if(this.qno==1){
        help_audio=help_audio2;
      }
      else if(this.qno==2){
        help_audio=help_audio3;
      }
      else{
        help_audio=help_audio4;
      }

      this.track = new Sound(help_audio,  (e) => {
        if (e) {
          console.log('error loading track:', e)
        } else {

            this.setState({audioImg: mice_off});
            this.track.play((success) => {
                 if (success) {
                     this.stop();
                 }
             });
          }
        })
      }
        else {
          this.stop();
        }
      }

  stop() {
     if (!this.track) return;
     this.track.stop();
     this.track.release();
     this.track = null;
     this.setState({audioStatus: false});
     this.setState({audioImg: mice});
 }

  Show_Custom_Alert(visible) {
    this.stop();
    this.setState({Modal_Visibility: visible});
  }

  componentDidMount() {
    Orientation.lockToPortrait();
    AsyncStorage.getItem('loggedIn').then((value) => {
      var loggedIn = JSON.parse(value);
      if (loggedIn) {
        AsyncStorage.getItem('UserData').then((UserData) => {
          const data = JSON.parse(UserData)
          var uid=data.id;
          _that.setState({uid:  uid});
          _that.validationAndApiParameter('getData',uid);
        })
      }
    })
    AsyncStorage.getItem('from').then((value) => {
      from = JSON.parse(value)
      console.log(from);
    })
    var date = new Date().getDate();
    var month = new Date().getMonth() + 1;
    var year = new Date().getFullYear();
    current_date=year + '-' + month + '-' + date
  }

  validationAndApiParameter(apiname,uid) {
    if(apiname == 'web_profile'){
      console.log(this.state.screentype);
      var json_options_arr = JSON.stringify(options_arr);
      var data = {
          uid: this.state.uid,
          phython:'python',
          type:'asthma',
          variable : json_options_arr,
      };
      console.log(data);
      _that.setState({isVisible: true});
      this.postToApiCalling('POST', apiname, Constant.URL_saveAsthma,data);
    }
    else if(apiname == 'vitals'){
      console.log(this.state.screentype);
      var json_options_arr = JSON.stringify(options_arr);

      var data = {
          uid: this.state.uid,
          phython:'python',
          type:'asthma',
          variable : json_options_arr,
      };
      _that.setState({isVisible: true});
      this.postToApiCalling('POST', apiname, Constant.URL_saveAsthma,data);
    }
    else if (apiname=='getData') {
      var data = {
          uid: uid
      };
      _that.setState({isVisible: true});
      this.postToApiCalling('POST', apiname, Constant.URL_asthmaProfileVariable,data);
    }
    else if (apiname == 'saveVital') {
      var heart
      var peak
      var peak_puff
      console.log('heart rate',this.state.heart_rate);
      if(this.state.heart_rate==''){
        heart=80
      }
      else{
        heart=this.state.heart_rate
      }
      if(this.state.peak==''){
        peak=0.79
      }
      else{
        peak=this.state.peak
      }
      if(this.state.peak_puff==''){
        peak_puff=cal
      }
      else{
        peak_puff=this.state.peak_puff
      }
      var variables=[{"var_name":"VTL_BM_P","value":heart},{"var_name":"VTL_BM_PEF","value":peak},{"var_name":"VTL_BM_PEF_PUFF","value":peak_puff}]

      var data = {
          uid: uid,
          variable:JSON.stringify(variables)
      };
      console.log(data);
      _that.setState({isVisible: true});
      this.postToApiCalling('POST', apiname, Constant.URL_saveVital,data);
    }
    else if (apiname == 'temp'){
      var json_options_arr = JSON.stringify([{"var_name":"VTL_BM_TMP","value":this.state.temp.toString()}]);

      var data = {
          uid: this.state.uid,
          variable : json_options_arr,
      };
      _that.setState({isVisible: true});
      this.postToApiCalling('POST', apiname, Constant.URL_Temp,data);
    }
    else if(apiname=='saveScore'){
      var amiok = [{var_name:'amiok',value:1}]
      var json_options_arr = JSON.stringify(amiok);

      var data = {
        uid: this.state.uid,
        variable : json_options_arr,
      };
      console.log(data);
      _that.setState({isVisible: true});
      this.postToApiCalling('POST', apiname, Constant.URL_saveRandom,data);
    }
    else if (apiname=='save') {
      var final=[{var_name:'amiok_inactive',value:0},{var_name:'inactive_sent',value:0}]
      var json_python_result = JSON.stringify(final);
      var data = {
          uid: this.state.uid,
          variable : json_python_result,
      };
      console.log(data);
      _that.setState({isVisible: true});
      this.postToApiCalling('POST', apiname, Constant.URL_saveUserData,data);
    }
    else if (apiname=='saveV') {
      var final=[]
      console.log('p',p);
      var json_python_result = JSON.stringify(final);
      var data = {
          uid: this.state.uid,
          variable : json_python_result,
      };
      console.log(data);
      //_that.setState({isVisible: true});
      //this.postToApiCalling('POST', apiname, Constant.URL_saveUserData,data);
    }
  }

  postToApiCalling(method, apiKey, apiUrl, data) {
  new Promise(function(resolve, reject) {
      if (method == 'POST') {
          resolve(WebServices.callWebService(apiUrl, data));
      } else {
          resolve(WebServices.callWebService_POST(apiUrl, data));
      }
  }).then((jsonRes) => {
    console.log(jsonRes)
    if ((!jsonRes) || (jsonRes.code == 0)) {
      setTimeout(() => {
            Alert.alert(jsonRes.message);
        }, 200);
    } else {
      _that.apiSuccessfullResponse(apiKey, jsonRes)
    }
  }).catch((error) => {
      console.log("ERROR" + error);
      _that.setState({ isVisible: false })
      setTimeout(() => {
          Alert.alert("Server Error");
      }, 200);
  })
}

  apiSuccessfullResponse(apiKey, jsonRes) {
    if(apiKey=="web_profile"){
        _that.setState({ isVisible: false })
        jsonResData=jsonRes
        arrnew = []
        ans_track = []
        final_arr = new Array();
        options_arr=[]
        vitals=[]
        vitals_api=[]
        vitals_data=[]
        current_ans=''
        move_last=0;
        next_move_last='';
        _that.validationAndApiParameter('saveScore')
    }
    else if (apiKey=='saveScore'){
      _that.setState({ isVisible: false })

      var triageproba=jsonResData.triageverproba.split(',')
      var one=triageproba[0]
      var second=triageproba[1]
      var add=(parseFloat(one)+parseFloat(second))*100
      triage=parseInt(add)
      exacerbation=parseInt(jsonResData.exacerbation*100)
      console.log(triage);

      _that.props.navigation.navigate('MedicalAsthmaAttentionScreen',{symptom:jsonResData.symptom,profile:jsonResData.profile,triagever:jsonResData.triagever,triageverproba:triage,exacerbation:exacerbation,vital:jsonResData.vital});
    }
    else if(apiKey=='vitals'){
      _that.setState({ isVisible: false })
      exacerbation=parseInt(jsonRes.exacerbation*100)
      triageproba=jsonRes.triageverproba.split(',')
      var one=triageproba[0]
      var second=triageproba[1]
      var add=(parseFloat(one)+parseFloat(second))*100
      var triage=parseInt(add)
      arrnew = []
      ans_track = []
      final_arr = new Array();
      options_arr=[]
      vitals=[]
      vitals_api=[]
      vitals_data=[]
      current_ans=''
      move_last=0;
      next_move_last='';
      _that.props.navigation.navigate('MedicalAsthmaAttentionScreen',{symptom:jsonRes.symptom,profile:jsonRes.profile,triagever:vital,triageverproba:triage,exacerbation:exacerbation,vital:jsonRes.vital});
    }
    else if (apiKey=='getData'){
      _that.setState({isVisible: false});
      var peak=jsonRes.profile.VTL_BM_PEF_BASE
      var heart=jsonRes.profile.VTL_BM_P_BASE
      this.setState({peak_base:jsonRes.profile.VTL_BM_PEF_BASE})
      this.setState({heart_base:jsonRes.profile.VTL_BM_P_BASE})
      this.setState({
        peak_base_status:jsonRes.profile.peak_base_status,
        heart_base_status:jsonRes.profile.heart_base_status,
      })
      before=peak*30/100
      heart_base=parseInt(heart)+35
    }
    else if (apiKey=='saveVital'){
      console.log(jsonRes);
      _that.setState({isVisible: false});
      _that.validationAndApiParameter('saveV')
    }
    else if (apiKey=='saveV'){
      console.log(jsonRes);
      _that.setState({isVisible: false});
    }
    else if (apiKey == 'temp'){
      _that.setState({ isVisible: false })
      console.log(jsonRes);
    }
    else if(apiKey=='save'){
      _that.setState({ isVisible: false })
      _that.validationAndApiParameter('web_profile')
    }
  }

  updateAlert = () => {
        this.setState({Modal_Visibility: !this.state.Modal_Visibility});
  }

  prev= () =>{
    console.log(this.qno)
    if(this.qno <= 2){
      if(this.qno==2 || this.qno==1){
        _that.setState({
          heart_rate : '',
          temperature : '',
        });
      }
      this.qno--
      this.setState({ qus_no: this.qno })
      this.setState({ question: arrnew[this.qno].question,qus_id: arrnew[this.qno].qus_id, options: arrnew[this.qno].options, type: arrnew[this.qno].type,suboption : arrnew[this.qno].suboption,modal_title : arrnew[this.qno].modal_title})
    }
    else if(this.qno==5 || this.qno==6 || this.qno==7 || this.qno==8 || this.qno==9 || this.qno==3 || this.qno==4){
      this.qno=2
      this.setState({ qus_no: this.qno })
      this.setState({ question: arrnew[this.qno].question,qus_id: arrnew[this.qno].qus_id, options: arrnew[this.qno].options, type: arrnew[this.qno].type,suboption : arrnew[this.qno].suboption,modal_title : arrnew[this.qno].modal_title})
    }
  }

  next= () =>{
    current_ans=0
    final_thank=0;

    const { countCheck} = this.state
    ans_track =[];

    que.push(this.qno)

    if(this.qno==1){
      p=this.state.peak
      h=this.state.heart_rate

      var p_b=1000*(parseFloat(this.state.peak_base))
      if(this.state.peak==''){
        cal=0.79
      }
      else{
        var p=1000*(parseFloat(this.state.peak))
        cal=(parseFloat(p/p_b).toFixed(2))
        if(cal>1){
          cal=1
        }
      }

      if(this.state.heart_rate!=''){
        options_arr.push({var_name: 'heart_status',value: 1});
      }
      if(this.state.peak!=''){
        options_arr.push({var_name: 'peak_status',value: 1});
      }

      if(this.state.heart_base_status=='1'){
        if (parseInt(this.state.heart_rate)>heart_base) {
          vital=3
          move_last=4
        }
      }
      if(this.state.peak_base_status=='1'){
        if(this.state.peak!='' &&this.state.peak<before){
          vital=4
          move_last=4
        }
        else if(cal<0.79){
          if(this.state.peak_after_puff){
            move_last=next_move_last
          }
          else{
            move_last=3
          }
        }
      }

      if (this.state.peak=='') {
        this.setState({peak:this.state.peak_base})
      }
      if(this.state.heart_rate == ''){
        this.setState({heart_rate:this.state.heart_base})
      }
      if(this.state.peak_puff==''){
        this.setState({peak_puff:cal})
      }
    }
    if(this.qno==2){
      if(countCheck.indexOf('SYM_ALLERGEN')!=-1){
        options_arr.push({var_name: 'SYM_ALLERGEN',value: 1});
      }
      else {
        options_arr.push({var_name: 'SYM_ALLERGEN',value: 0});
      }
    }
    if(this.qno==3){
      if(countCheck.indexOf('SYM_RIHMORE_N')!=-1){
        options_arr.push({var_name: 'SYM_RIHMORE_N',value: 1});
      }
      else {
        options_arr.push({var_name: 'SYM_RIHMORE_N',value: 0});
      }

      if(countCheck.indexOf('SYM_RIHMORE_LOW')!=-1){
        options_arr.push({var_name: 'SYM_RIHMORE_LOW',value: 1});
      }
      else {
        options_arr.push({var_name: 'SYM_RIHMORE_LOW',value: 0});
      }
      if(countCheck.indexOf('SYM_RIHMORE_MEDIUM')!=-1){
        options_arr.push({var_name: 'SYM_RIHMORE_MEDIUM',value: 1});
      }
      else {
        options_arr.push({var_name: 'SYM_RIHMORE_MEDIUM',value: 0});
      }
      if(countCheck.indexOf('SYM_RIHMORE_HIGH')!=-1){
        options_arr.push({var_name: 'SYM_RIHMORE_HIGH',value: 1});
      }
      else {
        options_arr.push({var_name: 'SYM_RIHMORE_HIGH',value: 0});
      }
    }
    if(this.qno==4){
      if(countCheck.indexOf('MED_COMP_25')!=-1){
        options_arr.push({var_name: 'MED_COMP_25',value: 1});
      }
      else {
        options_arr.push({var_name: 'MED_COMP_25',value: 0});
      }

      if(countCheck.indexOf('MED_COMP_50')!=-1){
        options_arr.push({var_name: 'MED_COMP_50',value: 1});
      }
      else {
        options_arr.push({var_name: 'MED_COMP_50',value: 0});
      }
      if(countCheck.indexOf('MED_COMP_75')!=-1){
        options_arr.push({var_name: 'MED_COMP_75',value: 1});
      }
      else {
        options_arr.push({var_name: 'MED_COMP_75',value: 0});
      }
      if(countCheck.indexOf('MED_COMP_100')!=-1){
        options_arr.push({var_name: 'MED_COMP_100',value: 1});
      }
      else {
        options_arr.push({var_name: 'MED_COMP_100',value: 0});
      }
    }
    if(this.qno==5){
      if(countCheck.indexOf('SYM_CGH_N')!=-1){
        options_arr.push({var_name: 'SYM_CGH_N',value: 0});
      }
      else {
        options_arr.push({var_name: 'SYM_CGH_N',value: 0});
      }
      if(countCheck.indexOf('SYM_CGH_LESS')!=-1){
        options_arr.push({var_name: 'SYM_CGH_LESS',value: 0});
      }
      else {
        options_arr.push({var_name: 'SYM_CGH_LESS',value: 0});
      }
      if(countCheck.indexOf('SYM_CGH_SAME')!=-1){
        options_arr.push({var_name: 'SYM_CGH_SAME',value: 1});
      }
      else {
        options_arr.push({var_name: 'SYM_CGH_SAME',value: 0});
      }
      if(countCheck.indexOf('SYM_CGH_MORE')!=-1){
        options_arr.push({var_name: 'SYM_CGH_MORE',value: 1});
      }
      else {
        options_arr.push({var_name: 'SYM_CGH_MORE',value: 0});
      }
    }
    if(this.qno==6){
      if(countCheck.indexOf('SYM_SOA_N')!=-1){
        options_arr.push({var_name: 'SYM_SOA_N',value: 0});
      }
      else {
        options_arr.push({var_name: 'SYM_SOA_N',value: 0});
      }

      if(countCheck.indexOf('SYM_SOA_LESS')!=-1){
        options_arr.push({var_name: 'SYM_SOA_LESS',value: 0});
      }
      else {
        options_arr.push({var_name: 'SYM_SOA_LESS',value: 0});
      }
      if(countCheck.indexOf('SYM_SOA_SAME')!=-1){
        options_arr.push({var_name: 'SYM_SOA_SAME',value: 1});
      }
      else {
        options_arr.push({var_name: 'SYM_SOA_SAME',value: 0});
      }
      if(countCheck.indexOf('SYM_SOA_MORE')!=-1){
        options_arr.push({var_name: 'SYM_SOA_MORE',value: 1});
      }
      else {
        options_arr.push({var_name: 'SYM_SOA_MORE',value: 0});
      }
    }
    if(this.qno==7){
      t=this.state.temp
      if(this.state.temp==''){
        this.setState({temp:'98.6'})
        options_arr.push({var_name: 'VTL_BM_TMP',value:'98.6'});
      }
      else if (this.state.temp>102 && this.state.temp<104) {
        console.log('2');
        vital=3
        final_thank=1
        options_arr.push({var_name: 'VTL_BM_TMP',value:this.state.temp});
      }
      else if (this.state.temp>104) {
        console.log('3');
        vital=4
        final_thank=1
        options_arr.push({var_name: 'VTL_BM_TMP',value:this.state.temp});
      }
      else{
        console.log(this.state.temp.toString());
      }

      if(this.state.temp!=''){
        options_arr.push({var_name: 'temp_status',value: 1});
      }
      _that.validationAndApiParameter('temp',this.state.uid);
    }
    if(this.qno==8){
      if(countCheck.indexOf('SYM_WAKE_N')!=-1){
        options_arr.push({var_name: 'SYM_WAKE_N',value: 1});
      }
      else {
        options_arr.push({var_name: 'SYM_WAKE_N',value: 0});
      }

      if(countCheck.indexOf('SYM_WAKE_LOW')!=-1){
        options_arr.push({var_name: 'SYM_WAKE_LOW',value: 1});
      }
      else {
        options_arr.push({var_name: 'SYM_WAKE_LOW',value: 0});
      }
      if(countCheck.indexOf('SYM_WAKE_MEDIUM')!=-1){
        options_arr.push({var_name: 'SYM_WAKE_MEDIUM',value: 1});
      }
      else {
        options_arr.push({var_name: 'SYM_WAKE_MEDIUM',value: 0});
      }
      if(countCheck.indexOf('SYM_WAKE_HIGH')!=-1){
        options_arr.push({var_name: 'SYM_WAKE_HIGH',value: 1});
      }
      else {
        options_arr.push({var_name: 'SYM_WAKE_HIGH',value: 0});
      }
    }
    if(this.qno==9){
      if(countCheck.indexOf('SYM_ADL_N')!=-1){
        options_arr.push({var_name: 'SYM_ADL_N',value: 1});
      }
      else {
        options_arr.push({var_name: 'SYM_ADL_N',value: 0});
      }

      if(countCheck.indexOf('SYM_ADL_LOW')!=-1){
        options_arr.push({var_name: 'SYM_ADL_LOW',value: 1});
      }
      else {
        options_arr.push({var_name: 'SYM_ADL_LOW',value: 0});
      }
      if(countCheck.indexOf('SYM_ADL_MEDIUM')!=-1){
        options_arr.push({var_name: 'SYM_ADL_MEDIUM',value: 1});
      }
      else {
        options_arr.push({var_name: 'SYM_ADL_MEDIUM',value: 0});
      }
      if(countCheck.indexOf('SYM_ADL_HIGH')!=-1){
        options_arr.push({var_name: 'SYM_ADL_HIGH',value: 1});
      }
      else {
        options_arr.push({var_name: 'SYM_ADL_HIGH',value: 0});
      }
    }

    if(this.state.countCheck.length <= 0 && this.state.qus_no!=1 && this.state.qus_no!=7){
      Alert.alert("Invalid Value","Missing or blank value is not allowed")
    }
    else if(this.state.qus_no==0){
      if(countCheck.indexOf('SYM_WORSE_N')!=-1){
        options_arr.push({var_name: 'SYM_WORSE_N',value: 1});
      }
      else {
        options_arr.push({var_name: 'SYM_WORSE_N',value: 0});
      }
      if(countCheck.indexOf('SYM_WORSE_LOW')!=-1){
        options_arr.push({var_name: 'SYM_WORSE_LOW',value: 1});
      }
      else {
        options_arr.push({var_name: 'SYM_WORSE_LOW',value: 0});
      }
      if(countCheck.indexOf('SYM_WORSE_MEDIUM')!=-1){
        options_arr.push({var_name: 'SYM_WORSE_MEDIUM',value: 1});
      }
      else {
        options_arr.push({var_name: 'SYM_WORSE_MEDIUM',value: 0});
      }
      if(countCheck.indexOf('SYM_WORSE_HIGH')!=-1){
        options_arr.push({var_name: 'SYM_WORSE_HIGH',value: 1});
      }
      else {
        options_arr.push({var_name: 'SYM_WORSE_HIGH',value: 0});
      }

      if(countCheck.indexOf('SYM_WORSE_N') != -1){
        next_move_last=1
        move_last=1
        this.qno++
        this.setState({ qus_no: this.qno })
      }
      else{
        next_move_last=0
        this.qno++
        this.setState({ qus_no: this.qno })
        move_last=0
      }
    }
    else if (this.qno==1) {
      if(move_last==1){
        this.qno=4
        this.setState({ qus_no: this.qno })
      }
      else if(move_last==0){
        this.qno++
        this.setState({ qus_no: this.qno })
      }
      else if(move_last==3){
        this.setState({timer:true})
      }
      else if (move_last==4) {
        final_thank=1
      }
      _that.validationAndApiParameter('saveVital',this.state.uid);
    }
    else if (this.qno==2 ){
      if(countCheck.indexOf('rescue_inhaler_usage')!=-1) {
        selected.push('rescue_inhaler_usage')
      }
      if(countCheck.indexOf('shofb')!=-1){
        selected.push('shofb')
      }
      if(countCheck.indexOf('runny_nose')!=-1){
        selected.push('runny_nose')
        options_arr.push({var_name: 'SYM_URTI',value: 1});
      }
      else{
        options_arr.push({var_name: 'SYM_URTI',value: 0});
      }
      if(countCheck.indexOf('wakingup_from_breating_symptoms')!=-1){
        selected.push('wakingup_from_breating_symptoms')
      }
      if(countCheck.indexOf('difficulty_performing_usual_activities')!=-1){
        selected.push('difficulty_performing_usual_activities')
      }
      if(countCheck.indexOf('cough')!=-1){
        selected.push('cough')
      }
      if(countCheck.indexOf('SYM_ALLERGEN')!=-1){
        selected.push('SYM_ALLERGEN')
      }
      if(selected.length>0){
        if(selected.indexOf('rescue_inhaler_usage')!=-1) {
          var index = selected.indexOf('rescue_inhaler_usage')
          if (index > -1) {
            selected.splice(index, 1);
          }
          this.qno++
          this.setState({ qus_no: this.qno })
        }
        else if(selected.indexOf('shofb')!=-1){
          var index = selected.indexOf('shofb')
          if (index > -1) {
            selected.splice(index, 1);
          }
          this.qno=6
          this.setState({ qus_no: this.qno })
        }
        else if(selected.indexOf('runny_nose')!=-1){
          var index = selected.indexOf('runny_nose')
          if (index > -1) {
            selected.splice(index, 1);
          }
          this.qno=7
          this.setState({ qus_no: this.qno })
        }
        else if(selected.indexOf('wakingup_from_breating_symptoms')!=-1){
          var index = selected.indexOf('wakingup_from_breating_symptoms')
          if (index > -1) {
            selected.splice(index, 1);
          }
          this.qno=8
          this.setState({ qus_no: this.qno })
        }
        else if(selected.indexOf('difficulty_performing_usual_activities')!=-1){
          var index = selected.indexOf('difficulty_performing_usual_activities')
          if (index > -1) {
            selected.splice(index, 1);
          }
          this.qno=9
          this.setState({ qus_no: this.qno })
        }
        else if(selected.indexOf('cough')!=-1){
          var index = selected.indexOf('cough')
          if (index > -1) {
            selected.splice(index, 1);
          }
          this.qno=5
          this.setState({ qus_no: this.qno })
        }
        else if(selected.indexOf('SYM_ALLERGEN')!=-1){
          var index = selected.indexOf('SYM_ALLERGEN')
          if (index > -1) {
            selected.splice(index, 1);
          }
          this.qno=4
          this.setState({ qus_no: this.qno })
        }
      }
    }
    else if (selected.length>0) {
      if(selected.indexOf('SYM_ALLERGEN')!=-1 && selected.length==1){
        var index = selected.indexOf('SYM_ALLERGEN')
        if (index > -1) {
          selected.splice(index, 1);
        }
        this.qno=4
        this.setState({ qus_no: this.qno })
      }
      else if(selected.indexOf('rescue_inhaler_usage')!=-1) {
        var index = selected.indexOf('rescue_inhaler_usage')
        if (index > -1) {
          selected.splice(index, 1);
        }
        this.qno++
        this.setState({ qus_no: this.qno })
      }
      else if(selected.indexOf('shofb')!=-1){
        var index = selected.indexOf('shofb')
        if (index > -1) {
          selected.splice(index, 1);
        }
        this.qno=6
        this.setState({ qus_no: this.qno })
      }
      else if(selected.indexOf('runny_nose')!=-1){
        var index = selected.indexOf('runny_nose')
        if (index > -1) {
          selected.splice(index, 1);
        }
        this.qno=7
        this.setState({ qus_no: this.qno })
      }
      else if(selected.indexOf('wakingup_from_breating_symptoms')!=-1){
        var index = selected.indexOf('wakingup_from_breating_symptoms')
        if (index > -1) {
          selected.splice(index, 1);
        }
        this.qno=8
        this.setState({ qus_no: this.qno })
      }
      else if(selected.indexOf('difficulty_performing_usual_activities')!=-1){
        var index = selected.indexOf('difficulty_performing_usual_activities')
        if (index > -1) {
          selected.splice(index, 1);
        }
        this.qno=9
        this.setState({ qus_no: this.qno })
      }
      else if(selected.indexOf('cough')!=-1){
        var index = selected.indexOf('cough')
        if (index > -1) {
          selected.splice(index, 1);
        }
        this.qno=5
        this.setState({ qus_no: this.qno })
      }
    }
    else if (selected.length==0 && this.qno != 4) {
      this.qno=4
    }
    else if (this.qno==4) {
      final_thank=1
    }
    else{
      this.qno++
      this.setState({ qus_no: this.qno })
    }

    if(final_thank != 1){
      this.setState({ countCheck: [], question: arrnew[this.qno].question, options: arrnew[this.qno].options, type: arrnew[this.qno].type, suboption : arrnew[this.qno].suboption});
    }
    if(final_thank ==1) {
      console.log(vital);
      if(que.indexOf(3) == -1){
        options_arr.push({var_name: 'SYM_RIHMORE_N',value: 1});
        options_arr.push({var_name: 'SYM_RIHMORE_LOW',value: 0});
        options_arr.push({var_name: 'SYM_RIHMORE_MEDIUM',value: 0});
        options_arr.push({var_name: 'SYM_RIHMORE_HIGH',value: 0});
      }
      if(que.indexOf(5) == -1){
        options_arr.push({var_name: 'SYM_CGH_N',value: 0});
        options_arr.push({var_name: 'SYM_CGH_LESS',value: 0});
        options_arr.push({var_name: 'SYM_CGH_SAME',value: 1});
        options_arr.push({var_name: 'SYM_CGH_MORE',value: 0});
      }
      if(que.indexOf(6) == -1){
        options_arr.push({var_name: 'SYM_SOA_N',value: 0});
        options_arr.push({var_name: 'SYM_SOA_LESS',value: 0});
        options_arr.push({var_name: 'SYM_SOA_SAME',value: 1});
        options_arr.push({var_name: 'SYM_SOA_MORE',value: 0});
      }
      if(que.indexOf(8) == -1){
        options_arr.push({var_name: 'SYM_WAKE_N',value: 1});
        options_arr.push({var_name: 'SYM_WAKE_LOW',value: 0});
        options_arr.push({var_name: 'SYM_WAKE_MEDIUM',value: 0});
        options_arr.push({var_name: 'SYM_WAKE_HIGH',value: 0});
      }
      if(que.indexOf(9) == -1){
        options_arr.push({var_name: 'SYM_ADL_N',value: 1});
        options_arr.push({var_name: 'SYM_ADL_LOW',value: 0});
        options_arr.push({var_name: 'SYM_ADL_MEDIUM',value: 0});
        options_arr.push({var_name: 'SYM_ADL_HIGH',value: 0});
      }
      if(que.indexOf(4) == -1){
        options_arr.push({var_name: 'MED_COMP_25',value: 0});
        options_arr.push({var_name: 'MED_COMP_50',value: 0});
        options_arr.push({var_name: 'MED_COMP_75',value: 0});
        options_arr.push({var_name: 'MED_COMP_100',value: 0});
      }

      // options_arr.push({var_name: 'VTL_BM_P',value: this.state.heart_rate});
      // options_arr.push({var_name: 'VTL_BM_PEF',value: this.state.peak});
      // options_arr.push({var_name: 'VTL_BM_PEF_PUFF',value: this.state.peak_puff});
      options_arr.push({var_name: 'amiok_date',value:current_date})
      options_arr.push({var_name:"PEF_ALGO",value:cal})

      if(que.indexOf(4) == -1){
        _that.validationAndApiParameter('vitals',this.state.uid);
      }
      else{
        _that.validationAndApiParameter('save',this.state.uid);
      }
      console.log(options_arr);
    }
  }

  _answer(status,ans){
    if(this.state.type=="multiple"){
      if(status == false){
        ans_track.push(ans);
      }
      else{
        var index = ans_track.indexOf(ans);
        if (index !== -1) ans_track.splice(index, 1);
        //ans_track.splice(-1,1);
      }
    }
    else if(this.state.type=="single"){
      ans_track.splice(-1,1);
      ans_track.push(ans);
    }

    current_ans=ans;
    this.setState({ countCheck: ans_track })
  }

  back_click() {
    if(from=='HomeScreen2'){
      const resetAction = NavigationActions.navigate({ routeName: 'StartScreen'})
      _that.props.navigation.dispatch(resetAction);
    }
    else if (from=='AsthmaToDo') {
      const resetAction = NavigationActions.navigate({ routeName: 'AsthmaToDo'})
      _that.props.navigation.dispatch(resetAction);
    }
  }

  temperature() {
      Picker.init({
          pickerData: this.state.items3,
          onPickerConfirm: pickedValue => {
              this.setState({temperature:Number(pickedValue)})
          },
          onPickerCancel: pickedValue => {
              console.log('area', pickedValue);
          },
          onPickerSelect: pickedValue => {
              console.log('area', pickedValue);
          }
      });
      Picker.show();
  }

  heart() {
      Picker.init({
          pickerData: this.state.items2,
          onPickerConfirm: pickedValue => {
              var picked=pickedValue.toString()
              var ar=picked.split(" ")
              var data=ar[0]
              this.setState({heart_rate:Number(data)})
          },
          onPickerCancel: pickedValue => {
              console.log('area', pickedValue);
          },
          onPickerSelect: pickedValue => {
              console.log('area', pickedValue);
          }
      });
      Picker.show();
  }

  render() {
    const headerProp = {
      title: 'Assessing your health!',
      screens: 'AmIOkQusScreen',
    };

    let _this = this
      const currentOptions = this.state.options
      const options = Object.keys(currentOptions).map( function(k) {
        if(k==current_ans){status=true}else{status=false}
        return (  <View key={k} style={{margin:5, }}>
          { _this.state.type=='multiple' ?
            <Animbutton onColor={"#2c808f"} effect={"pulse"}  _onPress={(status) => _this._answer(status,k)} text={currentOptions[k]} />
          :
            <AnimSinglebutton onColor={"#2c808f"} effect={"pulse"} status={status} _onPress={(status) => _this._answer(status,k)} text={currentOptions[k]} />
          }

        </View>)
      });

      var textInput = <View style={{margin:5, }}>
        <View style={{ paddingVertical: AppSizes.ResponsiveSize.Padding(2) }} />

        <TouchableOpacity onPress={()=>this.temperature()} style={{marginTop: 10,flexDirection:'row',paddingTop:20,paddingBottom:10}}>
          <View style={styles.icon}>
              <Image style={styles.cicon} source={temp_icon}/>
          </View>
          <View style={styles.texttbox}>
            <Text style={{color:'#000',fontSize:AppSizes.ResponsiveSize.Sizes(15)}}>Temperature</Text>
            {this.state.temperature>0?<Text style={{color:'#000',fontSize:AppSizes.ResponsiveSize.Sizes(15)}}>{this.state.temperature}</Text>:<Text style={{color:'#9d9d9d',fontSize:AppSizes.ResponsiveSize.Sizes(15)}}>Select Your Temperature</Text>}
          </View>
        </TouchableOpacity>
        <View style={{width:'100%',height:1,backgroundColor:'#000'}}/>
      </View>

      var peak_after_puff=<View style={{margin:5, }}>

         <View style={{ paddingVertical: AppSizes.ResponsiveSize.Padding(2) }} />
         <TouchableOpacity onPress={()=>this.heart()} style={{marginTop: 10,flexDirection:'row',paddingTop:20,paddingBottom:10}}>
           <View style={styles.icon}>
               <Image style={styles.cicon} source={heartRate_icon}/>
           </View>
           <View style={styles.texttbox}>
             <Text style={{color:'#000',fontSize:AppSizes.ResponsiveSize.Sizes(15)}}>Heart Rate</Text>
             {this.state.heart_rate.length>0?<Text style={{color:'#000',fontSize:AppSizes.ResponsiveSize.Sizes(15)}}>{this.state.heart_rate} bmp</Text>:<Text style={{color:'#9d9d9d',fontSize:AppSizes.ResponsiveSize.Sizes(15)}}>Select Your Heart Rate</Text>}
           </View>
         </TouchableOpacity>
         <View style={{width:'100%',height:1,backgroundColor:'#000'}}/>

        <View style={{ paddingVertical: AppSizes.ResponsiveSize.Padding(2) }} />

        <FloatingLabelBox labelIcon={peak_before} >
          <FloatingLabel
              labelStyle={styles.labelInput}
              inputStyle={styles.input}
              style={styles.formInput}
              value={this.state.weight}
              placeholder='Enter Peak Expiratory Flow'
              ref="peak"
              autoCapitalize = 'none'
              keyboardType={ 'numeric'}
              returnKeyType = { "next" }
              onChangeText={peak=> {this.setState({peak:(peak/1000).toString()})}}
            >Peak Expiratory Flow</FloatingLabel>
        </FloatingLabelBox>

        <FloatingLabelBox labelIcon={peak_after} >
          <FloatingLabel
              labelStyle={styles.labelInput}
              inputStyle={styles.input}
              style={styles.formInput}
              value={this.state.weight}
              placeholder='Enter Peak Flow After Puff'
              ref="peak_puff"
              autoCapitalize = 'none'
              keyboardType={ 'numeric'}
              returnKeyType = { "next" }
              onChangeText={peak_puff=> {this.setState({peak_puff:(peak_puff/1000).toString()})}}
            >Peak Flow After Puff</FloatingLabel>
        </FloatingLabelBox>

      </View>

      var DropDown=<View style={{margin:5, }}>

         <View style={{ paddingVertical: AppSizes.ResponsiveSize.Padding(2) }} />
         <TouchableOpacity onPress={()=>this.heart()} style={{marginTop: 10,flexDirection:'row',paddingTop:20,paddingBottom:10}}>
           <View style={styles.icon}>
               <Image style={styles.cicon} source={heartRate_icon}/>
           </View>
           <View style={styles.texttbox}>
             <Text style={{color:'#000',fontSize:AppSizes.ResponsiveSize.Sizes(15)}}>Heart Rate</Text>
             {this.state.heart_rate>0?<Text style={{color:'#000',fontSize:AppSizes.ResponsiveSize.Sizes(15)}}>{this.state.heart_rate} bmp</Text>:<Text style={{color:'#9d9d9d',fontSize:AppSizes.ResponsiveSize.Sizes(15)}}>Select Your Heart Rate</Text>}
           </View>
         </TouchableOpacity>
         <View style={{width:'100%',height:1,backgroundColor:'#000'}}/>

        <View style={{ paddingVertical: AppSizes.ResponsiveSize.Padding(2) }} />

        <FloatingLabelBox labelIcon={peak_before} >
          <FloatingLabel
              labelStyle={styles.labelInput}
              inputStyle={styles.input}
              style={styles.formInput}
              value={this.state.weight}
              placeholder='Enter Peak Expiratory Flow'
              ref="peak"
              autoCapitalize = 'none'
              keyboardType={ 'numeric'}
              returnKeyType = { "next" }
              onChangeText={peak=> {this.setState({peak:(peak/1000).toString()})}}
            >Peak Expiratory Flow</FloatingLabel>
        </FloatingLabelBox>
      </View>;
      var que=<Text allowFontScaling={false}>Which of these symptoms are <Text allowFontScaling={false} style={{color:'black',fontWeight:'800'}}>worse than usual</Text>?</Text>

      if(this.state.qus_no!=0){
          headerBack=<TouchableOpacity style={styles.menuWrapper} onPress={this.prev}>
                  <Image style={styles.image} source={back_icon}/>
                  </TouchableOpacity>;
        }
        else{
          headerBack=<TouchableOpacity style={styles.menuWrapper} onPress={this.back_click}>
                  <Image style={styles.image} source={back_icon}/>
                  </TouchableOpacity>;
  	  }

    return (
      <View style={styles.container}>
      <LinearGradient colors={['#48c3d5','#3fafc0','#369aaa','#287b88']}>

        <MyStatusBar barStyle="light-content"  backgroundColor="#4eb4c4"/>
          <View style={styles.appBar} >
              <View style={{ flex:1,flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', }}>
                <View style={styles.imageContainer}>
                {headerBack}
                </View>

                <View style={{ marginTop:0,width:'60%',justifyContent: 'center',alignItems:'center' }}>
                    <Text allowFontScaling={false} style={styles.headertitle}>Assessing your health!</Text>
                </View>

                <View style={{width:'15%',flexDirection: 'row',justifyContent: 'flex-end'}}>
        					<TouchableOpacity style={styles.qusWrapper} onPress={() => { this.Show_Custom_Alert(true) }}>
                      <Image style={styles.image} source={qus_icon}/>
                    </TouchableOpacity>
                </View>

              </View>
          </View>


      </LinearGradient>
      <View style={[styles.content,this.state.Modal_Visibility ? {backgroundColor: 'rgba(0,0,0,0.4)',zIndex:5} : '#ffffff']}>

      <ScrollView style={{paddingTop: 10}}>

      <View style={styles.container1}>
            <Text allowFontScaling={false} style={styles.heading}>{this.qno==2?que:this.state.question}</Text>
            <View style={styles.hr}></View>
      </View>

      <View style={styles.container2}>
          { this.qno == 1 && !this.state.peak_after_puff ? DropDown :this.qno == 1 && this.state.peak_after_puff?peak_after_puff: this.qno==7 ? textInput : options}
      </View>

      <View style={styles.container3}>
        <TouchableOpacity style={styles.btn} activeOpacity={.6} onPress={this.next}>
          <CommonButton label='Next'/>
          </TouchableOpacity>
      </View>
    </ScrollView>
    </View>
        <Spinner visible={this.state.isVisible}  />

        <Modal visible={this.state.Modal_Visibility}
          transparent={true}
          animationType={"fade"}
          onRequestClose={ () => { this.Show_Custom_Alert(!this.state.Modal_Visibility)} } >
              <View style={{ flex:1, alignItems: 'center', justifyContent: 'center',zIndex:20,backgroundColor:'#000000a3', }}>
              <View style={styles.Alert_Main_View}>
                  <View style={{ flex:1,flexDirection:'column',alignItems: 'center', justifyContent: 'center',}}>
                    <View style={{height:'15%',width:AppSizes.screen.width/10,paddingTop:'6%',marginBottom:8}}>
                        <TouchableOpacity style={{width:'100%',height:'100%'}} onPress={this.playTrack}>
                          <Image style={styles.image} source={this.state.audioImg}/>
                        </TouchableOpacity>
                      </View>
                      <View style={{height:'70%',flexDirection:'row',alignItems: 'center', justifyContent: 'center',paddingBottom:'2%',}}>
                        <ScrollView contentContainerStyle={styles.modal}>
                          <Text allowFontScaling={false} style={styles.Alert_Message}>
                            {this.state.modal_title}
                          </Text>
                        </ScrollView>
                      </View>
                  </View>
                  <View style={{width:'100%',flex:0.2,  backgroundColor: '#ebebeb',borderBottomLeftRadius:20,borderBottomRightRadius:20,}}>
                      <TouchableOpacity style={styles.buttonStyle} activeOpacity={0.7}
                        onPress={() => { this.Show_Custom_Alert(!this.state.Modal_Visibility)} }  >
                            <Text allowFontScaling={false} style={styles.TextStyle}> CANCEL </Text>
                      </TouchableOpacity>
                  </View>
                </View>
              </View>
          </Modal>

          <Modal visible={this.state.timer}
            transparent={true}
            animationType={"fade"}
            onRequestClose={ () => { this.Show_Custom_Alert(!this.state.Modal_Visibility)} } style={styles.mm}>
              <View style={{ flex:1, alignItems: 'center', justifyContent: 'center',zIndex:20,backgroundColor:'#000000a3', }}>
                <View style={styles.Alert_Main_View}>
                  <View style={{ flex:1,flexDirection:'column',alignItems: 'center', justifyContent: 'center',}}>
                      <View style={{height:'45%',width:AppSizes.screen.width/1.5,paddingTop:'8%'}}>
                        <Text allowFontScaling={false} style={{width:'100%',height:'100%',color:'#000'}} onPress={this.playTrack}>
                          If you haven’t used your rescue inhaler in the last 2 hours, please take a puff of your inhaler and re-enter your peak flow after 3-4 minutes
                        </Text>
                      </View>
                      <View style={{height:'55%',flexDirection:'row',alignItems: 'center', justifyContent: 'center'}}>
                        <CountDown
                          until={60*15}
                          size={30}
                          onFinish={() => {this.setState({timer:false});this.setState({peak_after_puff:true})}}
                          digitStyle={{backgroundColor: '#FFF'}}
                          digitTxtStyle={{color: '#48c3d5'}}
                          timeToShow={['M', 'S']}
                          timeLabels={{m: 'Minutes', s: 'Seconds'}}
                        />
                      </View>
                  </View>
                  <View style={{width:'100%',flex:0.2,  backgroundColor: '#ebebeb',borderBottomLeftRadius:20,borderBottomRightRadius:20,}}>
                      <TouchableOpacity style={styles.buttonStyle} activeOpacity={0.7}
                        onPress={() => this.setState({timer:false})} >
                            <Text allowFontScaling={false} style={styles.TextStyle}> CANCEL </Text>
                      </TouchableOpacity>
                  </View>
                </View>
              </View>
            </Modal>

      </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
    backgroundColor: '#F5FCFF',
  },
  content: {
      flex: 1,
  },
  statusBar: {
    height: AppSizes.statusBarHeight,
  },
  appBar: {
      height: AppSizes.navbarHeight,
      justifyContent:'center',
      ...Platform.select({
      android: {
      paddingTop:AppSizes.ResponsiveSize.Padding(4),
      },
    }),
    },
  headertitle:{
    color:'#ffffff',
    fontSize:(Platform.OS === 'ios') ? AppSizes.ResponsiveSize.Sizes(14) :AppSizes.ResponsiveSize.Sizes(16),
    fontWeight:'bold',
    letterSpacing:1,
    textAlign:'center'
  },
  imageContainer:{
    width:'20%'
  },
  menuWrapper: {
    width:'60%',
    height:'60%',
    marginLeft:'30%',
  },
  image: {
    flex: 1,
    width: undefined,
    height: undefined,
    resizeMode:'contain'
  },
  container1:{
    flex:2,
    flexDirection:'column',
    alignItems:'center',
    justifyContent:'center'
  },
  labelInput: {
    color: AppColors.contentColor,
    //fontWeight:'300',
    fontSize:AppSizes.ResponsiveSize.Sizes(13),
  },
  input: {
    borderWidth: 0,
    color: AppColors.contentColor,
    fontWeight:'300',
    fontSize:AppSizes.ResponsiveSize.Sizes(15),
  },
  formInput: {
    borderWidth: 0,
    borderColor: '#333',
  },
  heading:{
    fontSize:AppSizes.ResponsiveSize.Sizes(18),
    color:'#000',
    justifyContent:'center',
    alignItems:'center',
    fontWeight:'400',
    width:'89%',

  },
  hr:{
    width:50,
    height:3,
    backgroundColor:'#000',
    marginTop:10,
},
  container2:{
    flex:6,
    flexDirection:'column',
    padding: AppSizes.ResponsiveSize.Padding(5),
    width:AppSizes.screen.width,

  },
  container3:{
    flex:2,
    justifyContent:'flex-start',
    alignItems:'center',

  },

  dropdowntext:{
    fontSize:AppSizes.ResponsiveSize.Sizes(14),
    fontWeight:'500'
  },
  ansfield:{
    fontSize:AppSizes.ResponsiveSize.Sizes(13),
    color:'#5a5a5a',
  },
  btn:{
    width:'100%',
    marginBottom:20
  },
  imageContainer:{
    flex:1,
    alignItems: 'flex-start',
    //backgroundColor:'red',
  },
  qusWrapper: {
    width:'40%',
    height:'40%',
    marginRight:'30%',
    marginTop:'15%'
  },
  image: {
    flex: 1,
    width: undefined,
    height: undefined,
    resizeMode:'contain'
  },
  Alert_Main_View:{
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#ffffff',
    height:'40%',
    width: '80%',
    borderRadius:20,
    flexDirection:'column',
    justifyContent: 'center',
    alignItems: 'center',
  },

  Alert_Title:{
    fontSize: AppSizes.ResponsiveSize.Sizes(25),
    color: "#000",
    textAlign: 'center',
    padding: 10,
    height: '28%'
  },
    Alert_Message:{
      fontSize: AppSizes.ResponsiveSize.Sizes(18),
      color: "#000",
      textAlign: 'center',
      padding: 10,
    },
    buttonStyle: {
      width: '100%',
      height: '100%',
      justifyContent: 'center',
      alignItems: 'center'
    },
    TextStyle:{
      color:'#000',
      textAlign:'center',
      fontSize: AppSizes.ResponsiveSize.Sizes(18),
    },
    icon:{
      width:'10%',
      marginRight:10,
      alignItems:'center',
      justifyContent:'center',
    },
    texttbox:{
      width:'90%'
    },
    cicon:{
      width:30,
      height:30
    }
});
