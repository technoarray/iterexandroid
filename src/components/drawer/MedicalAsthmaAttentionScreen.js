import React, { Component } from 'react';
import {AsyncStorage,StyleSheet,Text,View,Image,TouchableOpacity,Platform,Alert,ScrollView,TextInput,Button,Dimensions} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions } from 'react-navigation'
import Spacer from '../common/Spacer'
import Header from '../common/signupHeader'
import CommonButton from '../common/CommonButton'
import ProgressCircle from 'react-native-progress-circle'
import LinearGradient from 'react-native-linear-gradient';
import * as commonFunctions from '../../utils/CommonFunctions'
import Spinner from 'react-native-loading-spinner-overlay';
import Orientation from 'react-native-orientation'

var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;

/* Images */
var smile_icon = require('../../themes/Images/smile.png');
var logo = require('../../themes/Images/logo.png');
var dont_need = require('../../themes/Images/dont_need.png');
var go_to_ER = require('../../themes/Images/Go_to_ER.png');
var call_Doctor = require('../../themes/Images/call_doctor1.png');
var usual_treatment = require('../../themes/Images/usual_treatment1.png');

let _that

export default class MedicalInAttentionScreen extends Component {


  constructor (props) {
      super(props)
      this.state = {
      isVisible: false,
      progress: 20,
      progressWithOnComplete: 0,
      triage_score:this.props.navigation.state.params.triagever,
      triage_data:'',
      triage_proba:this.props.navigation.state.params.triageverproba,
      med_data:'',
      med_percentage:'',
      med_color:'',
      profile:this.props.navigation.state.params.profile,
      profile_data:'',
      profile_percentage:'',
      vital:this.props.navigation.state.params.vital,
      vital_data:'',
      vital_percentage:'',
      symptom:this.props.navigation.state.params.symptom,
      symptom_data:'',
      symptom_percentage:'',
      symptom_color:'',
      progressCustomized: 0,
      Modal_Visibility: false,
      status: false,
      uid:''
    },
    _that = this;
  }

  componentDidMount() {
    AsyncStorage.getItem('loggedIn').then((value) => {
      var loggedIn = JSON.parse(value);
      if (loggedIn) {
        AsyncStorage.getItem('UserData').then((UserData) => {
            const data = JSON.parse(UserData)
            //console.log(val)
            var uid=data.uid;
            _that.setState({uid:  uid});
        })
     }
    })
    var profile=this.props.navigation.state.params.profile;
    var symptoms=this.props.navigation.state.params.symptom;
    var triage=this.props.navigation.state.params.triagever;
    var triage_proba=this.props.navigation.state.params.triageverproba;
    var vital=this.props.navigation.state.params.vital;
    var exacerbation=this.props.navigation.state.params.exacerbation;
    console.log(this.props.navigation.state.params.triage_proba);

    if(profile==1){ _that.setState({ profile_data: 'Low' , profile_percentage:20, profile_color:'#a7f171'}) }
    else if(profile==2){ _that.setState({ profile_data: 'Medium Low', profile_percentage:40, profile_color:'#fffc00' }) }
    else if(profile==3){ _that.setState({ profile_data: 'Medium', profile_percentage:60, profile_color:'#ffc300' }) }
    else if(profile==4){ _that.setState({ profile_data: 'Medium High', profile_percentage:80, profile_color:'#ff6d00' }) }
    else if(profile==5){ _that.setState({ profile_data: 'High', profile_percentage:100, profile_color:'#ff1800' }) }

    if(symptoms==1){ _that.setState({ symptom_data: 'Low' , symptom_percentage:20, symptom_color:'#a7f171'}) }
    else if(symptoms==2){ _that.setState({ symptom_data: 'Medium Low', symptom_percentage:40, symptom_color:'#fffc00' }) }
    else if(symptoms==3){ _that.setState({ symptom_data: 'Medium', symptom_percentage:60, symptom_color:'#ffc300' }) }
    else if(symptoms==4){ _that.setState({ symptom_data: 'Medium High', symptom_percentage:80, symptom_color:'#ff6d00' }) }
    else if(symptoms==5){ _that.setState({ symptom_data: 'High', symptom_percentage:100, symptom_color:'#ff1800' }) }

    if(vital==1){ _that.setState({ vital_data: 'Low' , vital_percentage:20, vital_color:'#a7f171'}) }
    else if(vital==2){ _that.setState({ vital_data: 'Medium Low', vital_percentage:40, vital_color:'#fffc00' }) }
    else if(vital==3){ _that.setState({ vital_data: 'Medium', vital_percentage:60, vital_color:'#ffc300' }) }
    else if(vital==4){ _that.setState({ vital_data: 'Medium High', vital_percentage:80, vital_color:'#ff6d00' }) }
    else if(vital==5){ _that.setState({ vital_data: 'High', vital_percentage:100, vital_color:'#ff1800' }) }

    if(triage==1){ _that.setState({ triage_score:triage,triage_img:usual_treatment,triage_data: 'You don’t need additional medical help'}) }
    else if(triage==2){ _that.setState({ triage_score:triage,triage_img:dont_need,triage_data: 'Continue with your usual treatment and check back in 1-2 days' }) }
    else if(triage==3){ _that.setState({ triage_score:triage,triage_img:call_Doctor,triage_data: 'Call your doctor’s office today' }) }
    else if(triage==4){ _that.setState({ triage_score:triage,triage_img:go_to_ER,triage_data: 'You need immediate medical attention. Please go to the emergency room or call 911' }) }

    if(exacerbation>=0 && exacerbation<=10){ _that.setState({ exacerb_data:exacerbation+'%', exacerb_percentage:Number(exacerbation), exacerb_color:'#a0f07a' }) }
    else if(exacerbation>=11 && exacerbation<=20){ _that.setState({ exacerb_data:exacerbation+'%',exacerb_percentage:Number(exacerbation), exacerb_color:'#c5f64a' }) }
    else if(exacerbation>=21 && exacerbation<=30){ _that.setState({ exacerb_data:exacerbation+'%',exacerb_percentage:Number(exacerbation), exacerb_color:'#eefc15' }) }
    else if(exacerbation>=31 && exacerbation<=40){ _that.setState({ exacerb_data:exacerbation+'%',exacerb_percentage:Number(exacerbation), exacerb_color:'#fff600' }) }
    else if(exacerbation>=41 && exacerbation<=50){ _that.setState({ exacerb_data:exacerbation+'%',exacerb_percentage:Number(exacerbation), exacerb_color:'#ffd300' }) }
    else if(exacerbation>=51 && exacerbation<=60){ _that.setState({ exacerb_data:exacerbation+'%',exacerb_percentage:Number(exacerbation), exacerb_color:'#ffba00' }) }
    else if(exacerbation>=61 && exacerbation<=70){ _that.setState({ exacerb_data:exacerbation+'%',exacerb_percentage:Number(exacerbation), exacerb_color:'#ff8c00' }) }
    else if(exacerbation>=71 && exacerbation<=80){ _that.setState({ exacerb_data:exacerbation+'%',exacerb_percentage:Number(exacerbation), exacerb_color:'#ff5900' }) }
    else if(exacerbation>=81 && exacerbation<=90){ _that.setState({ exacerb_data:exacerbation+'%',exacerb_percentage:Number(exacerbation), exacerb_color:'#ff2d00' }) }
    else if(exacerbation>=91 && exacerbation<=100){ _that.setState({ exacerb_data:exacerbation+'%',exacerb_percentage:Number(exacerbation), exacerb_color:'#ff0e00' }) }

    if(triage_proba>=0 && triage_proba<=10){ _that.setState({ med_data:triage_proba+'%', med_percentage:Number(triage_proba), med_color:'#a0f07a' }) }
    else if(triage_proba>=11 && triage_proba<=20){ _that.setState({ med_data:triage_proba+'%',med_percentage:Number(triage_proba), med_color:'#c5f64a' }) }
    else if(triage_proba>=21 && triage_proba<=30){ _that.setState({ med_data:triage_proba+'%',med_percentage:Number(triage_proba), med_color:'#eefc15' }) }
    else if(triage_proba>=31 && triage_proba<=40){ _that.setState({ med_data:triage_proba+'%',med_percentage:Number(triage_proba), med_color:'#fff600' }) }
    else if(triage_proba>=41 && triage_proba<=50){ _that.setState({ med_data:triage_proba+'%',med_percentage:Number(triage_proba), med_color:'#ffd300' }) }
    else if(triage_proba>=51 && triage_proba<=60){ _that.setState({ med_data:triage_proba+'%',med_percentage:Number(triage_proba), med_color:'#ffba00' }) }
    else if(triage_proba>=61 && triage_proba<=70){ _that.setState({med_data:triage_proba+'%',med_percentage:Number(triage_proba), med_color:'#ff8c00' }) }
    else if(triage_proba>=71 && triage_proba<=80){ _that.setState({ med_data:triage_proba+'%',med_percentage:Number(triage_proba), med_color:'#ff5900' }) }
    else if(triage_proba>=81 && triage_proba<=90){ _that.setState({ med_data:triage_proba+'%',med_percentage:Number(triage_proba), med_color:'#ff2d00' }) }
    else if(triage_proba>=91 && triage_proba<=100){ _that.setState({ med_data:triage_proba+'%',med_percentage:Number(triage_proba), med_color:'#ff0e00' }) }
  }

  increase = (key, value) => {
     this.setState({
       [key]: this.state[key] + value,
     });
   }


   morebtn=()=>{
     this.setState({ status: false});
     const currentStatus = this.state.status;
     this.setState({ status: !currentStatus});
 	}
  closebtn(){
    const {triage_score} = _that.state
    //console.log(triage_score)
    if(triage_score==1 || triage_score==2){
      _that.props.navigation.navigate('infoQusScreen', {qus_no: 2,  name: 'infoQusScreen' });
    }else if(triage_score==3){
      _that.props.navigation.navigate('infoQusScreen', {qus_no: 1,  name: 'infoQusScreen' });
    } else{
      _that.props.navigation.navigate('infoQusScreen', {qus_no: 0,  name: 'infoQusScreen' });
    }
 }

  render() {

    const headerProp = {
      title: 'Medical Help',
      screens: 'ThankYouScreen',
      qus_content:'',
    };

  const barWidth =AppSizes.screen.width/1.3;
   const progressCustomStyles = {
     backgroundColor: 'red',
     borderRadius: 0,
     borderColor: 'orange',
   };

    return (
      <View style={styles.container}>
        <Header info={headerProp} navigation={_that.props.navigation} updateAlert={this.updateAlert}/>
        <ScrollView>
            <View style={styles.container1}>
              <View style={{ flexDirection: 'column',alignItems:'center',width:'80%',marginTop:AppSizes.ResponsiveSize.Padding(2), }}>
                <View style={styles.boxiconcontainer}>
                 <Image source={this.state.triage_img} style={styles.boxicon} />
                </View>
                <View style={styles.mainContainer}>
                  <Text allowFontScaling={false} style={styles.mainTitle}>{this.state.triage_data}</Text>
                </View>
              </View>
            </View>
            <View style={styles.maincontainer2}>

            <View style={styles.container2}>
              <View style={[styles.barbox,styles.shadow]}>
              <View style={styles.leftbox}>
                <ProgressCircle
                     percent={this.state.med_percentage}
                     radius={50}
                     borderWidth={8}
                     color={this.state.med_color}
                     shadowColor="#999"
                     bgColor="#fff"
                 >
                <Text allowFontScaling={false} style={{ fontSize: 18 }}>{this.state.med_data}</Text>
                 </ProgressCircle>
                </View>
                  <View style={styles.rightbox}>
                      <Text allowFontScaling={false} style={styles.barboxtitle}>
                              Likelihood of Needing Medical Attention
                      </Text>
                  </View>
              </View>
            </View>

                    <TouchableOpacity activeOpacity={.6} onPress={this.morebtn}>

                          <View style={{ width: '100%',alignItems:'center', marginBottom:AppSizes.ResponsiveSize.Padding(4),marginTop:AppSizes.ResponsiveSize.Padding(0)}}>
                                <LinearGradient colors={['#36c4d8', '#2aa5b4', '#198391', '#15767d']} style={{width: '40%', height:AppSizes.screen.width/9, borderRadius: 25, alignItems:'center', justifyContent: 'center'}}>
                                    <Text allowFontScaling={false} style={styles.textbtn}>  {this.state.status==false ? "More" : "Less" }</Text>
                                </LinearGradient>
                          </View>

                      </TouchableOpacity>
                      {this.state.status==false ?
                         null
                        :
                        <View>
                        <View style={styles.container2}>
                          <View style={[styles.barbox,styles.shadow]}>
                          <View style={styles.leftbox}>
                              <ProgressCircle
                                   percent={this.state.exacerb_percentage}
                                   radius={50}
                                   borderWidth={8}
                                   color={this.state.exacerb_color}
                                   shadowColor="#999"
                                   bgColor="#fff"
                               >
                              <Text allowFontScaling={false} style={{ fontSize: 18 }}>{this.state.exacerb_data}</Text>
                               </ProgressCircle>
                            </View>
                              <View style={styles.rightbox}>
                                  <Text allowFontScaling={false} style={styles.barboxtitle}>
                                        Likelihood of Asthma exacerbation
                                  </Text>
                              </View>
                          </View>
                        </View>

                        <View style={styles.container2}>
                          <View style={[styles.barbox,styles.shadow]}>
                          <View style={styles.leftbox}>
                          <ProgressCircle
                               percent={this.state.symptom_percentage}
                               radius={50}
                               borderWidth={8}
                               color={this.state.symptom_color}
                               shadowColor="#999"
                               bgColor="#fff"
                           >
                          <Text allowFontScaling={false} style={{ fontSize: 15,textAlign:'center' }}>{this.state.symptom_data}</Text>
                          </ProgressCircle>
                            </View>
                                 <View style={styles.rightbox}>
                                      <Text allowFontScaling={false} style={styles.barboxtitle}>
                                            Severity of your Symptoms
                                      </Text>
                                  </View>
                          </View>
                        </View>

                        <View style={styles.container2}>
                          <View style={[styles.barbox,styles.shadow]}>
                          <View style={styles.leftbox}>
                          <ProgressCircle
                               percent={this.state.vital_percentage}
                               radius={50}
                               borderWidth={8}
                               color={this.state.vital_color}
                               shadowColor="#999"
                               bgColor="#fff"
                           >
                          <Text allowFontScaling={false} style={{ fontSize: 15,textAlign:'center' }}>{this.state.vital_data}</Text>
                          </ProgressCircle>
                            </View>
                          <View style={styles.rightbox}>
                              <Text allowFontScaling={false} style={styles.barboxtitle}>
                                    Severity of your Vital Signs
                              </Text>
                          </View>
                        </View>
                        </View>

                        <View style={styles.container2}>
                          <View style={[styles.barbox,styles.shadow]}>
                          <View style={styles.leftbox}>
                          <ProgressCircle
                               percent={this.state.profile_percentage}
                               radius={50}
                               borderWidth={8}
                               color={this.state.profile_color}
                               shadowColor="#999"
                               bgColor="#fff"
                           >
                          <Text allowFontScaling={false} style={{ fontSize: 15,textAlign:'center' }}>{this.state.profile_data}</Text>
                          </ProgressCircle>
                            </View>
                          <View style={styles.rightbox}>
                              <Text allowFontScaling={false} style={styles.barboxtitle}>
                                  Severity of your Usual Condition
                              </Text>
                          </View>
                        </View>
                        </View>


                    </View>

                      }

            </View>
            <View style={styles.container3}>
                <Text allowFontScaling={false} style={styles.textcontent}>
                  This application is not a subtitute for professional medical consultation.If you feel that you are experiencing a life-threatening emergency please call 911.If you are experiencing other symptoms not listed here,but do not feel that you are experiencing an emergency, please call your doctor.
                </Text>
                <Text allowFontScaling={false} style={styles.textcontent}>
                Iterex compares the health data that you have provided with your normal health profile to assess the likelihood of an Asthma flare-upThe risk of a flare-up, in combination with your vital signs and symptoms, provides the basis for the recommendation.
                </Text>
            </View>
        </ScrollView>
        <View style={styles.container4}>
        <TouchableOpacity activeOpacity={.6} onPress={this.closebtn}>
          <CommonButton label='Close'/>
          </TouchableOpacity>
        </View>
        <Spinner visible={this.state.isVisible}  />

      </View>


    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
	backgroundColor:'#ffffff'
  },
  container1:{
    height:AppSizes.screen.height/3,
    flexDirection:'column',
    alignItems:'center',
    //backgroundColor:'green'
  },
  boxiconcontainer:{
    width:AppSizes.screen.width/3,
    height:AppSizes.screen.width/3,
    justifyContent:'center',
  },
  boxicon:{
    width:null,
    height:null,
    flexGrow:1
  },
  mainContainer:{
    marginTop:AppSizes.ResponsiveSize.Padding(1),
    justifyContent: 'center',
    alignItems:'center'
  },
    mainTitle:{
      fontSize:AppSizes.ResponsiveSize.Sizes(19),
      justifyContent:'center',
      textAlign:'center',
      fontWeight:'700',
      textAlign:'center',
      color:AppColors.primary,
      paddingBottom:AppSizes.ResponsiveSize.Padding(3),
    },
    mainContent:{
        textAlign:'center',
        fontSize:AppSizes.ResponsiveSize.Sizes(18),
    },

    container2:{
        justifyContent: 'center',
        //height:AppSizes.screen.height/5,
       alignItems:'center',
       flex:1,
       paddingBottom:AppSizes.ResponsiveSize.Padding(2),
       //backgroundColor:'blue',
       //marginBottom:AppSizes.ResponsiveSize.Padding(2),
       //backgroundColor:'green'
    },
    barbox:{
      width:'90%',
      padding:AppSizes.ResponsiveSize.Padding(2),

      borderWidth:1,
      borderColor: '#ddd',
      backgroundColor:'#fff',
      flexDirection:'row',
      justifyContent:'center'
    },

   barboxtitle:{
     fontSize:AppSizes.ResponsiveSize.Sizes(15),
     fontWeight:'600',
     color:'#000',
     //backgroundColor:'red',
     paddingTop:AppSizes.ResponsiveSize.Padding(4),
     paddingLeft:AppSizes.ResponsiveSize.Padding(2)
   },
   shadow:{
     borderWidth:1,
     borderRadius: 2,
     borderColor: '#fff',
     justifyContent:'center',
     backgroundColor:'#fff',
     borderColor: '#ddd',
     borderBottomWidth: 1,
     shadowColor: '#999',
     shadowOffset: { width: 0, height: 2 },
     shadowOpacity: 0.8,
     shadowRadius: 2,
     elevation: 0,
     zIndex:0
     //backgroundColor:'red',
  },
  container3:{
    paddingLeft:AppSizes.ResponsiveSize.Padding(3),
    paddingRight:AppSizes.ResponsiveSize.Padding(3),
    textAlign:'center',
    //height:AppSizes.screen.height/4,
    //backgroundColor:'gray'
    paddingTop:AppSizes.ResponsiveSize.Padding(2)
  },
  container4:{
    height:AppSizes.screen.height/8,
    //backgroundColor:'red',
    justifyContent:'flex-end'
  },
  textcontent:{
    textAlign:'center',
    fontSize: AppSizes.ResponsiveSize.Sizes(11),
    marginBottom:AppSizes.ResponsiveSize.Padding(2)
  },

leftbox:{
  width:'40%',
  //backgroundColor:'red'
},
rightbox:{
  width:'60%',
  //backgroundColor:'green'
},
maincontainer2:{
  //height:AppSizes.screen.height/.90,
  //backgroundColor:'red',
  justifyContent:'space-between',
  flex:1
},
textbtn:{
  color:'#ffffff',
  fontSize:AppSizes.ResponsiveSize.Sizes(16),
}

});
