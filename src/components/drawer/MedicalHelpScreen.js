import React, { Component } from 'react';
import {StyleSheet,Text,View,Image,TouchableOpacity,Platform,Alert,ScrollView,TextInput,Button} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions, StackActions  } from 'react-navigation';
import Spacer from '../common/Spacer'
import Header from '../common/signupHeader'
import CommonButton from '../common/CommonButton'
import Orientation from 'react-native-orientation'

import * as commonFunctions from '../../utils/CommonFunctions'

var Constant = require('../../api/ApiRules').Constant;

/* Images */
var thankyou_icon = require('../../themes/Images/thank-you.png');
var logo = require('../../themes/Images/logo.png');
var medicalhelp=require('../../themes/Images/medicalhelp.png');


export default class MedicalHelpScreen extends Component {
  constructor (props) {
      super(props)
      this.state = {
      isVisible: false,
      Modal_Visibility: false
    },
    _that = this;
  }

  componentDidMount(){
    Orientation.lockToPortrait();
  }

	closebtn(){
    const resetAction = StackActions.reset({
          index: 0,
          actions: [NavigationActions.navigate({ routeName: 'StartScreen' })],
      });
  _that.props.navigation.dispatch(resetAction);
	}
  render() {
    const headerProp = {
      title: 'Am I OK?',
      screens: 'MedicalAttentionScreen',
    };

    return (
      <View style={styles.container}>
        <Header info={headerProp} navigation={_that.props.navigation} updateAlert={this.updateAlert}/>
        <View style={styles.content}>


        </View>
      </View>


    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
	backgroundColor:'#ffffff'
  },
  content: {
    flex: 1,
    padding:  AppSizes.ResponsiveSize.Padding(5),
  },
  container1:{
    flex:6,
    flexDirection:'column',
    alignItems:'center',

  },
  boxiconcontainer:{
    width:AppSizes.screen.width-500,
    height:'100%',
    justifyContent:'center',
  },
  boxicon:{
    width:null,
    height:null,
    flexGrow:1
  },
  thank:{

    width:'50%',
    height:'65%',
    flexDirection:'column',
    paddingTop:45,
    paddingBottom:10
  },
  thankcon:{
    justifyContent:'center',
    textAlign:'center',
    width:'90%'
  },
 head:{
    fontSize:AppSizes.ResponsiveSize.Sizes(32),
    color:'#000',
    justifyContent:'center',
    textAlign:'center',
    fontWeight:'800',
    marginBottom:10
  },
  subhead:{
     fontSize:AppSizes.ResponsiveSize.Sizes(12),
     color:'#000',
     justifyContent:'center',
     textAlign:'center',
     marginBottom:10
   },
  thankcontent:{
      textAlign:'center',
  },
  container2:{
     justifyContent: 'flex-end',
     flex:1,
     marginBottom:20,
  },

  container3:{
     flex:2,

    alignItems:'center',
    justifyContent:'center'
  },
  logoblk:{
    width:'80%',
    height:'50%',

  }

});
