import React, { Component } from 'react';
import {AsyncStorage,StyleSheet,Text,View,Image,TouchableOpacity,Platform,Alert,ScrollView,TextInput,Button,StatusBar} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions, StackActions  } from 'react-navigation';
import Spacer from '../common/Spacer'
import Spinner from 'react-native-loading-spinner-overlay';
import Header from '../common/signupHeader'
import CommonButton from '../common/CommonButton'
import Swiper from 'react-native-swiper';
import Animbutton from './animAnsbutton'
import AnimSinglebutton from './animSingleAnsOptions';
import { jsonData } from '../data/Question';
import { pickerData } from '../data/pickerData';
import Picker from 'react-native-picker';
import Orientation from 'react-native-orientation'
import LinearGradient from 'react-native-linear-gradient';
import * as commonFunctions from '../../utils/CommonFunctions'

var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;

/* Images */
//var thankyou_icon = require('../../themes/Images/thank-you.png');
var logo = require('../../themes/Images/logo.png');
var back_icon = require( '../../themes/Images/left-arrow.png')
var oxy_icon = require('../../themes/Images/oxy.png');
var temp_icon = require('../../themes/Images/temp.png');
var heartRate_icon = require('../../themes/Images/heart_rate.png');

var help_audio=require('../../themes/sound/VitalHelp.mp3')


let _that
let triage_data = []
let vital_data = []
let final_vital_data = []
let final_med_data = []
let python_result=[]
let options_arr=[]
let vitals=[]
let vitals_data=[]
let vitals_api=[]
const MyStatusBar = ({backgroundColor, ...props}) => (
  <View style={[styles.statusBar, { backgroundColor }]}>
    <StatusBar translucent backgroundColor={backgroundColor} {...props} />
  </View>
);

var oxygen_data = [];
var heart_data = [];
var fev_data= [];
var temperature_data= [];

//oxygen_data
for( var i = 73; i <=100; i++){
  oxygen_data.push(i);
}

//heart_data
for( var j = 30; j <151; j++){
  heart_data.push(j);
}

export default class VitalScreen extends Component {
  constructor (props) {
      super(props)

      this.state = {
        uid:'',
        isVisible: false,
        Modal_Visibility: false,
        status:'ans',
        oxygen_sat: 0,
        items1: oxygen_data,
        heart_rate: 0,
        items2: heart_data,
        temperature: 0,
        items3: pickerData.temprature,
        temperature_val:'',
        screentype:'',
        python_result:[],
        baseline_heart_rate:'',
        baseline_oxygen_sat:'',
        api:0,
        isshow1:false,
        isshow2:false,
        isshow3:false
  },
    _that = this;
  }

componentDidMount() {
  Orientation.lockToPortrait();
  AsyncStorage.getItem('loggedIn').then((value) => {
      var loggedIn = JSON.parse(value);
      if (loggedIn) {
          AsyncStorage.getItem('UserData').then((UserData) => {
              const data = JSON.parse(UserData)
              console.log(data)
              var uid=data.id;
              _that.setState({uid:  uid});

          })
     }
  })
}

validationAndApiParameter(apiname,uid) {
   if(apiname == 'web_triage'){
      var json_vitals_data=JSON.stringify(vitals_data)
      var data = {
          uid: this.state.uid,
          variable: json_vitals_data
      };
      console.log(data);
        _that.setState({isVisible: true});
        this.postToApiCalling('POST', apiname, Constant.URL_saveVitalData,data);
    }
    else if(apiname == 'save_data'){
      var date = new Date().getDate();
      var month = new Date().getMonth() + 1;
      var year = new Date().getFullYear();
      var current_date=year + '-' + month + '-' + date
      var final=[{var_name:'vital_date',value:current_date},{var_name:'vital_todo',value:1}]

      var data = {
          uid: this.state.uid,
          variable:JSON.stringify(final)
      };
      console.log(data)
      _that.setState({isVisible: true});
      this.postToApiCalling('POST', apiname, Constant.URL_saveVariable,data);
    }
    else if(apiname=='save'){
      var final=[]
      if(this.state.temperature!=0){
        final.push({var_name:'temperature_inactive',value:0})
      }
      else if (this.state.heart_rate!=0) {
        final.push({var_name:'heart_inactive',value:0})
      }
      else if (this.state.oxy_sat) {
        final.push({var_name:'oxygen_inactive',value:0})
      }
      var json_python_result = JSON.stringify(final);
      var data = {
          uid: this.state.uid,
          variable : json_python_result,
      };
      console.log(data);
      _that.setState({isVisible: true});
      this.postToApiCalling('POST', apiname, Constant.URL_saveUserData,data);
    }
  }

postToApiCalling(method, apiKey, apiUrl, data) {
  //console.log(apiUrl +" "+ apiKey)
    new Promise(function(resolve, reject) {
        if (method == 'POST') {
            resolve(WebServices.callWebService(apiUrl, data));
        } else {
          //console.log('callWebService_POST')
            resolve(WebServices.callWebService_POST(apiUrl, data));
        }
    }).then((jsonRes) => {
        //console.log(jsonRes)
          _that.setState({isVisible: false});
        if(apiKey=="python_vital"){
          _that.apiSuccessfullResponse(apiKey, jsonRes)
        }
        else{
          if ((!jsonRes) || (jsonRes.code == 0)) {
            setTimeout(() => {
                  Alert.alert(jsonRes.message);
              }, 200);
          } else {
              if (jsonRes.code == 1) {
                  _that.apiSuccessfullResponse(apiKey, jsonRes)
              }
          }
        }
    }).catch((error) => {
        console.log("ERROR" + error);
        _that.setState({ isVisible: false })
        setTimeout(() => {
            Alert.alert("Server Error");
        }, 200);
    })
}

apiSuccessfullResponse(apiKey, jsonRes) {
  if(apiKey=="web_triage"){
      _that.setState({ isVisible: false })
     triage_data = []
     vital_data = []
     final_vital_data = []
     final_med_data = []
     python_result=[]
     options_arr=[]
     vitals=[]
     vitals_data=[]
     vitals_api=[]
     _that.validationAndApiParameter('save_data');
  }
  if(apiKey=="save_data"){
    if(this.state.oxygen_sat!=0 || this.state.heart_rate!=0 || this.state.temperature!=0){
      _that.validationAndApiParameter('save')
    }
    else {
      const resetAction = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: 'TrackingScreen' })],
      });
      _that.props.navigation.dispatch(resetAction);
    }
  }
  if(apiKey=='save'){
    const resetAction = StackActions.reset({
          index: 0,
          actions: [NavigationActions.navigate({ routeName: 'TrackingScreen' })],
    });
    _that.props.navigation.dispatch(resetAction);
  }
}

  updateAlert = () => {
        this.setState({Modal_Visibility: !this.state.Modal_Visibility});
  }

  next= () =>{
    var today = new Date();
    current_date=today.getDate() + "/"+ parseInt(today.getMonth()+1) +"/"+ today.getFullYear();
    const { oxygen_sat, heart_rate, temperature,temperature_val, fev, baseline_oxygen_sat, baseline_heart_rate } = this.state
    var status=0;

    if(this.state.oxygen_sat!=0){
      vitals_data.push({var_name: 'oxygen_sat',value: this.state.oxygen_sat});
    }
    if(this.state.heart_rate!=0){
      vitals_data.push({var_name: 'heart_rate',value: this.state.heart_rate});
    }
    if(this.state.temperature!=0){
      vitals_data.push({var_name: 'temperature',value: this.state.temperature});
    }

    if(this.state.oxygen_sat!=0 || this.state.heart_rate!=0 || this.state.temperature!=0){
      _that.validationAndApiParameter('web_triage');
    }
    else{
      const resetAction = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: 'TrackingScreen' })],
      });
      _that.props.navigation.dispatch(resetAction);
    }
  }

  oxygen() {
        Picker.init({
            pickerData: this.state.items1,
            onPickerConfirm: pickedValue => {
                console.log('area', pickedValue);
                var data= pickedValue.toString()
                console.log('area', data);
                this.setState({oxygen_sat:Number(data)})
            },
            onPickerCancel: pickedValue => {
                console.log('area', pickedValue);
            },
            onPickerSelect: pickedValue => {
                //Picker.select(['山东', '青岛', '黄岛区'])
                console.log('area', pickedValue);
            }
        });
        Picker.show();
    }
    heart() {
        Picker.init({
            pickerData: this.state.items2,
            onPickerConfirm: pickedValue => {
                this.setState({heart_rate:Number(pickedValue)})
            },
            onPickerCancel: pickedValue => {
                console.log('area', pickedValue);
            },
            onPickerSelect: pickedValue => {
                //Picker.select(['山东', '青岛', '黄岛区'])
                console.log('area', pickedValue);
            }
        });
        Picker.show();
    }
    temperature() {
        Picker.init({
            pickerData: this.state.items3,
            onPickerConfirm: pickedValue => {
                this.setState({temperature:Number(pickedValue)})
            },
            onPickerCancel: pickedValue => {
                console.log('area', pickedValue);
            },
            onPickerSelect: pickedValue => {
                //Picker.select(['山东', '青岛', '黄岛区'])
                console.log('area', pickedValue);
            }
        });
        Picker.show();
    }

  render() {
    const headerProp = {
      title: 'Assessing your health!',
      screens: 'VitalScreen',
      qus_content:'Sit down and relax for at least two minutes before taking the measurements. If you regularly on oxygen, make sure you are wearing your oxygen mask and that the amount of oxygen flowing is set to the amount prescribed by your doctor. Use your pulse oximeter device following its instructions and measure your heart rate and oxygen saturation. Select the values that correspond to those displayed in your device. Use your thermometer to measure your temperature and select its value from the scroll wheel. Click “Next” when you are done!',
      qus_audio:help_audio
    };

    var DropDown = <View style={{margin:5}}>
        <TouchableOpacity onPress={()=>this.oxygen()} style={{marginTop: 10,flexDirection:'row',width:'100%',paddingTop:20,paddingBottom:10}}>
          <View style={styles.icon}>
              <Image style={styles.cicon} source={oxy_icon}/>
          </View>
          <View style={styles.texttbox}>
          <Text style={{color:'#000',fontSize:AppSizes.ResponsiveSize.Sizes(18)}}>Oxygen Saturation</Text>
          {this.state.oxygen_sat>0?<Text style={{color:'#000',fontSize:AppSizes.ResponsiveSize.Sizes(18)}}>{this.state.oxygen_sat}</Text>:<Text style={{color:'#bdc3c7',fontSize:AppSizes.ResponsiveSize.Sizes(18)}}>Select Your Oxygen Saturation</Text>}
          </View>
        </TouchableOpacity>
        <View style={{width:'100%',height:2,backgroundColor:'#000'}}/>
        <TouchableOpacity onPress={()=>this.heart()} style={{marginTop: 10,flexDirection:'row',width:'100%',paddingTop:20,paddingBottom:10}}>
          <View style={styles.icon}>
              <Image style={styles.cicon} source={heartRate_icon}/>
          </View>
          <View style={styles.texttbox}>
            <Text style={{color:'#000',fontSize:AppSizes.ResponsiveSize.Sizes(18)}}>Heart Rate</Text>
            {this.state.heart_rate>0?<Text style={{color:'#000',fontSize:AppSizes.ResponsiveSize.Sizes(18)}}>{this.state.heart_rate}</Text>:<Text style={{color:'#bdc3c7',fontSize:AppSizes.ResponsiveSize.Sizes(18)}}>Select Your Heart Rate</Text>}
          </View>
        </TouchableOpacity>
        <View style={{width:'100%',height:2,backgroundColor:'#000'}}/>
        <TouchableOpacity onPress={()=>this.temperature()} style={{marginTop: 10, flexDirection:'row',width:'100%',paddingTop:20,paddingBottom:10}}>
          <View style={styles.icon}>
              <Image style={styles.cicon} source={temp_icon}/>
          </View>
          <View style={styles.texttbox}>
            <Text style={{color:'#000',fontSize:AppSizes.ResponsiveSize.Sizes(18)}}>Temperature</Text>
            {this.state.temperature>0?<Text style={{color:'#000',fontSize:AppSizes.ResponsiveSize.Sizes(18)}}>{this.state.temperature}</Text>:<Text style={{color:'#bdc3c7',fontSize:AppSizes.ResponsiveSize.Sizes(18)}}>Select Your Temperature</Text>}
          </View>
        </TouchableOpacity>
        <View style={{width:'100%',height:2,backgroundColor:'#000'}}/>
    </View>


      {/*var DropDown=<View style={{margin:5, }}>
        <Picker PickerData={this.state.items1}
         selectedValue={0}
         labelIcon={oxy_icon}
         styleImage={styles.imageContainer}
         placeholder={"Select Your Oxygen Saturation"}
         title={"Oxygen Saturation"}
         isshow={this.state.isshow1}
         getTxt={(val,label)=> this.check_data(val,label,'oxy_sat')}
        />

         <View style={{ paddingVertical: AppSizes.ResponsiveSize.Padding(2) }} />
           <Picker PickerData={this.state.items2}
           selectedValue={0}
           labelIcon={heartRate_icon}
           styleImage={styles.imageContainer}
           placeholder={"Select Your Heart Rate"}
           title={"Heart Rate"}
           isshow={this.state.isshow2}
           //getTxt={(val,label)=>this.setState({heart_rate: val})}
            getTxt={(val,label)=> this.check_data(val,label,'heart_rate')}
           />

           <View style={{ paddingVertical: AppSizes.ResponsiveSize.Padding(2) }} />
             <Picker PickerData={this.state.items3}
              selectedValue={0}
              labelIcon={temp_icon}
              styleImage={styles.imageContainer}
              placeholder={"Select Your Temperature"}
              title={"Temperature"}
              isshow={this.state.isshow3}
              getTxt={(val,label)=> this.check_data(val,label,'temperature')}/>
      </View>;*/}



    return (
      <View style={styles.container}>
      <Header info={headerProp} navigation={_that.props.navigation} updateAlert={this.updateAlert}/>
      <View style={[styles.content,this.state.Modal_Visibility ? {backgroundColor: 'rgba(0,0,0,0.4)',zIndex:5} : '#ffffff']}>

      <ScrollView style={{paddingTop: 10}}>

      <View style={styles.container1}>
            <Text allowFontScaling={false} style={styles.heading}>While at rest please measure and enter the following</Text>
            <View style={styles.hr}></View>
      </View>

      <View style={styles.container2}>
          { DropDown }
      </View>

      <View style={styles.container3}>
        <TouchableOpacity style={styles.btn} activeOpacity={.6} onPress={this.next}>
          <CommonButton label='Submit'/>
          </TouchableOpacity>
      </View>
    </ScrollView>
    </View>

      <Spinner visible={this.state.isVisible}  />
    </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
    backgroundColor:'#ffffff'
  },
  content: {
      flex: 1,
      height:'70%',
      paddingLeft:'5%',
      paddingRight:'5%',
      paddingBottom:'5%',
  },

  image: {
    flex: 1,
    width: undefined,
    height: undefined,
    resizeMode:'contain'
  },
  container1:{
    flex:2,
    flexDirection:'column',
    alignItems:'center',
    justifyContent:'center'
  },
  heading:{
    fontSize:AppSizes.ResponsiveSize.Sizes(18),
    color:'#000',
    justifyContent:'center',
    alignItems:'center',
    fontWeight:'600',
    width:'89%',

  },
  hr:{
    width:50,
    height:3,
    backgroundColor:'#000',
    marginTop:10,
},
  container2:{
    flex:6,
    flexDirection:'column',
    padding: AppSizes.ResponsiveSize.Padding(5),
    width:AppSizes.screen.width,
    //backgroundColor:'green'
  },
  container3:{
    flex:2,
    justifyContent:'flex-start',
    alignItems:'center',

  },

  dropdowntext:{
    fontSize:AppSizes.ResponsiveSize.Sizes(14),
    fontWeight:'500'
  },
  ansfield:{
    fontSize:AppSizes.ResponsiveSize.Sizes(13),
    color:'#5a5a5a',
  },
  btn:{
    width:'100%',
    marginBottom:20
  },
  imageContainer:{
    flex:1,
    alignItems: 'flex-start',
    //backgroundColor:'red',
  },
  icon:{
    width:'10%',
    marginRight:10,
    alignItems:'center',
    justifyContent:'center',
  },
  texttbox:{
    width:'90%'
  },

  cicon:{
    width:30,
    height:30
  }
});
