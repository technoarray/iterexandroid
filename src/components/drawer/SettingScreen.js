import React, { Component } from 'react';
import {StyleSheet,Text,View,Image,TouchableOpacity,Platform,Alert,Button,AsyncStorage} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions } from 'react-navigation'
import SettingsList from 'react-native-settings-list';
import Orientation from 'react-native-orientation'
import Spacer from '../common/Spacer'
import Header from '../common/Header'
import FlipToggle from 'react-native-flip-toggle-button';
import * as commonFunctions from '../../utils/CommonFunctions'

var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;

/* Images */
var email_icon = require( '../../themes/Images/email.png');
var bell_icon = require( '../../themes/Images/bell.png');
var opt_icon = require( '../../themes/Images/otp-output.png');

let _that
export default class SettingScreen extends Component {
  constructor (props) {
    super(props)
    this.state = {
      isVisible: false,
      Modal_Visibility: false,
      email_status:'ON',
      push_notification_status:'ON',
      notification:true
    },
    _that = this;
  }

  componentDidMount(){
    Orientation.lockToPortrait();
    AsyncStorage.getItem('UserData').then((UserData) => {
        const data = JSON.parse(UserData)
        console.log(data);
        var uid=data.id;
        _that.setState({
          uid:  uid,
        });
        this.validationAndApiParameter('PushNotification',uid);
    })
  }

  updateAlert = () => {
    this.setState({Modal_Visibility: !this.state.Modal_Visibility});
  }

  validationAndApiParameter(apiname,id) {
      if(apiname == 'PushNotification'){
        var data = {
            uid:id
        };
        console.log(data);
        this.setState({isVisible: true});
        this.postToApiCalling('POST', apiname, Constant.URL_GetNotificationStatus,data);
      }
      else if (apiname=='saveNotificationStatus') {
        console.log(id);
        var status
        if(id){
          status='1'
        }
        else{
          status='0'
        }
        var data ={
          uid:this.state.uid,
          notification_status:status
        }
        console.log(data);
        this.setState({isVisible:true});
        this.postToApiCalling('POST',apiname,Constant.URL_SetNotificationStatus,data)
      }
    }

  postToApiCalling(method,apiKey, apiUrl, data) {
    //console.log(apiUrl)
      new Promise(function(resolve, reject) {
          if (method == 'POST') {
              resolve(WebServices.callWebService(apiUrl, data));
          } else {
              resolve(WebServices.callWebService_GET(apiUrl, data));
          }
      }).then((jsonRes) => {
          console.log(jsonRes)
            this.setState({ isVisible: false })
          if ((!jsonRes) || (jsonRes.code == 0)) {
            setTimeout(() => {
                  Alert.alert(jsonRes.message);
              }, 200);
          } else {
              if (jsonRes.code == 1) {
                  this.apiSuccessfullResponse(apiKey, jsonRes)
              }
          }
      }).catch((error) => {
          console.log("ERROR" + error);
          _that.setState({ isVisible: false })
          setTimeout(() => {
              Alert.alert("Server Error");
          }, 200);
      })
  }

  apiSuccessfullResponse(apiKey, jsonRes) {
    if(apiKey=='PushNotification'){
      var data=jsonRes.result[0]
      console.log(data.notification_status);
      if(data.notification_status=='1'){
        this.setState({notification:true})
      }
      else{
        this.setState({notification:false})
      }
    }
    else if (apiKey=='saveNotificationStatus') {
      var data=jsonRes
      console.log(data);
      //if(data.notification_status==1){
      //  this.setState({notification:true})
      //}
      //else{
      //  this.setState({notification:false})
      //}
    }
  }

  render() {
    const headerProp = {
      title: 'SETTINGS',
      screens: 'SettingScreen',
      qus_content:'',
      qus_audio:'https://houseofvirtruve.com/audio/settings.mp3'
    };

    return (

    <View style={styles.container}>
      <Header info={headerProp} navigation={_that.props.navigation} updateAlert={this.updateAlert}/>
      <View style={[styles.content,this.state.Modal_Visibility ? {backgroundColor: 'rgba(0,0,0,0.4)',zIndex:5} : '#ffffff']}>
        <Text style={styles.subtitle}>Notification</Text>
        {/*<View style={styles.mainbox}>
            <View style={styles.icon}>
                  <Image style={styles.cicon} source={email_icon}/>
            </View>
            <View style={styles.textbox}>
                  <Text style={styles.setting_title}>Email</Text>
            </View>
        </View>*/}
        <View style={styles.mainbox}>
            <View style={styles.icon}>
                  <Image style={styles.cicon} source={bell_icon}/>
            </View>
            <View style={{width:'65%'}}>
                  <Text style={styles.setting_title}>Push Notification</Text>
            </View>
            <View style={{width:'25%',alignSelf:'flex-end'}}>
            <FlipToggle
              value={this.state.notification}
              buttonOnColor={"#4eb4c4"}
              buttonOffColor={"#9e9e9e"}
              sliderOnColor={"#ffffff"}
              sliderOffColor={"#ffffff"}
              buttonWidth={AppSizes.screen.width/5}
              buttonHeight={AppSizes.screen.width/15}
              buttonRadius={50}
              onLabel={'On'}
              offLabel={'Off'}
              onToggle={(value) => {
                this.setState({ notification: value });
                this.validationAndApiParameter('saveNotificationStatus',value)
              }}
              changeToggleStateOnLongPress={false}
              onToggleLongPress={() => {
                console.log('Long Press');
              }}
            />
          </View>
        </View>
      </View>
    </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
  },

  content: {
      flex: 1,
      backgroundColor: '#ffffff',
      height:'70%',
      paddingLeft:'5%',
      paddingRight:'5%',
      paddingTop:15
  },
title:{
  textAlign:'center',
  color:AppColors.contentColor,
  fontSize:AppSizes.ResponsiveSize.Sizes(14),
  paddingBottom:'3%'
},
subtitle : {
  color:AppColors.contentColor,
  textAlign:'center',
  fontSize:AppSizes.ResponsiveSize.Sizes(18),
  fontWeight:'500',
  marginBottom:15
},
imageContainer:{
  width:'6%',
},
iconWrapper: {
  width:'100%',
  height:'100%',

},
image: {
  flex: 1,
  width: undefined,
  height: undefined,
  resizeMode:'contain'
},
setting_title:{
  color:AppColors.contentColor,
  color:'black',
  fontSize:AppSizes.ResponsiveSize.Sizes(14),
},
borderBottom:{
  borderBottomWidth:1,
  borderBottomColor:'#a9a9a9'
},
mainbox:{
  borderBottomWidth:1,
  borderColor:'#000',
  flexDirection:'row',
  paddingTop:8,
  marginBottom:15,
  paddingBottom:8
},
icon:{
  width:'10%',
  alignItems:'center',
  justifyContent:'center',
  //marginRight:15
},
texttbox:{
  width:'80%'
},
cicon:{
  width:20,
  height:20
}

});
