import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    TouchableHighlight,
    ScrollView,
    Dimensions,
    TextInput,
    TouchableOpacity,
    Alert,
    Keyboard,
    NetInfo,
    Platform,
    AsyncStorage,
    StatusBar
} from 'react-native';
import * as stateActions from '../../stateActions';
import { connect } from 'react-redux';
import { NavigationActions } from 'react-navigation'
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Spinner from 'react-native-loading-spinner-overlay';
import LinearGradient from 'react-native-linear-gradient';
import Header from '../common/HeaderBeforeLogin'
import Spacer from '../common/Spacer'
import * as commonFunctions from '../../utils/CommonFunctions'
import CommonButton from '../common/CommonButton'
import FloatingLabel from 'react-native-floating-labels';
import FloatingLabelBox from '../common/FloatingLabelBox';
import Orientation from 'react-native-orientation'

var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;

const logo = require('../../themes/Images/logo.png')
var email_icon = require('../../themes/Images/email.png');

let _that
export default class ForgetPasswordScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
          name: '',
          email: '',
          isVisible: false,
        },
        _that = this;
    }

    componentDidMount(){
      Orientation.lockToPortrait();
    }

  userLogin() {
    _that.props.navigation.navigate('LoginScreen');
  }

  submitForm() {
    _that.validationAndApiParameter()
  }

  validationAndApiParameter() {
        const { method, email, isVisible } = this.state

        if ((email.indexOf(' ') >= 0 || email.length <= 0)) {
            Alert.alert('','Please enter Email address!');
        }else if (!commonFunctions.validateEmail(email)) {
            Alert.alert('','Please enter valid Email address!');
        } else {
          var data = {
                email: email,
          };

            console.log(data);

              _that.setState({
                      isVisible: true
              });
              this.postToApiCalling('POST', 'forgetPassword', Constant.URL_forgotPassword, data);
        }

  }

  postToApiCalling(method, apiKey, apiUrl, data) {

     new Promise(function(resolve, reject) {
          if (method == 'POST') {
              resolve(WebServices.callWebService(apiUrl, data));
          } else {
              resolve(WebServices.callWebService_GET(apiUrl, data));
          }
      }).then((jsonRes) => {

          if ((!jsonRes) || (jsonRes.code == 0)) {

          _that.setState({ isVisible: false })
          setTimeout(()=>{
              Alert.alert(jsonRes.message);
          },200);

          } else {
              if (jsonRes.code == 1) {
                _that.setState({
                    isVisible: false,
                });
                  Alert.alert('',jsonRes.message);
              }
          }
      }).catch((error) => {
          console.log("ERROR" + error);
          _that.setState({ isVisible: false })

          setTimeout(()=>{
              Alert.alert("Server issue");
          },200);
      });
  }


    static navigationOptions = {
        header: null,
    }


    render() {
      const headerProp = {
        title: 'Forget Password',
        screens: 'ForgetScreen',
      };
        return (
          <View style={styles.wrapper}>
          <Header info={headerProp} navigation={_that.props.navigation} />
          <View style={styles.titleContainer}>
            <Text allowFontScaling={false} style={styles.headertitle}>Forget Password</Text>
          </View>
          <KeyboardAwareScrollView
              innerRef={() => {return [this.refs.email]}} >
              <View style={{paddingBottom:AppSizes.ResponsiveSize.Padding(5),}} />
            <FloatingLabelBox labelIcon={email_icon}>
            <FloatingLabel
                labelStyle={styles.labelInput}
                inputStyle={styles.input}
                style={styles.formInput}
                value={this.state.email}
                ref="email"
                autoCapitalize = 'none'
                placeholder='john@email.com'
                returnKeyType={ "next"}
                keyboardType={ 'default'}
                onChangeText={email=> this.setState({email})}
                keyboardType={ 'email-address'}
              >Email Address</FloatingLabel>
          </FloatingLabelBox>

          <TouchableOpacity activeOpacity={.6} onPress={this.submitForm}>
            <CommonButton label='Submit'/>
            </TouchableOpacity>
            </KeyboardAwareScrollView>


            <Spinner visible={this.state.isVisible}  />
          </View>
        );
    }
}

const styles = {
  wrapper: {
    flex: 1,
    flexDirection:'column',
    backgroundColor:'#ffffff'
  },
titleContainer:{
  flex:.3,
  justifyContent:'center',
  alignItems:'center',
},
  container2: {
    flex:1,
    //backgroundColor:'red'
  },

formContainer:{
  marginTop:5,
  borderBottomWidth:1,
  borderBottomColor:'#000',
},
  textbtn:{
    color: '#ffffff'
  },

  labelInput: {
    color: AppColors.contentColor,
    //fontWeight:'300',
    //fontSize:AppSizes.ResponsiveSize.Sizes(13),
  },
  input: {
    borderWidth: 0,
    color: AppColors.contentColor,
    fontWeight:'300',
    fontSize:AppSizes.ResponsiveSize.Sizes(15),
  },
  formInput: {
    borderWidth: 0,
    borderColor: '#333',
  },
  forgetWrapper:{
    flexDirection: 'row',
    width: '90%',
    alignItems:'center',
    justifyContent: 'center',
    padding:'10%'
  },

  forgetText:{
    color: AppColors.secondary,
    fontSize:AppSizes.ResponsiveSize.Sizes(16),
    fontWeight:'500'
  },

  signupWrapper:{
    flexDirection: 'row',
    width: '100%',
    alignItems:'center',
    justifyContent: 'center',
    paddingBottom:AppSizes.ResponsiveSize.Padding(5),

  },

 signupText:{
   color: AppColors.contentColor,
   fontWeight:'300',
   fontSize:AppSizes.ResponsiveSize.Sizes(14),
 },
headertitle:{
  textAlign:'center',
  color:AppColors.primary,
  fontSize:AppSizes.ResponsiveSize.Sizes(22),
  fontWeight:'500',
  letterSpacing:1,
},

}
