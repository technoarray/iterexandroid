import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    TouchableHighlight,
    ScrollView,
    Dimensions,
    TextInput,
    TouchableOpacity,
    Alert,
    Keyboard,
    NetInfo,
    Platform,
    AsyncStorage,
    StatusBar
} from 'react-native';
import * as stateActions from '../../stateActions';
import { connect } from 'react-redux';
import { NavigationActions } from 'react-navigation'
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Spinner from 'react-native-loading-spinner-overlay';
import LinearGradient from 'react-native-linear-gradient';
import Header from '../common/HeaderBeforeLogin'
import Spacer from '../common/Spacer'
import * as commonFunctions from '../../utils/CommonFunctions'
import CommonButton from '../common/CommonButton'
import FloatingLabel from 'react-native-floating-labels';
import FloatingLabelBox from '../common/FloatingLabelBox'
import Orientation from 'react-native-orientation'

var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;

const logo = require('../../themes/Images/logo.png')
var invitation_icon = require('../../themes/Images/invitation-icon.png');


let _that
export default class invitationScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
          uid:'',
          invitation_code: '',
          isVisible: false,
        },
        _that = this;
    }

    componentDidMount() {
      Orientation.lockToPortrait();
      AsyncStorage.getItem('loggedIn').then((value) => {
          var loggedIn = JSON.parse(value);
          if (loggedIn) {
              AsyncStorage.getItem('UserData').then((UserData) => {
                  const data = JSON.parse(UserData)
                  var uid=data.id;
                  _that.setState({uid:  uid});
              })
          }
      })
    }

  submitForm() {
    _that.validationAndApiParameter()
  }

  validationAndApiParameter() {
        const { method, invitation_code, isVisible } = this.state

        if ((invitation_code.indexOf(' ') >= 0 || invitation_code.length <= 0)) {
            Alert.alert('','Please enter invitation_code!');
        } else {
          var data = {
            uid:this.state.uid,
            invitation_code: invitation_code,
          };
          console.log(data);
          _that.setState({
                  isVisible: true
          });
          this.postToApiCalling('POST', 'invitationCode', Constant.URL_invitationCode, data);
        }

  }

  postToApiCalling(method, apiKey, apiUrl, data) {

     new Promise(function(resolve, reject) {
          if (method == 'POST') {
              resolve(WebServices.callWebService(apiUrl, data));
          } else {
              resolve(WebServices.callWebService_GET(apiUrl, data));
          }
      }).then((jsonRes) => {
          _that.setState({ isVisible: false })
          if ((!jsonRes) || (jsonRes.code == 0)) {


          setTimeout(()=>{
              Alert.alert(jsonRes.message);
          },200);

          } else {
            console.log(jsonRes);
            if (jsonRes.code == 1) {
              AsyncStorage.getItem('screenName').then((screenName) => {
                  var screenName = JSON.parse(screenName);
                  console.log('screenName',screenName);
                  if(screenName=='invitationScreen'){
                    AsyncStorage.setItem('screenName', JSON.stringify('signupPolicies'));
                    _that.props.navigation.navigate('signupPolicies');
                  }
                  else{
                    if(this.state.invitation_code==='Sunovion-p1'){
                      console.log('hi');
                      AsyncStorage.setItem('group',JSON.stringify(1))
                    }
                    else{
                      AsyncStorage.setItem('group',JSON.stringify(0))
                    }
                    AsyncStorage.setItem('screenName', JSON.stringify('SignupScreen'));
                    _that.props.navigation.navigate('SignupScreen');
                  }
              })
            }
          }
      }).catch((error) => {
          console.log("ERROR" + error);
          _that.setState({ isVisible: false })

          setTimeout(()=>{
              Alert.alert("Server issue");
          },200);
      });
  }


    static navigationOptions = {
        header: null,
    }


    render() {
      const headerProp = {
        title: '',
        screens: 'invitationScreen',
      };
        return (
          <View style={styles.wrapper}>
          <Header info={headerProp} navigation={_that.props.navigation} />
          <View style={styles.titleContainer}>
            <Text allowFontScaling={false} style={styles.headertitle}>Please Enter Invitation Code</Text>
          </View>
            <KeyboardAwareScrollView
                innerRef={() => {return [this.refs.invitation_code]}} >
                  <View style={{marginTop:AppSizes.ResponsiveSize.Padding(7)}}/>
              {/* Form Setion */}
              <FloatingLabelBox labelIcon={invitation_icon}>
                <FloatingLabel
                    labelStyle={styles.labelInput}
                    inputStyle={styles.input}
                    style={[styles.formInput]}
                    value={this.state.invitation_code}
                    ref="invitation_code"
                    keyboardType={ 'default'}
                    onChangeText={invitation_code=> this.setState({invitation_code})}
                  >Invitation Code</FloatingLabel>
                  </FloatingLabelBox>
                <TouchableOpacity activeOpacity={.6} onPress={this.submitForm} style={{marginTop:AppSizes.ResponsiveSize.Padding(3),}}>
                    <CommonButton label='Continue'/>
                </TouchableOpacity>

                    <View style={styles.signupWrapper}>
                    </View>

                  <View style={{marginTop:AppSizes.ResponsiveSize.Padding(7)}}/>
            </KeyboardAwareScrollView>
            <Spinner visible={this.state.isVisible}  />
          </View>
        );
    }
}

const styles = {
  wrapper: {
    flex: 1,
    flexDirection:'column',
    backgroundColor:'#ffffff'
  },
titleContainer:{
  flex:.3,
  justifyContent:'center',
  alignItems:'center',
},
  container2: {
    flex:10,
  },

formContainer:{
  marginTop:5,
  borderBottomWidth:1,
  borderBottomColor:'#000',
},
  textbtn:{
    color: '#ffffff'
  },


  labelInput: {
    color: AppColors.contentColor,
    //fontWeight:'300',
    //fontSize:AppSizes.ResponsiveSize.Sizes(13),
  },
  input: {
    borderWidth: 0,
    color: AppColors.contentColor,
    fontWeight:'300',
    fontSize:AppSizes.ResponsiveSize.Sizes(15),
  },
  formInput: {
    borderWidth: 0,
    borderColor: '#333',
  },

  forgetText:{
    color: AppColors.secondary,
    fontSize:AppSizes.ResponsiveSize.Sizes(16),
    fontWeight:'500'
  },

  signupWrapper:{
    flexDirection: 'row',
    width: '100%',
    alignItems:'center',
    justifyContent: 'center',
    paddingBottom:AppSizes.ResponsiveSize.Padding(5),

  },

 signupText:{
   color: AppColors.contentColor,
   fontWeight:'300',
   fontSize:AppSizes.ResponsiveSize.Sizes(14),
 },
headertitle:{
  textAlign:'center',
  color:AppColors.primary,
  fontSize:(Platform.OS === 'ios') ? AppSizes.ResponsiveSize.Sizes(18) :AppSizes.ResponsiveSize.Sizes(20),
  fontWeight:'500',
  letterSpacing:1,
},

}
