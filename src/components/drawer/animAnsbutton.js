import React, { Component } from 'react';
import {
  Text,
  Image,
  Platform,
  View,
  StyleSheet,
  TouchableWithoutFeedback
} from 'react-native';
import * as Animatable from 'react-native-animatable';
import { AppStyles, AppSizes, AppColors } from '../../themes/'

var logo = require('../../themes/Images/logo.png');
var tick = require('../../themes/Images/tick.png');
var tick_blue = require('../../themes/Images/tick_blue.png');

export default class AnimAnsbutton extends Component {
  constructor(props) {
    super(props);
    this.state = {
      status: false,
    }
  }
  componentWillReceiveProps(nextProps){
   //console.log(nextProps.status)
   //
   if(nextProps.status=="next"){
     this.setState({ status: false});
   }
}

  _onPress(){
    const currentStatus = this.state.status;
    this.setState({ status: !currentStatus});
    this.props._onPress(this.state.status);
    switch (this.props.effect) {
      case 'bounce':
        this.refs.view.bounce(800);
        break;
      case 'flash':
        this.refs.view.flash(800);
        break;
      case 'jello':
        this.refs.view.jello(800);
        break;
      case 'pulse':
        this.refs.view.pulse(800);
        break;
      case 'rotate':
        this.refs.view.rotate(800);
        break;
      case 'rubberBand':
        this.refs.view.rubberBand(800);
        break;
      case 'shake':
        this.refs.view.shake(800);
        break;
      case 'swing':
        this.refs.view.swing(800);
        break;
      case 'tada':
        this.refs.view.tada(800);
        break;
      case 'wobble':
        this.refs.view.wobble(800);
        break;
    }

  }

  render() {
    //console.log(this.state.status)
    return (

      <TouchableWithoutFeedback onPress={() => this._onPress()}>
        <Animatable.View ref="view" style={{borderBottomColor:this.state.status ? this.props.onColor : "#696969",borderBottomWidth:1,flexDirection:'row',justifyContent: 'space-between',alignItems: 'center'}}>
        <View style={styles.leftContainer}>
        <Text allowFontScaling={false} style={{color: this.state.status ? this.props.onColor : "#696969", fontWeight: "bold",fontSize:(Platform.OS === 'ios') ? AppSizes.ResponsiveSize.Sizes(17) :AppSizes.ResponsiveSize.Sizes(17),paddingLeft:(Platform.OS === 'ios') ? AppSizes.ResponsiveSize.Padding(1) :AppSizes.ResponsiveSize.Padding(2), }}>{this.props.text}</Text>
        </View>
          <View style={styles.rightContainer}>
          {this.state.status ?
            <Image source={tick_blue} style={styles.boxicon} />
            :
            null
          }
          {/*<Image source={tick} style={styles.boxicon} />*/}

          </View>

        </Animatable.View>
      </TouchableWithoutFeedback>
    );
  }
}

const styles = StyleSheet.create({

  boxicon:{
    width:AppSizes.screen.width/20,
    height:AppSizes.screen.width/20,
    resizeMode: 'contain',
  },
  leftContainer: {
    flex: 4,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    paddingBottom: AppSizes.ResponsiveSize.Padding(2),
    //backgroundColor: 'green'
  },
  rightContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    paddingRight: AppSizes.ResponsiveSize.Padding(3),
    paddingBottom: AppSizes.ResponsiveSize.Padding(2),
    //backgroundColor: 'red',
  },
});
