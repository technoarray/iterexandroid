import React, { Component } from 'react';
import {AsyncStorage,StyleSheet,Text,View,Image,TouchableOpacity,Platform,Alert,ScrollView,TextInput,Button} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions, StackActions  } from 'react-navigation';
import Spacer from '../common/Spacer'
import Header from '../common/signupHeader'
import CommonButton from '../common/CommonButton'
import Orientation from 'react-native-orientation'
import * as commonFunctions from '../../utils/CommonFunctions'
import Spinner from 'react-native-loading-spinner-overlay';

var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;

/* Images */
var thankyou_icon = require('../../themes/Images/thank-you.png');
var logo = require('../../themes/Images/logo.png');


let _that
export default class ThankYouScreen extends Component {
  constructor (props) {
      super(props)
      this.state = {
        uid:'',
        isVisible: false,
        Modal_Visibility: false,
        screen:this.props.navigation.state.params.screen
    },
    _that = this;
  }

  componentDidMount() {
    Orientation.lockToPortrait();
    console.log(this.props.navigation.state.params.screen);
  }

	closebtn(){
    if(_that.state.screen=='medical'){
      AsyncStorage.setItem('screenName', JSON.stringify('actqus'));
      const resetAction = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: 'ActSetup' })],
        });
        _that.props.navigation.dispatch(resetAction);
    }
    else{
      AsyncStorage.setItem('screenName', JSON.stringify('thanku'));
      const resetAction = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: 'StartScreen' })],
        });
        _that.props.navigation.dispatch(resetAction);
    }
	}

  render() {
    const headerProp = {
      title: 'Thank You',
      screens: 'ThankYouScreen',
      qus_content:'',
    };

    return (
      <View style={styles.container}>
        <Header info={headerProp} navigation={_that.props.navigation} updateAlert={this.updateAlert}/>

        <View style={styles.container1}>
          <View style={{ flexDirection: 'column',alignItems:'center',width:'80%',marginTop:AppSizes.ResponsiveSize.Padding(3), }}>
          <View style={styles.boxiconcontainer}>
            <Image source={thankyou_icon} style={styles.boxicon} />
          </View>
          <View style={styles.mainContainer}>
            <Text allowFontScaling={false} style={styles.mainTitle}>Thank You</Text>
            <Text allowFontScaling={false} style={styles.mainContent}>{this.props.navigation.state.params.message}</Text>
          </View>
          </View>
        </View>
        <View style={styles.container2}>
        <View style={styles.logoimageContainer}>
        <View style={styles.logoWrapper}>
            <Image style={styles.image} source={logo}/>
        </View>
        </View>
        </View>
        <View style={styles.container3}>
        <TouchableOpacity activeOpacity={.6} onPress={this.closebtn}>
          <CommonButton label='Close'/>
          </TouchableOpacity>
        </View>
        <Spinner visible={this.state.isVisible}  />

      </View>


    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
	backgroundColor:'#ffffff'
  },
  container1:{
    flex:5,
    flexDirection:'column',
    alignItems:'center',
    //backgroundColor:'green'
  },
  container3:{
     justifyContent: 'flex-end',
     flex:1,
     marginBottom:AppSizes.ResponsiveSize.Padding(5),
     //backgroundColor:'blue'
  },

  container2:{
     flex:2,
     alignItems:'center',
    justifyContent:'center',
    //backgroundColor:'red'
  },
  imageContainer:{
    width:'80%'
  },
  logoimageContainer:{
    width:'80%'
  },
  logoWrapper:{
    width:'100%',
    height:'100%',
  },
  image: {
    flex: 1,
    width: undefined,
    height: undefined,
    resizeMode:'contain',
  },
  titleContainer:{
    marginTop:0,
    justifyContent: 'center',
    alignItems:'center',
  },
mainContainer:{
  marginTop:AppSizes.ResponsiveSize.Padding(3),
  justifyContent: 'center',
  alignItems:'center'
},
  mainTitle:{
    fontSize:AppSizes.ResponsiveSize.Sizes(32),
    color:'#000',
    justifyContent:'center',
    textAlign:'center',
    fontWeight:'800',
    marginBottom:10,
  },
  mainContent:{
      textAlign:'center',
      fontSize:AppSizes.ResponsiveSize.Sizes(18),
  },
  boxiconcontainer:{
    width:AppSizes.screen.width/2,
    height:AppSizes.screen.width/2,
    justifyContent:'center',
  },
  boxicon:{
    width:null,
    height:null,
    flexGrow:1
  },
});
