import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    TouchableHighlight,
    ScrollView,
    Dimensions,
    TextInput,
    TouchableOpacity,
    Alert,
    Keyboard,
    NetInfo,
    Platform,
    AsyncStorage,
    StatusBar,
} from 'react-native';
import * as stateActions from '../../stateActions';
import { connect } from 'react-redux';
import { NavigationActions, StackActions  } from 'react-navigation';
import { AppStyles, AppSizes, AppColors } from '../../themes/';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import Spinner from 'react-native-loading-spinner-overlay';
import LinearGradient from 'react-native-linear-gradient';
import Spacer from '../common/Spacer';
import * as commonFunctions from '../../utils/CommonFunctions';
import CommonButton from '../common/CommonButton';
import FloatingLabel from 'react-native-floating-labels';
import Header from '../common/HeaderBeforeLogin';
import FloatingLabelBox from '../common/FloatingLabelBox';
import Orientation from 'react-native-orientation';

var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;

const logo = require('../../themes/Images/logo.png')

let _that

var email_icon = require('../../themes/Images/email.png');
var lock_icon = require('../../themes/Images/lock.png');

const MyStatusBar = ({backgroundColor, ...props}) => (
  <View style={[styles.statusBar, { backgroundColor }]}>
    <StatusBar translucent backgroundColor={backgroundColor} {...props} />
  </View>
);
export default class LoginScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
          devicetoken: '',
          devicetype: '',
          method:'',
          first_name: '',
          last_name: '',
          email: '',
          password: '',
          isVisible: false,
          loggedInn: false,
          userProfileData: {},
          emailErrorMessage:'',
          passwordErrorMessage:'',
          errorMessage:''
        },
        _that = this;
    }

    /*componentDidMount(){
      Orientation.lockToPortrait();

    }*/

    componentDidMount() {
        Orientation.lockToPortrait();
        AsyncStorage.getItem('DeviceToken').then((value) => {
          var token = value
          this.setState({devicetoken:value})
        })
        if(Platform.OS=='ios'){
          this.setState({devicetype:'1'})
        }
        else{
          this.setState({devicetype:'2'})
        }
    }


  userLogin() {
  //  const resetAction = NavigationActions.navigate({ routeName: 'StartScreen'})
  //  _that.props.navigation.dispatch(resetAction);
    _that.validationAndApiParameter()
  }

  validationAndApiParameter() {
        const { method, devicetoken, devicetype, email, password, isVisible } = this.state
        var error=0

        if ((email.indexOf(' ') >= 0 || email.length <= 0)) {
          this.setState({emailErrorMessage:'Please enter Email address!'});
          error=1;
            //Alert.alert('Login Error','Please enter Email address!');
        }else if (!commonFunctions.validateEmail(email)) {
          this.setState({emailErrorMessage:'Please enter valid Email address!'})
          error=1;
            //Alert.alert('Login Error','Please enter valid Email address!');
        }
        if ((password.indexOf(' ') >= 0 || password.length <= 0)) {
          this.setState({passwordErrorMessage:'Please enter password!'})
          error=1;
            //Alert.alert('Login Error','Please enter password!');
        }
        if(error==0){

           var data = {
            deviceToken: devicetoken,
            deviceType: devicetype,
            email: email,
            password: password
           };

          console.log(data);

          _that.setState({
            isVisible: true
          });
          this.postToApiCalling('POST', 'login', Constant.URL_login, data);
        }

  }

  postToApiCalling(method, apiKey, apiUrl, data) {

     new Promise(function(resolve, reject) {
          if (method == 'POST') {
              resolve(WebServices.callWebService(apiUrl, data));
          } else {
              resolve(WebServices.callWebService_GET(apiUrl, data));
          }
      }).then((jsonRes) => {
          if ((!jsonRes) || (jsonRes.code == 0)) {
            console.log(jsonRes);
          _that.setState({ isVisible: false })
          setTimeout(()=>{
            this.setState({errorMessage:jsonRes.message})
          },200);

        }else if(jsonRes.code ==2){
          this.setState({passwordErrorMessage:jsonRes.message});
          _that.setState({ isVisible: false })
        }else if(jsonRes.code ==3){
          this.setState({emailErrorMessage:jsonRes.message});
          _that.setState({ isVisible: false })
        }else {
              if (jsonRes.code == 1) {
                  _that.apiSuccessfullResponse(apiKey, jsonRes)
              }
          }
      }).catch((error) => {
          console.log("ERROR" + error);
          _that.setState({ isVisible: false })

          setTimeout(()=>{
              Alert.alert("Server issue" + error);
          },200);
      });
  }

  apiSuccessfullResponse(apiKey, jsonRes) {
      if (apiKey == 'login') {
        console.log(jsonRes)
          _that.setState({
              isVisible: false,
              userProfileData:jsonRes.data
          });
          var vitals=[]

          AsyncStorage.setItem("loggedIn", JSON.stringify(true)).done();
          AsyncStorage.setItem('screenName', JSON.stringify('thank'));
          AsyncStorage.setItem('UserData', JSON.stringify(jsonRes.data));

          vitals.push({name: 'oxygen_sat',value:jsonRes.vitals.oxygen_sat});
          vitals.push({name: 'heart_rate',value:jsonRes.vitals.heart_rate});
          vitals.push({name: 'temperature',value:jsonRes.vitals.temperature});
          //console.log(vitals)
          AsyncStorage.setItem('vitals', JSON.stringify(vitals));

          setTimeout(()=>{
            if(jsonRes.status=="exist"){
              const resetAction = StackActions.reset({
                    index: 0,
                    actions: [NavigationActions.navigate({ routeName: 'StartScreen' })],
                });
            _that.props.navigation.dispatch(resetAction);
            }
            else {
              _that.props.navigation.navigate('signupPolicies');
            }
          },200);
      }
  }

  userSignup() {
    _that.props.navigation.navigate('invitationScreen');
  }

  userForgetPassword() {
    _that.props.navigation.navigate('ForgetScreen');

  }

    static navigationOptions = {
        header: null,
    }


    render() {
      const headerProp = {
        title: '',
        screens: 'LoginScreen',
      };
      return (

        <View style={styles.container}>
          <Header info={headerProp} navigation={_that.props.navigation} />
          <View style={styles.titleContainer}>
            <Text allowFontScaling={false} style={styles.headertitle}>Login</Text>
          </View>
          {this.state.errorMessage != ''?
            <View style={styles.error}>
              <Text allowFontScaling={false} style={styles.errorMessageText}>{this.state.errorMessage}</Text>
            </View>
          :
            null
        }
          <View style={[styles.content,this.state.Modal_Visibility ? {backgroundColor: 'rgba(0,0,0,0.4)',zIndex:5} : '#ffffff']}>
          <KeyboardAwareScrollView
              innerRef={() => {return [this.refs.first_name,this.refs.last_name, this.refs.email,this.refs.password]}} >
              <View style={{paddingBottom:AppSizes.ResponsiveSize.Padding(5),}} />
              <FloatingLabelBox labelIcon={email_icon}>
            <FloatingLabel
                labelStyle={styles.labelInput}
                inputStyle={styles.input}
                style={styles.formInput}
                value={this.state.email}
                ref="email"
                autoCapitalize = 'none'
                placeholder='john@email.com'
                returnKeyType={ "next"}
                onChangeText={(email)=> {this.setState({email});
              this.setState({emailErrorMessage:''});
              this.setState({errorMessage:''}) } }
                keyboardType={ 'email-address'}
              >Email Address</FloatingLabel>
          </FloatingLabelBox>
          <View style={styles.errorContainer}>
            <Text allowFontScaling={false} style={styles.errorText}>{this.state.emailErrorMessage}</Text>
          </View>

          <FloatingLabelBox labelIcon={lock_icon}>
            <FloatingLabel
                labelStyle={styles.labelInput}
                inputStyle={styles.input}
                style={styles.formInput}
                value={this.state.password}
                placeholder='Enter Password'
                ref="password"
                autoCapitalize = 'none'
                secureTextEntry={true}
                returnKeyType = { "done" }
                onChangeText={(password)=> {this.setState({password});
              this.setState({passwordErrorMessage:''});
              this.setState({errorMessage:''}) } }
              >Password</FloatingLabel>
          </FloatingLabelBox>
          <View style={styles.errorContainer}>
            <Text allowFontScaling={false} style={styles.errorText}>{this.state.passwordErrorMessage}</Text>
          </View>

          <TouchableOpacity activeOpacity={.6} onPress={this.userLogin}>
            <CommonButton label='Login'/>
            </TouchableOpacity>

            <View style={styles.forgetContainer}>
               <TouchableOpacity activeOpacity={.6} onPress={this.userForgetPassword}>
                 <Text allowFontScaling={false} style={styles.textforget}>Forgot your Password?</Text>
                 </TouchableOpacity>
             </View>

            </KeyboardAwareScrollView>




        </View>
          <Spinner visible={this.state.isVisible}  />

      </View>

      );
    }
  }

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      paddingTop:  0,
      backgroundColor: '#ffffff',
      justifyContent: 'center',
    },
    content: {
        flex: 1,
        backgroundColor: '#ffffff',
        justifyContent: 'center',
    },
    titleContainer:{
      flex:.3,
      justifyContent:'center',
      alignItems:'center',
    },
    headertitle:{
      textAlign:'center',
      color:AppColors.primary,
      fontSize:AppSizes.ResponsiveSize.Sizes(22),
      fontWeight:'500',
      letterSpacing:1,
    },
    title:{
      textAlign:'center',
      fontSize:AppSizes.ResponsiveSize.Sizes(14),
      paddingTop:'3%'
    },

    ToggalContainer: {
      backgroundColor: '#fff',
      borderBottomWidth: 2,
      borderBottomColor:'#000'
    },

    ToggleInner: {
    height: 100,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#ebebeb'
  },

  labelInput: {
    color: AppColors.contentColor,
    //fontWeight:'300',
    fontSize:AppSizes.ResponsiveSize.Sizes(14),
  },
  input: {
    borderWidth: 0,
    color: AppColors.contentColor,
    fontWeight:'300',
    fontSize:AppSizes.ResponsiveSize.Sizes(15),
  },
  formInput: {
    borderWidth: 0,
    borderColor: '#333',

  },
  titleContainer1: {
    width:'92%',
    flex:1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomWidth:1,
    borderBottomColor:'#000'
  },
  forgetContainer:{
    width:'90%',
    //backgroundColor:'red'
  },
  textforget:{
   color:'#000',
   fontSize:AppSizes.ResponsiveSize.Sizes(12),
   fontWeight:'600',
   textAlign:'right',
 },
 errorContainer:{
   paddingRight:'3%',
   paddingLeft:'15%',
   paddingTop:'3%'
 },
  errorText:{
    color:'red',
  },
  error:{
    backgroundColor:'#bdc3c7',
    justifyContent:'center',
    height:'8%'
  },
  errorMessageText:{
    color:'red',
    paddingLeft:'17%',
    paddingRight:'3%'
  }
  });
