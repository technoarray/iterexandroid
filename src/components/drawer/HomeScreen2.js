import React, { Component } from 'react';
import {AsyncStorage,StyleSheet,Text,View,Image,TouchableOpacity,Platform,Alert,Modal,ScrollView} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions } from 'react-navigation'
import Spacer from '../common/Spacer'
import Spinner from 'react-native-loading-spinner-overlay';
import Sound from 'react-native-sound'
import Orientation from 'react-native-orientation'
import Header from '../common/Header'
import * as commonFunctions from '../../utils/CommonFunctions'

var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;

let _that
var peak_status='0'
var temp_status='0'
var heart_status='0'
var amiok
var act
var vital
var medical
var age
var height
var weight
var uid

/* Images */
var heart_icon=require('../../themes/Images/heart_icon_196.png')
var share_icon=require('../../themes/Images/share.png')
var chart_icon=require('../../themes/Images/chart_icon_196.png')
var medical_icon=require('../../themes/Images/medical_icon_196.png')
var lungs_icon=require('../../themes/Images/lungs_icon_196.png')
var arrow_icon=require('../../themes/Images/arrow_icon_196.png')
var qus_icon = require('../../themes/Images/que_icon_196.png');
var mice=require('../../themes/Images/mice.png')
var mice_off=require('../../themes/Images/mice-off.png')

var help_audio=require('../../themes/sound/HomeHelp.mp3')

export default class HomeScreen extends Component {
  constructor (props) {
      super(props)
      this.state = {
      uid:'',
      peak_flow: '-',
      heart_rate: '-',
      temperature:'-',
      isVisible: false,
      Modal_Visibility: false,
      audioStatus: false,
      audioImg: mice,
      modal_title:'',
      account_type:'',
      act:''
    },
    _that = this;
  }

  componentWillMount() {
    Orientation.unlockAllOrientations();
    Orientation.lockToPortrait();

    AsyncStorage.getItem('loggedIn').then((value) => {
        var loggedIn = JSON.parse(value);
        if (loggedIn) {
          AsyncStorage.getItem('UserData').then((UserData) => {
              const data = JSON.parse(UserData)
              console.log(data);
              uid=data.id;
              var type=data.account_type

              _that.setState({
                uid: uid,
                account_type:type
              });
              _that.validationAndApiParameter('getData',uid)
          })
        }
    })
  }

  validationAndApiParameter(apiname,uid) {
    if(apiname=='getData'){
      var data = {
          uid: uid
      };
      console.log(data);
      _that.setState({isVisible: true});
      this.postToApiCalling('POST', 'getData', Constant.URL_asthmaProfileVariable,data);
    }
  }

  postToApiCalling(method, apiKey, apiUrl, data) {
      new Promise(function(resolve, reject) {
          if (method == 'POST') {
              resolve(WebServices.callWebService(apiUrl, data));
          } else {
              resolve(WebServices.callWebService_GET(apiUrl, data));
          }
      }).then((jsonRes) => {
        _that.setState({ isVisible: false })
        _that.apiSuccessfullResponse(apiKey, jsonRes)

      }).catch((error) => {
          console.log("ERROR" + error);
          _that.setState({ isVisible: false })
          setTimeout(() => {
              Alert.alert("Server Error" + error);
          }, 200);
      })
  }

  apiSuccessfullResponse(apiKey, jsonRes) {
      if (apiKey == 'getData') {
        console.log(jsonRes)
        peak_status=jsonRes.profile.peak_status
        temp_status=jsonRes.profile.temp_status
        heart_status=jsonRes.profile.heart_status
        this.setState({act_score:jsonRes.profile.act_score})
        if(peak_status=='1'){
          var peak=jsonRes.profile.VTL_BM_PEF
          _that.setState({peak_flow:parseFloat(peak*1000)})
        }
        if(heart_status=='1'){
          _that.setState({heart_rate:jsonRes.profile.VTL_BM_P})
        }
        if(temp_status=='1'){
          _that.setState({temperature:jsonRes.profile.VTL_BM_TMP})
        }
        age=jsonRes.profile.BL_AGE
        weight=jsonRes.profile.BL_WEIGHT
        height=jsonRes.profile.OUTP_HEIGHT

        act=jsonRes.profile.act_date
        amiok=jsonRes.profile.amiok_date
        vital=jsonRes.profile.vital_date
        medical=jsonRes.profile.medical_que_date
      }
    }

  updateAlert = (visible) => {
        this.setState({Modal_Visibility: !this.state.Modal_Visibility});
  }

  userAmIOk=()=>{
   AsyncStorage.setItem('from', JSON.stringify('HomeScreen2'));
  _that.props.navigation.navigate('AmIOkAsthmaQusScreen', {qus_no: 0,  name: 'HomeScreen' });
  }

  tracking=()=>{
   _that.props.navigation.navigate('AsthmaToDo',{amiok:amiok,act:act,vital:vital,medical:medical,act_score:this.state.act_score});
  }

  healthProfile=()=>{
    _that.props.navigation.navigate('AsthmaHealthProfile', {peak_flow:_that.state.peak_flow,heart_rate: _that.state.heart_rate,temperature:_that.state.temperature,age:age,height:height,weight:weight});
  }

  chartbtn1(){
    _that.props.navigation.navigate('AsthmaCharts', {vital_type:'pef_sat' });
  }

  chartbtn2(){
    _that.props.navigation.navigate('AsthmaCharts', {vital_type:'heart_rate' });
  }

  chartbtn3(){
    _that.props.navigation.navigate('AsthmaCharts', {vital_type:'act' });
  }


  shareClick=()=>{
    _that.props.navigation.navigate('ShareScreen');
  }

  render() {
  //  console.log("hello");
    var popup_data=<Text>
    <Text>The Home page enables easy access to your health tools, just tap or click on the one you want to use.{"\n\n"}</Text>
    <Text><Text style={{fontWeight:'bold'}}>•	Am I OK?</Text> Use this at least once a week or if you are not feeling well.{"\n\n"}</Text>
    <Text><Text style={{fontWeight:'bold'}}>•	To Do</Text> Check regularly for health tasks that require periodic updates or to access questionnaires.{"\n\n"}</Text>
    <Text><Text style={{fontWeight:'bold'}}>•	Health Profile</Text> Use this to check or update your baseline health profile.{"\n\n"}</Text>
    <Text>• Share icon Use this to share your data with your physician.</Text></Text>;

    const headerProp = {
      title: 'HOME',
      screens: 'HomeScreen',
      qus_content:popup_data,
      qus_audio:help_audio
    };
    return (

      <View style={styles.container}>
        <Header info={headerProp} navigation={_that.props.navigation} updateAlert={this.updateAlert}/>

        <View style={[styles.content,this.state.Modal_Visibility ? {backgroundColor: 'rgba(0,0,0,0.4)',zIndex:5} : '#ffffff']}>
          <View style={{flex:.5,alignItems:'center', justifyContent: 'center'}}>
          </View>

          <View style={{flex:3,flexDirection:'row',marginBottom:'2%'}} >
            <View style={[styles.boxDesign,styles.shadow]}>

              <TouchableOpacity activeOpacity={.6} onPress={this.userAmIOk}>
                <Text allowFontScaling={false} style={styles.boxtitle}>AM  I  OK</Text>
              </TouchableOpacity>
            </View>
            <View style={{position: 'absolute',top: '35%',right: 0,width: '25%',height: '70%',zIndex:2}}>
              <Image style={styles.boxicon} source={lungs_icon}/>
            </View>
          </View>

          <View style={{flex:3,flexDirection:'row',justifyContent: 'space-around',marginBottom:'2%'}}>
            <TouchableOpacity activeOpacity={.6} onPress={this.tracking} style={[styles.boxDesign1,styles.shadow]}>
              <View style={styles.boxiconcontainer}>
                <Image source={heart_icon} style={styles.boxicon} />
              </View>
              <View>
                 <Text allowFontScaling={false} style={styles.boxtitle1}>TO DO</Text>
              </View>
            </TouchableOpacity>
            <View style={{position: 'absolute',bottom: '15%',left:'35%',width: '20%',height: '20%'}}>
              <Image style={styles.boxicon} source={arrow_icon}/>
            </View>

            <TouchableOpacity activeOpacity={.6} onPress={this.healthProfile} style={[styles.boxDesign1,styles.shadow]}>

              <View style={styles.boxiconcontainer}>
                <Image source={medical_icon} style={styles.boxicon} />
              </View>
              <View>
                 <Text allowFontScaling={false} style={styles.boxtitle1}>HEALTH PROFILE</Text>
              </View>
            </TouchableOpacity>
            <View style={{position: 'absolute',bottom: '15%',left:'85%',width: '20%',height: '20%'}}>
              <Image style={styles.boxicon} source={arrow_icon}/>
            </View>
          </View>

          <View style={{flex:3}}>
            <View style={styles.boxDesign2}>
              {peak_status=='1'?
                <TouchableOpacity activeOpacity={.6} onPress={()=>this.chartbtn1()} style={[styles.boxcontainer1,styles.shadow,{paddingLeft:5}]}>
                  <Text allowFontScaling={false} style={styles.boxnumber}>{this.state.peak_flow} L/Min</Text>
                  <Text allowFontScaling={false} style={styles.boxtitle1}>Peak Flow </Text>
                </TouchableOpacity>
                :
                <TouchableOpacity activeOpacity={.6} onPress={()=>this.chartbtn1()} style={[styles.boxcontainer1,styles.shadow,{paddingLeft:5}]}>
                  <Text allowFontScaling={false} style={styles.boxnumber}>{this.state.peak_flow}</Text>
                  <Text allowFontScaling={false} style={styles.boxtitle1}>Peak Flow </Text>
                </TouchableOpacity>
              }
              {heart_status=='1'?
                <TouchableOpacity activeOpacity={.6} onPress={()=>this.chartbtn2()} style={[styles.boxcontainer1,styles.shadow,{paddingLeft:5}]}>
                  <Text allowFontScaling={false} style={styles.boxnumber}>{this.state.heart_rate} bpm</Text>
                  <Text allowFontScaling={false} style={styles.boxtitle1}>Heart Rate</Text>
                </TouchableOpacity>
              :
                <TouchableOpacity activeOpacity={.6} onPress={()=>this.chartbtn2()} style={[styles.boxcontainer1,styles.shadow]}>
                  <Text allowFontScaling={false} style={styles.boxnumber}>{this.state.heart_rate}</Text>
                  <Text allowFontScaling={false} style={styles.boxtitle1}>Heart Rate</Text>
                </TouchableOpacity>
              }

                {this.state.act_score!= '-'?
                <TouchableOpacity activeOpacity={.6} onPress={()=>this.chartbtn3()} style={[styles.boxcontainer1,styles.shadow]}>
                  <Text allowFontScaling={false} style={styles.boxnumber}>{this.state.act_score}/25</Text>
                  <Text allowFontScaling={false} style={styles.boxtitle1}>ACT</Text>
                </TouchableOpacity>
                :
                <TouchableOpacity activeOpacity={.6} onPress={()=>this.chartbtn3()} style={[styles.boxcontainer1,styles.shadow]}>
                  <Text allowFontScaling={false} style={styles.boxnumber}>{this.state.act_score}</Text>
                  <Text allowFontScaling={false} style={styles.boxtitle1}>ACT</Text>
                </TouchableOpacity>
                }
            </View>
          </View>

        <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>

          <View style={[styles.imageContainer,{alignItems:'center',justifyContent:'center'}]}>
            <TouchableOpacity style={styles.menuWrapper} activeOpacity={.6} onPress={this.shareClick}>
              <Image style={styles.image} source={share_icon}/>
            </TouchableOpacity>
          </View>
        </View>

        </View>
        <Spinner visible={this.state.isVisible}  />

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
  },

  content: {
      flex: 1,
      height:'70%',
      paddingLeft:'5%',
      paddingRight:'5%',
      paddingBottom:'5%',
  },
  headTitle:{
    textAlign:'center',
    fontWeight:'500',
    fontSize: AppSizes.ResponsiveSize.Sizes(16),
    color:'#5397a0'
  },
  boxDesign:{
    flexDirection: 'row',
    width: '90%',
    alignItems:'center',
    justifyContent: 'center',
    height:'65%',
    position: 'relative',
    marginBottom:'6%',
    marginLeft:'2%',
    position:'relative'
  },
  boxDesign1:{
    width:'42%',
     height:'80%',
    justifyContent:'center',
    alignItems:'center',
    position: 'relative',
   marginRight:'5%',
  },
  boxDesign2:{
    height:'78%',
    flexDirection: 'row',
    flexWrap:'wrap',
    alignItems:'center',
    justifyContent: 'space-between',
    marginBottom:'2%',

  },
  shadow:{
    borderWidth:1,
    borderRadius: 2,
    borderColor: '#fff',
    justifyContent:'center',
    backgroundColor:'#fff',
    borderColor: '#ddd',
    borderBottomWidth: 0,
    shadowColor: '#999',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 0,
    zIndex:0,
    //backgroundColor:'red',
 },
  boxtitle:{
    textAlign:'center',
    fontWeight:'bold',
    fontSize:AppSizes.ResponsiveSize.Sizes(32),
    color:AppColors.primary,
    shadowOpacity: 0,
  },

  boxcontainer1:{
    width:'32%',
    height:'100%',
    justifyContent:'center',
    alignItems:'center',

  },
  boxiconcontainer:{
    width:AppSizes.screen.width-150,
    height:'50%',
    justifyContent: 'center',
    paddingBottom:'10%'
  },
  boxicon:{
    flexGrow:1,
    height:null,
    width:null,
    alignItems: 'center',
    justifyContent:'center',
    resizeMode:'contain',
  },
  boxtitle1:{
    fontSize: AppSizes.ResponsiveSize.Sizes(12),
    fontWeight:'bold',
    color:AppColors.primary,
    textAlign:'center',
    shadowOpacity: 0,
    marginBottom:AppSizes.ResponsiveSize.Padding(5)
  },

  imageContainer:{
    width:'20%',
  },
  menuWrapper: {
    width:90,
    height:90,
  },

  image: {
    flex: 1,
    width: 90,
    height: 90,
    resizeMode:'contain'
  },
  boxnumber:{
    fontSize:AppSizes.ResponsiveSize.Sizes(13),
    color:AppColors.contentColor,
    fontWeight:'bold',
    textAlign:'center'
  },
  spaceBox:{
    marginBottom:'10%'
  },
  extrapad:{
    paddingTop:AppSizes.ResponsiveSize.Padding(14)
  },
  thintext:{
  fontSize:AppSizes.ResponsiveSize.Sizes(6)
},
que:{
  position:'absolute',
  right:10,
  top:4
},
que2:{
  position:'absolute',
  right:-13,
  top:5,
  zIndex:1111
},
qusWrapper1:{
  backgroundColor:'#2596a6',
  height:25,
  width:25,
  padding:6,
  borderRadius:30
},
qusWrapper: {
  width:'40%',
  height:'40%',
  marginRight:'30%',
  marginTop:'15%'
},
image: {
  flex: 1,
  width: undefined,
  height: undefined,
  resizeMode:'contain'
},
Alert_Main_View:{
  alignItems: 'center',
  justifyContent: 'center',
  backgroundColor: '#ffffff',
  height:'40%',
  width: '80%',
  borderRadius:20,
  flexDirection:'column',
  justifyContent: 'center',
  alignItems: 'center',
},

Alert_Title:{
  fontSize: AppSizes.ResponsiveSize.Sizes(25),
  color: "#000",
  textAlign: 'center',
  padding: 10,
  height: '28%'
},
  Alert_Message:{
    fontSize: AppSizes.ResponsiveSize.Sizes(18),
    color: "#000",
    textAlign: 'center',
    padding: 10,
  },
  buttonStyle: {
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  TextStyle:{
    color:'#000',
    textAlign:'center',
    fontSize: AppSizes.ResponsiveSize.Sizes(18),
  },

});
