import React, { Component } from 'react';
import {StyleSheet,Text,View,Image,TouchableOpacity,Platform,ScrollView,Alert,AsyncStorage,Keyboard} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions } from 'react-navigation'
import Spacer from '../common/Spacer'
import Header from '../common/Header'
import ToggleBox from '../common/ToggleBox'
import CommonButton from '../common/CommonButton'
import LinearGradient from 'react-native-linear-gradient';
import FloatingLabelBox from '../common/FloatingLabelBox'
import FloatingLabel from 'react-native-floating-labels';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Spinner from 'react-native-loading-spinner-overlay';
import { pickerData } from '../data/pickerData';
import Picker from 'react-native-q-picker';
import * as commonFunctions from '../../utils/CommonFunctions'
import Orientation from 'react-native-orientation'

var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;

/* Images */
var email_icon = require('../../themes/Images/email.png');
var lock_icon = require('../../themes/Images/lock.png');
var navigation_icon = require('../../themes/Images/navigation.png');
var group_code_icon = require('../../themes/Images/group-code.png');
var user_icon = require('../../themes/Images/user.png');
var unique_code = require('../../themes/Images/unique-code.png');

let _that

export default class AccountScreen extends Component {
  constructor (props) {
    super(props)
    this.state = {
    isVisible: false,
    Modal_Visibility: false,
    uid: '',
    email:'',
    username : '',
    password : '',
    password : '',
    group_code :'',
    unique_code:''
  },
  _that = this;
}

  componentWillMount() {
    Orientation.lockToPortrait();
    AsyncStorage.getItem('loggedIn').then((value) => {
      var loggedIn = JSON.parse(value);
      if (loggedIn) {
        AsyncStorage.getItem('UserData').then((UserData) => {
          const data = JSON.parse(UserData)
          console.log(data)
          if(data.account_type=='asthma'){
            console.log(data.id);
            this.validationAndApiParameter('userdata',data.id)
          }else{
            console.log('hello');
            _that.setState({
              uid : data.id,
              email : data.email,
              password : data.password,
              username : data.username,
              group_code : data.group_code,
              unique_code:data.unique_code
            });
          }
        })
      }
    })
  }

updateAlert = () => {
      this.setState({Modal_Visibility: !this.state.Modal_Visibility});
  }

accountSave(){
    _that.validationAndApiParameter('save')
}

validationAndApiParameter(apikey,id) {
      const { method, devicetoken, devicetype, uid, username, email, password,group_code,isVisible } = this.state
      if(apikey=='save'){
        if ((username.length <= 0)) {
            Alert.alert('','Please enter User name!');
        }
        else {
          Keyboard.dismiss()
          var data = {
              device_token: "",
              device_type: "",
              uid:uid,
              username: username,
              email: email,
              password: password,
              group_code: group_code,
          };
          console.log(data);
          _that.setState({
              isVisible: true
          });
          this.postToApiCalling('POST', 'account_edit', Constant.URL_AccountEdit, data);
        }
      }
      if(apikey=='generate'){
        var data = {
          uid :uid,
        };
        console.log(data);
        this.postToApiCalling('POST', 'generate', Constant.URL_Unique, data);
      }
      if(apikey=='userdata'){
        var data = {
            uid: id
        };
        console.log(data);
        _that.setState({isVisible: true});
        this.postToApiCalling('POST', apikey, Constant.URL_Userdata,data);
      }
}

postToApiCalling(method, apiKey, apiUrl, data) {
  //console.log(apiUrl)
    new Promise(function(resolve, reject) {
        if (method == 'POST') {
            resolve(WebServices.callWebService(apiUrl, data));
        } else {
            resolve(WebServices.callWebService_GET(apiUrl, data));
        }
    }).then((jsonRes) => {
        console.log(jsonRes)
          _that.setState({ isVisible: false })
        if ((!jsonRes) || (jsonRes.code == 0)) {
          setTimeout(() => {
                Alert.alert(jsonRes.message);
            }, 200);
        } else {
            if (jsonRes.code == 1) {
                _that.apiSuccessfullResponse(apiKey, jsonRes)
            }
        }
    }).catch((error) => {
        console.log("ERROR" + error);
        _that.setState({ isVisible: false })
        setTimeout(() => {
            Alert.alert("Server Error");
        }, 200);
    })
}

apiSuccessfullResponse(apiKey, jsonRes) {
  //console.log(jsonRes);
    if (apiKey == 'account_edit') {
      const jdata = jsonRes.result
    //  console.log(jdata)
      _that.setState({
        username : jdata.username,
        password: jdata.password,
        group_code : jdata.group_code,
      });
        AsyncStorage.setItem('UserData', JSON.stringify(jdata));
      setTimeout(() => {
          Alert.alert("Record Updated successfully");
      }, 200);
    }
    if(apiKey=='generate'){
      this.setState({unique_code:jsonRes.unique_code})
    }
    if(apiKey=='userdata'){
       var data=jsonRes.result[0]
       console.log('Data',data);
      _that.setState({
        uid : data.id,
        email : data.email,
        password : data.password,
        username : data.username,
        group_code : data.group_code,
        unique_code:data.unique_code
      });
    }
}

updateAlert = () => {
      this.setState({Modal_Visibility: !this.state.Modal_Visibility});
}

  render() {
    //console.log(this.state.height)
    const headerProp = {
      title: 'Account',
      screens: 'AccountScreen',
      qus_content:'',
      qus_audio:'https://houseofvirtruve.com/audio/account.mp3'
    };
    return (

      <View style={styles.container}>
        <Header info={headerProp} navigation={_that.props.navigation} updateAlert={this.updateAlert}/>
        <View style={[styles.content,this.state.Modal_Visibility ? {backgroundColor: 'rgba(0,0,0,0.4)',zIndex:5} : '#ffffff']}>
        <KeyboardAwareScrollView
            innerRef={() => {return [this.refs.username,this.refs.last_name, this.refs.email,this.refs.password]}} >
            <View style={{padding:AppSizes.ResponsiveSize.Padding(3)}} />
            <FloatingLabelBox labelIcon={user_icon}>
              <FloatingLabel
                  labelStyle={styles.labelInput}
                  inputStyle={styles.input}
                  style={[styles.formInput]}
                  value={this.state.username}
                  placeholder='Enter username'
                  ref="username"
                  autoCapitalize = 'none'
                  returnKeyType={ "next"}
                  keyboardType={ 'default'}
                  onChangeText={username=> this.setState({username})}
                >Username</FloatingLabel>
            </FloatingLabelBox>
            <View style={{padding:AppSizes.ResponsiveSize.Padding(2)}} />

          <FloatingLabelBox labelIcon={email_icon}>
              <FloatingLabel
                  labelStyle={styles.labelInput}
                  inputStyle={styles.input}
                  style={styles.formInput}
                  value={this.state.email}
                  placeholder='Enter email'
                  ref="email"
                  autoCapitalize = 'none'
                  keyboardType={ 'default'}
                  returnKeyType = { "next" }
                  editable={false}
                  onChangeText={email=> this.setState({email})}
                >Email</FloatingLabel>
          </FloatingLabelBox>
          <View style={{padding:AppSizes.ResponsiveSize.Padding(2)}} />
        <FloatingLabelBox labelIcon={lock_icon}>
          <FloatingLabel
              labelStyle={styles.labelInput}
              inputStyle={styles.input}
              style={styles.formInput}
              placeholder='Enter password'
              value={this.state.password}
              ref="password"
              autoCapitalize = 'none'
              secureTextEntry={true}
              returnKeyType = { "next" }
              onChangeText={password=> this.setState({password})}
            >New Password</FloatingLabel>
        </FloatingLabelBox>
          <View style={{padding:AppSizes.ResponsiveSize.Padding(2)}} />
        <FloatingLabelBox labelIcon={group_code_icon}>
          <FloatingLabel
              labelStyle={styles.labelInput}
              inputStyle={styles.input}
              style={styles.formInput}
              placeholder='Enter code'
              value={this.state.group_code}
              ref="group_code"
              autoCapitalize = 'none'
              keyboardType={ 'default'}
              returnKeyType = { "next" }
              onChangeText={group_code=> this.setState({group_code})}
            >Group Code</FloatingLabel>
        </FloatingLabelBox>
        {this.state.group_code!=''?
          <View>
            <View style={{padding:AppSizes.ResponsiveSize.Padding(2)}} />
            {this.state.unique_code!=''?
              <FloatingLabelBox labelIcon={unique_code}>
                <FloatingLabel
                    labelStyle={styles.labelInput}
                    inputStyle={styles.input}
                    style={styles.formInput}
                    placeholder='Enter code'
                    value={this.state.unique_code}
                    ref="unique_code"
                    autoCapitalize = 'none'
                    editable={false}
                    keyboardType={ 'default'}
                    returnKeyType = { "next" }
                    onChangeText={unique_code=> this.setState({unique_code})}
                  >Unique Code</FloatingLabel>
              </FloatingLabelBox>
            :
              <TouchableOpacity style={{flexDirection:'row',width:'100%',alignItems:'flex-start',paddingLeft:35}} onPress={()=> this.validationAndApiParameter('generate')}>
                <Image style={styles.unique} source={unique_code}/>
                <Text style={{color:'#000',marginTop:20}}>Generate Unique Code</Text>
              </TouchableOpacity>
            }
          </View>
        :
          null
        }

        </KeyboardAwareScrollView>



        <TouchableOpacity activeOpacity={.6} onPress={this.accountSave}>
          <CommonButton label='Save'/>
          </TouchableOpacity>
      </View>
        <Spinner visible={this.state.isVisible}  />

    </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
  },

  content: {
      flex: 1,
      backgroundColor: '#ffffff',
      height:'70%',
      //paddingLeft:AppSizes.ResponsiveSize.Padding(5),
      //paddingRight:AppSizes.ResponsiveSize.Padding(5),
  },

  title:{
    textAlign:'center',
    fontSize:AppSizes.ResponsiveSize.Sizes(14),
    paddingTop:'3%'
  },

  ToggalContainer: {
    backgroundColor: '#fff',
    borderBottomWidth: 2,
    borderBottomColor:'#000'
  },

  ToggleInner: {
  height: 100,
  alignItems: 'center',
  justifyContent: 'center',
  backgroundColor: '#ebebeb'
},

labelInput: {
  color: AppColors.contentColor,
  fontSize:AppSizes.ResponsiveSize.Sizes(13),
  //fontWeight:'300',
  //fontSize:AppSizes.ResponsiveSize.Sizes(13),
},
input: {
  borderWidth: 0,
  color: AppColors.contentColor,
  fontWeight:'300',
  fontSize:AppSizes.ResponsiveSize.Sizes(15),
},
formInput: {
  borderWidth: 0,
  borderColor: '#333',
},
titleContainer1: {
  width:'92%',
  flex:1,
  flexDirection: 'column',
  justifyContent: 'center',
  alignItems: 'center',
  borderBottomWidth:1,
  borderBottomColor:'#000'
},
formContainer:{
  flexDirection: 'row',
//  paddingLeft:AppSizes.ResponsiveSize.Padding(5),
//  paddingRight:AppSizes.ResponsiveSize.Padding(5),
},
imageContainer:{
  flex:1.3,
  alignItems: 'flex-end',
  //backgroundColor:'red',
},
unique:{
  height:20,
  width:20,
  marginRight:11,
  marginTop:20,

}
});
