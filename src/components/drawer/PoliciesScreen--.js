import React, { Component } from 'react';
import {StyleSheet,Text,View,Image,TouchableOpacity,Platform,ScrollView} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions } from 'react-navigation'
import LinearGradient from 'react-native-linear-gradient';
import Header from '../common/Header'
import * as commonFunctions from '../../utils/CommonFunctions'
import Orientation from 'react-native-orientation'

var Constant = require('../../api/ApiRules').Constant;

let _that
export default class PoliciesScreen extends Component {
    constructor (props) {
      super(props)
      this.state = {
      isVisible: false,
      Modal_Visibility: false,
      tab_tos:true,
      tab_privacy:false,
    },
    _that = this;
  }

  componentDidMount(){
    Orientation.lockToPortrait();
  }

  updateAlert = (visible) => {
        this.setState({Modal_Visibility: !this.state.Modal_Visibility});
  }


  tabtosChange=()=>{
    _that.scroll.scrollTo({y: 0})
    this.setState({tab_tos: true});
    this.setState({tab_privacy: false});
  }

  tabPrivacyChange=()=>{
    _that.scroll.scrollTo({y: 0})
    this.setState({tab_privacy: true});
    this.setState({tab_tos: false});
  }

  activeTab(title){
    return (
      <LinearGradient colors={['#36c4d8', '#2aa5b4', '#198391', '#15767d']} style={styles.tab_linear}>
      <Text style={[styles.textbtn,styles.tab_activeText]}>{title}</Text>
      </LinearGradient>
    );
  }

  inactiveTab(title){
    return (
      <LinearGradient colors={['#ebebeb', '#ebebeb', '#ebebeb', '#ebebeb']} style={styles.tab_linear}>
      <Text style={styles.textbtn}>{title}</Text>
      </LinearGradient>
    );
  }

  render() {
    const headerProp = {
      title: 'POLICIES',
      screens: 'PoliciesScreen',
      qus_content:'',
      qus_audio:'https://houseofvirtruve.com/audio/policies.mp3'
    };


    return (

      <View style={styles.container}>
      <Header info={headerProp} navigation={_that.props.navigation} updateAlert={this.updateAlert}/>
      <View style={[styles.content,this.state.Modal_Visibility ? {backgroundColor: 'rgba(0,0,0,0.4)',zIndex:5} : '#ffffff']}>
            <View style={styles.tabContainer}>
                <View style={{ width: '50%'}}>
                    <TouchableOpacity activeOpacity={.6} onPress={this.tabtosChange}>
                      {this.state.tab_tos ?
                        this.activeTab('TOS')
                      :
                        this.inactiveTab('TOS')
                      }
                      </TouchableOpacity>
                </View>

                <View style={{ width: '50%',}} >
                    <TouchableOpacity activeOpacity={.6} onPress={this.tabPrivacyChange}>
                    {this.state.tab_privacy ?
                      this.activeTab('Privacy')
                    :
                      this.inactiveTab('Privacy')
                    }
                    </TouchableOpacity>
                </View>
            </View>

            <View style={styles.tabContent}>
            {this.state.tab_privacy ?
              <Text style={styles.tab_title}>Privacy Policy</Text>
              :
              <Text style={styles.tab_title}>Terms of Service</Text>
            }

            <ScrollView ref={(c) => {this.scroll = c}} style={styles.scrollView}>

                {this.state.tab_tos ?
                  <View style={styles.tab_content}>

                                  <Text style={styles.contentText}>
                                  These Terms of Service are entered into by and between you and e-Thera Technologies (“e-Thera”).  The following terms and conditions, together with any documents incorporated by reference (collectively, these “Terms of Service”), govern your access to, and use of, e-Thera’s application, software, and other platforms, including, but not limited to, any content, functionality, and services offered on or through the application, software, or other platform and any future content, functionality, and services offered on or through the application, software, or platform (hereinafter, the “<Text style={styles.bold}>e-Thera Technology</Text>”). </Text>

                                  <Text style={styles.contentText}>
                                  Please read these Terms of Service carefully before you begin using the e-Thera Technology.  By (i) accessing the e-Thera Technology or (ii) clicking “I accept,” “I agree,” or similar terms indicating your assent to these Terms of Service when that option is made available to you, you accept and agree to be bound and abide by these Terms of Service and our Privacy Policy, found at https://e-Thera.ai/privacy/ (as the same may be updated from time to time, the “<Text style={styles.bold}>Privacy Policy</Text>”), which is incorporated herein by reference, in their current form as of such date.  
                                  </Text>
                                  <Text style={styles.contentText}>
                                  If you do not agree to these Terms of Service or our use of your information in compliance with our Privacy Policy, you must not access or use the e-Thera Technology.  
                                  </Text>
                                  <Text style={styles.contentText}>
                                  These Terms of Service will continue in full force and effect between you and e-Thera from the date that you access the e-Thera Technology or other assent to these Terms of Service until the date that either you or e-Thera terminate these Terms of Service or your access to the e-Thera Technology.
                                  </Text>
                                  <Text style={styles.contentText}>
                                  E-Thera may terminate these Terms of Service or deactivate your account at any time for any reason or no reason whatsoever.
                                  </Text>
                                  <Text style={styles.contentText}>
                                  Upon termination all rights granted to you by these Terms of Service will also terminate. Termination will not limit any of e-Thera's rights or remedies at law or in equity.
                                  </Text>
                                  <Text style={styles.smalltitle}>
                                  Changes to Terms of Service
                                  </Text>
                                  <Text style={styles.contentText}>
                                  We may revise and update these Terms of Service from time to time in our sole discretion.  All changes are effective immediately when we post them and apply to all access to, and use of, the e-Thera Technology thereafter.  e-Thera may inform you of its updates to these Terms of Service and may require your assent to the updates before allowing you to continue using the e-Thera Technology.  Your continued use of the e-Thera Technology following the posting of revised Terms of Service means that you accept and agree to the changes.  You are expected to check this page prior to each use of the e-Thera Technology so that you are aware of any changes as they are binding on you.
                                  </Text>

                                  <Text style={styles.smalltitle}>
                                  Access to Data and e-Thera Technology
                                  </Text>
                                  <Text style={styles.contentText}>
                                  Upon your acceptance of these Terms of Service, e-Thera grants you a non-exclusive, revocable, nontransferable limited license to access and use the e-Thera Technology solely for your personal use (and not for any commercial use or purpose on your part) in the United States of America for purposes of monitoring and evaluating your health and submitting your health information and data to e-Thera for e-Thera’s use and the uses of other third parties in accordance with the Privacy Policy.
                                  </Text>

                                  <Text style={styles.contentText}>
                                  It is a condition of your use of the e-Thera Technology that all information you provide to e-Thera or input into the e-Thera Technology is correct, current, and complete. You agree that e-Thera may collect, maintain, organize, modify, use, publish, and disseminate any data or information you submit and any other data or information about you that e-Thera collects or receives, either through or in connection with the e-Thera Technology or from any other person or entity, in accordance with our Privacy Policy.  In the event that you are using the e-Thera Technology in connection with a clinical trial or study, you specifically agree that the health information and data may be used by e-Thera and third parties in accordance with the Privacy Policy and to the extent otherwise specified in any informed consent form provided to you prior to your participation in such clinical trial or study.
                                  </Text>
                                  <Text style={styles.contentText}>
                                  By agreeing to these Terms of Service, you agree that e-Thera and third parties may access and share your information and data submitted through, or otherwise collected by, the e-Thera Technology, including, but not limited to, medications prescribed and instructions for managing your health or handling an emergency provided to you, for purposes of providing care to you and, if applicable, for purposes of conducting the clinical trial or study.
                                  </Text>
                                  <Text style={styles.contentText}>
                                  By agreeing to these Terms of Service, you agree that you will not submit any information or data using the e-Thera Technology other than information or data which either (i) relates solely to your health, your health care, or payment for your health care, or (ii) relates solely to the health, health care, or payment for health care of a third party who has given you permission to submit that information.
                                  </Text>

                                  <Text style={styles.contentText}>
                                  You acknowledge and agree that in the event a patient representative who is legally entitled to act on your behalf accesses the e-Thera Technology on your behalf, that patient representative will be provided access to all of the information and data which you yourself could access through the e-Thera Technology, including, but not limited to, your personally identifiable health information.  
                                  </Text>

                                  <Text style={styles.contentText}>
                                  You acknowledge that misuse of information which identifies another individual and pertains to that individual’s health, health care, payment for health care, or any other protected health information may be a criminal offense. You agree that, except in instances where you are serving as a patient representative for a patient who has expressly authorized your receipt, access, or use of information collected, maintained, organized, modified, used, published, or disseminated by e-Thera, in the event you receive, access, or use information relating to another individual’s health, health care, payment for health care, or other protected health information using the e-Thera Technology, you will immediately report such receipt, access, or use to e-Thera; will keep such information confidential; and will cooperate with e-Thera in responding to such receipt, access, or use, including, but not limited to, taking any reasonable actions to mitigate any harm that might result from such access.
                                  </Text>

                                  <Text style={styles.contentText}>
                                  You acknowledge and agree that, while e-Thera may collect, maintain, organize, modify, use, publish, and disseminate information that is submitted to it, e-Thera is not responsible for the accuracy, completeness, timeliness, quality, or any other aspect of such information. Any reliance you place on such information is strictly at your own risk. E-THERA DOES NOT ASSUME, AND WILL NOT HAVE, ANY LIABILITY OR RESPONSIBILITY TO YOU OR ANY OTHER PERSON OR ENTITY FOR ANY INFORMATION YOU MAY OBTAIN USING THE E-THERA TECHNOLOGY.
                                  </Text>

                                  <Text style={styles.smalltitle}>
                                  Updates to and Availability of e-Thera Technology
                                  </Text>

                                  <Text style={styles.contentText}>
                                  E-Thera may, from time to time, in its sole discretion, develop and provide updates to the e-Thera Technology, which may include upgrades, bug fixes, patches and other error corrections, or new features (collectively, including related documentation, "Updates"). Updates may also modify or delete in their entirety certain features and functionality. You agree that, e-Thera has no obligation to provide any Updates or to continue to provide or enable any particular features or functionality.
                                  </Text>

                                  <Text style={styles.contentText}>
                                  If the e-Thera Technology is provided to you in the form of an application downloaded to a mobile device, depending upon the settings of that mobile device, when the mobile device is connected to the internet:
                                  </Text>

                                  <Text style={styles.list}>
                                  (a) Updates to the e-Thera Technology may automatically download and install
                                  </Text>
                                  <Text style={styles.list}>
                                  (b) you may receive notice of, or be prompted to download and install, available Updates.
                                  </Text>

                                  <Text style={styles.contentText}>
                                  You may receive no notice of a particular Update if your device is configured to automatically download and install Updates. E-Thera recommends that you promptly download and install all Updates. You acknowledge and agree that the e-Thera Technology or portions thereof may not properly operate if you do not download and install Updates. You further agree that all Updates will be deemed part of the e-Thera Technology and be subject to all terms and conditions set forth in these Terms of Service.</Text>

                                  <Text style={styles.contentText}>
                                  E-Thera reserves the right to alter, modify, or terminate the e-Thera Technology and any service or material e-Thera provides in connection with the e-Thera Technology without notice at any time in e-Thera’s sole discretion.  From time to time, e-Thera may restrict access to some portions of the e-Thera Technology or the entire e-Thera Technology. <Text style={styles.bold}>  E-THERA WILL NOT BE LIABLE IF, FOR ANY REASON, ALL OR ANY PART OF THE E-THERA TECHNOLOGY IS UNAVAILABLE AT ANY TIME OR FOR ANY PERIOD. </Text> 
                              </Text>

                              <Text style={styles.smalltitle}>Account Security</Text>

                              <Text style={styles.contentText}>
                              You agree that you will treat your username, password, or other security credentials or identifying information you choose or are provided (collectively, the “Credentials”) as confidential and that you will not disclose such Credentials to any other person or entity unless you are required to do so by law, in which case you will inform e-Thera prior to making the disclosure and will only disclose that portion of the Credentials to the extent necessary to comply with the legal requirement. In addition, you may disclose your Credentials to a patient care representative who provide health care services to you if you inform e-Thera prior to making such disclosure.  </Text>

                              <Text style={styles.contentText}>
                              You also acknowledge that your account is personal to you and agree not to provide any other person with access to the e-Thera Technology or portions of it using your Credentials. You agree to notify e-Thera immediately of any unauthorized access to, or use of, your Credentials or any other breach of security and to assist e-Thera in mitigating any inappropriate use, or disclosure of the information as may be requested by e-Thera. You also agree to protect any physical devices you own or control which you use to access the e-Thera Technology and to ensure that you click the “Log Out” link and complete the log out function at the end of each session.  If you access your account from a public or shared computer, you should use particular caution when doing so to prevent others from viewing or recording your Credentials or your protected health information.</Text>

                              <Text style={styles.contentText}>
                                E-Thera reserves the right to monitor and audit all use of the e-Thera Technology and to disable any user name, password or other identifier at any time in its sole discretion, without notice and without cause.</Text>
                                <Text style={styles.smalltitle}>Intellectual Property</Text>
                                <Text style={styles.contentText}>
                                  You agree that, as between you and e-Thera, the e-Thera Technology (including, but not limited to, all information, software, text, displays, images, video and audio, and the design, selection, and arrangement thereof) and any material which can be accessed or collected through or in connection with, or generated or provided by, the e-Thera Technology is the sole property of e-Thera or its licensors, as applicable, and is protected by United States and international copyright, trademark, patent, trade secret, and other intellectual property or proprietary rights laws.</Text>


                                <Text style={styles.contentText}>
                                  E-Thera’s name and all related names, logos, product and service names, designs, and slogans are trademarks of e-Thera or its affiliates or licensors.  You must not use such marks without the prior written permission of e-Thera.</Text>

                                <Text style={styles.contentText}>You may not, and agree that you will not, and will not permit or assist others to:</Text>
                                <View style={styles.listmain}>
                                  <Text style={styles.list}>
                                    • copy the e-Thera Technology or any part thereof;
                                  </Text>
                                  <Text style={styles.list}>
                                    • modify, translate, adapt, or otherwise create derivative works or improvements, whether or not copyrightable or patentable, of the e-Thera Technology or any part thereof</Text>
                                  <Text style={styles.list}>
                                  • reverse engineer, disassemble, decompile, decode, or otherwise attempt to derive or gain access to the source code of the e-Thera Technology or any part thereof</Text>
                                  <Text style={styles.list}>
                                  • use the e-Thera Technology or any part thereof in any way that violates any applicable federal, state, local, or international law or regulations</Text>
                                  <Text style={styles.list}>
                                  • use the e-Thera Technology or any part thereof for the purpose of exploiting or harming, or attempting to exploit or harm, any other person in any way</Text>
                                  <Text style={styles.list}>
                                  • use the e-Thera Technology or any part thereof to transmit, or procure the sending of, any advertising or promotion material, including, but not limited to, “spam” or similar solicitation</Text>
                                  <Text style={styles.list}>
                                  • use the e-Thera Technology or any part thereof to impersonate e-Thera, an e-Thera employee, another user, or any other person or entity, including, but not limited to, by using an e-mail address associated with any of the foregoing</Text>
                                  <Text style={styles.list}>
                                  • use the e-Thera Technology or any part thereof to restrict or inhibit any other person or entity’s use or enjoyment of the e-Thera Technology or any portion thereof or which could expose any other person or entity to liability</Text>
                                  <Text style={styles.list}>
                                  • use the e-Thera Technology or any part thereof in a manner that could disable, overburden, damage, or impair the e-Thera Technology or interfere with any other person or entity’s use of the e-Thera Technology</Text>
                                  <Text style={styles.list}>
                                  • use a robot, spider, or other automatic device, process, or means to access the e-Thera Technology for any purpose, including, but not limited to, monitoring or copying any of the material collected, maintained, organized, modified, used, published, or disseminated by e-Thera or through the e-Thera Technology without e-Thera’s prior written consent</Text>
                                  <Text style={styles.list}>
                                  • use any manual process to monitor or copy any of the material collected, maintained, organized, modified, used, published, or disseminated by e-Thera or through the e-Thera Technology without e-Thera’s prior written consent;</Text>
                                  <Text style={styles.list}>
                                  • use any device, software, or routine that interferes with the proper working of the e-Thera Technology or any part there</Text>
                                  <Text style={styles.list}>
                                  • attempt to gain unauthorized access to, interfere with, damage, or disrupt any part of the e-Thera Technology, the server on which the e-Thera Technology is stored, or any server, computer, or database connected to the e-Thera Technology</Text>
                                  <Text style={styles.list}>
                                  • introduce any virus, Trojan horse, worm, logic bomb, or other material which is malicious or technologically harmful to the e-Thera Technology or any part thereof</Text>
                                  <Text style={styles.list}>
                                  • remove, delete, alter, or obscure any trademarks or any copyright, trademark, patent or other intellectual property or proprietary rights notice from the e-Thera Technology or any part thereof</Text>
                                  <Text style={styles.list}>
                                  • rent, lease, lend, sell, sublicense, assign, distribute, publish, transfer, or otherwise make available the e-Thera Technology or any of its features or functionality or any part thereof to any third party for any reason, including, but not limited to, by making the e-Thera Technology available on a network where it is capable of being accessed by more than one device at any time.
                                  </Text>
                              </View>

                              <Text style={styles.contentText}>
                                You further acknowledge that the data submitted by you and third parties to e-Thera or through the e-Thera Technology is maintained by e-Thera as a compilation, and that both the compilation and any de-identified data or compilation of such de-identified data derived by e-Thera from data submitted by you or third parties are trade secrets of e-Thera which derive independent economic value from not being generally known or readily discernible by proper means.  You agree that, except as explicitly permitted by these Terms of Service, you will not access or attempt to access such trade secrets or intellectual property. You also agree that you will not, and will not permit or assist others to, use the e-Thera Technology or data accessed through the e-Thera Technology to create similar compilations, de-identified data, or compilations of de-identified data.</Text>
                              <Text style={styles.contentText}>
                                You agree and acknowledge that e-Thera will be irreparably harmed by any breach of this section, other misappropriation or threatened misappropriation of its trade secrets or other intellectual property rights, or violation of these Terms of Use and that money damages will not be a sufficient remedy for such breach, misappropriation, or threatened misappropriation. Accordingly, you agree that e-Thera will be entitled to equitable relief, including, without limitation, injunction and specific performance, in the event of any breach of this section, misappropriation or threatened misappropriation of e-Thera’s trade secrets, or breach of these Terms of Use without any obligation by e-Thera to post bond or other security. Such remedies shall not be deemed to be the exclusive remedies, but shall instead be in addition to all other remedies available at law or in equity.</Text>

                              <Text style={styles.smalltitle}>Disclaimer of Warranties</Text>
                              <Text style={styles.contentText}>
                              THE E-THERA TECHNOLOGY AND ALL INFORMATION OBTAINED USING THE E-THERA TECHNOLOGY IS PROVIDED "AS IS" AND WITH ALL FAULTS AND DEFECTS WITHOUT WARRANTY OF ANY KIND. TO THE MAXIMUM EXTENT PERMITTED UNDER APPLICABLE LAW, E-THERA, ON ITS OWN BEHALF AND ON BEHALF OF ITS AFFILIATES AND ITS AND THEIR RESPECTIVE EMPLOYEES, AGENTS, OFFICERS, DIRECTORS, SHAREHOLDERS, LICENSORS, AND SERVICE PROVIDERS, EXPRESSLY DISCLAIMS ALL WARRANTIES, WHETHER EXPRESS, IMPLIED, STATUTORY, OR OTHERWISE, WITH RESPECT TO THE E-THERA TECHNOLOGY, INCLUDING, BUT NOT LIMITED TO, ALL IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT, AND WARRANTIES THAT MAY ARISE OUT OF COURSE OF DEALING, COURSE OF PERFORMANCE, USAGE OR TRADE PRACTICE.  WITHOUT LIMITATION TO THE FOREGOING, E-THERA PROVIDES NO WARRANTY OR UNDERTAKING, AND MAKES NO REPRESENTATION OF ANY KIND, THAT THE E-THERA TECHNOLOGY OR ANY DATA OBTAINED USING THE E-THERA TECHNOLOGY WILL MEET YOUR REQUIREMENTS; ACHIEVE ANY INTENDED RESULTS; BE COMPATIBLE OR WORK WITH ANY OTHER SOFTWARE, APPLICATIONS, SYSTEMS OR SERVICES; OPERATE WITHOUT INTERRUPTION; MEET ANY PERFORMANCE OR RELIABILITY STANDARDS; OR BE ERROR FREE OR THAT ANY ERRORS OR DEFECTS CAN OR WILL BE CORRECTED.</Text>

                              <Text style={styles.contentText}>
                              SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OF, OR LIMITATIONS ON, IMPLIED WARRANTIES OR THE LIMITATIONS ON THE APPLICABLE STATUTORY RIGHTS OF A CONSUMER, SO SOME OR ALL OF THE ABOVE EXCLUSIONS AND LIMITATIONS MAY NOT APPLY TO YOU, AND THE FOREGOING DOES NOT AFFECT ANY LIABILITY WHICH CANNOT BE EXCLUDED OR LIMITED UNDER APPLICABLE LAW.</Text>


                            <Text style={styles.smalltitle}>
                            Limitation on Liability
                            </Text>
                            <Text style={styles.contentText}>
                            TO THE FULLEST EXTENT PERMITTED BY APPLICABLE LAW, IN NO EVENT WILL E-THERA OR ITS AFFILIATES, OR ANY OF ITS OR THEIR RESPECTIVE EMPLOYEES, AGENTS, OFFICERS, DIRECTORS, SHAREHOLDERS, CONTRACTORS, LICENSORS, OR SERVICE PROVIDERS, HAVE ANY LIABILITY ARISING FROM, OR RELATED TO, (I) YOUR USE OF, OR INABILITY TO USE, THE E-THERA TECHNOLOGY OR ANY INFORMATION ACCESSED THROUGH OR PROVIDED BY THE E-THERA TECHNOLOGY OR (II) THE USE OF THE E-THERA TECHNOLOGY OR ANY INFORMATION ACCESSED USING, OR PROVIDED BY, THE E-THERA TECHNOLOGY BY A THIRD PARTY TO PROVIDE CARE TO YOU FOR:
                            </Text>

                            <Text style={styles.contentText}>
                            (A) PERSONAL INJURY, PROPERTY DAMAGE, LOST PROFITS, COST OF SUBSTITUTE GOODS OR SERVICES, LOSS OF DATA, LOSS OF GOODWILL, BUSINESS INTERRUPTION, COMPUTER FAILURE OR MALFUNCTION, OR ANY OTHER CONSEQUENTIAL, INCIDENTAL, INDIRECT, EXEMPLARY, SPECIAL, OR PUNITIVE DAMAGES</Text>

                            <Text style={styles.contentText}>
                            (B) DIRECT DAMAGES IN AMOUNTS THAT IN THE AGGREGATE EXCEED THE AMOUNT ACTUALLY PAID BY YOU FOR THE E-THERA TECHNOLOGY.</Text>

                            <Text style={styles.contentText}>
                            THE FOREGOING LIMITATIONS WILL APPLY WHETHER SUCH DAMAGES ARISE OUT OF BREACH OF CONTRACT, TORT (INCLUDING, BUT NOT LIMITED TO, NEGLIGENCE), OR OTHERWISE AND REGARDLESS OF WHETHER SUCH DAMAGES WERE FORESEEABLE OR E-THERA WAS ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.  </Text>

                            <Text style={styles.contentText}>
                            SOME JURISDICTIONS DO NOT ALLOW CERTAIN LIMITATIONS OF LIABILITY SO SOME OR ALL OF THE ABOVE LIMITATIONS OF LIABILITY MAY NOT APPLY TO YOU, AND THE FOREGOING DOES NOT AFFECT ANY LIABILITY WHICH CANNOT BE EXCLUDED OR LIMITED UNDER APPLICABLE LAW.</Text>

                            <Text style={styles.smalltitle}>
                            Third Party Materials
                            </Text>
                            <Text style={styles.contentText}>
                            If the e-Thera Technology contains links to other sites and resources provided by third parties, these links are provided for your convenience only. This includes links contained in advertisements, including, but not limited to, banner advertisements and sponsored links. WE HAVE NO CONTROL OVER THE CONTENTS OF THOSE SITES OR RESOURCES AND ACCEPT NO RESPONSIBILITY FOR THEM OR FOR ANY LOSS OR DAMAGE THAT MAY ARISE FROM YOUR USE OF THEM.  IF YOU DECIDE TO ACCESS ANY OF THE THIRD PARTY WEBSITES LINKED TO OR THROUGH THE E-THERA TECHNOLOGY, YOU DO SO ENTIRELY AT YOUR OWN RISK AND SUBJECT TO THE TERMS AND CONDITIONS OF USE FOR SUCH WEBSITES.
                            Indemnification</Text>

                            <Text style={styles.contentText}>
                            You agree to indemnify, defend and hold harmless e-Thera and its officers, directors, employees, agents, affiliates, successors and assigns from and against any and all losses, damages, liabilities, deficiencies, claims, actions, judgments, settlements, interest, awards, penalties, fines, costs, or expenses of whatever kind and nature, including attorneys' fees, expert witness fees and costs, and other costs, arising from, or relating to, your misuse of the e-Thera Technology or your breach of these Terms of Service.  Furthermore, you agree that, e-Thera assumes no responsibility for the content submitted, accessed, or made available through the e-Thera Technology by you or any other person.</Text>


                            <Text style={styles.smalltitle}>
                            Governing Law and Forum Selection
                            </Text>
                            <Text style={styles.contentText}>
                            All matters relating to the e-Thera Technology and these Terms of Service and any dispute or claim arising therefrom or related thereto (in each case, including non-contractual disputes or claims) shall be governed by, and construed in accordance with, the internal laws of the State of New York without giving effect to any choice or conflict of law provision or rule (whether of the State of New York or any other jurisdiction).</Text>

                            <Text style={styles.contentText}>
                            Any legal suit, action or proceeding arising out of, or related to, these Terms of Service or the e-Thera Technology shall be instituted exclusively in the federal courts of the United States or the courts of the State of New York located in New York County. You waive any and all objections or affirmative defenses to the exercise of jurisdiction over you by such courts and to venue of any such legal suit, action, or proceeding in such courts.</Text>


                          <Text style={styles.smalltitle}>
                          Limitation on Time to File Claims
                          </Text>
                          <Text style={styles.contentText}>
                          ANY CAUSE OF ACTION OR CLAIM YOU MAY HAVE ARISING OUT OF, OR RELATING TO, THESE TERMS OF SERVICE OR THE E-THERA TECHNOLOGY MUST BE COMMENCED WITHIN ONE (1) YEAR AFTER THE CAUSE OF ACTION OR CLAIM ACCRUES; OTHERWISE, SUCH CAUSE OF ACTION OR CLAIM IS PERMANENTLY BARRED.</Text>

                          <Text style={styles.smalltitle}>
                          Waiver and Severability
                          </Text>
                          <Text style={styles.contentText}>
                          No waiver by e-Thera of any term or condition set forth in these Terms of Service shall be deemed a further or continuing waiver of such term or condition or a waiver of any other term or condition, and any failure of e-Thera to assert a right or provision under these Terms of Service shall not constitute a waiver of such right or provision.
                          </Text>

                          <Text style={styles.contentText}>
                          If any provision of these Terms of Service is held by a court or other tribunal of competent jurisdiction to be invalid, illegal or unenforceable for any reason, such provision shall be eliminated or limited to the minimum extent such that the remaining provisions of the Terms of Service will continue in full force and effect.
                          </Text>

                          <Text style={styles.smalltitle}>
                          Entire Agreement
                          </Text>
                          <Text style={styles.contentText}>
                          These Terms of Service and the Privacy Policy constitute the entire agreement between you and e-Thera with respect to the e-Thera Technology and supersede all prior or contemporaneous understandings and agreements, whether written or oral, with respect to the e-Thera Technology.
                          </Text>
                          <Text style={styles.smalltitle}>
                          Your Comments and Concerns
                          </Text>
                          <Text style={styles.contentText}>
                          The e-Thera Technology is operated by the following:
                          </Text>

                          <View style={styles.bottombox}>
                              <Text style={styles.bottomtext}>
                              e-Thera Systems, Inc.{"\n"}
                                500 7th Ave{"\n"}
                                New York, NY 10018{"\n"}
                                support@etheratechnologies.com
                              </Text>
                          </View>
                          <Text style={styles.contentText1}>
                          All feedback, comments, requests for technical support, and other communications relating to the e-Thera Technology should be in writing and directed to the foregoing address or e-mail address.
                          </Text>

                    </View>
                : null
              }
              </ScrollView>
              <ScrollView ref={(c) => {this.scroll = c}} style={styles.scrollView}>
              {this.state.tab_privacy ?
                <View style={styles.tab_content}>
                            <Text style={styles.con_title}>
                            Introduction
                            </Text>
                            <Text style={styles.contentText}>
                              E-Thera Technologies, Inc. (""Company"" or ""We"") respect your privacy and are committed to protecting it through our compliance with this policy as well as with all applicable laws, including the privacy and security rules and regulations of the Health Insurance Portability and Accountability Act of 1996, as amended (""HIPAA""). Specifically, We maintain reasonable administrative, technical and physical safeguards which are designed to ensure the confidentiality, integrity and availability of the personally identifiable health information that We maintain on our website and related information systems. However, no system is impenetrable and We cannot guarantee the confidentiality, integrity and availability of such personally identifiable health information.
                              </Text>
                              <Text style={styles.contentText}>
                              This policy describes the types of non-health information we may collect from you or that you may provide when you visit the website www.ethera.com (the ""Website"") or use a mobile application provided by us (collectively, with the Website, the ""e-Thera Technology""), and our practices for collecting, using, maintaining, protecting and disclosing that information. Our practices for collecting, using, maintaining, protecting and disclosing information on our users' health, health care, providers of healthcare (including information on the names and addresses of hospitals or other health care providers) and payment for health care (including payments made by third parties, such as insurance companies), regardless of whether such information is or can be associated with any individual, (""Health Information"") are provided in the terms of service for our health information and care management software, which can be found here: TERMS OF SERVICE.
                              </Text>

                            <Text style={styles.listtitle}>
                            This policy applies to non-Health Information we collect:
                            </Text>
                            <Text style={styles.list}>
            	                  •	On this Website.
                            </Text>

                            <Text style={styles.list}>
                                • In email, text and other electronic messages between you and this Website.
                             </Text>
                          <Text style={styles.list}>
                        	      • Through mobile applications we provide.
                           </Text>

                          <Text style={styles.listtitle}>
                            It does not apply to information:
                        </Text>
                        <Text style={styles.list}>
                      • collected by us offline or through any other means, including on any other website operated by Company or any third party (including our affiliates and subsidiaries);
                      </Text>
                      <Text style={styles.list}>
                      • collected by any third party, including through any application or content (including advertising) that may link to or be accessible from or on the Website; or</Text>

                    <Text style={styles.list}>
                    	•	which is Health Information. 
                      </Text>
                      <Text style={styles.contentText}>
                    Please read this policy carefully to understand our policies and practices regarding your information and how we will treat it. If you do not agree with our policies and practices, your choice is not to use the e-Thera Technology. By accessing or using the e-Thera Technology, you agree to this privacy policy. This policy may change from time to time. Your continued use of the e-Thera Technology after we make changes is deemed to be acceptance of those changes, so please check the policy periodically for updates.
                    </Text>

                    <Text style={styles.smalltitle}>
                    Children Under the Age of 13
                    </Text>

                    <Text style={styles.contentText}>
                    The e-Thera Technology is not intended for children under 13 years of age. No one under age 13 may provide any information to the e-Thera Technology. We do not knowingly collect personal information from children under 13. If you are under 13, do not use or provide any information to or through any of the features of the e-Thera Technology, or provide any information about yourself to us, including your name, address, telephone number, e-mail address or any screen name or user name you may use. If we learn we have collected or received personal information from a child under 13 without verification of parental consent, we will delete that information. If you believe we might have any information from or about a child under 13, please contact us at admin@etheratechnologies.com.</Text>

                    <Text style={styles.smalltitle}>
                    Information We Collect About You and How We Collect It</Text>

                    <Text style={styles.contentText}>
                    We collect several types of non-Health Information from and about users of the e-Thera Technology, including information:
                    </Text>
                    <Text style={styles.list}>
                    • by which you may be personally identified, such as name, postal address, e-mail address, social security number and telephone number<Text style={styles.bold}> ("" Personal Information"")</Text>;
                    </Text>

                    <Text style={styles.list}>
                    • about your internet connection, the equipment you use to access our Website or otherwise interact with the e-Thera Technology (including information about health devices or fitness trackers you may choose to allow the e-Thera Technology to communicate with), and usage details.
                    </Text>
                    <Text style={styles.listtitle}>
                    We collect this information:</Text>

                  <Text style={styles.list}>
                    •	Directly from you when you provide it to us. 
                  </Text>
                    <Text style={styles.list}>
                  • Automatically as you use the e-Thera Technology, such as by navigating through the Website. Information collected automatically may include usage details, IP addresses and information collected through cookies.</Text>


                  <Text style={styles.smalltitle}>
                  Information You Provide to Us.</Text>

                  <Text style={styles.listtitle}>The non-Health Information we collect on or through the e-Thera Technology may include:</Text>

                  <Text style={styles.list}>
                  • Information, such as passwords, that you provide by filling in forms provided by the e-Thera Technology, such as forms on our Website. This includes information provided at the time of subscribing to our service. We may also ask you for information when you report a problem with the e-Thera Technology.
                  </Text>
                  <Text style={styles.list}>
                  • Records and copies of your correspondence (including email addresses), if you contact us.
                  </Text>
                  <Text style={styles.list}>
                  • Information you provide to allow communication between the e-Thera Technology and health devices or fitness trackers and/or their corresponding online accounts.</Text>

                  <Text style={styles.smalltitle}>Information We Collect Through Automatic Data Collection Technologies.</Text>

                 <Text style={styles.listtitle}>As you navigate through our Website or otherwise interact with the e-Thera Technology, we may use automatic data collection technologies to collect certain non-Health Information about your equipment, browsing actions and patterns, including:</Text>
                 <Text style={styles.list}>
                • Details of your visits to our Website, including location data and other communication data and the resources that you access and use on the Website.
                </Text>
                <Text style={styles.list}>
                • Information about your computer and internet connection, including your IP address, operating system and browser type.
                </Text>
                <Text style={styles.contentText}>
                At this time we do not collect information about your online activities over time or across third-party websites or with respect to other online services, but we reserve the right to do so in the future.
                </Text>
                  <Text style={styles.listtitle}>
                  The non-Health Information we collect automatically may include Personal Information or we may maintain it or associate it with Personal Information we collect in other ways or receive from third parties. The technologies we use for this automatic data collection may include:</Text>

                  <Text style={styles.smalltitle}>
                  	•	Cookies (or browser cookies).</Text>

                  <Text style={styles.contentText}>
                     A cookie is a small file placed on the hard drive of your computer. You may refuse to accept browser cookies by activating the appropriate setting on your browser. However, if you select this setting you may be unable to access certain parts of our Website. Unless you have adjusted your browser setting so that it will refuse cookies, our system will issue cookies when you direct your browser to our Website. </Text>

                     <Text style={styles.smalltitle}>
                  	• Flash Cookies.</Text>

                  <Text style={styles.contentText}>
                     Certain features of our Website may use local stored objects (or Flash cookies) to collect and store information about your preferences and navigation to, from and on our Website. Flash cookies are not managed by the same browser settings as are used for browser cookies. </Text>

                     <Text style={styles.smalltitle}>
                  Third-party Use of Cookies and Other Tracking Technologies</Text>


                  <Text style={styles.contentText}>
                  Some content or applications, including advertisements, on the Website are served by third-parties, including advertisers, ad networks and servers, content providers and application providers. These third parties may use cookies alone or in conjunction with web beacons or other tracking technologies to collect information about you when you use our website. The information they collect may be associated with your Personal Information or they may collect information, including Personal Information, about your online activities over time and across different websites and other online services. They may use this information to provide you with interest-based (behavioral) advertising or other targeted content.</Text>
                  <Text style={styles.contentText}>
                  We do not control these third parties' tracking technologies or how they may be used. If you have any questions about an advertisement or other targeted content, you should contact the responsible provider directly.</Text>

                  <Text style={styles.smalltitle}>
                  How We Use Your Information</Text>


                  <Text style={styles.listtitle}>
                    We use the non-Health Information that we collect about you or that you provide to us, including any Personal Information:</Text>

                    <Text style={styles.list}>
            	         •	To estimate our audience size and usage patterns. </Text>

                    <Text style={styles.listtitle}>
                      • To store information about your preferences, allowing us to customize the e-Thera Technology according to your individual interests.</Text>

                    <Text style={styles.list}>
            	       •	To recognize you when you return to our Website. </Text>

                     <Text style={styles.list}>
            	        •	To present our Website and its contents to you. </Text>

                      <Text style={styles.listtitle}>
                        • To provide you with information, products or services that you request from us.</Text>

                      <Text style={styles.listtitle}>
                      • To fulfill any other purpose for which you provide it.</Text>

                      <Text style={styles.list}>
                      • To provide you with notices about your account, including expiration and renewal notices.</Text>

                      <Text style={styles.listtitle}>
                      • To carry out our obligations and enforce our rights arising from any contracts entered into between you and us, including for billing and collection.</Text>

                      <Text style={styles.listtitle}>
                      • To notify you about changes to the e-Thera Technology or any products or services we offer or provide.</Text>

                      <Text style={styles.listtitle}>
                      • In any other way we may describe when you provide the information.</Text>

                      <Text style={styles.list}>
                      	•	For any other purpose with your consent. </Text>

                      <Text style={styles.contentText}>
                    We may also use your information to contact you about our own and third-parties' goods and services that may be of interest to you. If you do not want us to use your information in this way, you may opt out of the disclosure of your information for third party advertising, targeted advertising, and promotional offers from the Company as described in this policy under the heading Choices About How We Use and Disclose Your Information.</Text>

                    <Text style={styles.contentText}>
                    We may use the non-Health Information we have collected from you to enable us to display advertisements to our advertisers' target audiences. Even though we do not disclose your Personal Information for these purposes without your consent, if you click on or otherwise interact with an advertisement, the advertiser may assume that you meet its target criteria.</Text>

                    <Text style={styles.smalltitle}>
                    Disclosure of Your Information</Text>



                    <Text style={styles.list}>
                    We may disclose aggregated information about our users, and information that does not identify any individual, without restriction.</Text>

                    <Text style={styles.listtitle}>
                    We may disclose personally identifiable non-Health Information that we collect or you provide as described in this privacy policy:
                    </Text>
                    <Text style={styles.list}>
                    	•	To our subsidiaries and affiliates. </Text>

                      <Text style={styles.list}>
                    • To contractors, service providers and other third parties we use to support our business and who are bound by contractual obligations to keep Personal Information confidential and use it only for the purposes for which we disclose it to them.</Text>
                    <Text style={styles.list}>
                    • To a buyer or other successor in the event of a merger, divestiture, restructuring, reorganization, dissolution or other sale or transfer of some or all of our assets, whether as a going concern or as part of bankruptcy, liquidation or similar proceeding, in which Personal Information held by us about the e-Thera Technology's users is among the assets transferred.</Text>
                    <Text style={styles.list}>
                    • To third parties to market their products or services to you if you have consented to these disclosures. We contractually require these third parties to keep Personal Information confidential and use it only for the purposes for which we disclose it to them.</Text>
                    <Text style={styles.list}>
                    	•	To fulfill the purpose for which you provide it. </Text>
                      <Text style={styles.listtitle}>
                    • For any other purpose disclosed by us when you provide the information.
                    </Text>
                    <Text style={styles.list}>
                    	•	With your consent. </Text>
                      <Text style={styles.list}>
                    We may also disclose your Personal Information:</Text>

                    <Text style={styles.list}>
                    • To comply with any court order, law or legal process, including to respond to any government or regulatory request.</Text>
                    <Text style={styles.list}>
                    • To enforce or apply our terms of service (which can be found here: TERMS OF SERVICE) and other agreements, including for billing and collection purposes.</Text>
                    <Text style={styles.list}>
                    • If we believe disclosure is necessary or appropriate to protect the rights, property, or safety of the Company, our customers or others.</Text>

                    <Text style={styles.smalltitle}>
                    Choices About How We Use and Disclose Your Information</Text>

                    <Text style={styles.contentText}>
                    We strive to provide you with choices regarding the Personal Information you provide to us. We have created mechanisms to provide you with the following control over your information:</Text>

                    <Text style={styles.bold}>
                    	•	Tracking Technologies and Advertising.</Text>

                    <Text style={styles.contentText}>
                       You can set your browser to refuse all or some browser cookies, or to alert you when cookies are being sent. To learn how you can manage your Flash cookie settings, visit the Flash player settings page on Adobe's website. If you disable or refuse cookies, please note that some parts of this site may then be inaccessible or not function properly. </Text>

                       <Text style={styles.bold}>
                    	•	Disclosure of Your Information for Third-Party Advertising.</Text>

                    <Text style={styles.contentText}>
                      If you do not want us to share your Personal Information with unaffiliated or non-agent third parties for promotional purposes, you can opt-out by sending an email to admin@etheratechnologies.com stating that you do not want us to share your Personal Information for that purpose. 
                      </Text>

                      <Text style={styles.bold}>
                    	•	Promotional Offers from the Company.
                    </Text>
                      <Text style={styles.contentText}>
                      If you do not wish to have your Personal Information used by the Company to promote our own or third parties' products or services, you can opt-out by sending an email to admin@etheratechnologies.com stating that you do not want us to share your Personal Information for that purpose. 
                      </Text>

                      <Text style={styles.bold}>
                    	•	Targeted Advertising.</Text>
                    <Text style={styles.contentText}>
                      If you do not want us to use information that we collect or that you provide to us to deliver advertisements according to our advertisers' target-audience preferences, you can opt-out by sending an email to admin@etheratechnologies.com stating that you do not want us to share your Personal Information for that purpose. </Text>
                      <Text style={styles.contentText}>
                    We do not control third parties' collection or use of your information to serve interest-based advertising. However these third parties may provide you with ways to choose not to have your information collected or used in this way. You can opt out of receiving targeted ads from members of the Network Advertising Initiative (""NAI"") on the NAI's website.</Text>

                    <Text style={styles.smalltitle}>
                    Accessing and Correcting Your Information</Text>
                    <Text style={styles.contentText}>

                    You can review and change your Personal Information by logging into the Website and visiting your account profile page.
                    </Text>
                    <Text style={styles.smalltitle}>
                    Your California Privacy Rights</Text>
                    <Text style={styles.contentText}>
                    California Civil Code Section § 1798.83 permits users of our Website that are California residents to request certain information regarding our disclosure of Personal Information to third parties for their direct marketing purposes. To make such a request, please contact us at admin@etheratechnologies.com.
                    </Text>
                    <Text style={styles.smalltitle}>
                    Changes to Our Privacy Policy</Text>
                    <Text style={styles.contentText}>
                    It is our policy to post any changes we make to our privacy policy on this page. If we make material changes to how we treat our users' Personal Information, we will notify you. The date the privacy policy was last revised is identified at the top of the page. You are responsible for ensuring we have an up-to-date active and deliverable email address for you, and for periodically visiting our Website and this privacy policy to check for any changes.
                    </Text>
                    <Text style={styles.smalltitle}>
                    Contact Information
                    </Text>
                    <Text style={styles.contentText}>
                    To ask questions or comment about this privacy policy and our privacy practices, contact us at: admin@etheratechnologies.com.
                    </Text>

            </View>
                  : null
                }
          </ScrollView>
        </View>
      </View>
    </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
  },

  content: {
      flex: 1,
      backgroundColor: '#ffffff',
      height:'70%',
  },
tabContainer:{
  flexDirection:'row',
  justifyContent:'space-around',
  alignItems:'center',
  paddingBottom:AppSizes.ResponsiveSize.Padding(5),
  height:'13%'
},
tabContent:{
  paddingLeft:AppSizes.ResponsiveSize.Padding(5),
  paddingRight:AppSizes.ResponsiveSize.Padding(5),
},
tab_title:{
  textAlign:'center',
  fontSize:AppSizes.ResponsiveSize.Sizes(20),
  color:AppColors.primary,
  paddingBottom:AppSizes.ResponsiveSize.Padding(5),
},
tab_content:{
  color:AppColors.contentColor,
  textAlign:'center',
  fontSize:AppSizes.ResponsiveSize.Sizes(13),
  lineHeight: AppSizes.ResponsiveSize.Sizes(13 * 1.70),
  fontWeight:'300',
},
textbtn:{
  fontWeight:'bold',
  textAlign:'center',
  textTransform: 'uppercase',
  fontSize:AppSizes.ResponsiveSize.Sizes(14),
  color:'#3e3e3e'
},
tab_activeText:{
  color:'#ffffff'
},
tab_linear:{
  width: '100%',
  height: '100%',
  justifyContent: 'center'
},
con_title:{
  fontSize:AppSizes.ResponsiveSize.Sizes(18),
  fontWeight:'600',
  color:'#000',
  width:'100%',
  marginBottom:10
},
smalltitle:{
  width:'100%',
  marginBottom:AppSizes.ResponsiveSize.Padding(10),
  marginBottom:10,
  fontWeight:'600'
},
bold:{
  fontWeight:'600'
},
contentText:{
  width:'100%',
  marginBottom:10,
  //backgroundColor:'red',
  paddingBottom:20
},
contentText1:{
  width:'100%',
  marginBottom:10,
  //backgroundColor:'red',
  paddingBottom:20,
  textAlign:'center'
},
list:{
  width:'100%',
  marginBottom:AppSizes.ResponsiveSize.Padding(5)
},
listtitle:{
  fontWeight:'500',
  width:'100%',
  marginBottom:10
},
bold:{
  fontWeight:'600'
},
scrollView:{
  marginBottom:AppSizes.ResponsiveSize.Padding(5)
},
bottombox:{
  alignItems:'center',
  width:'100%',
  marginBottom:15
},
bottomtext:{
  textAlign:'center'
}
});
