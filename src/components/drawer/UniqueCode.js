import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    TouchableHighlight,
    ScrollView,
    Dimensions,
    TextInput,
    TouchableOpacity,
    Alert,
    Keyboard,
    NetInfo,
    Platform,
    AsyncStorage,
    StatusBar
} from 'react-native';
import * as stateActions from '../../stateActions';
import { connect } from 'react-redux';
import { NavigationActions } from 'react-navigation'
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Spinner from 'react-native-loading-spinner-overlay';
import LinearGradient from 'react-native-linear-gradient';
import Header from '../common/signupHeader'
import Spacer from '../common/Spacer'
import * as commonFunctions from '../../utils/CommonFunctions'
import CommonButton from '../common/CommonButton'
import FloatingLabel from 'react-native-floating-labels';
import FloatingLabelBox from '../common/FloatingLabelBox'
import Orientation from 'react-native-orientation'

var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;

const logo = require('../../themes/Images/logo.png')
var group_code = require('../../themes/Images/group-code.png');

let _that
export default class SignupGroupCode extends Component {
    constructor(props) {
        super(props)
        this.state = {
          uid:'',
          group_code: '',
          isVisible: false,
          clicked:false,
          generatedCode:''
        },
        _that = this;
    }

    componentDidMount() {
      Orientation.lockToPortrait();
      AsyncStorage.getItem('loggedIn').then((value) => {
          var loggedIn = JSON.parse(value);
          if (loggedIn) {
              AsyncStorage.getItem('UserData').then((UserData) => {
                  const data = JSON.parse(UserData)
                  //console.log(val)
                  var uid=data.id;
                  _that.setState({uid:  uid});
                  console.log('uid='+_that.state.uid)
              })

          }
      })
    }

  userLogin() {
    _that.props.navigation.navigate('LoginScreen');
  }
  updateAlert = (visible) => {
        this.setState({Modal_Visibility: !this.state.Modal_Visibility});
  }
  generateCode(){
    this.setState({clicked:true})
    _that.validationAndApiParameter()

  }
  skipbtn(){
    AsyncStorage.setItem('screenName', JSON.stringify('MedicalProfileScreen'));
	 _that.props.navigation.navigate('MedicalProfileScreen');
  }

  savebtn(){
    AsyncStorage.setItem('screenName', JSON.stringify('MedicalProfileScreen'));
    _that.props.navigation.navigate('MedicalProfileScreen');
  }

  validationAndApiParameter() {
    const { method, group_code, isVisible } = this.state
    var data = {
      uid : this.state.uid,
    };
    console.log(data);
    _that.setState({isVisible: true});
    this.postToApiCalling('POST', 'uniquecode', Constant.URL_Unique, data);
  }

  postToApiCalling(method, apiKey, apiUrl, data) {

     new Promise(function(resolve, reject) {
          if (method == 'POST') {
              resolve(WebServices.callWebService(apiUrl, data));
          } else {
              resolve(WebServices.callWebService_GET(apiUrl, data));
          }
      }).then((jsonRes) => {
        _that.setState({ isVisible: false })

          if ((!jsonRes) || (jsonRes.code == 0)) {

          _that.setState({ isVisible: false })
          setTimeout(()=>{
              Alert.alert(jsonRes.message);
          },200);

          } else {
            if (jsonRes.code == 1) {
              console.log(jsonRes)
              this.apiSuccessfullResponse(jsonRes.unique_code)
            }
          }
      }).catch((error) => {
          console.log("ERROR" + error);
          _that.setState({ isVisible: false })

          setTimeout(()=>{
              Alert.alert("Server issue");
          },200);
      });
  }

  apiSuccessfullResponse(jsonRes){
    this.setState({generatedCode:jsonRes})

  }

    static navigationOptions = {
        header: null,
    }


    render() {
      const headerProp = {
        title: 'Unique Code',
        screens: 'signupPolicies',
        qus_content:'',
        qus_audio:'https://houseofvirtruve.com/audio/home.mp3'
      };
        return (
          <View style={styles.wrapper}>
          <Header info={headerProp} navigation={_that.props.navigation} updateAlert={this.updateAlert}/>

          <View style={[styles.titleContainer,this.state.Modal_Visibility ? {backgroundColor: 'rgba(0,0,0,0.4)',zIndex:5} : '#ffffff']}>
            <Text allowFontScaling={false} style={styles.headertitle}>Please generate your unique code</Text>
          </View>
            <KeyboardAwareScrollView
                innerRef={() => {return [this.refs.username, this.refs.password]}} >
                  <View style={{marginTop:AppSizes.ResponsiveSize.Padding(1)}}/>

            <View style={styles.container2}>
              <TouchableOpacity activeOpacity={.6} onPress={()=>this.generateCode()} style={{marginTop:AppSizes.ResponsiveSize.Padding(3),}}>
                  <View style={{ alignItems:'center', justifyContent: 'center', marginBottom:10,marginTop:20}}>
                    <View style={{ width: '90%', justifyContent:'space-around',alignItems:'center'}}>
                          <LinearGradient colors={['#36c4d8', '#2aa5b4', '#198391', '#15767d']} style={{width:200, height:200, borderRadius: 200,margin:'auto', alignItems:'center', justifyContent: 'center'}}>
                            {this.state.clicked?
                              <Text allowFontScaling={false} style={styles.textbtn}>{this.state.generatedCode}</Text>
                            :
                              <Text allowFontScaling={false} style={styles.textbtn}>Generate Code</Text>
                            }

                          </LinearGradient>
                    </View>
                  </View>
              </TouchableOpacity>

                {/*<View style={styles.formContainer}>
                <FloatingLabelBox labelIcon={group_code}>
                <FloatingLabel
                    labelStyle={styles.labelInput}
                    inputStyle={styles.input}
                    style={styles.formInput}
                    value={this.state.group_code}
                    ref="UniqueCode"
                    autoCapitalize = 'none'
                    keyboardType={ 'default'}
                    editable={false}
                    onChangeText={UniqueCode=> this.setState({UniqueCode})}
                  >Unique Code</FloatingLabel>
              </FloatingLabelBox>
              </View>*/}

                {this.state.clicked?
                  <TouchableOpacity activeOpacity={.6} onPress={this.savebtn} style={{paddingBottom:AppSizes.ResponsiveSize.Padding(3),}}>
                      <CommonButton label='Next'/>
                  </TouchableOpacity>
                :
                <TouchableOpacity activeOpacity={.6} onPress={this.skipbtn} style={{paddingBottom:AppSizes.ResponsiveSize.Padding(3),}}>
                  <CommonButton label='Skip'/>
                </TouchableOpacity>
                }

                  <View style={{marginTop:AppSizes.ResponsiveSize.Padding(7)}}/>
            </View>
            </KeyboardAwareScrollView>
            <Spinner visible={this.state.isVisible}  />
          </View>
        );
    }
}

const styles = {
  wrapper: {
    flex: 1,
    flexDirection:'column',
    backgroundColor:'#ffffff'
  },
titleContainer:{
  height:AppSizes.screen.height/8,
  justifyContent:'center',
  alignItems:'center',
  padding:AppSizes.ResponsiveSize.Padding(1),
  //backgroundColor:'red'
},
  container2: {
    height:AppSizes.screen.height/2,
    //backgroundColor:'green'
  },

formContainer:{
  marginTop:5,
//  borderBottomWidth:1,
  //borderBottomColor:'#000',
},
  textbtn:{
    color: '#ffffff',
    fontSize:AppSizes.ResponsiveSize.Sizes(25),
    fontWeight:'600'
  },
  textbtn2:{
    color: '#ffffff',
    fontSize:AppSizes.ResponsiveSize.Sizes(18),
    fontWeight:'600'
  },

  labelInput: {
    color: AppColors.contentColor,
    fontSize:AppSizes.ResponsiveSize.Sizes(14),
    //fontWeight:'300',
    //fontSize:AppSizes.ResponsiveSize.Sizes(13),
  },
  input: {
    borderWidth: 0,
    color: AppColors.contentColor,
    fontWeight:'300',
    fontSize:AppSizes.ResponsiveSize.Sizes(15),
  },
  formInput: {
    borderWidth: 0,
    borderColor: '#333',
    //paddinBottom:10,
  },
  forgetWrapper:{
    flexDirection: 'row',
    width: '90%',
    alignItems:'center',
    justifyContent: 'center',
    padding:'10%'
  },

  forgetText:{
    color: AppColors.secondary,
    fontSize:AppSizes.ResponsiveSize.Sizes(16),
    fontWeight:'500'
  },

  signupWrapper:{
    flexDirection: 'row',
    width: '100%',
    alignItems:'center',
    justifyContent: 'center',
    paddingBottom:AppSizes.ResponsiveSize.Padding(5),

  },

 signupText:{
   color: AppColors.contentColor,
   fontWeight:'300',
   fontSize:AppSizes.ResponsiveSize.Sizes(14),
 },
headertitle:{
  textAlign:'center',
  color:AppColors.primary,
  fontSize:AppSizes.ResponsiveSize.Sizes(18),
  fontWeight:'500',
  letterSpacing:1,
},

}
