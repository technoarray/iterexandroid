import React, { Component } from 'react';
import {AsyncStorage,StyleSheet,Text,View,Image,TouchableOpacity,Platform,Alert,Modal,ScrollView,TextInput,Button,StatusBar} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions, StackActions  } from 'react-navigation';
import Header from '../common/signupHeader'
import CommonButton from '../common/CommonButton'
import Animbutton from './animAnsbutton'
import AnimSinglebutton from './animSingleAnsOptions';
import { jsonData } from '../data/Question';
import LinearGradient from 'react-native-linear-gradient';
import Spinner from 'react-native-loading-spinner-overlay';
import Sound from 'react-native-sound'
import Orientation from 'react-native-orientation'

var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;

import * as commonFunctions from '../../utils/CommonFunctions'

var Constant = require('../../api/ApiRules').Constant;

/* Images */
//var thankyou_icon = require('../../themes/Images/thank-you.png');
var logo = require('../../themes/Images/logo.png');
var back_icon = require( '../../themes/Images/left-whitearrow.png')
var qus_icon = require('../../themes/Images/que_icon_196.png');
var mice=require('../../themes/Images/mice.png')
var mice_off=require('../../themes/Images/mice-off.png')

var help_audio1=require('../../themes/sound/CCQHelp1.mp3')
var help_audio2=require('../../themes/sound/CCQHelp2.mp3')

let _that
let arrnew = []
let ans_track = []
let python_result=[]
var current_ans=''
let score_result = []

const MyStatusBar = ({backgroundColor, ...props}) => (
  <View style={[styles.statusBar, { backgroundColor }]}>
    <StatusBar translucent backgroundColor={backgroundColor} {...props} />
  </View>
);
export default class CCQQusScreen extends Component {
  constructor (props) {
      super(props)
      this.qno = 0
      this.score = 0
      this.type ='qus'
      this.inputRefs = {};
      const jdata = jsonData.copdqus.qus1
      arrnew = Object.keys(jdata).map( function(k) { return jdata[k] });
      this.state = {
        question : arrnew[this.qno].question,
        title : arrnew[this.qno].title,
        options : arrnew[this.qno].options,
        suboption : arrnew[this.qno].suboption,
        type : arrnew[this.qno].type,
        countCheck : [],
        selectedradio:'',
        qus_no : 0,
        isVisible: false,
        Modal_Visibility: false,
        status:'ans',
        uid:'',
        Modal_Visibility: false,
        audioStatus: false,
        audioImg: mice,
        modal_qus1:'This questionnaire will help you and your healthcare professional measure the impact COPD is having on your wellbeing and daily life. Your answers and test score can be used by you and your healthcare professional to help improve the management of your COPD and get the greatest benefit from treatment.',
        modal_qus2:'Select the most applicable choice.'
    },
    _that = this;
  }

  playTrack = () => {
    this.setState({audioStatus: !this.state.audioStatus})
    if(this.state.audioStatus==true){
      if(this.qno==0){
        help_audio=help_audio1;
      }
      else{
        help_audio=help_audio2;
      }
      this.track = new Sound(help_audio, (e) => {
        if (e) {
          console.log('error loading track:', e)
        } else {

            this.setState({audioImg: mice_off});
            this.track.play((success) => {
                 if (success) {
                     this.stop();
                 }
             });
          }
        })
      }
        else {
          this.stop();
        }
      }

  stop() {
     if (!this.track) return;
     this.track.stop();
     this.track.release();
     this.track = null;
     this.setState({audioStatus: false});
     this.setState({audioImg: mice});
 }

  Show_Custom_Alert(visible) {
    this.stop();
    this.setState({Modal_Visibility: visible});
  }

  componentDidMount() {
    Orientation.lockToPortrait();
    AsyncStorage.getItem('loggedIn').then((value) => {
        var loggedIn = JSON.parse(value);
        if (loggedIn) {
            AsyncStorage.getItem('UserData').then((UserData) => {
                const data = JSON.parse(UserData)
                //console.log(val)
                var uid=data.id;
                _that.setState({uid:  uid});
            })
       }
    })
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.question !== this.state.question) {
      setTimeout(() => {
        _that.scroll_to_top();
      }, 400)
    }
    //_that.scroll_to_top();
  }

  validationAndApiParameter(apiname,score,status) {
    if(apiname == 'addScore'){
      var date = new Date().getDate();
      var month = new Date().getMonth() + 1;
      var year = new Date().getFullYear();
      var current_date=year + '-' + month + '-' + date

      var final=[{var_name:'ccq_score',value:score},{var_name:'ccq_status',value:status},{var_name:'ccq_todo',value:1},{var_name:'ccq_date',value:current_date}]

      var json_python_result = JSON.stringify(final);

      var data = {
          uid: this.state.uid,
          variable : json_python_result,
      };
      console.log(data);
      _that.setState({isVisible: true});
      this.postToApiCalling('POST', apiname, Constant.URL_saveVariable,data);
    }
    if(apiname=='save'){
      var final=[{var_name:'qol_inactive',value:0}]
      var json_python_result = JSON.stringify(final);
      var data = {
          uid: this.state.uid,
          variable : json_python_result,
      };
      console.log(data);
      _that.setState({isVisible: true});
      this.postToApiCalling('POST', apiname, Constant.URL_saveUserData,data);
    }
  }
  postToApiCalling(method, apiKey, apiUrl, data) {
    //console.log(apiUrl)
      new Promise(function(resolve, reject) {
          if (method == 'POST') {
              resolve(WebServices.callWebService(apiUrl, data));
          } else {
              resolve(WebServices.callWebService_POST(apiUrl, data));
          }
      }).then((jsonRes) => {
          console.log(jsonRes)
            _that.setState({ isVisible: false })
            if ((!jsonRes) || (jsonRes.code == 0)) {
              setTimeout(() => {
                    Alert.alert(jsonRes.message);
                }, 200);
            } else {
                if (jsonRes.code == 1) {
                    _that.apiSuccessfullResponse(apiKey, jsonRes)
                }
            }

      }).catch((error) => {
          console.log("ERROR" + error);
          _that.setState({ isVisible: false })
          setTimeout(() => {
              Alert.alert("Server Error");
          }, 200);
      })
  }

  apiSuccessfullResponse(apiKey, jsonRes) {
    if(apiKey=="addScore"){
      //console.log(jsonRes)

      arrnew = []
      ans_track = []
      current_ans=''
      python_result=[]
      score_result = []
      _that.validationAndApiParameter('save');
    }
    if(apiKey=='save'){
      const resetAction = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: 'TrackingScreen' })],
      });
      _that.props.navigation.dispatch(resetAction);
    }
  }


  prev= () =>{
    //console.log(this.qno)
    if(this.qno > 0){
      score_result.splice(-1,1);
      this.qno--
      this.setState({ qus_no: this.qno })
      this.setState({ question: arrnew[this.qno].question, options: arrnew[this.qno].options, type: arrnew[this.qno].type,title: arrnew[this.qno].title,suboption : arrnew[this.qno].suboption})
    }
  }
  next= () =>{
      current_ans=0
    //console.log(this.qno)
    //console.log(this.state.countCheck.length)
    const { countCheck} = this.state
    //console.log('count'+countCheck)

    if(this.state.countCheck.length <= 0 && this.state.qus_no!=1){
      Alert.alert("Invalid Value","Missing or blank value is not allowed")
    }

    else if(this.qno < arrnew.length-1){
      score_result.push({ans_track});
      ans_track =[];
      console.log(score_result)
      this.setState({ countCheck: [] })
      this.setState({ status: 'next' })
      this.qno++
      this.setState({ qus_no: this.qno })
      this.setState({ countCheck: [], question: arrnew[this.qno].question, options: arrnew[this.qno].options, type: arrnew[this.qno].type, title: arrnew[this.qno].title,suboption : arrnew[this.qno].suboption})

    }
    else {
      var score1=avg=avg_score=avg_status=0
      //console.log(ans_track)
      score_result.push({ans_track});
      ans_track =[];
      score_result.map((data, index) => {
        score1=score1+parseInt(score_result[index]['ans_track']);
        //console.log("Score 1: " + score1+" "+index);
        /* if(index <= 3){
          score1=score1+parseInt(score_result[index]['ans_track']);
          //console.log("Score 1: " + score1+" "+index);
        }
        else if(index > 3 && index <= 5){
          score2=score2+parseInt(score_result[index]['ans_track']);
          //console.log("Score 2: " + score2+" "+index);
        }
        else{
          score3=score3+parseInt(score_result[index]['ans_track']);
          //console.log("Score 3: " + score3+" "+index);
        }*/

      });
      //console.log('finish='+ score1)
      // <1: acceptable; ≥1 but <2: acceptable only in patients with COPD stage 3 or 4; ≥2 but <3: unstable; ≥3 indicative of very unstable health status.
      avg=score1/10;
      avg_score=avg.toFixed(0)
      if(avg_score <1){
        avg_status='Acceptable'
      }else if(avg_score >= 1 && avg_score <2){
        avg_status='Acceptable only in patients with COPD stage 3 or 4'
      }
      else if(avg_score >= 2 && avg_score <3){
        avg_status='Unstable'
      }else if(avg_score >= 3){
        avg_status='Indicative of very unstable health status'
      }
      _that.validationAndApiParameter('addScore',avg_score,avg_status);
    }
  }

  _answer(status,ans){
    var ansarray = ans.split("_");
    ans1=ansarray[1]
    ans_track.splice(-1,1);
    ans_track.push(ans1);
    //console.log(this.state.qus_no)
    //ans_track[this.state.qus_no] = new Array(ans);
    this.setState({ countCheck: ans_track })
    //console.log(this.state.countCheck)
    this.setState({ status: 'ans' })
    current_ans=ans;
  }

  onSelect(index, value){
    this.setState({ countCheck: ans_track })
    console.log(this.state.countCheck)
    this.setState({ status: 'ans' })
  }


  scroll_to_top=()=>{
    _that.scroll.scrollTo({x: 0, y: 0, animated: false})
  }

  back_click() {
    _that.props.navigation.goBack()
  }

  render() {
    const headerProp = {
      title: 'COPD Questionnaire',
      screens: 'HADSScreen',
    };
    let _this = this
      const currentOptions = this.state.options
      const options = Object.keys(currentOptions).map( function(k) {
        if(k==current_ans){status=true}else{status=false}
        return (  <View key={k} style={{margin:5, }}>
          { _this.state.type=='multiple' ?
            <Animbutton onColor={"#2c808f"} effect={"pulse"}  _onPress={(status) => _this._answer(status,k)} text={currentOptions[k]} />
          :
            <AnimSinglebutton onColor={"#2c808f"} effect={"pulse"} status={status} _onPress={(status) => _this._answer(status,k)} text={currentOptions[k]} />
          }
        </View>)
      });

      if(this.state.qus_no!=0){
          headerBack=<TouchableOpacity style={styles.menuWrapper} onPress={this.prev}>
                  <Image style={styles.image} source={back_icon}/>
                  </TouchableOpacity>;
        }
        else{
          headerBack=<TouchableOpacity style={styles.menuWrapper} onPress={this.back_click}>
                  <Image style={styles.image} source={back_icon}/>
                  </TouchableOpacity>;
  	  }

    return (
      <View style={styles.container}>
      <LinearGradient colors={['#48c3d5','#3fafc0','#369aaa','#287b88']}>

        <MyStatusBar barStyle="light-content"  backgroundColor="#4eb4c4"/>
          <View style={styles.appBar} >
              <View style={{ flex:1,flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', }}>
                <View style={styles.imageContainer}>
                {headerBack}
                </View>

                <View style={{ marginTop:0,width:'60%',justifyContent: 'center',alignItems:'center' }}>
                    <Text allowFontScaling={false} style={styles.headertitle}>{this.state.title}</Text>
                </View>

                <View style={{width:'15%',flexDirection: 'row',justifyContent: 'flex-end'}}>
        					 <TouchableOpacity style={styles.qusWrapper} onPress={() => { this.Show_Custom_Alert(true) }}>
                            <Image style={styles.image} source={qus_icon}/>
                            </TouchableOpacity>
                </View>

              </View>
          </View>


      </LinearGradient>
      <View style={[styles.content,this.state.Modal_Visibility ? {backgroundColor: 'rgba(0,0,0,0.4)',zIndex:5} : '#ffffff']}>

      <ScrollView style={{backgroundColor: '#F5FCFF',paddingTop: 10}}
      ref={(c) => {this.scroll = c}} >

		<View style={styles.container1}>
          <Text allowFontScaling={false} style={styles.heading}>  {this.state.question} </Text>
          <View style={styles.hr}></View>
      </View>
      <View style={styles.container2}>
            { options }
      </View>


      <View style={styles.container3}>
        <TouchableOpacity style={styles.btn} activeOpacity={.6} onPress={this.next}>
          <CommonButton label='Next'/>
          </TouchableOpacity>
      </View>

      </ScrollView>
    </View>
  <Spinner visible={this.state.isVisible}  />

  <Modal visible={this.state.Modal_Visibility}
    transparent={true}
    animationType={"fade"}
    onRequestClose={ () => { this.Show_Custom_Alert(!this.state.Modal_Visibility)} } >
        <View style={{ flex:1, alignItems: 'center', justifyContent: 'center',zIndex:20,backgroundColor:'#000000a3', }}>
        <View style={styles.Alert_Main_View}>
            <View style={{ flex:1,flexDirection:'column',alignItems: 'center', justifyContent: 'center',}}>
                <View style={{height:'15%',width:AppSizes.screen.width/10,paddingTop:'6%',marginBottom:8}}>
                  <TouchableOpacity style={{width:'100%',height:'100%'}} onPress={this.playTrack}>
                    <Image style={styles.image} source={this.state.audioImg}/>
                  </TouchableOpacity>
                </View>
                <View style={{height:'70%',flexDirection:'row',alignItems: 'center', justifyContent: 'center',paddingBottom:'2%',}}>
                  <ScrollView contentContainerStyle={styles.modal}>
                    <Text allowFontScaling={false} style={styles.Alert_Message}>
                      {this.state.qus_no==0 ? this.state.modal_qus1 : this.state.modal_qus2}
                    </Text>
                  </ScrollView>
                </View>
            </View>
            <View style={{width:'100%',flex:0.2,  backgroundColor: '#ebebeb',borderBottomLeftRadius:20,borderBottomRightRadius:20,}}>
                <TouchableOpacity style={styles.buttonStyle} activeOpacity={0.7}
                  onPress={() => { this.Show_Custom_Alert(!this.state.Modal_Visibility)} }  >
                      <Text allowFontScaling={false} style={styles.TextStyle}> CANCEL </Text>
                </TouchableOpacity>
            </View>
          </View>
        </View>
    </Modal>

      </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
    backgroundColor: '#F5FCFF',
  },
  content: {
      flex: 1,
  },
  statusBar: {
    height: AppSizes.statusBarHeight,
  },
  appBar: {
      height: AppSizes.navbarHeight1,
      justifyContent:'center',
      ...Platform.select({
      android: {
      paddingTop:AppSizes.ResponsiveSize.Padding(4),
      },
    }),
    },
  headertitle:{
    color:'#ffffff',
    fontSize:(Platform.OS === 'ios') ? AppSizes.ResponsiveSize.Sizes(11) :AppSizes.ResponsiveSize.Sizes(13),
    fontWeight:'bold',
    letterSpacing:1,
  },
  imageContainer:{
    width:'20%'
  },
  menuWrapper: {
    width:'60%',
    height:'60%',
    marginLeft:'30%',
  },
  image: {
    flex: 1,
    width: undefined,
    height: undefined,
    resizeMode:'contain'
  },
  container1:{
    flex:2,
    flexDirection:'column',
    alignItems:'center',
    justifyContent:'center'
  },
  heading:{
    fontSize:AppSizes.ResponsiveSize.Sizes(18),
    color:'#000',
    justifyContent:'center',
    alignItems:'center',
    fontWeight:'600',
    width:'89%',

  },
  hr:{
    width:50,
    height:3,
    backgroundColor:'#000',
    marginTop:10,
},
  container2:{
    flex:6,
    flexDirection:'column',
    padding: AppSizes.ResponsiveSize.Padding(5),
    width:AppSizes.screen.width,

  },
  container3:{
    flex:2,
    justifyContent:'flex-start',
    alignItems:'center',

  },

  dropdowntext:{
    fontSize:AppSizes.ResponsiveSize.Sizes(14),
    fontWeight:'500'
  },
  ansfield:{
    fontSize:AppSizes.ResponsiveSize.Sizes(13),
    color:'#5a5a5a',
  },
  btn:{
    width:'100%',
    marginBottom:20
  },
  text: {
      padding: 10,
      fontSize: 14,
  },
  qusWrapper: {
    width:'40%',
    height:'40%',
    marginRight:'30%',
    marginTop:'15%'
  },
  image: {
    flex: 1,
    width: undefined,
    height: undefined,
    resizeMode:'contain'
  },
  Alert_Main_View:{
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#ffffff',
    height:'40%',
    width: '80%',
    borderRadius:20,
    flexDirection:'column',
    justifyContent: 'center',
    alignItems: 'center',
  },

  Alert_Title:{
    fontSize: AppSizes.ResponsiveSize.Sizes(25),
    color: "#000",
    textAlign: 'center',
    padding: 10,
    height: '28%'
  },
    Alert_Message:{
      fontSize: AppSizes.ResponsiveSize.Sizes(18),
      color: "#000",
      textAlign: 'center',
      padding: 10,
    },
    buttonStyle: {
      width: '100%',
      height: '100%',
      justifyContent: 'center',
      alignItems: 'center'
    },
    TextStyle:{
      color:'#000',
      textAlign:'center',
      fontSize: AppSizes.ResponsiveSize.Sizes(18),
    },
});

const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        fontSize: AppSizes.ResponsiveSize.Sizes(14),
        paddingTop: AppSizes.ResponsiveSize.Padding(5),
        paddingHorizontal: AppSizes.ResponsiveSize.Padding(5),
        paddingBottom: AppSizes.ResponsiveSize.Padding(5),
        borderWidth: 1,
        borderColor: 'gray',
        borderRadius: 4,
        backgroundColor: 'white',
        color: 'black',
    },
});
