import React, { Component } from 'react';
import {AsyncStorage,StyleSheet,Text,View,Image,TouchableOpacity,Platform,Alert,ScrollView,TextInput,Button} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions, StackActions  } from 'react-navigation';
import Spacer from '../common/Spacer'
import Header from '../common/signupHeader'
import CommonButton from '../common/CommonButton'
import Orientation from 'react-native-orientation'
import * as commonFunctions from '../../utils/CommonFunctions'
import Spinner from 'react-native-loading-spinner-overlay';

var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;

/* Images */
var thankyou_icon = require('../../themes/Images/thank-you.png');
var logo = require('../../themes/Images/logo.png');


let _that
export default class ThankYouScreen extends Component {
  constructor (props) {
      super(props)
      this.state = {
        uid:'',
        isVisible: false,
        Modal_Visibility: false
    },
    _that = this;
  }

  componentDidMount() {
    Orientation.lockToPortrait();
    AsyncStorage.getItem('loggedIn').then((value) => {
        var loggedIn = JSON.parse(value);
        if (loggedIn) {
            AsyncStorage.getItem('UserData').then((UserData) => {
                const data = JSON.parse(UserData)
                console.log(data)
                var uid=data.id;
                _that.setState({uid:  uid});
            })
       }
    })


  }

  validationAndApiParameter(apiname) {
      if(apiname == 'todo'){
        var data = {
            uid: this.state.uid,

        };
        console.log(data);
        _that.setState({isVisible: true});
        this.postToApiCalling('POST', apiname, Constant.URL_todo,data);
      }
    }

    postToApiCalling(method, apiKey, apiUrl, data) {
      //console.log(apiUrl)
        new Promise(function(resolve, reject) {
            if (method == 'POST') {
                resolve(WebServices.callWebService(apiUrl, data));
            } else {
              //console.log('callWebService_POST')
                resolve(WebServices.callWebService_POST(apiUrl, data));
            }
        }).then((jsonRes) => {
          //  console.log(jsonRes)
              _that.setState({ isVisible: false })
              if ((!jsonRes) || (jsonRes.code == 0)) {
                setTimeout(() => {
                      Alert.alert(jsonRes.message);
                  }, 200);
              } else {
                  if (jsonRes.code == 1) {
                      _that.apiSuccessfullResponse(apiKey, jsonRes)
                  }
              }

        }).catch((error) => {
            console.log("ERROR" + error);
            _that.setState({ isVisible: false })
            setTimeout(() => {
                Alert.alert("Server Error");
            }, 200);
        })
    }

    apiSuccessfullResponse(apiKey, jsonRes) {
      if(apiKey=="todo"){
        //console.log(jsonRes.pending)
        if(jsonRes.pending==1){
          const resetAction = StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: 'StartScreen' })],
            });
        _that.props.navigation.dispatch(resetAction);
        }else{
          _that.props.navigation.navigate('TrackingScreen');
        }
      }
    }

	closebtn(){
    AsyncStorage.getItem('screenName').then((screenName) => {
        var screenName = JSON.parse(screenName);
        console.log(screenName);
        if(screenName=='MedicalQusScreen')
        {
          AsyncStorage.setItem('screenName', JSON.stringify('thank'));
          const resetAction = StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: 'StartScreen' })],
            });
            _that.props.navigation.dispatch(resetAction);
        }
        else if(screenName=='MedicalAsthmaQusScreen'){
          console.log('hello');
          AsyncStorage.setItem('screenName', JSON.stringify('thank'));
          const resetAction = StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: 'StartScreen' })],
            });
            _that.props.navigation.dispatch(resetAction);
        }
        else{
          _that.validationAndApiParameter('todo');
        }
    })
	}
  render() {
    const headerProp = {
      title: 'Thank You',
      screens: 'ThankYouScreen',
      qus_content:'',
    };

    return (
      <View style={styles.container}>
        <Header info={headerProp} navigation={_that.props.navigation} updateAlert={this.updateAlert}/>

        <View style={styles.container1}>
          <View style={{ flexDirection: 'column',alignItems:'center',width:'80%',marginTop:AppSizes.ResponsiveSize.Padding(3), }}>
          <View style={styles.boxiconcontainer}>
            <Image source={thankyou_icon} style={styles.boxicon} />
          </View>
          <View style={styles.mainContainer}>
            <Text allowFontScaling={false} style={styles.mainTitle}>Thank You</Text>
            <View style={styles.sbox}>
                <ScrollView>
                <Text allowFontScaling={false} style={styles.mainContent}>{this.props.navigation.state.params.message}</Text>
                </ScrollView>
            </View>
          </View>
          </View>
        </View>
        <View style={styles.container2}>
              <View style={styles.logoimageContainer}>
                  <View style={styles.logoWrapper}>
                      <Image style={styles.image} source={logo}/>
                  </View>
              </View>
        </View>
        <View style={styles.container3}>
        <TouchableOpacity activeOpacity={.6} onPress={this.closebtn}>
          <CommonButton label='Close'/>
          </TouchableOpacity>
        </View>
        <Spinner visible={this.state.isVisible}  />

      </View>


    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
	backgroundColor:'#fff'
  },
  container1:{
    flex:6,
    flexDirection:'column',
    alignItems:'center',
    //backgroundColor:'red'
  },
  container3:{
     justifyContent: 'flex-end',
     flex:1,

     marginBottom:AppSizes.ResponsiveSize.Padding(1),
     //backgroundColor:'blue',

  },

  container2:{
     flex:1,
     alignItems:'center',
    justifyContent:'center',
    //backgroundColor:'red',

  },
  imageContainer:{
    width:'80%'
  },
  logoimageContainer:{
    width:'60%'
  },
  logoWrapper:{
    width:'100%',
    height:'100%',
  },
  image: {
    flex: 1,
    width: undefined,
    height: undefined,
    resizeMode:'contain',
  },
  titleContainer:{
    marginTop:0,
    justifyContent: 'center',
    alignItems:'center',
  },
mainContainer:{
  marginTop:AppSizes.ResponsiveSize.Padding(3),
  justifyContent: 'center',
  alignItems:'center'
},
  mainTitle:{
    fontSize:AppSizes.ResponsiveSize.Sizes(28),
    color:'#000',
    justifyContent:'center',
    textAlign:'center',
    fontWeight:'800',
    marginBottom:10,
  },
  mainContent:{
      textAlign:'center',
      fontSize:AppSizes.ResponsiveSize.Sizes(15),
  },
  boxiconcontainer:{
    width:AppSizes.screen.width/2.3,
    height:AppSizes.screen.width/2.3,
    justifyContent:'center',
  },
  boxicon:{
    width:null,
    height:null,
    flexGrow:1
  },
  sbox:{
    height:AppSizes.screen.height/4.2,
    //backgroundColor:'gray',
    width:'100%'
  }
});
