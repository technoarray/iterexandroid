import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    TouchableHighlight,
    ScrollView,
    Dimensions,
    TextInput,
    TouchableOpacity,
    Alert,
    Keyboard,
    NetInfo,
    Platform,
    AsyncStorage,
    StatusBar
} from 'react-native';
import * as stateActions from '../../stateActions';
import { connect } from 'react-redux';
import { NavigationActions } from 'react-navigation'
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Spinner from 'react-native-loading-spinner-overlay';
import LinearGradient from 'react-native-linear-gradient';
import Header from '../common/signupHeader'
import Spacer from '../common/Spacer'
import * as commonFunctions from '../../utils/CommonFunctions'
import CommonButton from '../common/CommonButton'
import FloatingLabel from 'react-native-floating-labels';
import Orientation from 'react-native-orientation'

var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;

const logo = require('../../themes/Images/logo.png')

let _that
export default class AmIOkScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
          uid:'',
          isVisible: false,
        },
        _that = this;
    }

    componentDidMount() {
      Orientation.lockToPortrait();
      AsyncStorage.getItem('loggedIn').then((value) => {
          var loggedIn = JSON.parse(value);
          if (loggedIn) {
              AsyncStorage.getItem('UserData').then((UserData) => {
                  const data = JSON.parse(UserData)
                  //console.log(val)
                  var uid=data.id;
                  _that.setState({uid:  uid});
                  //console.log('uid='+_that.state.uid)
              })

          }
      })
    }
  continuebtn(){
    _that.props.navigation.navigate('AmIOkQusScreen', {qus_no: 0,  name: 'AmIOkScreen' });
    //_that.validationAndApiParameter('checkdata');
  }

  validationAndApiParameter(apiname) {
      //console.log(apiname)
      //console.log('uid='+_that.state.uid)
      if(apiname == 'checkdata'){
        var data = {
            uid: this.state.uid
        };
          _that.setState({isVisible: true});
          this.postToApiCalling('POST', 'checkdata', Constant.URL_Checkdata,data);
      }
    }

  postToApiCalling(method, apiKey, apiUrl, data) {
    //console.log(apiUrl)
      new Promise(function(resolve, reject) {
          if (method == 'POST') {
              resolve(WebServices.callWebService(apiUrl, data));
          } else {
              resolve(WebServices.callWebService_GET(apiUrl, data));
          }
      }).then((jsonRes) => {
          console.log(jsonRes)
            _that.setState({ isVisible: false })
          if ((!jsonRes) || (jsonRes.code == 0)) {
            setTimeout(() => {
                  Alert.alert(jsonRes.message);
              }, 200);
          } else {
              if (jsonRes.code == 1) {
                  _that.apiSuccessfullResponse(apiKey, jsonRes)
              }
          }
      }).catch((error) => {
          console.log("ERROR" + error);
          _that.setState({ isVisible: false })
          setTimeout(() => {
              Alert.alert("Server Error");
          }, 200);
      })
  }

  apiSuccessfullResponse(apiKey, jsonRes) {
    console.log(jsonRes);
      if (apiKey == 'checkdata') {
        const days = jsonRes.days
        //console.log(days)
        //if(days < 7){
        //  Alert.alert("","Am I Ok Process completed for this week!")
        //}
        //else{
          _that.props.navigation.navigate('AmIOkQusScreen', {qus_no: 0,  name: 'AmIOkScreen' });
        //}
      }
  }


    static navigationOptions = {
        header: null,
    }


    render() {
      const headerProp = {
        title: '1st vital update and AIO',
        screens: 'AmIOkScreen',

      };
        return (
          <View style={styles.wrapper}>
          <Header info={headerProp} navigation={_that.props.navigation} updateAlert={this.updateAlert}/>
            <View style={{flex:.2}}></View>
          <View style={styles.titleContainer}>
            <Text allowFontScaling={false} style={styles.headertitle}>Great job! Your profile is fully completed.</Text>
            <Text></Text>
            <Text allowFontScaling={false} style={styles.headertitle}>
                Finally let’s run through the Am I Ok process. Please use this feature at least once per week and whenever you are feeling worse than usual
            </Text>
            <TouchableOpacity activeOpacity={.6} onPress={this.continuebtn} >
                    <CommonButton label='Continue'/>
                </TouchableOpacity>
          </View>
            <Spinner visible={this.state.isVisible}  />
          </View>
        );
    }
}

const styles = {
  wrapper: {
    flex: 1,
    flexDirection:'column',
    backgroundColor:'#ffffff'
  },
titleContainer:{
  flex:1,
  justifyContent:'flex-start',
},

headertitle:{
  textAlign:'center',
  color:AppColors.primary,
  fontSize:AppSizes.ResponsiveSize.Sizes(18),
  fontWeight:'500',
  letterSpacing:1,
  padding:AppSizes.ResponsiveSize.Padding(2),
},

}
