import { Dimensions, Platform } from 'react-native';

const { width, height } = Dimensions.get('window');
const screenHeight = width < height ? height : width;
const screenWidth = width < height ? width : height;

var ResponsiveSize = {
  Sizes: function(size) {
    if(screenHeight===568) { size= size }
    else if(screenHeight===667) { size=size*1.17}
    else if(screenHeight===736) { size=size*1.29}
    else if(screenHeight===1024){ size= size*1.8}
    return size;
  },

  Padding: function(size) {
    if(screenHeight===568) { size= size }
    else if(screenHeight===667) { size=size+2}
    else if(screenHeight===736) { size=size+3}
    else if(screenHeight===1024){ size= size+4}
    return size+'%';
  }

}

export default {
    // Window Dimensions
    screen: {
        height: screenHeight,
        width: screenWidth,

        widthHalf: screenWidth * 0.5,
        widthThird: screenWidth * 0.333,
        widthTwoThirds: screenWidth * 0.666,
        widthQuarter: screenWidth * 0.25,
        widthThreeQuarters: screenWidth * 0.85,
    },

    navbarHeight: (Platform.OS === 'ios') ? 54 :84,
    navbarHeight1: (Platform.OS === 'ios') ? 74 :94,
    statusBarHeight: (Platform.OS === 'ios') ? 20 : 0,
    tabbarHeight: 51,

    padding: 20,
    paddingSml: 10,

    borderRadius: 2,

    ResponsiveSize
};
